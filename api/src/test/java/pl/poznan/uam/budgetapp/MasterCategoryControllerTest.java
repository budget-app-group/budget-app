package pl.poznan.uam.budgetapp;


import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.testcontainers.containers.PostgreSQLContainer;
import pl.poznan.uam.dto.MasterCategoryDto;

import static org.junit.Assert.assertNotEquals;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:/testDatabase.sql")
public class MasterCategoryControllerTest {
    @Autowired
    private WebApplicationContext webApplicationContext;

    @ClassRule
    public static PostgreSQLContainer postgreSQLContainer = TestPostgresContainer.getInstance()
            .withDatabaseName("integration-tests-db")
            .withUsername("sa")
            .withPassword("sa");

    private MockMvc mvc;

    private static final String LOGIN = "test123@gmail.com";
    private static final String LOGIN2 = "test222@gmail.com";
    private TestUtil testUtil = new TestUtil();

    @Before
    public void setup() {
        mvc = MockMvcBuilders
                .webAppContextSetup(webApplicationContext)
                .apply(springSecurity())
                .alwaysDo(print())
                .build();
    }

    @WithMockUser(username = LOGIN)
    @Test
    public void shouldGetBudgetOfLoggedUser__andReturn200() throws Exception {
        mvc.perform(get("/api/budget/1/master"))
                .andExpect(status().isOk())
                .andExpect(content().json(testUtil.openJson("masterCategories/allMasterCategories.json")));

    }

    @WithMockUser(username = LOGIN2)
    @Test
    public void shouldNotGetBudgetOfAnotherUserOrGetWrongBudget__andReturnEmptyList() throws Exception {
        mvc.perform(get("/api/budget/1/master"))
//                .andExpect(content().string("[]"));
                .andExpect(status().isNotFound());
    }

    @WithMockUser(username = LOGIN)
    @Test
    public void shouldAddMasterCategory__andReturn201() throws Exception {
        //given
        MasterCategoryDto.MinimalDto masterCat1DTO = MasterCategoryDto.MinimalDto.builder()
                .name("testMasterCategoryFromPost")
                .budgetAllocation(49)
                .build();
        mvc.perform(get("/api/budget/1/master"))
                .andExpect(status().isOk())
                .andExpect(content().json(testUtil.openJson("masterCategories/allMasterCategories.json")));
        //when
        mvc.perform(post("/api/budget/1/master")
                .contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.asJsonString(masterCat1DTO)))
                .andExpect(status().isCreated())
                .andExpect(content().json(testUtil.openJson("masterCategories/postMasterCategory.json")));

        //then
        mvc.perform(get("/api/budget/1/master"))
                .andExpect(status().isOk())
                .andExpect(content().json(testUtil.openJson("masterCategories/masterCategoriesAfterPost.json")));

    }

    @WithMockUser(username = LOGIN2)
    @Test
    public void shouldNotAddCategoryToAnotherUser__andReturn404() throws Exception {
        MasterCategoryDto.MinimalDto masterCatDTO = MasterCategoryDto.MinimalDto.builder()
                .name("testMasterCategoryFromPost")
                .budgetAllocation(49)
                .build();
        mvc.perform(post("/api/budget/1/master")
                .contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.asJsonString(masterCatDTO)))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser(username = LOGIN)
    public void shouldModifyMasterCategoryOfLoggedUser__andReturn200() throws Exception {
        //given
        MasterCategoryDto.MinimalDto masterCatDTO = MasterCategoryDto.MinimalDto.builder()
                .name("modifiedMasterCategoryFromPut")
                .budgetAllocation(19)
                .build();
        MvcResult beforePut = mvc.perform(get("/api/budget/1/master"))
                .andExpect(status().isOk())
                .andReturn();
        //when
        mvc.perform(put("/api/budget/1/master/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.asJsonString(masterCatDTO)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value(masterCatDTO.getName()));
        //then
        MvcResult afterPut = mvc.perform(get("/api/budget/1/master"))
                .andExpect(status().isOk())
                .andReturn();
        assertNotEquals(beforePut, afterPut);
    }

    @Test
    @WithMockUser(username = LOGIN2)
    public void shouldNotModifyMasterCategoryOfAnotherUser__andReturn404() throws Exception {
        MasterCategoryDto.MinimalDto masterCatDTO = MasterCategoryDto.MinimalDto.builder()
                .name("modifiedMasterCategoryFromPut")
                .budgetAllocation(19)
                .build();
        mvc.perform(put("/api/budget/1/master/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.asJsonString(masterCatDTO)))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser(username = LOGIN)
    public void shouldDeleteMasterCategoryCategoryAndExpensesOfLoggedUser__andReturn200() throws Exception {
        //given
        mvc.perform(get("/api/budget/1/master"))
                .andExpect(status().isOk())
                .andExpect(content().json(testUtil.openJson("masterCategories/allMasterCategories.json")));
        mvc.perform(get("/api/budget/1/master/1/category"))
                .andExpect(status().isOk())
                .andExpect(content().json(testUtil.openJson("categories/categoriesFromSelectedBudget.json")));
        mvc.perform(get("/api/budget/1/master/1/category/1/expense"))
                .andExpect(status().isOk())
                .andExpect(content().json(testUtil.openJson("expenses/expensesFromMasterCategoryAndCategory1.json")));
        //when
        mvc.perform(delete("/api/budget/1/master/1"))
                .andExpect(status().isOk());
        //then
        mvc.perform(get("/api/budget/1/master"))
                .andExpect(status().isOk())
                .andExpect(content().json(testUtil.openJson("masterCategories/masterCategoriesAfterDelete.json")));
        mvc.perform(get("/api/budget/1/master/1/category"))
                .andExpect(content().string("[]"));
        mvc.perform(get("/api/budget/1/master/1/category/1/expense"))
                .andExpect(content().string("[]"));
    }

    @Test
    @WithMockUser(username = LOGIN2)
    public void shouldNotDeleteMasterCategoryOfAnotherUser__andReturnEmptyList() throws Exception {
        mvc.perform(delete("/api/budget/1/master/1}"))
                .andExpect(status().is4xxClientError());
    }


}
