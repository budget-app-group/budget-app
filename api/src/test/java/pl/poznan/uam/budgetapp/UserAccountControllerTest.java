package pl.poznan.uam.budgetapp;

import com.google.gson.Gson;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.testcontainers.containers.PostgreSQLContainer;
import pl.poznan.uam.dto.RegisterDto;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertNotEquals;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:/testDatabase.sql")
public class UserAccountControllerTest {
    @Autowired
    private WebApplicationContext webApplicationContext;

    private MockMvc mvc;
    @ClassRule
    public static PostgreSQLContainer postgreSQLContainer = TestPostgresContainer.getInstance()
            .withDatabaseName("integration-tests-db")
            .withUsername("sa")
            .withPassword("sa");
    private Gson gson = new Gson();

    private static final String LOGIN = "test123@gmail.com";
    private static final String LOGIN2 = "test222@gmail.com";
    private TestUtil testUtil = new TestUtil();

    @Before
    public void setup() {
        mvc = MockMvcBuilders
                .webAppContextSetup(webApplicationContext)
                .apply(springSecurity())
                .alwaysDo(print())
                .build();
    }

    @Test
    @WithMockUser(username = LOGIN)
    public void shouldGetLoggedUser_andRetrun200() throws Exception {
        mvc.perform(get("/api/useraccount"))
                .andExpect(status().isOk())
                .andExpect(content().json(testUtil.openJson("userAccount/loggedUser.json")));

    }

    @Test
    @WithMockUser(username = LOGIN)
    public void shouldGetUserById_andReturn200() throws Exception {
        mvc.perform(get("/api/useraccount/1"))
                .andExpect(status().isOk())
                .andExpect(content().json(testUtil.openJson("userAccount/userId1.json")));
    }

    @Test
    public void shouldNotGetUserByBeingNotLoggedIn_andReturn403() throws Exception {
        mvc.perform(get("/api/useraccount/1"))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser(username = LOGIN2)
    public void shouldDeleteAccountOfLoggedUserWithOneBudget_andReturn200() throws Exception {
        //given
        mvc.perform(get("/api/useraccount"))
                .andExpect(status().isOk())
                .andExpect(content().json(testUtil.openJson("userAccount/loggedUser2.json")));
        mvc.perform(get("/api/budget"))
                .andExpect(status().isOk())
                .andExpect(content().json(testUtil.openJson("budget/allBudgetsUser2.json")));
        //when
        mvc.perform(delete("/api/useraccount"))
                .andExpect(status().isOk());
        //then
        mvc.perform(get("/api/useraccount"))
                .andExpect(status().isNotFound());
        mvc.perform(get("/api/budget"))
                .andExpect(content().json("[]"));
    }

    @Test
    @WithMockUser(username = LOGIN)
    public void shouldDeleteAccountOfLoggedUserWithManyExistingBudget_andReturnXXX() throws Exception {
        //TODO
    }

    @Test
    @WithMockUser(username = "nonexisting@gmail.com")
    public void shouldDeleteNonExistingUserAccount__andReturn404() throws Exception {
        mvc.perform(delete("/api/useraccount"))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser(username = LOGIN)
    public void shouldModifyUserAccount_andReturn200() throws Exception {
        //given
        RegisterDto modifiedUser = RegisterDto.builder()
                .email("modified@gmail.com")
                .name("modifiedName")
                .surname("modifiedSurname")
                .password("SomePassword")
                .build();
        MvcResult original = mvc.perform(get("/api/useraccount/1"))
                .andExpect(status().isOk())
                .andExpect(content().json(testUtil.openJson("userAccount/loggedUser.json")))
                .andReturn();
        //when
        mvc.perform(put("/api/useraccount").contentType(MediaType.APPLICATION_JSON).content(gson.toJson(modifiedUser)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.email").value(modifiedUser.getEmail()));
        //then
        MvcResult modified = mvc.perform(get("/api/useraccount/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", notNullValue()))
                .andExpect(content().string(containsString(modifiedUser.getEmail())))
                .andReturn();
        assertNotEquals(original, modified);
    }

    @Test
    @WithMockUser(username = LOGIN)
    public void shouldNotModifyUserAccount_emailNotUnique_andReturn400() throws Exception {
        RegisterDto modifiedUser = RegisterDto.builder()
                .email("test222@gmail.com")
                .name("modifiedName")
                .surname("modifiedSurname")
                .password("SomePassword")
                .build();

        mvc.perform(put("/api/useraccount").contentType(MediaType.APPLICATION_JSON).content(gson.toJson(modifiedUser)))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void shouldNotModifyUserAccount_beingNotLoggedIn_andReturn403() throws Exception {
        RegisterDto modifiedUser = RegisterDto.builder()
                .email("modified@gmail.com")
                .name("modifiedName")
                .surname("modifiedSurname")
                .password("SomePassword")
                .build();
        mvc.perform(put("/api/useraccount").contentType(MediaType.APPLICATION_JSON).content(gson.toJson(modifiedUser)))
                .andExpect(status().isUnauthorized());
    }


}