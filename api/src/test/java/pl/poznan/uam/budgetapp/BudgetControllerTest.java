package pl.poznan.uam.budgetapp;

import com.google.gson.Gson;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.testcontainers.containers.PostgreSQLContainer;
import pl.poznan.uam.dto.BudgetDto;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertNotEquals;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:/testDatabase.sql")
public class BudgetControllerTest {

    @Autowired
    private WebApplicationContext webApplicationContext;

    private static final String LOGIN = "test123@gmail.com";
    private static final String LOGIN2 = "test222@gmail.com";
    @ClassRule
    public static PostgreSQLContainer postgreSQLContainer = TestPostgresContainer.getInstance()
            .withDatabaseName("integration-tests-db")
            .withUsername("sa")
            .withPassword("sa");
    private Gson gson = new Gson();
    private TestUtil testUtil = new TestUtil();
    private MockMvc mvc;

    @Before
    public void setup() {
        mvc = MockMvcBuilders
                .webAppContextSetup(webApplicationContext)
                .apply(springSecurity())
                .alwaysDo(print())
                .build();
    }

    @Test
    @WithMockUser(username = LOGIN)
    public void shouldGetBudgetById_andReturn200() throws Exception {
        mvc.perform(get("/api/budget/1"))
                .andExpect(status().isOk())
                .andExpect(content().json(testUtil.openJson("budget/budget1.json")));
    }

    @Test
    @WithMockUser(username = LOGIN2)
    public void shouldNotGetBudgetOfAnotherUser_andReturn403() throws Exception {
        mvc.perform(get("/api/budget/1"))
                .andExpect(status().is4xxClientError());
    }

    @Test
    @WithMockUser(username = LOGIN)
    public void shouldGetAllBudgetsOfUser_andReturn200() throws Exception {
        mvc.perform(get("/api/budget"))
                .andExpect(status().isOk())
                .andExpect(content().json(testUtil.openJson("budget/allBudgetsFromBudget.json")));

    }

    @Test
    @WithMockUser(username = LOGIN)
    public void shouldAddBudget_andReturn201() throws Exception {
        //given
        BudgetDto.MinimalDto userGrpDTO = BudgetDto.MinimalDto.builder()
                .name("postTestBudget")
                .build();
        mvc.perform(get("/api/budget"))
                .andExpect(status().isOk())
                .andExpect(content().json(testUtil.openJson("budget/allBudgetsFromBudget.json")));
        //when
        mvc.perform(post("/api/budget")
                .contentType(MediaType.APPLICATION_JSON).content(gson.toJson(userGrpDTO)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.name").value(userGrpDTO.getName()));
        //then
        mvc.perform(get("/api/budget"))
                .andExpect(status().isOk())
                .andExpect(content().json(testUtil.openJson("budget/afterNewBudget.json")));
    }

    @Test
    @WithMockUser(username = LOGIN)
    public void shouldModifyBudget_andReturn200() throws Exception {
        //given
        BudgetDto.ModifyDto modfiedTestBudget = BudgetDto.ModifyDto.builder()
                .name("modfiedTestBudget")
                .admin(LOGIN)
                .build();
        MvcResult beforePut = mvc.perform(get("/api/budget/1"))
                .andExpect(status().isOk())
                .andExpect(content().json(testUtil.openJson("budget/budget1.json")))
                .andReturn();
        //when
        mvc.perform(put("/api/budget/1")
                .contentType(MediaType.APPLICATION_JSON).content(gson.toJson(modfiedTestBudget)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value(modfiedTestBudget.getName()));
        //then
        MvcResult afterPut = mvc.perform(get("/api/budget/1"))
                .andExpect(status().isOk())
                .andReturn();
        assertNotEquals(beforePut,afterPut);
    }

    @Test
    @WithMockUser(username = LOGIN2)
    public void shouldNotModifyBudgetOfAnotherUser_andReturn404() throws Exception {
        BudgetDto.MinimalDto modfiedTestBudget = BudgetDto.MinimalDto.builder()
                .name("modfiedTestBudget")
                .build();
        mvc.perform(put("/api/budget/1")
                .contentType(MediaType.APPLICATION_JSON).content(gson.toJson(modfiedTestBudget)))
                .andExpect(status().is4xxClientError());
    }

    @Test
    @WithMockUser(username = LOGIN)
    public void shouldDeleteBudget_andReturn200() throws Exception {
        //given
        mvc.perform(get("/api/budget/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value("Budżet osobisty"));
        mvc.perform(get("/api/budget/1/master/1/category"))
                .andExpect(status().isOk())
                .andExpect(content().json(testUtil.openJson("categories/categoriesFromSelectedBudget.json")));
        //when
        mvc.perform(delete("/api/budget/1"))
                .andExpect(status().isOk());
        //then
        mvc.perform(get("/api/budget/1"))
                .andExpect(status().isNotFound());

    }

    @Test
    @WithMockUser(username = LOGIN2)
    public void shouldNotDeleteBudgetOfAnotherUser_andReturn403() throws Exception {
        mvc.perform(delete("/api/budget/1"))
                .andExpect(status().is4xxClientError());
    }

    @Test
    @WithMockUser(username = LOGIN)
    public void shouldAddUserToBudget_andReturn200() throws Exception {
        mvc.perform(get("/api/budget"))
                .andExpect(status().isOk())
                .andExpect(content().json(testUtil.openJson("budget/allBudgetsFromBudget.json")));
        mvc.perform(put("/api/budget/3/3"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value("Obiady firmowe"));
        mvc.perform(get("/api/budget"))
                .andExpect(status().isOk())
                .andExpect(content().json(testUtil.openJson("budget/afterNewUser.json")));
    }

    @Test
    @WithMockUser(username = LOGIN)
    public void shouldListAllUsersOfBudget_andReturn200() throws Exception {
        mvc.perform(get("/api/budget/1/users"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)));
    }

    @Test
    @WithMockUser(username = LOGIN)
    public void shouldNotRemoveLoggedUserFromBudget_andReturn403() throws Exception {
        mvc.perform(get("/api/budget"))
                .andExpect(status().isOk())
                .andExpect(content().json(testUtil.openJson("budget/allBudgetsFromBudget.json")));
        mvc.perform(put("/api/budget/1/leave"))
                .andExpect(status().isForbidden());

    }

    @Test
    @WithMockUser(username = LOGIN2)
    public void shouldRemoveLoggedUserFromBudget_andReturn200() throws Exception {
        mvc.perform(get("/api/budget"))
                .andExpect(status().isOk())
                .andExpect(content().json(testUtil.openJson("budget/allBudgetsUser2.json")));
        mvc.perform(put("/api/budget/3/leave"))
                .andExpect(status().isOk());
        mvc.perform(get("/api/budget"))
                .andExpect(status().isOk())
                .andExpect(content().json(testUtil.openJson("budget/afterLeave.json")));
    }


    @Test
    @WithMockUser(username = LOGIN)
    public void shouldRemoveUserFromBudget_andReturn200() throws Exception {
        //given
        mvc.perform(put("/api/budget/3/3"))
                .andExpect(status().isOk());
        mvc.perform(get("/api/budget/3/users"))
                .andExpect(status().isOk())
                .andExpect(content().json(testUtil.openJson("budget/shouldRemoveUsersList.json")));
        //when
        mvc.perform(put("/api/budget/3/remove/3"))
                .andExpect(status().isOk());
        //then
        mvc.perform(get("/api/budget/3/users"))
                .andExpect(status().isOk())
                .andExpect(content().json(testUtil.openJson("budget/usersAfterUserRemoval.json")));
    }

}