package pl.poznan.uam.budgetapp;

import com.google.gson.Gson;
import org.hamcrest.core.IsNull;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.testcontainers.containers.PostgreSQLContainer;
import pl.poznan.uam.dto.LoginDto;
import pl.poznan.uam.dto.RegisterDto;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:/testDatabase.sql")
public class UserAuthControllerTest {

    @Autowired
    private WebApplicationContext webApplicationContext;
    @ClassRule
    public static PostgreSQLContainer postgreSQLContainer = TestPostgresContainer.getInstance()
            .withDatabaseName("integration-tests-db")
            .withUsername("sa")
            .withPassword("sa");
    private MockMvc mvc;
    private Gson gson = new Gson();

    @Before
    public void setup() {
        mvc = MockMvcBuilders
                .webAppContextSetup(webApplicationContext)
                .apply(springSecurity())
                .alwaysDo(print())
                .build();
    }

    @Test
    public void shouldLoginUser_andReturn201() throws Exception {
        LoginDto loginDto = LoginDto.builder()
                .email("test123@gmail.com")
                .password("Haslo24")
                .build();
        mvc.perform(post("/auth/login")
                .contentType(MediaType.APPLICATION_JSON)
                .content(gson.toJson(loginDto)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.user").exists())
                .andExpect(jsonPath("$.token.tokenType").value("Bearer"))
                .andExpect(jsonPath("$.token.accessToken").value(IsNull.notNullValue()));
    }

    @Test
    public void shouldNotLoginUserWithWrongPassword_andReturn401() throws Exception {
        LoginDto loginDto = LoginDto.builder()
                .email("test123@gmail.com")
                .password("wrong_password")
                .build();
        mvc.perform(post("/auth/login").contentType(MediaType.APPLICATION_JSON)
                .content(gson.toJson(loginDto)))
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void shouldNotLoginUser_notVerifiedEmail() throws Exception {
        LoginDto loginDto = LoginDto.builder()
                .email("test3@gmail.com")
                .password("password")
                .build();
        mvc.perform(post("/auth/login").contentType(MediaType.APPLICATION_JSON)
                .content(gson.toJson(loginDto)))
                .andExpect(status().isBadRequest())
                        .andExpect(status().reason("Account is not verified"));
    }

    @Test
    public void shouldRegisterUser_andReturn201() throws Exception {
        RegisterDto registerDto = RegisterDto.builder()
                .email("regtest@gmail.com")
                .name("regtest")
                .surname("regtest")
                .password("password")
                .build();
        mvc.perform(post("/auth/register").contentType(MediaType.APPLICATION_JSON)
                .content(gson.toJson(registerDto)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.email").value(registerDto.getEmail()));
    }

    @Test
    public void shouldNotRegisterUserWithExistingEmail_andReturn400() throws Exception {
        RegisterDto registerDto = RegisterDto.builder()
                .email("test3@gmail.com")
                .name("regtest")
                .surname("regtest")
                .password("password")
                .build();
        mvc.perform(post("/auth/register").contentType(MediaType.APPLICATION_JSON)
                .content(gson.toJson(registerDto)))
                .andExpect(status().isBadRequest())
                .andExpect(status().reason("Account with given email already exists"));
    }

    @Test
    public void shouldNotRegisterUserWithEmptyEmail_andReturn400() throws Exception {
        RegisterDto registerDto = RegisterDto.builder()
                .email("")
                .name("regtest")
                .surname("regtest")
                .password("password")
                .build();
        mvc.perform(post("/auth/register").contentType(MediaType.APPLICATION_JSON)
                .content(gson.toJson(registerDto)))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void shouldNotRegisterUserWithEmptyPassword_andReturn400() throws Exception {
        RegisterDto registerDto = RegisterDto.builder()
                .email("test@gmail.com")
                .name("regtest")
                .surname("regtest")
                .password("")
                .build();
        mvc.perform(post("/auth/register").contentType(MediaType.APPLICATION_JSON)
                .content(gson.toJson(registerDto)))
                .andExpect(status().isBadRequest());
    }

}

//TODO check error messages in json arrays
//TODO find a way to test email confirmation