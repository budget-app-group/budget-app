package pl.poznan.uam.budgetapp;

import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.testcontainers.containers.PostgreSQLContainer;
import pl.poznan.uam.dto.CategoryDto;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertNotEquals;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:/testDatabase.sql")
public class CategoryControllerTest {

    @Autowired
    private WebApplicationContext webApplicationContext;

    private MockMvc mvc;

    @ClassRule
    public static PostgreSQLContainer postgreSQLContainer = TestPostgresContainer.getInstance()
            .withDatabaseName("integration-tests-db")
            .withUsername("sa")
            .withPassword("sa");

    private static final String LOGIN = "test123@gmail.com";
    private static final String LOGIN2 = "test222@gmail.com";
    private TestUtil testUtil = new TestUtil();

    @Before
    public void setup() {
        mvc = MockMvcBuilders
                .webAppContextSetup(webApplicationContext)
                .apply(springSecurity())
                .alwaysDo(print())
                .build();
    }

    @Test
    @WithMockUser(username = LOGIN)
    public void shouldReturnCategoriesFromSelectedBudget__andReturn200() throws Exception {
        mvc.perform(get("/api/budget/1/master/1/category"))
                .andExpect(status().isOk())
                .andExpect(content().json(testUtil.openJson("categories/categoriesFromSelectedBudget.json")));
    }

    @Test
    @WithMockUser(username = LOGIN2)
    public void shouldNotReturnCategoriesFromSelectedBudgetAsWrongUser__andReturnEmptyList() throws Exception {
        mvc.perform(get("/api/budget/1/master/1/category"))
//                .andExpect(content().string("[]"));
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser(username = LOGIN)
    public void shouldNotReturnCategoriesFromWrongBudget__andReturnEmptyList() throws Exception {
        mvc.perform(get("/api/budget/100/master/1/category"))
//                .andExpect(content().string("[]"));
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser(username = LOGIN)
    public void shouldNotReturnCategoriesFromWrongMasterCategory__andReturnEmptyList() throws Exception {
        mvc.perform(get("/api/budget/1/master/100/category"))
                .andExpect(content().string("[]"));
    }

    @Test
    @WithMockUser(username = LOGIN)
    public void shouldAddCategory__andReturn201() throws Exception {
        //given
        CategoryDto.MinimalDto categoryDTO1 = CategoryDto.MinimalDto.builder()
                .name("testPostCategory")
                .build();
        mvc.perform(get("/api/budget/1/master/1/category"))
                .andExpect(status().isOk())
                .andExpect(content().json(testUtil.openJson("categories/categoriesFromSelectedBudget.json")));
        //when
        mvc.perform(post("/api/budget/1/master/1/category")
                .contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.asJsonString(categoryDTO1)))
                .andExpect(status().isCreated())
                .andExpect(content().json(testUtil.openJson("categories/postCategory.json")));

        //then
        mvc.perform(get("/api/budget/1/master/1/category"))
                .andExpect(status().isOk())
                .andExpect(content().json(testUtil.openJson("categories/categoriesAfterPost.json")));

    }

    @Test
    @WithMockUser(username = LOGIN2)
    public void shouldNotAddCategoryAsWrongUser__andReturn404() throws Exception {
        //given
        List<CategoryDto.MinimalDto> categoryDTOList = new ArrayList<>();
        CategoryDto.MinimalDto categoryDTO1 = CategoryDto.MinimalDto.builder()
                .name("testPostCategory")
                .build();
        CategoryDto.MinimalDto categoryDTO2 = CategoryDto.MinimalDto.builder()
                .name("testPostCategory2")
                .build();
        categoryDTOList.add(categoryDTO1);
        categoryDTOList.add(categoryDTO2);
        //when
        mvc.perform(post("/api/budget/1/master/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.asJsonString(categoryDTOList)))
                .andExpect(status().is4xxClientError());
    }

    @Test
    @WithMockUser(username = LOGIN)
    public void shouldNotAddCategoryToOtherUserBudget__andReturn400() throws Exception {
        //given
        List<CategoryDto.MinimalDto> categoryDTOList = new ArrayList<>();
        CategoryDto.MinimalDto categoryDTO1 = CategoryDto.MinimalDto.builder()
                .name("testPostCategory")
                .build();
        CategoryDto.MinimalDto categoryDTO2 = CategoryDto.MinimalDto.builder()
                .name("testPostCategory2")
                .build();
        categoryDTOList.add(categoryDTO1);
        categoryDTOList.add(categoryDTO2);
        //when
        mvc.perform(post("/api/budget/2/master/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.asJsonString(categoryDTOList)))
                .andExpect(status().is4xxClientError());
    }

    @Test
    @WithMockUser(username = LOGIN)
    public void shouldNotAddCategoryToWrongBudget__andReturn400() throws Exception {
        //given
        List<CategoryDto.MinimalDto> categoryDTOList = new ArrayList<>();
        CategoryDto.MinimalDto categoryDTO1 = CategoryDto.MinimalDto.builder()
                .name("testPostCategory")
                .build();
        CategoryDto.MinimalDto categoryDTO2 = CategoryDto.MinimalDto.builder()
                .name("testPostCategory2")
                .build();
        categoryDTOList.add(categoryDTO1);
        categoryDTOList.add(categoryDTO2);
        //when
        mvc.perform(post("/api/budget/100/master/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.asJsonString(categoryDTOList)))
                .andExpect(status().is4xxClientError());
    }

    @Test
    @WithMockUser(username = LOGIN)
    public void shouldNotAddCategoryToWrongMasterCategory__andReturn400() throws Exception {
        //given
        List<CategoryDto.MinimalDto> categoryDTOList = new ArrayList<>();
        CategoryDto.MinimalDto categoryDTO1 = CategoryDto.MinimalDto.builder()
                .name("testPostCategory")
                .build();
        CategoryDto.MinimalDto categoryDTO2 = CategoryDto.MinimalDto.builder()
                .name("testPostCategory2")
                .build();
        categoryDTOList.add(categoryDTO1);
        categoryDTOList.add(categoryDTO2);
        //when
        mvc.perform(post("/api/budget/2/master/199")
                .contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.asJsonString(categoryDTOList)))
                .andExpect(status().is4xxClientError());
    }

    @Test
    @WithMockUser(username = LOGIN)
    public void shouldModifyCategory__andReturn200() throws Exception {
        //given
        CategoryDto.MinimalDto categoryDTO = CategoryDto.MinimalDto.builder()
                .name("testCategoryFromPut")
                .build();
        MvcResult beforePut = mvc.perform(get("/api/budget/1/master/1"))
                .andExpect(status().isOk())
                .andReturn();
        //when
        mvc.perform(put("/api/budget/1/master/1/category/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.asJsonString(categoryDTO)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value(categoryDTO.getName()));
        //then
        MvcResult afterPut = mvc.perform(get("/api/budget/1/master/1"))
                .andExpect(status().isOk())
                .andReturn();
        assertNotEquals(beforePut, afterPut);
    }

    @Test
    @WithMockUser(username = LOGIN2)
    public void shouldNotModifyCategoryAsWrongUser__andReturn404() throws Exception {
        mvc.perform(put("/api/budget/1/master/1/category/1"))
                .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser(username = LOGIN)
    public void shouldNotModifyCategoryFromWrongBudget__andReturn400() throws Exception {
        mvc.perform(put("/api/budget/100/master/1/category/1"))
                .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser(username = LOGIN)
    public void shouldNotModifyCategoryFromWrongMasterCategory__andReturn400() throws Exception {
        mvc.perform(put("/api/budget/1/master/100/category/1"))
                .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser(username = LOGIN)
    public void shouldNotModifyWrongCategory__andReturn400() throws Exception {
        mvc.perform(put("/api/budget/1/master/1/category/100"))
                .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser(username = LOGIN)
    public void shouldDeleteCategoryAndConnectedExpenses__andReturn200() throws Exception {
        //given
        mvc.perform(get("/api/budget/1/master/1/category"))
                .andExpect(status().isOk())
                .andExpect(content().json(testUtil.openJson("categories/categoriesFromSelectedBudget.json")));
        mvc.perform(get("/api/budget/1/master/1/category/1/expense"))
                .andExpect(status().isOk())
                .andExpect(content().json(testUtil.openJson("expenses/expensesFromMasterCategoryAndCategory1.json")));
        //when
        mvc.perform(delete("/api/budget/1/master/1/category/1"))
                .andExpect(status().isOk());
        //then
        mvc.perform(get("/api/budget/1/master/1/category"))
                .andExpect(status().isOk())
                .andExpect(content().json(testUtil.openJson("categories/categoriesAfterDelete.json")));
        mvc.perform(get("/api/budget/1/master/1/category/1/expense"))
                .andExpect(content().string("[]"));
    }

    @Test
    @WithMockUser(username = LOGIN2)
    public void shouldNotDeleteCategoryAsWrongUser__andReturn404() throws Exception {
        mvc.perform(delete("/api/budget/1/master/1/category/1"))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser(username = LOGIN)
    public void shouldNotDeleteCategoryFromOtherBudget__andReturn404() throws Exception {
        mvc.perform(delete("/api/budget/100/master/1/category/1"))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser(username = LOGIN)
    public void shouldNotDeleteCategoryFromOtherMasterCategory__andReturn404() throws Exception {
        mvc.perform(delete("/api/budget/1/master/100/category/1"))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser(username = LOGIN)
    public void shouldNotDeleteOtherCategory__andReturn404() throws Exception {
        mvc.perform(delete("/api/budget/1/master/1/category/100"))
                .andExpect(status().isNotFound());
    }

}
