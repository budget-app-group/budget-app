package pl.poznan.uam.budgetapp;

import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.testcontainers.containers.PostgreSQLContainer;
import pl.poznan.uam.dto.BankAccountDto;
import pl.poznan.uam.model.BankAccountBankName;

import java.math.BigDecimal;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertNotEquals;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:/testDatabase.sql")
public class BankAccountControllerTest {

    @Autowired
    private WebApplicationContext webApplicationContext;

    private MockMvc mvc;

    @ClassRule
    public static PostgreSQLContainer postgreSQLContainer = TestPostgresContainer.getInstance()
            .withDatabaseName("integration-tests-db")
            .withUsername("sa")
            .withPassword("sa");

    private static final String LOGIN = "test123@gmail.com";
    private static final String LOGIN2 = "test222@gmail.com";

    @Before
    public void setup() {
        mvc = MockMvcBuilders
                .webAppContextSetup(webApplicationContext)
                .apply(springSecurity())
                .alwaysDo(print())
                .build();
    }

    private TestUtil testUtil = new TestUtil();

    @WithMockUser(username = LOGIN)
    @Test
    public void shouldReturnBankAccountsListOfLoggedUser__andReturn200() throws Exception {

        mvc.perform(get("/api/budget/1/bankaccount"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                //                .andExpect(content().string(containsString(TestUtil.asJsonString(testBankAccount.getName()))));
                .andExpect(content().json(testUtil.openJson("bankAccounts/bankAccountsOfLoggedUser.json")));
    }

    @Test
    @WithMockUser(username = LOGIN)
    public void shouldReturnSpecificBankAccountOfLoggedUser__andReturn200() throws Exception {
        mvc.perform(get("/api/budget/1/bankaccount/1"))
                .andExpect(status().isOk())
                .andExpect(content().json(testUtil.openJson("bankAccounts/specificBankAccount.json")));

    }

    @Test
    @WithMockUser(username = LOGIN2)
    public void shouldNotReturnSpecificBankAccountOfAnotherUser__andReturn404() throws Exception {
        mvc.perform(get("/api/budget/1/bankaccount/1"))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser(username = LOGIN)
    public void shouldAddBankAccount__andReturn201() throws Exception {
        //given
        BankAccountDto.MinimalDto bankAccMinimalDto = BankAccountDto.MinimalDto.builder()
                .name("bankAccFromPost")
                .bankName(BankAccountBankName.CASH)
                .type("postType")
                .startingAmount(BigDecimal.valueOf(12.87))
                .build();
        mvc.perform(get("/api/budget/1/bankaccount"))
                .andExpect(status().isOk())
                .andExpect(content().json(testUtil.openJson("bankAccounts/bankAccountsOfLoggedUser.json")));
        //when
        mvc.perform(post("/api/budget/1/bankaccount")
                .contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.asJsonString(bankAccMinimalDto)))
                .andExpect(status().isCreated())
                .andExpect(content().json(testUtil.openJson("bankAccounts/postBankAccount.json")));
        //then
        mvc.perform(get("/api/budget/1/bankaccount"))
                .andExpect(status().isOk())
//                .andExpect(content().string(containsString(TestUtil.asJsonString(bankAccountDTO))));
                .andExpect(content().json(testUtil.openJson("bankAccounts/bankAccountsAfterPost.json")));
    }

    @Test
    @WithMockUser(username = LOGIN)
    public void shouldDeleteBankAccountOfLoggedUser__andReturn200() throws Exception {
        //given
        mvc.perform(get("/api/budget/1/bankaccount"))
                .andExpect(status().isOk())
                .andExpect(content().json(testUtil.openJson("bankAccounts/bankAccountsOfLoggedUser.json")));

        //when
        mvc.perform(delete("/api/budget/1/bankaccount/1"))
                .andExpect(status().isOk());
        //then
        mvc.perform(get("/api/budget/1/bankaccount"))
                .andExpect(status().isOk())
                .andExpect(content().json(testUtil.openJson("bankAccounts/bankAccountsAfterDelete.json")));
    }

    @Test
    @WithMockUser(username = LOGIN2)
    public void shouldNotdeleteBankAccountOfAnotherUser__andReturn404() throws Exception {
        mvc.perform(delete("/api/budget/1/bankaccount/1"))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser(username = LOGIN)
    public void shouldNotDeleteNonExistingAccount__andReturn404() throws Exception {
        mvc.perform(delete("/api/budget/1/bankaccount/100"))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser(username = LOGIN)
    public void shouldModifyBankAccountOfLoggedUser__andReturn200() throws Exception {
        //given
        BankAccountDto.MinimalDto bankAccMinimalDto = BankAccountDto.MinimalDto.builder()
                .name("modifiedBankAccFromPut")
                .bankName(BankAccountBankName.PKO)
                .type("putType")
                .startingAmount(BigDecimal.valueOf(13.37))
                .build();
        MvcResult beforePut = mvc.perform(get("/api/budget/1/bankaccount/1"))
                .andExpect(status().isOk())
                .andReturn();
        //when
        mvc.perform(put("/api/budget/1/bankaccount/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.asJsonString(bankAccMinimalDto)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value(bankAccMinimalDto.getName()));
        //then
        MvcResult afterPut = mvc.perform(get("/api/budget/1/bankaccount/1"))
                .andExpect(status().isOk())
                .andReturn();
        assertNotEquals(beforePut, afterPut);
    }

    @Test
    @WithMockUser(username = LOGIN2)
    public void shouldNotModifyBankAccountOfAnotherUser__andReturn404() throws Exception {
        BankAccountDto.MinimalDto bankAccMinimalDto = BankAccountDto.MinimalDto.builder()
                .name("modifiedBankAccFromPut")
                .bankName(BankAccountBankName.PKO)
                .type("putType")
                .startingAmount(BigDecimal.valueOf(13.37))
                .build();
        mvc.perform(put("/api/budget/1/bankaccount/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.asJsonString(bankAccMinimalDto)))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser(username = LOGIN)
    public void shouldNotModifyNonExistingBankAccount__andReturn404() throws Exception {
        BankAccountDto.MinimalDto bankAccMinimalDto = BankAccountDto.MinimalDto.builder()
                .name("modifiedBankAccFromPut")
                .bankName(BankAccountBankName.PKO)
                .type("putType")
                .startingAmount(BigDecimal.valueOf(13.37))
                .build();
        mvc.perform(put("/api/budget/1/bankaccount/100")
                .contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.asJsonString(bankAccMinimalDto)))
                .andExpect(status().isNotFound());
    }

    @Test
    public void shouldNotGetBankAccountsListAsNotLoggedUser_andReturn401() throws Exception {
        mvc.perform(get("/api/budget/1/bankaccount"))
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void shouldNotGetBankAccountAsNotLoggedUser__andReturn401() throws Exception {
        mvc.perform(get("/api/budget/1/bankaccount"))
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void shouldNotAddBankAccountAsNotLoggedUser__andReturn401() throws Exception {
        BankAccountDto.MinimalDto bankAccMinimalDto = BankAccountDto.MinimalDto.builder()
                .name("modifiedBankAccFromPost")
                .bankName(BankAccountBankName.PKO)
                .type("putType")
                .startingAmount(BigDecimal.valueOf(13.37))
                .build();
        mvc.perform(post("/api/budget/1/bankaccount")
                .contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.asJsonString(bankAccMinimalDto)))
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void shouldNotModifyBankAccountAsNotLoggedUser__andReturn401() throws Exception {
        BankAccountDto.MinimalDto bankAccMinimalDto = BankAccountDto.MinimalDto.builder()
                .name("modifiedBankAccFromPut")
                .bankName(BankAccountBankName.PKO)
                .type("putType")
                .startingAmount(BigDecimal.valueOf(13.37))
                .build();
        mvc.perform(put("/api/budget/1/bankaccount/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(TestUtil.asJsonString(bankAccMinimalDto)))
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void shouldNotDeleteBankAccountAsNotLoggedUser__andReturn401() throws Exception {
        mvc.perform(delete("/api/budget/1/bankaccount/1"))
                .andExpect(status().isUnauthorized());
    }

}
