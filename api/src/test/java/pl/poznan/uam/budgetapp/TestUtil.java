package pl.poznan.uam.budgetapp;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

public class TestUtil {

    private static final ObjectMapper mapper = new ObjectMapper();
    public static String asJsonString(Object object) throws JsonProcessingException {
        return mapper.writeValueAsString(object);
    }

    public String openJson(String fileName) {
        JsonParser parser = new JsonParser();
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream("jsons/" + fileName);
        if (inputStream != null) {
            Reader reader = new InputStreamReader(inputStream);
            JsonElement jsonElement = parser.parse(reader);
            return jsonElement.toString();
        } else {
            return null;
        }
    }

}
