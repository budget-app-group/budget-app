package pl.poznan.uam.budgetapp;

import com.google.gson.Gson;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.testcontainers.containers.PostgreSQLContainer;
import pl.poznan.uam.dto.ExpenseDto;

import java.math.BigDecimal;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertNotEquals;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:/testDatabase.sql")
public class ExpenseControllerTest {

    @Autowired
    private WebApplicationContext webApplicationContext;

    private MockMvc mvc;
    @ClassRule
    public static PostgreSQLContainer postgreSQLContainer = TestPostgresContainer.getInstance()
            .withDatabaseName("integration-tests-db")
            .withUsername("sa")
            .withPassword("sa");
    private Gson gson = new Gson();

    private static final String LOGIN = "test123@gmail.com";
    private static final String LOGIN2 = "test222@gmail.com";
    private TestUtil testUtil = new TestUtil();

    @Before
    public void setup(){
        mvc = MockMvcBuilders
                .webAppContextSetup(webApplicationContext)
                .apply(springSecurity())
                .alwaysDo(print())
                .build();
    }

    @Test
    @WithMockUser(username = LOGIN)
    public void shouldGetAllExpensesFromCategoryAndMasterCategory_andReturn200() throws Exception {
        mvc.perform(get("/api/budget/1/master/1/category/1/expense"))
                .andExpect(status().isOk())
                .andExpect(content().json(testUtil.openJson("expenses/expensesFromMasterCategoryAndCategory1.json")));
    }

    @Test
    @WithMockUser(username = LOGIN)
    public void shouldNotGetAllExpensesFromNotYourBudget_andReturnEmptyList() throws Exception {
        mvc.perform(get("/api/budget/2/master/1/category/1/expense"))
                .andExpect(content().string("[]"));
    }

    @Test
    @WithMockUser(username = LOGIN)
    public void shouldNotGetAllExpensesFromWrongMasterCategory_andEmptyList() throws Exception {
        mvc.perform(get("/api/budget/1/master/999/category/1/expense"))
                .andExpect(content().string("[]"));
    }

    @Test
    @WithMockUser(username = LOGIN)
    public void shouldNotGetAllExpensesFromWrongCategory_andReturnEmptyList() throws Exception {
        mvc.perform(get("/api/budget/1/master/1/category/100/expense"))
                .andExpect(content().string("[]"));
    }

    @Test
    @WithMockUser(username = LOGIN)
    public void shouldGetExpenseByID_andReturn200() throws Exception {
        mvc.perform(get("/api/budget/1/master/1/category/1/expense/1"))
                .andExpect(status().isOk())
                .andExpect(content().json(testUtil.openJson("expenses/expense1.json")));
    }

    @Test
    @WithMockUser(username = LOGIN2)
    public void shouldNotGetExpenseByIDOfAnotherUser_andReturn404() throws Exception {
        mvc.perform(get("/api/budget/2/master/3/category/3/expense/2"))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser(username = LOGIN)
    public void shouldGetAllExpensesFromBankAccount_andReturn200() throws Exception {
        mvc.perform(get("/api/budget/1/bankaccount/1/expense"))
                .andExpect(status().isOk())
                .andExpect(content().json(testUtil.openJson("expenses/expansesFromBankAccount1.json")));
    }

    @Test
    @WithMockUser(username = LOGIN)
    public void shouldGetAllExpensesFromBudget_andReturn200() throws Exception {
        mvc.perform(get("/api/budget/1/expense"))
                .andExpect(status().isOk())
                .andExpect(content().json(testUtil.openJson("expenses/expensesFromBudget1.json")));
    }

    @Test
    @WithMockUser(username = LOGIN)
    public void shouldReturnExpensesFromGivenPeriod_andReturn200() throws Exception{
        mvc.perform(get("/api/budget/1/expense/period?from={fromDate}&to={toDate}", "1524076800000", "1591852800000"))
                .andExpect(status().isOk())
                .andExpect(content().json(testUtil.openJson("expenses/expensesFromGivenPeriod.json")));
    }

    @Test
    @WithMockUser(username = LOGIN)
    public void shouldAddNewExpense_andReturn201() throws Exception{
        //given
        ExpenseDto.PostDto testExpense1 = ExpenseDto.PostDto.builder()
                .amount(BigDecimal.valueOf(42.22))
                .name("testExpense1")
                .expenseDate(1570884044771L)
                .shopName("Tesco")
                .bankAccountId(1)
                .income(false)
                .build();

        mvc.perform(get("/api/budget/1/expense"))
                .andExpect(status().isOk())
                .andExpect(content().json(testUtil.openJson("expenses/expensesFromBudget1.json")));
        //when
        mvc.perform(post("/api/budget/1/master/1/category/1/expense")
                .contentType(MediaType.APPLICATION_JSON).content(gson.toJson(testExpense1)))
                .andExpect(status().isCreated())
                .andExpect(content().json(testUtil.openJson("expenses/postExpenses.json")));

        mvc.perform(get("/api/budget/1/expense"))
                .andExpect(status().isOk())
                .andExpect(content().json(testUtil.openJson("expenses/expensesAfterPost.json")));
    }

    @Test
    @WithMockUser(username = LOGIN)
    public void shouldReturnListOfIncomesForBankAccount__andReturn200() throws Exception {
        mvc.perform(get("/api/budget/1/bankaccount/1/income"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$",hasSize(1)));
    }

    @Test
    @WithMockUser(username = LOGIN)
    public void shouldModifyExpense_andReturn200() throws Exception {
        //given
        ExpenseDto.PutDto expenseDto = ExpenseDto.PutDto.builder()
                .amount(BigDecimal.valueOf(311.21))
                .name("editedExpense")
                .expenseDate(1570884044771L)
                .shopName("editedShop")
                .income(false)
                .categoryId(1)
                .mastCatId(1)
                .bankAccountId(1)
                .build();
        MvcResult beforePut = mvc.perform(get("/api/budget/1/master/1/category/1/expense/1"))
                .andExpect(status().isOk())
                .andReturn();
        //when
        mvc.perform(put("/api/budget/1/master/1/category/1/expense/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(gson.toJson(expenseDto)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value(expenseDto.getName()));
        //then
        MvcResult afterPut = mvc.perform(get("/api/budget/1/master/1/category/1/expense/"))
                .andExpect(status().isOk())
                .andReturn();

        assertNotEquals(beforePut, afterPut);
    }

    @Test
    @WithMockUser(username = "wrongUser@gmail.com")
    public void shouldNotModifyExpenseLoggedAsWrongUser_andReturn400() throws Exception {
        mvc.perform(put("/api/budget/1/master/1/category/1/expense/1"))
                .andExpect(status().isBadRequest());
    }

    @Test
    @WithMockUser(username = LOGIN)
    public void shouldNotModifyExpenseWithWrongCategory_andReturn404() throws Exception {

        ExpenseDto.PutDto expenseDto = ExpenseDto.PutDto.builder()
                .amount(BigDecimal.valueOf(311.21))
                .name("editedExpense")
                .expenseDate(1570884044771L)
                .shopName("editedShop")
                .categoryId(100)
                .mastCatId(1)
                .bankAccountId(1)
                .build();

        mvc.perform(put("/api/budget/1/master/1/category/1/expense/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(gson.toJson(expenseDto)))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser(username = LOGIN)
    public void shouldNotModifyExpenseWithWrongBankAccount_andReturn404() throws Exception {
        ExpenseDto.PutDto expenseDto = ExpenseDto.PutDto.builder()
                .amount(BigDecimal.valueOf(311.21))
                .name("editedExpense")
                .expenseDate(1570884044771L)
                .shopName("editedShop")
                .categoryId(1)
                .mastCatId(1)
                .bankAccountId(100)
                .build();
        mvc.perform(put("/api/budget/1/master/1/category/1/expense/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(gson.toJson(expenseDto)))
                .andExpect(status().isNotFound());
    }


    @Test
    @WithMockUser(username = LOGIN)
    public void shouldDeleteExpense_andReturn200() throws Exception {
        mvc.perform(get("/api/budget/1/expense"))
                .andExpect(status().isOk())
                .andExpect(content().json(testUtil.openJson("expenses/expensesFromBudget1.json")));
        mvc.perform(delete("/api/budget/1/master/1/category/1/expense/1"))
                .andExpect(status().isOk());
        mvc.perform(get("/api/budget/1/expense"))
                .andExpect(status().isOk())
                .andExpect(content().json(testUtil.openJson("expenses/expensesAfterDelete.json")));
    }

    @Test
    @WithMockUser(username = "wrongUser@gmail.com")
    public void shouldNotDeleteExpenseAsWrongUser_andReturn404() throws Exception {
        mvc.perform(delete("/api/budget/1/master/1/category/1/expense/1"))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser(username = LOGIN)
    public void shouldNotDeleteExpenseFromOtherCategory_andReturn404() throws Exception {
        mvc.perform(delete("/api/budget/1/master/1/category/100/expense/1"))
                .andExpect(status().isNotFound());
    }

}
