create sequence hibernate_sequence;

create table budget
(
    id    bigint not null
        constraint budget_pkey
            primary key,
    admin varchar(255),
    name  varchar(255)
);

create table bank_account
(
    id              bigint not null
        constraint bank_account_pkey
            primary key,
    bank_name       varchar(255),
    means           numeric(19, 2),
    starting_amount numeric(19, 2),
    name            varchar(255),
    type            varchar(255),
    budget_id       bigint
        constraint fkm336ift5qxr6drwd8qk6rddem
            references budget
);

create table goal
(
    id               bigint not null
        constraint goal_pkey
            primary key,
    collected_amount numeric(19, 2),
    name             varchar(255),
    target_amount    numeric(19, 2),
    target_date      timestamp,
    budget_id        bigint
        constraint fko3j9493wpjabkbrwjgu77kjlp
            references budget
);

create table master_category
(
    id                bigint  not null
        constraint master_category_pkey
            primary key,
    budget_allocation integer not null,
    name              varchar(255),
    budget_id         bigint
        constraint fk163chgoeo1v8ji9825cxq8j0i
            references budget
);

create table category
(
    id                 bigint not null
        constraint category_pkey
            primary key,
    name               varchar(255),
    master_category_id bigint
        constraint fkrmxwe8ncr3mlgqslpykqhyu7k
            references master_category
);

create table receipt
(
    id                     bigint  not null
        constraint receipt_pkey
            primary key,
    after_ocr              varchar(8196),
    expense                varchar(255),
    is_date_incorrect      boolean not null,
    is_price_incorrect     boolean not null,
    is_shop_name_incorrect boolean not null,
    org_receipt            oid
);

create table shop
(
    id      bigint not null
        constraint shop_pkey
            primary key,
    address varchar(255),
    city    varchar(255),
    name    varchar(255)
);

create table test_endpoint
(
    id     bigint not null
        constraint test_endpoint_pkey
            primary key,
    string varchar(4096)
);

create table user_account
(
    id            bigint    not null
        constraint user_account_pkey
            primary key,
    creation_date timestamp not null,
    update_date   timestamp not null,
    email         varchar(255)
        constraint uk_hl02wv5hym99ys465woijmfib
            unique,
    is_enabled    boolean   not null,
    name          varchar(255),
    password      varchar(255),
    surname       varchar(255)
);

create table budget_user_accounts
(
    budgets_id       bigint not null
        constraint fksjaorjv9378lq53jju5o24k99
            references budget,
    user_accounts_id bigint not null
        constraint fkrwqrtu4kxywpkx4b1amhvpe31
            references user_account,
    constraint budget_user_accounts_pkey
        primary key (budgets_id, user_accounts_id)
);

create table expense
(
    id              bigint  not null
        constraint expense_pkey
            primary key,
    amount          numeric(19, 2),
    expense_date    timestamp,
    is_income       boolean not null,
    item_name       varchar(255),
    shop_name       varchar(255),
    bank_account_id bigint
        constraint fkc0cqfeddr0t4u9xj1boe4mqti
            references bank_account,
    budget_id       bigint
        constraint fkf7fru65n5syjnfkfov9ma34g1
            references budget,
    category_id     bigint
        constraint fkmvjm59reb5i075vu38bwcaqj6
            references category,
    goal_id         bigint
        constraint fk2h1slkmv95wgrds8j8lfs6cs1
            references goal,
    shop_id         bigint
        constraint fk215y1sygsdddcwrw7acxpw30u
            references shop,
    user_account_id bigint
        constraint fkoog8fi98i5vu8ssjsvqb1sbtk
            references user_account
);

create table verification_token
(
    id                 bigint not null
        constraint verification_token_pkey
            primary key,
    created_date       timestamp,
    expiration_date    timestamp,
    verification_token varchar(255),
    user_account_id    bigint
        constraint fks94q2qhekephicakyqnj6twu1
            references user_account
);
