ALTER TABLE user_account
    ADD COLUMN first_time boolean not null default true;