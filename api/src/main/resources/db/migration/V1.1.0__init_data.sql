INSERT INTO public.user_account (id, email, password, name, surname, is_enabled, creation_date, update_date) VALUES (1, 'test123@gmail.com', '$2a$10$kZ55gJpWg7GQRBESiZKKleT2lxaz40/kQvz3FpV6Restw.jSxfD8q', 'username1', 'usersurname1', true, '2019-03-19 20:05:06', '2019-03-19 20:05:06');
INSERT INTO public.user_account (id, email, password, name, surname, is_enabled, creation_date, update_date)
VALUES (2, 'test222@gmail.com', '$2a$10$kZ55gJpWg7GQRBESiZKKleT2lxaz40/kQvz3FpV6Restw.jSxfD8q', 'username2',
        'usersurname2', true, '2019-03-19 20:05:06', '2019-03-19 20:05:06');
INSERT INTO public.user_account (id, email, password, name, surname, is_enabled, creation_date, update_date)
VALUES (3, 'test3@gmail.com', '$2a$10$kZ55gJpWg7GQRBESiZKKleT2lxaz40/kQvz3FpV6Restw.jSxfD8q', 'username3',
        'usersurname3', false, '2019-03-19 20:05:06', '2019-03-19 20:05:06');

INSERT INTO public.budget (id, name, admin)
VALUES (1, 'Budżet osobisty', 'test123@gmail.com');
INSERT INTO public.budget (id, name, admin)
VALUES (2, 'Budżet osobisty', 'test222@gmail.com');
INSERT INTO public.budget (id, name, admin)
VALUES (3, 'Obiady firmowe', 'test123@gmail.com');
INSERT INTO public.budget (id, name, admin)
VALUES (4, 'Budżet osobisty', 'test3@gmail.com');

INSERT INTO public.budget_user_accounts (user_accounts_id, budgets_id)
VALUES (1, 1);
INSERT INTO public.budget_user_accounts (user_accounts_id, budgets_id)
VALUES (2, 2);
INSERT INTO public.budget_user_accounts (user_accounts_id, budgets_id)
VALUES (1, 3);
INSERT INTO public.budget_user_accounts (user_accounts_id, budgets_id)
VALUES (3, 3);
INSERT INTO public.budget_user_accounts (user_accounts_id, budgets_id)
VALUES (3, 4);

INSERT INTO public.bank_account (id, bank_name, starting_amount, means, name, type, budget_id)
VALUES (1, 'ING', 0, 0, 'Główne', 'Konto Zwykłe', 1);
INSERT INTO public.bank_account (id, bank_name, starting_amount, means, name, type, budget_id)
VALUES (2, 'PKO', 0, 0, 'Oszczędności', 'Konto oszczędnościowe', 1);
INSERT INTO public.bank_account (id, bank_name, starting_amount, means, name, type, budget_id)
VALUES (3, 'MBANK', 0, 0, 'Główne', 'Konto główne', 2);
INSERT INTO public.bank_account (id, bank_name, starting_amount, means, name, type, budget_id)
VALUES (4, 'SANTANDER', 0, 0, 'Główne', 'Konto główne', 3);
INSERT INTO public.bank_account (id, bank_name, starting_amount, means, name, type, budget_id)
VALUES (5, 'SANTANDER', 0, 0, 'Główne', 'Konto główne', 4);


INSERT INTO public.master_category (id, budget_allocation, name, budget_id)
VALUES (1, 60, 'Opłaty stałe', 1);
INSERT INTO public.master_category (id, budget_allocation, name, budget_id)
VALUES (2, 20, 'Oszczędnosci i Inwestycje', 1);
INSERT INTO public.master_category (id, budget_allocation, name, budget_id)
VALUES (3, 5, 'Rozrywka i Wypoczynek', 1);
INSERT INTO public.master_category (id, budget_allocation, name, budget_id)
VALUES (4, 10, 'Wydatki bieżące', 1);
INSERT INTO public.master_category (id, budget_allocation, name, budget_id)
VALUES (5, 5, 'Wydatki nieregularne', 1);

INSERT INTO public.master_category (id, budget_allocation, name, budget_id)
VALUES (6, 60, 'Opłaty stałe', 2);
INSERT INTO public.master_category (id, budget_allocation, name, budget_id)
VALUES (7, 20, 'Oszczędnosci i Inwestycje', 2);
INSERT INTO public.master_category (id, budget_allocation, name, budget_id)
VALUES (8, 5, 'Rozrywka i Wypoczynek', 2);
INSERT INTO public.master_category (id, budget_allocation, name, budget_id)
VALUES (9, 10, 'Wydatki bieżące', 2);
INSERT INTO public.master_category (id, budget_allocation, name, budget_id)
VALUES (10, 5, 'Wydatki nieregularne', 2);

INSERT INTO public.master_category (id, budget_allocation, name, budget_id)
VALUES (11, 10, 'Wydatki bieżące', 3);

INSERT INTO public.category (id, name, master_category_id)
VALUES (1, 'Dom', 1);
INSERT INTO public.category (id, name, master_category_id)
VALUES (2, 'Rachunki', 1);
INSERT INTO public.category (id, name, master_category_id)
VALUES (3, 'Podatki i Opłaty', 1);

INSERT INTO public.category (id, name, master_category_id)
VALUES (4, 'Inwestycje emerytalne', 2);
INSERT INTO public.category (id, name, master_category_id)
VALUES (5, 'Oszczędności długoterminowe', 2);

INSERT INTO public.category (id, name, master_category_id)
VALUES (6, 'Restauracje', 3);
INSERT INTO public.category (id, name, master_category_id)
VALUES (7, 'Sport', 3);
INSERT INTO public.category (id, name, master_category_id)
VALUES (8, 'Multimedia', 3);
INSERT INTO public.category (id, name, master_category_id)
VALUES (9, 'Kino i teatr', 3);

INSERT INTO public.category (id, name, master_category_id)
VALUES (10, 'Zakupy spożywcze', 4);
INSERT INTO public.category (id, name, master_category_id)
VALUES (11, 'Kosmetyki i uroda', 4);

INSERT INTO public.category (id, name, master_category_id)
VALUES (12, 'Zdrowie', 5);
INSERT INTO public.category (id, name, master_category_id)
VALUES (13, 'Odzież', 5);


INSERT INTO public.category (id, name, master_category_id)
VALUES (14, 'Dom', 6);
INSERT INTO public.category (id, name, master_category_id)
VALUES (15, 'Rachunki', 6);
INSERT INTO public.category (id, name, master_category_id)
VALUES (16, 'Podatki i Opłaty', 6);

INSERT INTO public.category (id, name, master_category_id)
VALUES (17, 'Inwestycje emerytalne', 7);
INSERT INTO public.category (id, name, master_category_id)
VALUES (18, 'Oszczędności długoterminowe', 7);

INSERT INTO public.category (id, name, master_category_id)
VALUES (19, 'Restauracje', 8);
INSERT INTO public.category (id, name, master_category_id)
VALUES (20, 'Sport', 8);
INSERT INTO public.category (id, name, master_category_id)
VALUES (21, 'Multimedia', 8);
INSERT INTO public.category (id, name, master_category_id)
VALUES (22, 'Kino i teatr', 8);

INSERT INTO public.category (id, name, master_category_id)
VALUES (23, 'Zakupy spożywcze', 9);
INSERT INTO public.category (id, name, master_category_id)
VALUES (24, 'Kosmetyki i uroda', 9);

INSERT INTO public.category (id, name, master_category_id)
VALUES (25, 'Zdrowie', 10);
INSERT INTO public.category (id, name, master_category_id)
VALUES (26, 'Odzież', 10);


INSERT INTO public.category (id, name, master_category_id)
VALUES (27, 'Zakupy spożywcze', 11);



INSERT INTO public.expense (id, amount, item_name, bank_account_id, category_id, user_account_id, budget_id,
                            expense_date, is_income)
VALUES (1, 9.99, 'Pronto', 1, 1, 1, 1, '2019-06-01 19:10:25-07', false);
INSERT INTO public.expense (id, amount, item_name, bank_account_id, category_id, user_account_id, budget_id,
                            expense_date, is_income)
VALUES (2, 14.99, 'Papier toaletowy', 1, 1, 1, 1, '2019-06-02 19:10:25-07', false);
INSERT INTO public.expense (id, amount, item_name, bank_account_id, category_id, user_account_id, budget_id,
                            expense_date, is_income)
VALUES (3, 49.99, 'Prąd', 1, 2, 1, 1, '2019-06-13 19:10:25-07', false);
INSERT INTO public.expense (id, amount, item_name, bank_account_id, category_id, user_account_id, budget_id,
                            expense_date, is_income)
VALUES (5, 500, 'Wyjazd do Stanów', 1, 4, 1, 1, '2019-06-10 19:10:25-07', false);
INSERT INTO public.expense (id, amount, item_name, bank_account_id, category_id, user_account_id, budget_id,
                            expense_date, is_income)
VALUES (6, 36, 'FatBob', 1, 6, 1, 1, '2019-06-12 19:10:25-07', false);
INSERT INTO public.expense (id, amount, item_name, bank_account_id, category_id, user_account_id, budget_id,
                            expense_date, is_income)
VALUES (7, 14, 'Kolorowa', 1, 6, 1, 1, '2019-06-12 19:10:25-07', false);
INSERT INTO public.expense (id, amount, item_name, bank_account_id, category_id, user_account_id, budget_id,
                            expense_date, is_income)
VALUES (8, 69, 'Siłownia', 1, 7, 1, 1, '2019-06-12 19:10:25-07', false);
INSERT INTO public.expense (id, amount, item_name, bank_account_id, category_id, user_account_id, budget_id,
                            expense_date, is_income)
VALUES (9, 14.50, 'John Wick 3', 1, 9, 1, 1, '2019-06-12 19:10:25-07', false);
INSERT INTO public.expense (id, amount, item_name, bank_account_id, category_id, user_account_id, budget_id,
                            expense_date, is_income)
VALUES (10, 4.99, 'Chleb', 1, 10, 1, 1, '2019-06-12 19:10:25-07', false);
INSERT INTO public.expense (id, amount, item_name, bank_account_id, category_id, user_account_id, budget_id,
                            expense_date, is_income)
VALUES (11, 15, 'Ziemniaki', 1, 10, 1, 1, '2019-06-12 19:10:25-07', false);
INSERT INTO public.expense (id, amount, item_name, bank_account_id, category_id, user_account_id, budget_id,
                            expense_date, is_income)
VALUES (12, 15, 'Dezodorant', 1, 11, 1, 1, '2019-06-12 19:10:25-07', false);
INSERT INTO public.expense (id, amount, item_name, bank_account_id, category_id, user_account_id, budget_id,
                            expense_date, is_income)
VALUES (13, 20, 'Witamina C', 1, 12, 1, 1, '2019-06-12 19:10:25-07', false);
INSERT INTO public.expense (id, amount, item_name, bank_account_id, category_id, user_account_id, budget_id,
                            expense_date, is_income)
VALUES (14, 49.99, 'T-Shirt', 1, 13, 1, 1, '2019-06-12 19:10:25-07', false);
INSERT INTO public.expense (id, amount, item_name, bank_account_id, category_id, user_account_id, budget_id,
                            expense_date, is_income)
VALUES (15, 1500, 'Wypłata', 1, 1, 1, 1, '2019-06-12 19:10:25-07', true);

INSERT INTO public.expense (id, amount, item_name, bank_account_id, category_id, user_account_id, budget_id,
                            expense_date, is_income)
VALUES (16, 9.99, 'Pronto', 3, 14, 2, 2, '2019-06-01 19:10:25-07', false);
INSERT INTO public.expense (id, amount, item_name, bank_account_id, category_id, user_account_id, budget_id,
                            expense_date, is_income)
VALUES (17, 14.99, 'Papier toaletowy', 3, 14, 2, 2, '2019-06-02 19:10:25-07', false);
INSERT INTO public.expense (id, amount, item_name, bank_account_id, category_id, user_account_id, budget_id,
                            expense_date, is_income)
VALUES (18, 49.99, 'Prąd', 3, 15, 1, 1, '2019-06-13 19:10:25-07', false);
INSERT INTO public.expense (id, amount, item_name, bank_account_id, category_id, user_account_id, budget_id,
                            expense_date, is_income)
VALUES (19, 500, 'Wyjazd do Stanów', 3, 18, 2, 2, '2019-06-10 19:10:25-07', false);
INSERT INTO public.expense (id, amount, item_name, bank_account_id, category_id, user_account_id, budget_id,
                            expense_date, is_income)
VALUES (20, 36, 'FatBob', 3, 19, 2, 2, '2019-06-12 19:10:25-07', false);
INSERT INTO public.expense (id, amount, item_name, bank_account_id, category_id, user_account_id, budget_id,
                            expense_date, is_income)
VALUES (21, 14, 'Kolorowa', 3, 19, 2, 2, '2019-06-12 19:10:25-07', false);
INSERT INTO public.expense (id, amount, item_name, bank_account_id, category_id, user_account_id, budget_id,
                            expense_date, is_income)
VALUES (22, 69, 'Siłownia', 3, 20, 2, 2, '2019-06-12 19:10:25-07', false);
INSERT INTO public.expense (id, amount, item_name, bank_account_id, category_id, user_account_id, budget_id,
                            expense_date, is_income)
VALUES (23, 14.50, 'John Wick 3', 3, 22, 2, 2, '2019-06-12 19:10:25-07', false);
INSERT INTO public.expense (id, amount, item_name, bank_account_id, category_id, user_account_id, budget_id,
                            expense_date, is_income)
VALUES (24, 4.99, 'Chleb', 3, 23, 2, 2, '2019-06-12 19:10:25-07', false);
INSERT INTO public.expense (id, amount, item_name, bank_account_id, category_id, user_account_id, budget_id,
                            expense_date, is_income)
VALUES (25, 15, 'Ziemniaki', 3, 23, 2, 2, '2019-06-12 19:10:25-07', false);
INSERT INTO public.expense (id, amount, item_name, bank_account_id, category_id, user_account_id, budget_id,
                            expense_date, is_income)
VALUES (26, 15, 'Dezodorant', 3, 24, 2, 2, '2019-06-12 19:10:25-07', false);
INSERT INTO public.expense (id, amount, item_name, bank_account_id, category_id, user_account_id, budget_id,
                            expense_date, is_income)
VALUES (27, 20, 'Witamina C', 3, 25, 2, 2, '2019-06-12 19:10:25-07', false);
INSERT INTO public.expense (id, amount, item_name, bank_account_id, category_id, user_account_id, budget_id,
                            expense_date, is_income)
VALUES (28, 49.99, 'T-Shirt', 3, 26, 2, 2, '2019-06-12 19:10:25-07', false);
INSERT INTO public.expense (id, amount, item_name, bank_account_id, category_id, user_account_id, budget_id,
                            expense_date, is_income)
VALUES (29, 1500, 'Wypłata', 3, 13, 2, 2, '2019-06-12 19:10:25-07', true);


INSERT INTO public.expense (id, amount, item_name, bank_account_id, category_id, user_account_id, budget_id,
                            expense_date, is_income)
VALUES (30, 9.99, 'Kolorowa', 4, 27, 1, 3, '2019-06-01 19:10:25-07', false);
INSERT INTO public.expense (id, amount, item_name, bank_account_id, category_id, user_account_id, budget_id,
                            expense_date, is_income)
VALUES (31, 14.99, 'FatBob', 4, 27, 3, 3, '2019-06-02 19:10:25-07', false);

ALTER SEQUENCE hibernate_sequence RESTART WITH 50;

INSERT INTO public.goal (id, collected_amount, name, target_amount, target_date, budget_id)
VALUES (40, 145.00, 'Supra', 319000, '2022-06-02 19:10:25-07', 1);
INSERT INTO public.goal (id, collected_amount, name, target_amount, target_date, budget_id)
VALUES (41, 100.00, 'Wakacje', 5000, '2020-06-30 19:10:25-07', 1);
INSERT INTO public.goal (id, collected_amount, name, target_amount, target_date, budget_id)
VALUES (42, 500.00, 'OnePlus', 3200, '2020-05-02 19:10:25-07', 1);

INSERT INTO public.expense (id, amount, item_name, bank_account_id, category_id, user_account_id, budget_id,
                            expense_date, is_income, goal_id)
VALUES (32, 145.00, 'Supra', 1, 1, 1, 1, '2019-08-02 19:10:25-07', false, 40);
INSERT INTO public.expense (id, amount, item_name, bank_account_id, category_id, user_account_id, budget_id,
                            expense_date, is_income, goal_id)
VALUES (33, 100.00, 'Wakacje', 1, 1, 1, 1, '2019-09-02 19:10:25-07', false, 41);
INSERT INTO public.expense (id, amount, item_name, bank_account_id, category_id, user_account_id, budget_id,
                            expense_date, is_income, goal_id)
VALUES (34, 500.00, 'OnePlus', 1, 1, 1, 1, '2019-12-02 19:10:25-07', false, 42);

