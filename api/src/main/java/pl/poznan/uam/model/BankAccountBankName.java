package pl.poznan.uam.model;

public enum BankAccountBankName {
    CASH,
    ING,
    PKO,
    PEKAO,
    MBANK,
    SANTANDER
}
