package pl.poznan.uam.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.poznan.uam.model.audit.DateAudit;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.*;

@Getter
@NoArgsConstructor
@Entity
public class UserAccount extends DateAudit {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @Column(unique = true)
    private String email;
    private String name;
    private String surname;
    private String password;
    @Setter
    private boolean isEnabled;
    @Setter
    private boolean firstTime;
    @Setter
    private LocalDateTime lastLogin;


    @ManyToMany(mappedBy = "userAccounts", cascade = CascadeType.ALL)
    private Set<Budget> budgets = new HashSet<>();

    @OneToMany(mappedBy = "userAccount", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Expense> expenses = new ArrayList<>();

    @OneToMany(mappedBy = "userAccount", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<VerificationToken> verificationToken = new ArrayList<>();

    private UserAccount(Builder builder) {
        id = builder.id;
        email = builder.email;
        name = builder.name;
        surname = builder.surname;
        password = builder.password;
        setEnabled(builder.isEnabled);
        setFirstTime(builder.firstTime);
        budgets = builder.budgets;
        expenses = builder.expenses;
    }

    public void addBudget(Budget budget) {
        this.budgets.add(budget);
    }

    public void removeBudget(Budget budget) {
        this.budgets.remove(budget);

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserAccount that = (UserAccount) o;
        return isEnabled() == that.isEnabled() &&
                isFirstTime() == that.isFirstTime() &&
                Objects.equals(getEmail(), that.getEmail()) &&
                Objects.equals(getName(), that.getName()) &&
                Objects.equals(getSurname(), that.getSurname()) &&
                Objects.equals(getPassword(), that.getPassword());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getEmail(), getName(), getSurname(), getPassword(), isEnabled(), isFirstTime());
    }


    public static final class Builder {
        private long id;
        private String email;
        private String name;
        private String surname;
        private String password;
        private boolean isEnabled;
        private boolean firstTime;
        private Set<Budget> budgets = new HashSet<>();
        private List<Expense> expenses = new ArrayList<>();

        public Builder() {
        }

        public Builder id(long val) {
            id = val;
            return this;
        }

        public Builder email(String val) {
            email = val;
            return this;
        }

        public Builder name(String val) {
            name = val;
            return this;
        }

        public Builder surname(String val) {
            surname = val;
            return this;
        }

        public Builder password(String val) {
            password = val;
            return this;
        }

        public Builder isEnabled(boolean val) {
            isEnabled = val;
            return this;
        }

        public Builder firstTime(boolean val) {
            firstTime = val;
            return this;
        }

        public Builder budgets(Set<Budget> val) {
            budgets = val;
            return this;
        }

        public Builder expenses(List<Expense> val) {
            expenses = val;
            return this;
        }

        public UserAccount build() {
            return new UserAccount(this);
        }
    }
}

//TODO decide if creation and update audit are necessary