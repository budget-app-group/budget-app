package pl.poznan.uam.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Getter
@NoArgsConstructor
@Entity
public class Goal {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String name;
    private LocalDateTime targetDate;
    @NotNull
    private BigDecimal targetAmount;
    @Setter
    @NotNull
    private BigDecimal collectedAmount = BigDecimal.ZERO;

    @ManyToOne
    private Budget budget;

    @OneToMany(mappedBy = "goal", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Expense> expenses = new ArrayList<>();

    private Goal(Builder builder){
        id = builder.id;
        name = builder.name;
        targetDate = builder.targetDate;
        targetAmount = builder.targetAmount;
        collectedAmount = builder.collectedAmount;
        budget = builder.budget;
        expenses = builder.expenses;
    }

    public static final class Builder {
        private long id;
        private String name;
        private LocalDateTime targetDate;
        private BigDecimal targetAmount;
        private BigDecimal collectedAmount;
        private Budget budget;
        private List<Expense> expenses = new ArrayList<>();

        public Builder(){}

        public Builder id(long val) {
            id = val;
            return this;
        }

        public Builder name(String val) {
            name = val;
            return this;
        }

        public Builder targetDate(LocalDateTime val) {
            targetDate = val;
            return this;
        }

        public Builder targetAmount(BigDecimal val) {
            targetAmount = val;
            return this;
        }

        public Builder collectedAmount(BigDecimal val) {
            collectedAmount = val;
            return this;
        }

        public Builder budget(Budget val) {
            budget = val;
            return this;
        }

        public Builder expenses(List<Expense> val) {
            expenses = val;
            return this;
        }

        public Goal build(){
            return new Goal(this);
        }
    }
}
