package pl.poznan.uam.model;

import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Getter
@Entity
@NoArgsConstructor
public class Receipt {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Lob
    private byte[] orgReceipt;
    @Column(length = 8196)
    private String afterOcr;
    private String expense;
    private boolean isDateIncorrect = false;
    private boolean isPriceIncorrect = false;
    private boolean isShopNameIncorrect = false;

    private Receipt(Builder builder) {
        id = builder.id;
        orgReceipt = builder.orgReceipt;
        afterOcr = builder.afterOcr;
        expense = builder.expense;
        isPriceIncorrect = builder.isPriceIncorrect;
        isShopNameIncorrect = builder.isShopNameIncorrect;
    }


    public static final class Builder {
        private long id;
        private byte[] orgReceipt;
        private String afterOcr;
        private String expense;
        private boolean isPriceIncorrect;
        private boolean isShopNameIncorrect;
        private boolean isDateIncorrect;

        public Builder() {
        }

        public Builder id(long id) {
            this.id = id;
            return this;
        }

        public Builder orgReceipt(byte[] orgReceipt) {
            this.orgReceipt = orgReceipt;
            return this;
        }

        public Builder afterOcr(String afterOcr) {
            this.afterOcr = afterOcr;
            return this;
        }

        public Builder expense(String expense) {
            this.expense = expense;
            return this;
        }

        public Builder isPriceIncorrect(boolean isPriceIncorrect) {
            this.isPriceIncorrect = isPriceIncorrect;
            return this;
        }

        public Builder isShopNameIncorrect(boolean isShopNameIncorrect) {
            this.isShopNameIncorrect = isShopNameIncorrect;
            return this;
        }

        public Builder isDateIncorrect(boolean isDateIncorrect) {
            this.isDateIncorrect = isDateIncorrect;
            return this;
        }

        public Receipt build() {
            return new Receipt(this);
        }
    }
}
