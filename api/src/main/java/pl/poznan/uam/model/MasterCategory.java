package pl.poznan.uam.model;

import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Getter
@Entity
@NoArgsConstructor
public class MasterCategory {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String name;
    private int budgetAllocation;
    private String color;

    @ManyToOne
    private Budget budget;

    @OneToMany(mappedBy = "masterCategory", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Category> categories = new HashSet<>();

    private MasterCategory(Builder builder) {
        id = builder.id;
        name = builder.name;
        budgetAllocation = builder.budgetAllocation;
        budget = builder.budget;
        categories = builder.categories;
        color = builder.color;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MasterCategory that = (MasterCategory) o;
        return getBudgetAllocation() == that.getBudgetAllocation() &&
                Objects.equals(getName(), that.getName()) &&
                Objects.equals(getColor(), that.getColor()) &&
                Objects.equals(getBudget(), that.getBudget());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getBudgetAllocation(), getColor(), getBudget());
    }

    public void addCategory(Category category) {
        this.categories.add(category);
    }

    public void removeCategory(Category category) {
        this.categories.remove(category);
    }


    public static final class Builder {
        private long id;
        private String name;
        private int budgetAllocation;
        private Budget budget;
        private Set<Category> categories = new HashSet<>();
        private String color;

        public Builder() {
        }

        public Builder id(long val) {
            id = val;
            return this;
        }

        public Builder name(String val) {
            name = val;
            return this;
        }

        public Builder budgetAllocation(int val) {
            budgetAllocation = val;
            return this;
        }

        public Builder budget(Budget val) {
            budget = val;
            return this;
        }

        public Builder categories(Set<Category> val) {
            categories = val;
            return this;
        }

        public Builder color(String val) {
            color = val;
            return this;
        }

        public MasterCategory build() {
            return new MasterCategory(this);
        }
    }
}
