package pl.poznan.uam.model;

import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Getter
@Entity
@NoArgsConstructor
public class Category {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String name;
    private String color;

    @ManyToOne
    @JoinColumn(name = "masterCategory.id")
    private MasterCategory masterCategory;

    @OneToMany(mappedBy = "category", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Expense> expenses = new ArrayList<>();

    private Category(Builder builder) {
        id = builder.id;
        name = builder.name;
        color = builder.color;
        masterCategory = builder.masterCategory;
        expenses = builder.expenses;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Category category = (Category) o;
        return getName().equals(category.getName()) &&
                getColor().equals(category.getColor()) &&
                getMasterCategory().equals(category.getMasterCategory());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getColor(), getMasterCategory());
    }

    public void addExpense(Expense expense){
        this.expenses.add(expense);
    }
    public void removeExpense(Expense expense){
        this.expenses.remove(expense);
    }


    public static final class Builder {
        private long id;
        private String name;
        private MasterCategory masterCategory;
        private List<Expense> expenses = new ArrayList<>();
        private String color;

        public Builder() {
        }

        public Builder id(long val) {
            id = val;
            return this;
        }

        public Builder name(String val) {
            name = val;
            return this;
        }

        public Builder color(String val) {
            color = val;
            return this;
        }

        public Builder masterCategory(MasterCategory val) {
            masterCategory = val;
            return this;
        }

        public Builder expenses(List<Expense> val) {
            expenses = val;
            return this;
        }

        public Category build() {
            return new Category(this);
        }
    }
}
