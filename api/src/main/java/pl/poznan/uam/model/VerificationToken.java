package pl.poznan.uam.model;

import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.UUID;

@Getter
@NoArgsConstructor
@Entity
public class VerificationToken {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String verificationToken;
    private LocalDateTime createdDate;
    private LocalDateTime expirationDate;

    @ManyToOne
    private UserAccount userAccount;

    public VerificationToken(UserAccount userAccount, int validTime) {
        this.userAccount = userAccount;
        this.createdDate = LocalDateTime.now();
        this.expirationDate = LocalDateTime.now().plusDays(validTime);
        this.verificationToken = UUID.randomUUID().toString();
    }

}
