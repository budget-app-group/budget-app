package pl.poznan.uam.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Getter
@Entity
@NoArgsConstructor
public class Expense {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @NotNull
    private BigDecimal amount;
    private String itemName;
    private LocalDateTime expenseDate;
    private String shopName;
    private boolean isIncome;

    @ManyToOne
    private Shop shop;

    @ManyToOne
    @Setter
    private BankAccount bankAccount;

    @ManyToOne
    @Setter
    private Category category;

    @ManyToOne
    private UserAccount userAccount;

    @ManyToOne
    @Setter
    private Budget budget;

    @ManyToOne
    private Goal goal;

    private Expense(Builder builder) {
        id = builder.id;
        amount = builder.amount;
        itemName = builder.itemName;
        expenseDate = builder.expenseDate;
        shopName = builder.shopName;
        shop = builder.shop;
        isIncome = builder.isIncome;
        bankAccount = builder.bankAccount;
        category = builder.category;
        userAccount = builder.userAccount;
        budget = builder.budget;
        goal = builder.goal;
    }

    public static final class Builder {
        private long id;
        private BigDecimal amount;
        private String itemName;
        private LocalDateTime expenseDate;
        private String shopName;
        private Shop shop;
        private boolean isIncome;
        private BankAccount bankAccount;
        private Category category;
        private UserAccount userAccount;
        private Budget budget;
        private Goal goal;

        public Builder() {
        }

        public Builder id(long val) {
            id = val;
            return this;
        }

        public Builder amount(BigDecimal val) {
            amount = val;
            return this;
        }

        public Builder itemName(String val) {
            itemName = val;
            return this;
        }

        public Builder expenseDate(LocalDateTime val) {
            expenseDate = val;
            return this;
        }

        public Builder shopName(String val) {
            shopName = val;
            return this;
        }

        public Builder shop(Shop val) {
            shop = val;
            return this;
        }

        public Builder isIncome(boolean val){
            isIncome = val;
            return this;
        }

        public Builder bankAccount(BankAccount val) {
            bankAccount = val;
            return this;
        }

        public Builder category(Category val) {
            category = val;
            return this;
        }

        public Builder userAccount(UserAccount val) {
            userAccount = val;
            return this;
        }

        public Builder budget(Budget val) {
            budget = val;
            return this;
        }

        public Builder goal(Goal val) {
            goal = val;
            return this;
        }

        public Expense build() {
            return new Expense(this);
        }
    }

}

