package pl.poznan.uam.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.*;

@Getter
@Entity
@NoArgsConstructor
public class Budget {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String name;
    @Setter
    private String admin;

    @ManyToMany
    private Set<UserAccount> userAccounts = new HashSet<>();

    @OneToMany(mappedBy = "budget", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<MasterCategory> masterCategories = new HashSet<>();

    @OneToMany(mappedBy = "budget", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<BankAccount> bankAccounts = new ArrayList<>();

    @OneToMany(mappedBy = "budget", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Expense> expenses = new ArrayList<>();

    @OneToMany(mappedBy = "budget", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Goal> goals = new ArrayList<>();

    private Budget(Builder builder) {
        id = builder.id;
        name = builder.name;
        setAdmin(builder.admin);
        userAccounts = builder.userAccounts;
        masterCategories = builder.masterCategories;
        bankAccounts = builder.bankAccounts;
        expenses = builder.expenses;
        goals = builder.goals;
    }

    public void addUserAccount(UserAccount userAccount){
        this.userAccounts.add(userAccount);
    }

    public void removeUserAccount(UserAccount userAccount) {
        this.userAccounts.remove(userAccount);
    }

    public void addMasterCategory(MasterCategory masterCategory) {
        this.masterCategories.add(masterCategory);
    }
    public void removeMasterCategory(MasterCategory masterCategory) {
        this.masterCategories.remove(masterCategory);
    }

    public void addBankAccount(BankAccount bankAccount) {
        this.bankAccounts.add(bankAccount);
    }

    public void removeBankAccount(BankAccount bankAccount) {
        this.bankAccounts.remove(bankAccount);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Budget budget = (Budget) o;
        return Objects.equals(getName(), budget.getName()) &&
                Objects.equals(getAdmin(), budget.getAdmin());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getAdmin());
    }

    public static final class Builder {
        private long id;
        private String name;
        private String admin;
        private Set<UserAccount> userAccounts = new HashSet<>();
        private Set<MasterCategory> masterCategories = new HashSet<>();
        private List<BankAccount> bankAccounts = new ArrayList<>();
        private List<Expense> expenses = new ArrayList<>();
        private List<Goal> goals = new ArrayList<>();

        public Builder() {
        }

        public Builder id(long val) {
            id = val;
            return this;
        }

        public Builder name(String val) {
            name = val;
            return this;
        }

        public Builder admin(String val) {
            admin = val;
            return this;
        }

        public Builder userAccounts(Set<UserAccount> val) {
            userAccounts = val;
            return this;
        }

        public Builder masterCategories(Set<MasterCategory> val) {
            masterCategories = val;
            return this;
        }

        public Builder bankAccounts(List<BankAccount> val) {
            bankAccounts = val;
            return this;
        }

        public Builder expenses(List<Expense> val) {
            expenses = val;
            return this;
        }

        public Builder goals(List<Goal> val){
            goals = val;
            return this;
        }

        public Budget build() {
            return new Budget(this);
        }
    }
}
