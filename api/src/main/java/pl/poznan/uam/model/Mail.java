package pl.poznan.uam.model;

import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

@Getter
public class Mail {
    private String templateName;
    private String recipient;
    private String topic;
    private Map<String, Object> templateOptions = new HashMap<>();

    private Mail(Builder builder) {
        templateName = builder.templateName;
        recipient = builder.recipient;
        topic = builder.topic;
        templateOptions = builder.templateOptions;
    }

    public static final class Builder {
        private String templateName;
        private String recipient;
        private String topic;
        private Map<String, Object> templateOptions;

        public Builder() {
        }

        public Builder templateName(String templateName) {
            this.templateName = templateName;
            return this;
        }

        public Builder recipient(String recipient) {
            this.recipient = recipient;
            return this;
        }

        public Builder topic(String topic) {
            this.topic = topic;
            return this;
        }

        public Builder templateOptions(Map<String, Object> templateOptions) {
            this.templateOptions = templateOptions;
            return this;
        }

        public Mail build() {
            return new Mail(this);
        }
    }
}
