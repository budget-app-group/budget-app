package pl.poznan.uam.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Getter
@Entity
@NoArgsConstructor

public class BankAccount {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String name;
    @Enumerated(EnumType.STRING)
    private BankAccountBankName bankName;
    private String type;
    @Setter
    @NotNull
    private BigDecimal means;
    @NotNull
    private BigDecimal startingAmount;

    @ManyToOne
    private Budget budget;

    @OneToMany(mappedBy = "bankAccount", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Expense> expense = new ArrayList<>();

    private BankAccount(Builder builder) {
        id = builder.id;
        name = builder.name;
        bankName = builder.bankName;
        type = builder.type;
        setMeans(builder.means);
        startingAmount = builder.startingAmount;
        budget = builder.budget;
        expense = builder.expense;
    }

    public void addExpense(Expense expense){
        this.expense.add(expense);
    }
    public void removeExpense(Expense expense){
        this.expense.remove(expense);
    }


    public static final class Builder {
        private long id;
        private String name;
        private BankAccountBankName bankName;
        private String type;
        private BigDecimal means;
        private BigDecimal startingAmount;
        private Budget budget;
        private List<Expense> expense = new ArrayList<>();

        public Builder() {
        }

        public Builder id(long val) {
            id = val;
            return this;
        }

        public Builder name(String val) {
            name = val;
            return this;
        }

        public Builder bankName(BankAccountBankName val) {
            bankName = val;
            return this;
        }

        public Builder type(String val) {
            type = val;
            return this;
        }

        public Builder means(BigDecimal val) {
            means = val;
            return this;
        }

        public Builder startingAmount(BigDecimal val) {
            startingAmount = val;
            return this;
        }

        public Builder budget(Budget val) {
            budget = val;
            return this;
        }

        public Builder expense(List<Expense> val) {
            expense = val;
            return this;
        }

        public BankAccount build() {
            return new BankAccount(this);
        }
    }
}
