package pl.poznan.uam.security;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import pl.poznan.uam.model.UserAccount;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class MyUserPrincipal implements UserDetails {
    private Long id;
    private String email;
    private String name;
    private String surname;
    //@JsonIgnore
    private String password;
    private Collection<? extends GrantedAuthority> authorities;

    public MyUserPrincipal(Long id, String name, String surname, String email, String password, Collection<? extends GrantedAuthority> authorities) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.password = password;
        this.authorities = authorities;
    }


    public static MyUserPrincipal create(UserAccount user){
        final List<GrantedAuthority> authorities = Collections.singletonList(new SimpleGrantedAuthority("User"));
        return new MyUserPrincipal(user.getId(),
                                    user.getName(),
                                    user.getSurname(),
                                    user.getEmail(),
                                    user.getPassword(),
                                    authorities);
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        //final List<GrantedAuthority> authorities = Collections.singletonList(new SimpleGrantedAuthority("User"));
        return authorities;
    }

    public String getUsername() { //In our application email is an username
        return email;
    }

    @Override
    public String getPassword() {
        return password;
    }

    public Long getId() {
        return id;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
//TODO add equals and hashCode methods