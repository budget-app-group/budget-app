package pl.poznan.uam.security;

import lombok.Data;

@Data
public class JwtAuthenticationResponse {

    private String accessToken;
    private String tokenType = "Bearer";

    public  JwtAuthenticationResponse(String accessToken){
        this.accessToken = accessToken;
    }

}
