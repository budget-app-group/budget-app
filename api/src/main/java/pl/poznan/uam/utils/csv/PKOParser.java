package pl.poznan.uam.utils.csv;

import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import org.springframework.web.multipart.MultipartFile;
import pl.poznan.uam.dto.ExpenseDto;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static pl.poznan.uam.utils.csv.CSVUtils.getData;
import static pl.poznan.uam.utils.csv.CSVUtils.isIncome;

public class PKOParser {

    private static String getItemName(String[] line) {
        if (line[6].contains("Tytuł: ")) {
            return line[7].substring(line[7].lastIndexOf("Adres: ") + "Adres: ".length());
        } else if (line[8].contains("Tytuł: ")) {
            return line[8].substring(line[8].lastIndexOf("Tytuł: ") + "Tytuł: ".length());
        } else if (line[9].contains("Tytuł: ")) {
            return line[9].substring(line[9].lastIndexOf("Tytuł: ") + "Tytuł: ".length());
        } else {
            return "";
        }
    }

    public static List<ExpenseDto.CSVDto> parsePKO(MultipartFile fileToParse) throws IOException {
        CSVParser parser = new CSVParserBuilder()
                .withSeparator(',')
                .withIgnoreQuotations(true)
                .build();

        CSVReader csvReader = new CSVReaderBuilder(new BufferedReader(new InputStreamReader(fileToParse.getInputStream(), "CP1250")))
                .withSkipLines(1)
                .withCSVParser(parser)
                .build();

        String[] line;
        List<ExpenseDto.CSVDto> expenseList = new ArrayList<>();
        while ((line = csvReader.readNext()) != null) {

            ExpenseDto.CSVDto expense = ExpenseDto.CSVDto.builder()
                    .expenseDate(getData(line[0], "yyyy-MM-dd"))
                    .amount(new BigDecimal(line[3].substring(1)))
                    .income(isIncome(line[3]))
                    .name(getItemName(line))
                    .build();
            expenseList.add(expense);
        }

        csvReader.close();
        return expenseList;
    }
}
