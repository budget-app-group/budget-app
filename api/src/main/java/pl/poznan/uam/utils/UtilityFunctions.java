package pl.poznan.uam.utils;

import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import pl.poznan.uam.model.UserAccount;
import pl.poznan.uam.repository.UserRepository;

import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

import static pl.poznan.uam.utils.ExceptionEnums.USER_NOT_FOUND;

public class UtilityFunctions {

    public static String getLoggedUsername(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            return authentication.getName();
        }else{
            return null;
        }
    }

    public static UserAccount getLoggedUser(UserRepository userRepository){
        String userEmail  = Optional.ofNullable(getLoggedUsername()).orElseThrow(()->new ResponseStatusException(HttpStatus.NOT_FOUND, USER_NOT_FOUND.getMessage()));
        return userRepository.findByEmailIgnoreCase(userEmail).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, USER_NOT_FOUND.getMessage()));
    }

    public static Long getLoggedUserId() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            return ((UserAccount) principal).getId();
//            return authentication.getName();
        } else {
            return null;
        }
    }

    public static LocalDateTime convertStringToLocalDateTime(String stringDate){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate dateLd = LocalDate.parse(stringDate, formatter);
        LocalDateTime dateFrom = LocalDateTime.of(dateLd, LocalTime.of(0,0));
        return dateFrom;
    }

    public static File multipartToFile(MultipartFile multipart) throws IllegalStateException, IOException {
        File convFile = new File(multipart.getOriginalFilename());
        multipart.transferTo(convFile);
        return convFile;
    }

}
