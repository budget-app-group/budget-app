package pl.poznan.uam.utils.csv;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

public class CSVUtils {

    public static long getData(String date, String format) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
        return LocalDate.parse(date, formatter).atStartOfDay().atZone(ZoneId.of("Europe/Berlin")).toInstant().toEpochMilli();
    }

    public static boolean isIncome(String amount) {
        if (amount.contains("-")) {
            return false;
        } else {
            return true;
        }
    }
}
