package pl.poznan.uam.utils.ocr;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.poznan.uam.dto.ExpenseDto;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeParseException;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class OCRParser {

    private static final String PRICE = "(PLN \\d+(\\.|\\,) *\\d+)|(\\d+(\\.|\\,) *\\d+ PLN)|(PLN\\n\\d+(\\.|\\,) *\\d+)|(\\d+(\\.|\\,) *\\d+\\nPLN)";
    private static final String DATE = "(\\d\\d\\d\\d(-| )\\d\\d(-| )\\d\\d)|(\\d\\d(-| )\\d\\d(-| )\\d\\d\\d\\d)";
    private static final String TIME = "(\\d\\d:\\d\\d)";
    private static final String PARSE_ERROR = "ocrParseError";
    private static final Logger logger = LoggerFactory.getLogger(OCRParser.class);


    private String getFromOcr(String dataToParse, String regex) {
        Pattern pricePattern = Pattern.compile(regex);
        Matcher matcher = pricePattern.matcher(dataToParse);

        if (matcher.find()) {
            logger.info(matcher.group());
            return matcher.group();
        } else {
            logger.info(PARSE_ERROR);
            return PARSE_ERROR;
        }
    }

    private long parseDate(String ocrDate, String ocrTime) {
        String yearRegex = "\\d\\d\\d\\d";
        String monthRegex = "(-| |\\.)\\d\\d(-| |\\.)";
        String dayRegex = "(^\\d\\d(-| |\\.))|((-| |\\.)\\d\\d$)";

        int year = 1970;
        int month = 1;
        int day = 1;

        List<String> patternList = Arrays.asList(yearRegex, monthRegex, dayRegex);

        for (String regexPattern : patternList) {
            Pattern pattern = Pattern.compile(regexPattern);
            Matcher matcher = pattern.matcher(ocrDate);

            if (matcher.find()) {
                String result = matcher.group().replaceAll("[^\\d\\.]| \\.|\\.$", "");
                if (regexPattern.equals(yearRegex)) year = Integer.parseInt(result);
                if (regexPattern.equals(monthRegex)) month = Integer.parseInt(result);
                if (regexPattern.equals(dayRegex)) day = Integer.parseInt(result);
            }
        }

        LocalDate date = LocalDate.of(year, month, day);
        LocalTime time;
        try {
            time = LocalTime.parse(ocrTime);

        } catch (DateTimeParseException e) {
            time = LocalTime.of(0, 0);
        }

        return LocalDateTime.of(date, time).atZone(ZoneId.of("Europe/Berlin")).toInstant().toEpochMilli();
    }

    public ExpenseDto.OCRPostDto parseOCR(String dataToParse) {

        ExpenseDto.OCRPostDto.OCRPostDtoBuilder builder = ExpenseDto.OCRPostDto.builder();
        try {
            builder.amount(new BigDecimal(getFromOcr(dataToParse, PRICE)
                    .replaceAll("[ \\nPLN]", "")
                    .replaceAll(",", ".")));
        } catch (NumberFormatException e) {
            logger.error(PARSE_ERROR, e);
            builder.amount(new BigDecimal(0));
        }

        builder.expenseDate(parseDate(getFromOcr(dataToParse, DATE), getFromOcr(dataToParse, TIME)));

        for (String split : dataToParse.split("\\r?\\n")) {
            if (split.length() > 3) {
                builder.name(split);
                builder.shopName(split);
                break;
            } else {
                builder.name(PARSE_ERROR);
                builder.shopName(PARSE_ERROR);
            }
        }

        return builder.build();
    }
}
