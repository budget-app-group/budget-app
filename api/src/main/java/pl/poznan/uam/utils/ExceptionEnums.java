package pl.poznan.uam.utils;


public enum ExceptionEnums {
    USER_NOT_FOUND("User not found"),
    BUDGET_NOT_FOUND("Budget not found"),
    BANK_ACCOUNT_NOT_FOUND("Bank account not found"),
    NOT_YOUR_ACCOUNT("Thats not your account."),
    NOT_YOUR_BUDGET("That's not your budget."),
    MASTER_CAT_NOT_FOUND("Budget category not found"),
    EMAIL_EXISTS("Account with given email already exists"),
    ACCOUNT_NOT_VERIFIED("Account is not verified"),
    ACCOUNT_VERIFIED("Account is already verified"),
    TOKEN_NOT_FOUND("Token not found"),
    EMAIL_VERIFIED("Email adress has been verified"),
    EXPENSE_NOT_FOUND("Expense not found"),
    TOKEN_EXPIRED("Verification Token expired"),
    NO_ADMIN_PERMISSION("Account does not have admin permissions for that budget"),
    ADMIN_CANNOT_LEAVE("You need to pass admin permissions to other user before leaving!"),
    CATEGORY_NOT_FOUND("Category not found.");


    public String getMessage() {
        return message;
    }

    private final String message;

    ExceptionEnums(final String message) {
        this.message = message;
    }

}
