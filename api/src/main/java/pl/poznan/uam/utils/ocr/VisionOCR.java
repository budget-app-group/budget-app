package pl.poznan.uam.utils.ocr;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.vision.v1.Vision;
import com.google.api.services.vision.v1.VisionScopes;
import com.google.api.services.vision.v1.model.*;
import com.google.common.collect.ImmutableList;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Collections;

public class VisionOCR {

    private static final String APPLICATION_NAME = "Google-VisionTextSample/1.0";
    private final Vision vision;


    public VisionOCR(Vision vision) {
        this.vision = vision;
    }

    public static Vision getVisionService() throws IOException, GeneralSecurityException {
        Resource resource = new ClassPathResource("GVisionCreds.json");
        GoogleCredential credential =
                GoogleCredential.fromStream(resource.getInputStream()).createScoped(VisionScopes.all());
        JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();
        return new Vision.Builder(GoogleNetHttpTransport.newTrustedTransport(), jsonFactory, credential)
                .setApplicationName(APPLICATION_NAME)
                .build();
    }

    public String detectText(byte[] byteArray) {

        try {
            AnnotateImageRequest request = new AnnotateImageRequest();
            request.setImage(new Image().encodeContent(byteArray))
                    .setFeatures(ImmutableList.of(
                            new Feature()
                                    .setType("TEXT_DETECTION")));

            Vision.Images.Annotate annotate =
                    vision.images()
                            .annotate(new BatchAnnotateImagesRequest()
                                    .setRequests(Collections.singletonList(request)));

            BatchAnnotateImagesResponse batchResponse = annotate.execute();
//            List<AnnotateImageResponse> responses = batchResponse.getResponses();
//            AnnotateImageResponse annotateImageResponse = responses.get(0);
//            List<EntityAnnotation> textAnnotations = annotateImageResponse.getTextAnnotations();
//            EntityAnnotation entityAnnotation = textAnnotations.get(0);
//            String description = entityAnnotation.getDescription();


            return batchResponse.getResponses().get(0).getTextAnnotations().get(0).getDescription();


        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        } catch (NullPointerException npe) {
            npe.printStackTrace();
            return "ocrParseError";
        }

    }
}
