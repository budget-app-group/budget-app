package pl.poznan.uam.utils.csv;

import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import org.springframework.web.multipart.MultipartFile;
import pl.poznan.uam.dto.ExpenseDto;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static pl.poznan.uam.utils.csv.CSVUtils.getData;
import static pl.poznan.uam.utils.csv.CSVUtils.isIncome;

public class INGParser {

    private static String getItemName(String[] line) {
        Matcher matcher = Pattern.compile("\\d+xx\\d+|BLIK").matcher(line[3]);

        if (line[3].contains("Smart Saver")) {
            return "Smart Saver";
        } else if (matcher.find()) {
            return line[2];
        } else {
            return line[3];
        }
    }

    public static List<ExpenseDto.CSVDto> parseING(MultipartFile fileToParse) throws IOException {

        CSVParser parser = new CSVParserBuilder()
                .withSeparator(';')
                .withIgnoreQuotations(true)
                .build();

        CSVReader csvReader = new CSVReaderBuilder(new BufferedReader(new InputStreamReader(fileToParse.getInputStream(), "CP1250")))
                .withSkipLines(21)
                .withCSVParser(parser)
                .build();

        String[] line;
        List<ExpenseDto.CSVDto> expenseList = new ArrayList<>();
        while ((line = csvReader.readNext()) != null && !line[0].isEmpty()) {
            ExpenseDto.CSVDto expense = ExpenseDto.CSVDto.builder()
                    .expenseDate(getData(line[0], "yyyy-MM-dd"))
                    .amount(new BigDecimal((line[8].replace("-", "")).replace(',', '.')))
                    .income(isIncome(line[8]))
                    .name(getItemName(line))
                    .build();
            expenseList.add(expense);
        }

        csvReader.close();
        return expenseList;
    }
}
