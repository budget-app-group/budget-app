package pl.poznan.uam.utils.csv;

import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import org.springframework.web.multipart.MultipartFile;
import pl.poznan.uam.dto.ExpenseDto;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static pl.poznan.uam.utils.csv.CSVUtils.getData;
import static pl.poznan.uam.utils.csv.CSVUtils.isIncome;

public class SantanderParser {

    private static String getItemName(String line) {
        Matcher matcher = Pattern.compile("VISA \\d+\\*+\\d+").matcher(line);
        if (matcher.find()) {
            line = line.replaceAll(".*\\d+.\\d+ [A-Z]{3} ", "");
            return line;
        } else {
            return line;
        }
    }

    public static List<ExpenseDto.CSVDto> parseSANTANDER(MultipartFile fileToParse) throws IOException {
        CSVParser parser = new CSVParserBuilder()
                .withSeparator(';')
                .withIgnoreQuotations(true)
                .build();

        CSVReader csvReader = new CSVReaderBuilder(new BufferedReader(new InputStreamReader(fileToParse.getInputStream(), "CP1250")))
                .withSkipLines(1)
                .withCSVParser(parser)
                .build();

        String[] line;
        List<ExpenseDto.CSVDto> expenseList = new ArrayList<>();
        while ((line = csvReader.readNext()) != null) {

            ExpenseDto.CSVDto expense = ExpenseDto.CSVDto.builder()
                    .expenseDate(getData(line[1], "dd-MM-yyyy"))
                    .income(isIncome(line[5]))
                    .amount(new BigDecimal((line[5].replace(",", ".")).replace("-", "")))
                    .name(getItemName(line[2]))
                    .build();
            expenseList.add(expense);
        }

        csvReader.close();
        return expenseList;
    }
}
