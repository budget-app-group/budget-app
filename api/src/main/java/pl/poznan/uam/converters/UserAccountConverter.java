package pl.poznan.uam.converters;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import pl.poznan.uam.dto.UserAccountDto;
import pl.poznan.uam.model.UserAccount;

import java.util.List;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class UserAccountConverter {

    private final PasswordEncoder passwordEncoder;


    public UserAccountDto.FullDto toFullDTO(UserAccount userAccount) {
        return UserAccountDto.FullDto.builder()
                .id(userAccount.getId())
                .email(userAccount.getEmail())
                .name(userAccount.getName())
                .surname(userAccount.getSurname())
                .isEnabled(userAccount.isEnabled())
                .build();
    }

    public UserAccountDto.MinimalDto toMinimalDTO(UserAccount userAccount) {
        return UserAccountDto.MinimalDto.builder()
                .email(userAccount.getEmail())
                .name(userAccount.getName())
                .surname(userAccount.getSurname())
                .build();
    }

    public UserAccount updateUserAccount(UserAccount userAccount, UserAccountDto.MinimalDto minimalDto) {
        return new UserAccount.Builder()
                .id(userAccount.getId())
                .email(minimalDto.getEmail())
                .name(minimalDto.getName())
                .surname(minimalDto.getSurname())
                .isEnabled(userAccount.isEnabled())
                .budgets(userAccount.getBudgets())
                .expenses(userAccount.getExpenses())
                .password(userAccount.getPassword())
                .build();
    }

    public UserAccount updateUserPassword(UserAccount userAccount, UserAccountDto.PasswordDto passwordDto) {
        return new UserAccount.Builder()
                .id(userAccount.getId())
                .email(userAccount.getEmail())
                .name(userAccount.getName())
                .surname(userAccount.getSurname())
                .isEnabled(userAccount.isEnabled())
                .budgets(userAccount.getBudgets())
                .expenses(userAccount.getExpenses())
                .password(passwordEncoder.encode(passwordDto.getPassword()))
                .build();
    }

    public List<UserAccountDto.FullDto> listToFullDTO(List<UserAccount> usersList) {
        return usersList.stream()
                .map(this::toFullDTO)
                .collect(Collectors.toList());
    }

}
