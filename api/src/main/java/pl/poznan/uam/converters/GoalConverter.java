package pl.poznan.uam.converters;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.poznan.uam.dto.ExpenseDto;
import pl.poznan.uam.dto.GoalDto;
import pl.poznan.uam.model.Budget;
import pl.poznan.uam.model.Goal;

import java.time.ZoneId;
import java.util.List;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class GoalConverter {
    private final BudgetConverter budgetConverter;

    public GoalDto.FullDto toFullDto(Budget budget, Goal goal) {

        return GoalDto.FullDto.builder()
                .id(goal.getId())
                .name(goal.getName())
                .collectedAmount(goal.getCollectedAmount())
                .targetAmount(goal.getTargetAmount())
                .targetDate(goal.getTargetDate().atZone(ZoneId.of("Europe/Berlin")).toInstant().toEpochMilli())
                .budgetData(budgetConverter.toModifyDto(budget))
                .build();
    }

    public GoalDto.PostDto toPostDto(Goal goal) {

        return GoalDto.PostDto.builder()
                .id(goal.getId())
                .name(goal.getName())
                .collectedAmount(goal.getCollectedAmount())
                .targetAmount(goal.getTargetAmount())
                .targetDate(goal.getTargetDate().atZone(ZoneId.of("Europe/Berlin")).toInstant().toEpochMilli())
                .build();
    }

    public GoalDto.GoalDonationDto toGoalDonationDto(Goal goal, List<ExpenseDto.FullDto> donations) {
        return GoalDto.GoalDonationDto.builder()
                .name(goal.getName())
                .collectedAmount(goal.getCollectedAmount())
                .targetAmount(goal.getTargetAmount())
                .donations(donations)
                .build();
    }
}
