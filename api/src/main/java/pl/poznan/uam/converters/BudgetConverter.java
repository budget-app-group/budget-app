package pl.poznan.uam.converters;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.poznan.uam.dto.BudgetDto;
import pl.poznan.uam.dto.UserAccountDto;
import pl.poznan.uam.model.Budget;
import pl.poznan.uam.model.UserAccount;

import java.math.BigDecimal;
import java.util.List;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))

public class BudgetConverter {

    private final UserAccountConverter userAccountConverter;

    public BudgetDto.FullDto toFullDto(Budget budget, UserAccount userAccount, List<UserAccountDto.FullDto> users) {
        return BudgetDto.FullDto.builder()
                .id(budget.getId())
                .name(budget.getName())
                .admin(userAccountConverter.toFullDTO(userAccount))
                .users(users)
                .build();
    }

    public BudgetDto.PostDto toPostDto(Budget budget, UserAccount userAccount) {
        return BudgetDto.PostDto.builder()
                .id(budget.getId())
                .name(budget.getName())
                .admin(userAccountConverter.toFullDTO(userAccount))
                .build();
    }

    public BudgetDto.MinimalDto toMinimalDto(Budget budget) {
        return BudgetDto.MinimalDto.builder()
                .name(budget.getName())
                .build();
    }

    public BudgetDto.ModifyDto toModifyDto(Budget budget) {
        return BudgetDto.ModifyDto.builder()
                .name(budget.getName())
                .admin(budget.getAdmin())
                .build();
    }

    public Budget updateBudget(Budget budget, BudgetDto.ModifyDto budgetDTO) {
        return new Budget.Builder()
                .id(budget.getId())
                .admin(budgetDTO.getAdmin())
                .name(budgetDTO.getName())
                .bankAccounts(budget.getBankAccounts())
                .masterCategories(budget.getMasterCategories())
                .userAccounts(budget.getUserAccounts())
                .expenses(budget.getExpenses())
                .goals(budget.getGoals())
                .build();
    }

    public BudgetDto.BalanceDto toBalanceDto(String name, BigDecimal sumOfExpenses, BigDecimal sumOfIncomes, BigDecimal balance){
        return BudgetDto.BalanceDto.builder()
                .name(name)
                .sumOfExpenses(sumOfExpenses)
                .sumOfIncomes(sumOfIncomes)
                .balance(balance)
                .build();
    }

    public BudgetDto.AverageExpenditureDto toAverageExpenditureDto(BigDecimal dailyAverage, BigDecimal weeklyAverage, BigDecimal monthlyAverage){
        return BudgetDto.AverageExpenditureDto.builder()
                .dailyAverage(dailyAverage)
                .weeklyAverage(weeklyAverage)
                .monthlyAverage(monthlyAverage)
                .build();
    }

}
