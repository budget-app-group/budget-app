package pl.poznan.uam.converters;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.poznan.uam.dto.CategoryDto;
import pl.poznan.uam.model.Budget;
import pl.poznan.uam.model.Category;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CategoryConverter {

    private final MasterCategoryConverter masterCategoryConverter;
    private final BudgetConverter budgetConverter;


    public CategoryDto.FullDto toFullDto(Budget budget, Category category) {
        return CategoryDto.FullDto.builder()
                .id(category.getId())
                .name(category.getName())
                .color(Optional.ofNullable(category.getColor()).orElse("#919191"))
                .budgetCategory(masterCategoryConverter.toFullDtoWithoutBudgetInfo(category.getMasterCategory()))
                .budgetData(budgetConverter.toModifyDto(budget))
                .build();
    }

    public CategoryDto.FullDtoWithoutBudgetInfo toFullDtoWithoutBudgetInfo(Category category) {
        return CategoryDto.FullDtoWithoutBudgetInfo.builder()
                .id(category.getId())
                .name(category.getName())
                .color(Optional.ofNullable(category.getColor()).orElse("#919191"))
                .budgetCategory(masterCategoryConverter.toFullDtoWithoutBudgetInfo(category.getMasterCategory()))
                .build();
    }

    public CategoryDto.MinimalDto toMinimalDto(Category category) {
        return CategoryDto.MinimalDto.builder()
                .id(category.getId())
                .name(category.getName())
                .color(Optional.ofNullable(category.getColor()).orElse("#919191"))
                .build();
    }

    public Category updateCategory(Category category, CategoryDto.MinimalDto categoryDTO) {
        return new Category.Builder()
                .id(category.getId())
                .name(categoryDTO.getName())
                .color(Optional.ofNullable(categoryDTO.getColor()).orElse("#919191"))
                .masterCategory(category.getMasterCategory())
                .expenses(category.getExpenses())
                .build();
    }

    public CategoryDto.AnalysisCategoryDto toAnalysisCategoryDto(Category category, BigDecimal sum){
        return CategoryDto.AnalysisCategoryDto.builder()
                .id(category.getId())
                .name(category.getName())
                .color(Optional.ofNullable(category.getColor()).orElse("#919191"))
                .sumOfExpenses(sum)
                .build();
    }

    public List<CategoryDto.FullDto> listToFullDTO(Budget budget, List<Category> categoryList) {
        return categoryList.stream()
                .map(cat -> toFullDto(budget, cat))
                .collect(Collectors.toList());
    }

}
