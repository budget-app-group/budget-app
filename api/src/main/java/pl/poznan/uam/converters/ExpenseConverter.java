package pl.poznan.uam.converters;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.poznan.uam.dto.ExpenseDto;
import pl.poznan.uam.model.Expense;

import java.time.ZoneId;
import java.util.List;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ExpenseConverter {
    private final BankAccountConverter bankAccountConverter;
    private final CategoryConverter categoryConverter;
    private final UserAccountConverter userAccountConverter;
    private final BudgetConverter budgetConverter;

    public ExpenseDto.FullDto toFullDTO(Expense expense) {
        return ExpenseDto.FullDto.builder()
                .id(expense.getId())
                .amount(expense.getAmount())
                .name(expense.getItemName())
                .expenseDate(expense.getExpenseDate().atZone(ZoneId.of("Europe/Berlin")).toInstant().toEpochMilli())
                .bankAccount(bankAccountConverter.toBankNameDTO(expense.getBankAccount()))
                .category(categoryConverter.toFullDtoWithoutBudgetInfo(expense.getCategory()))
                .shopName(expense.getShopName())
                .income(expense.isIncome())
                .budgetData(budgetConverter.toModifyDto(expense.getBudget()))
                .expenseOwner(userAccountConverter.toFullDTO(expense.getUserAccount()))
                .build();
    }

    public ExpenseDto.PutDto toPutDTO(Expense expense, long categoryId, long mastCatId, long budgetId, long bankAccountId) {
        return ExpenseDto.PutDto.builder()
                .amount(expense.getAmount())
                .name(expense.getItemName())
                .expenseDate(expense.getExpenseDate().atZone(ZoneId.of("Europe/Berlin")).toInstant().toEpochMilli())
                .bankAccountId(bankAccountId)
                .shopName(expense.getShopName())
                .categoryId(categoryId)
                .mastCatId(mastCatId)
                .income(expense.isIncome())
                .build();
    }


    public ExpenseDto.PostDto toPostDto(Expense expense) {
        return ExpenseDto.PostDto.builder()
                .id(expense.getId())
                .amount(expense.getAmount())
                .name(expense.getItemName())
                .expenseDate(expense.getExpenseDate().atZone(ZoneId.of("Europe/Berlin")).toInstant().toEpochMilli())
                .shopName(expense.getShopName())
                .income(expense.isIncome())
                .bankAccountId(expense.getBankAccount().getId())
                .build();
    }

    public ExpenseDto.MinimalDto toMinimalDTO(Expense expense) {
        return ExpenseDto.MinimalDto.builder()
                .amount(expense.getAmount())
                .name(expense.getItemName())
                .shopName(expense.getShopName())
                .expenseDate(expense.getExpenseDate().atZone(ZoneId.of("Europe/Berlin")).toInstant().toEpochMilli())
                .income(expense.isIncome())
                .build();
    }

    public List<ExpenseDto.FullDto> listToFullDTO(List<Expense> expenseList) {
        return expenseList.stream()
                .map(this::toFullDTO)
                .collect(Collectors.toList());
    }

    public List<ExpenseDto.MinimalDto> listToMinimalDTO(List<Expense> expenseList) {
        return expenseList.stream()
                .map(this::toMinimalDTO)
                .collect(Collectors.toList());
    }

    public ExpenseDto.OCRPostDto toOcrPostDto (Expense expense) {
        return ExpenseDto.OCRPostDto.builder()
                .name(expense.getItemName())
                .amount(expense.getAmount())
                .shopName(expense.getShopName())
                .expenseDate(expense.getExpenseDate().atZone(ZoneId.of("Europe/Berlin")).toInstant().toEpochMilli())
                .build();
    }
}


