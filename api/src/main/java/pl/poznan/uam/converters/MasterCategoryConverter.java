package pl.poznan.uam.converters;


import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.poznan.uam.dto.CategoryDto;
import pl.poznan.uam.dto.MasterCategoryDto;
import pl.poznan.uam.model.Budget;
import pl.poznan.uam.model.MasterCategory;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class MasterCategoryConverter {
    private final BudgetConverter budgetConverter;

    public MasterCategoryDto.FullDto toFullDto(Budget budget, MasterCategory masterCategory) {
        return MasterCategoryDto.FullDto.builder()
                .id(masterCategory.getId())
                .name(masterCategory.getName())
                .budgetAllocation(masterCategory.getBudgetAllocation())
                .color(Optional.ofNullable(masterCategory.getColor()).orElse("#919191"))
                .budgetData(budgetConverter.toModifyDto(budget))
                .build();
    }

    public MasterCategoryDto.FullDtoWithoutBudgetInfo toFullDtoWithoutBudgetInfo(MasterCategory masterCategory) {
        return MasterCategoryDto.FullDtoWithoutBudgetInfo.builder()
                .id(masterCategory.getId())
                .name(masterCategory.getName())
                .budgetAllocation(masterCategory.getBudgetAllocation())
                .color(Optional.ofNullable(masterCategory.getColor()).orElse("#919191"))
                .build();
    }

    public MasterCategoryDto.MinimalDto toMinimalDto(MasterCategory masterCategory) {
        return MasterCategoryDto.MinimalDto.builder()
                .id(masterCategory.getId())
                .name(masterCategory.getName())
                .budgetAllocation(masterCategory.getBudgetAllocation())
                .color(Optional.ofNullable(masterCategory.getColor()).orElse("#919191"))
                .build();
    }

    public MasterCategory updateMasterCategory(MasterCategory masterCategory, MasterCategoryDto.MinimalDto masterCatDTO) {
        return new MasterCategory.Builder()
                .id(masterCategory.getId())
                .name(masterCatDTO.getName())
                .budgetAllocation(masterCatDTO.getBudgetAllocation())
                .color(Optional.ofNullable(masterCatDTO.getColor()).orElse("#919191"))
                .budget(masterCategory.getBudget())
                .categories(masterCategory.getCategories())
                .build();
    }

    public MasterCategoryDto.AnalysisMasterCatDto toAnalysisMasterCatDto(MasterCategory masterCategory, BigDecimal sum, List<CategoryDto.AnalysisCategoryDto> categoryDtos){
        return MasterCategoryDto.AnalysisMasterCatDto.builder()
                .id(masterCategory.getId())
                .name(masterCategory.getName())
                .color(Optional.ofNullable(masterCategory.getColor()).orElse("#919191"))
                .budgetAllocation(masterCategory.getBudgetAllocation())
                .sumOfExpenses(sum)
                .categoryDtos(categoryDtos)
                .build();
    }

    public List<MasterCategoryDto.FullDto> listToFullDTO(Budget budget, List<MasterCategory> masterCategoryList) {
        return masterCategoryList.stream()
                .map(mcat -> toFullDto(budget, mcat))
                .collect(Collectors.toList());
    }

    public MasterCategoryDto.ExceedingMasterCatAllocationDto toExceedingMasterCatAllocationDto(String name, int budgetAllocation, int allocationAmount, int balance,int actualShare, boolean exceeded){
        return MasterCategoryDto.ExceedingMasterCatAllocationDto.builder()
                .name(name)
                .budgetAllocation(budgetAllocation)
                .allocationAmount(allocationAmount)
                .balance(balance)
                .expenseAmount(actualShare)
                .exceeded(exceeded)
                .build();
    }

}
