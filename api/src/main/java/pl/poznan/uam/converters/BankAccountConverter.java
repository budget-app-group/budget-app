package pl.poznan.uam.converters;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.poznan.uam.dto.BankAccountDto;
import pl.poznan.uam.model.BankAccount;
import pl.poznan.uam.model.Budget;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class BankAccountConverter {

    private final BudgetConverter budgetConverter;

    public BankAccountDto.FullDto toFullDTO(Budget budget, BankAccount bankAccount) {

        return BankAccountDto.FullDto.builder()
                .budgetData(budgetConverter.toModifyDto(budget))
                .id(bankAccount.getId())
                .name(bankAccount.getName())
                .bankName(bankAccount.getBankName())
                .type(bankAccount.getType())
                .means(bankAccount.getMeans())
                .startingAmount(bankAccount.getStartingAmount())
                .build();
    }

    public BankAccountDto.MinimalDto toMinimalDTO(BankAccount bankAccount) {
        return BankAccountDto.MinimalDto.builder()
                .id(bankAccount.getId())
                .name(bankAccount.getName())
                .bankName(bankAccount.getBankName())
                .type(bankAccount.getType())
                .startingAmount(bankAccount.getMeans())
                .build();
    }

    public BankAccountDto.BankNameDto toBankNameDTO(BankAccount bankAccount) {
        return BankAccountDto.BankNameDto.builder()
                .id(bankAccount.getId())
                .name(bankAccount.getName())
                .bankName(bankAccount.getBankName())
                .build();
    }

    public BankAccount updateBankAcc(BankAccount bankAccount, BankAccountDto.MinimalDto bankAccountDTO) {

        BigDecimal updatedMeans;

        if (bankAccountDTO.getStartingAmount().compareTo(bankAccount.getStartingAmount()) > 0) {
            updatedMeans = bankAccount.getMeans().add(bankAccountDTO.getStartingAmount().subtract(bankAccount.getStartingAmount()));
        } else {
            updatedMeans = bankAccount.getMeans().subtract(bankAccount.getStartingAmount().subtract(bankAccountDTO.getStartingAmount()));
        }


        return new BankAccount.Builder()
                .id(bankAccount.getId())
                .name(bankAccountDTO.getName())
                .bankName(bankAccountDTO.getBankName())
                .type(bankAccountDTO.getType())
                .startingAmount(bankAccountDTO.getStartingAmount())
                .means(updatedMeans)
                .expense(bankAccount.getExpense())
                .budget(bankAccount.getBudget())
                .build();
    }

    public List<BankAccountDto.FullDto> listToFullDTO(Budget budget, List<BankAccount> bankAccountList) {
        return bankAccountList.stream()
                .map(bAcc -> toFullDTO(budget, bAcc))
                .collect(Collectors.toList());
    }
}
