package pl.poznan.uam.converters;

import org.springframework.stereotype.Component;
import pl.poznan.uam.dto.PeriodicExpensesDto;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.ZoneId;

@Component
public class PeriodicExpensesConverter {

    public PeriodicExpensesDto.SumOfExpensesDto toDailyExpensesDto(LocalDateTime date, BigDecimal sumOfExpenses, String mode){
        switch (mode) {
            case "income":
                return PeriodicExpensesDto.SumOfExpensesDto.builder()
                        .date(date.atZone(ZoneId.of("Europe/Berlin")).toInstant().toEpochMilli())
                        .sumOfIncomes(sumOfExpenses)
                        .build();
            case "balance":
                return PeriodicExpensesDto.SumOfExpensesDto.builder()
                        .date(date.atZone(ZoneId.of("Europe/Berlin")).toInstant().toEpochMilli())
                        .balance(sumOfExpenses)
                        .build();
            default:
                return PeriodicExpensesDto.SumOfExpensesDto.builder()
                        .date(date.atZone(ZoneId.of("Europe/Berlin")).toInstant().toEpochMilli())
                        .sumOfExpenses(sumOfExpenses)
                        .build();
        }
    }
}

