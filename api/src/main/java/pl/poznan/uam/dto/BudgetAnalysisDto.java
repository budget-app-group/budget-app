package pl.poznan.uam.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;
import lombok.experimental.NonFinal;

import java.math.BigDecimal;

@Value
@Builder
@AllArgsConstructor
public class BudgetAnalysisDto {
        private long id;
        private String name;
        private int budgetAllocation;
    @NonFinal
    private BigDecimal sumOfExpenses;

    public void setSumOfExpenses(BigDecimal sumOfExpenses) {
        this.sumOfExpenses = sumOfExpenses.setScale(2, BigDecimal.ROUND_HALF_EVEN);
    }
}
