package pl.poznan.uam.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.Value;
import lombok.experimental.NonFinal;

import java.math.BigDecimal;


public class ExpenseDto {

    @Getter
    @Builder
    public static class FullDto {

        private long id;
        private BigDecimal amount;
        private String name;
        private long expenseDate;
        private BankAccountDto.BankNameDto bankAccount;
        private CategoryDto.FullDtoWithoutBudgetInfo category;
        private String shopName;
        private boolean income;
        private BudgetDto.ModifyDto budgetData;
        private UserAccountDto.FullDto expenseOwner;


        public void setAmount(BigDecimal amount) {
            this.amount = amount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
        }
    }

    @Value
    @Builder
    public static class PostDto {
        private long id;
        @NonFinal
        private BigDecimal amount;
        private String name;
        private long expenseDate;
        private String shopName;
        private long bankAccountId;
        private boolean income;
        private long goalId;

        public void setAmount(BigDecimal amount) {
            this.amount = amount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
        }
    }

    @Value
    @Builder
    public static class PutDto {
        @NonFinal
        private BigDecimal amount;
        private String name;
        private long expenseDate;
        private String shopName;
        private boolean income;
        private long categoryId;
        private long mastCatId;
        private long bankAccountId;

        public void setAmount(BigDecimal amount) {
            this.amount = amount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
        }
    }


    @Value
    @Builder
    public static class MinimalDto {
        @NonFinal
        private BigDecimal amount;
        private String name;
        private String shopName;
        private long expenseDate;
        private boolean income;

        public void setAmount(BigDecimal amount) {
            this.amount = amount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
        }
    }

    @Value
    @Builder
    public static class OCRPostDto {
        @NonFinal
        private BigDecimal amount;
        private String name;
        private long expenseDate;
        private String shopName;
        private BudgetDto.ModifyDto budgetData;
        private UserAccountDto.FullDto expenseOwner;

        public void setAmount(BigDecimal amount) {
            this.amount = amount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
        }
    }

    @Getter
    @Builder
    public static class CSVDto {
        private BigDecimal amount;
        private String name;
        private long expenseDate;
        private String shopName;
        @Setter
        private long bankAccountId;
        @Setter
        private long masterCatId;
        @Setter
        private long categoryId;
        private boolean income;

        public void setAmount(BigDecimal amount) {
            this.amount = amount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
        }
    }

}
