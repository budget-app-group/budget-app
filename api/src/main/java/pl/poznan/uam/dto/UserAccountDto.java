package pl.poznan.uam.dto;

import lombok.Builder;
import lombok.Value;

public class UserAccountDto {

    @Value
    @Builder
    public static class FullDto {
        private long id;
        private String email;
        private String name;
        private String surname;
        private boolean isEnabled;

    }

    @Value
    @Builder
    public static class MinimalDto {
        private String email;
        private String name;
        private String surname;

    }

    @Value
    @Builder
    public static class PasswordDto {
        private String password;
    }
}
