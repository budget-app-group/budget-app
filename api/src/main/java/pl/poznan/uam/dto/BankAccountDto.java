package pl.poznan.uam.dto;

import lombok.Builder;
import lombok.Value;
import lombok.experimental.NonFinal;
import pl.poznan.uam.model.BankAccountBankName;

import java.math.BigDecimal;

public class BankAccountDto {

    @Value
    @Builder
    public static class FullDto {
        private long id;
        private String name;
        private BankAccountBankName bankName;
        private String type;
        @NonFinal
        private BigDecimal startingAmount;
        @NonFinal
        private BigDecimal means;
        private BudgetDto.ModifyDto budgetData;

        public void setStartingAmount(BigDecimal startingAmount) {
            this.startingAmount = startingAmount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
        }

        public void setMeans(BigDecimal means) {
            this.means = means.setScale(2, BigDecimal.ROUND_HALF_EVEN);
        }

    }

    @Value
    @Builder
    public static class MinimalDto {
        private long id;
        private String name;
        private BankAccountBankName bankName;
        private String type;
        @NonFinal
        private BigDecimal startingAmount;

        public void setStartingAmount(BigDecimal startingAmount) {
            this.startingAmount = startingAmount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
        }

    }

    @Value
    @Builder
    public static class BankNameDto {
        private long id;
        private String name;
        private BankAccountBankName bankName;

    }

}
