package pl.poznan.uam.dto;

import lombok.Builder;
import lombok.Value;
import lombok.experimental.NonFinal;

import java.math.BigDecimal;
import java.util.List;

public class MasterCategoryDto {

    @Value
    @Builder
    public static class FullDto {
        private long id;
        private String name;
        private int budgetAllocation;
        private String color;
        private BudgetDto.ModifyDto budgetData;

    }

    @Value
    @Builder
    public static class FullDtoWithoutBudgetInfo {
        private long id;
        private String name;
        private int budgetAllocation;
        private String color;
    }

    @Value
    @Builder
    public static class MinimalDto {
        private long id;
        private String name;
        private int budgetAllocation;
        private String color;
    }

    @Value
    @Builder
    public static class AnalysisMasterCatDto {
        private long id;
        private String name;
        private int budgetAllocation;
        private String color;
        @NonFinal
        private BigDecimal sumOfExpenses;
        private List<CategoryDto.AnalysisCategoryDto> categoryDtos;

        public void setSumOfExpenses(BigDecimal sumOfExpenses) {
            this.sumOfExpenses = sumOfExpenses.setScale(2, BigDecimal.ROUND_HALF_EVEN);
        }
    }

    @Value
    @Builder
    public static class ExceedingMasterCatAllocationDto {
        private String name;
        private int budgetAllocation;
        private int allocationAmount;
        private int expenseAmount;
        private int balance;
        private boolean exceeded;
    }
}
