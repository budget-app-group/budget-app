package pl.poznan.uam.dto;

import lombok.Builder;
import lombok.Value;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Value
@Builder
public class RegisterDto {
    @NotBlank
    @Email
    private String email;

    @NotBlank
    @Size(min = 1, max = 30)
    private String name;

    @NotBlank
    @Size(min = 1, max = 30)
    private String surname;

    @NotBlank
    @Size(min = 7, max = 30)
    private String password;
}

//TODO add custom validation for email and password