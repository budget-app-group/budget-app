package pl.poznan.uam.dto;

import lombok.Builder;
import lombok.Value;

import javax.validation.constraints.NotBlank;

@Value
@Builder
public class LoginDto {

    @NotBlank
    private String email;
    private String password;

    @Value
    @Builder
    public static class EmailDto {
        private String email;
    }

    @Value
    @Builder
    public static class PasswordResetDto {
        private String password;
    }
}
