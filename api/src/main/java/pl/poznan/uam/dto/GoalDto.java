package pl.poznan.uam.dto;

import lombok.Builder;
import lombok.Getter;

import java.math.BigDecimal;
import java.util.List;

public class GoalDto {

    @Getter
    @Builder
    public static class FullDto {
        private long id;
        private String name;
        private long targetDate;
        private BigDecimal targetAmount;
        private BigDecimal collectedAmount;
        private BudgetDto.ModifyDto budgetData;

        public void setTargetAmount(BigDecimal targetAmount) {
            this.targetAmount = targetAmount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
        }

        public void setCollectedAmount(BigDecimal collectedAmount) {
            this.collectedAmount = collectedAmount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
        }
    }

    @Getter
    @Builder
    public static class GoalDonationDto {
        private String name;
        private BigDecimal targetAmount;
        private BigDecimal collectedAmount;
        private List<ExpenseDto.FullDto> donations;


        public void setTargetAmount(BigDecimal targetAmount) {
            this.targetAmount = targetAmount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
        }

        public void setCollectedAmount(BigDecimal collectedAmount) {
            this.collectedAmount = collectedAmount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
        }
    }

    @Getter
    @Builder
    public static class PostDto {
        private long id;
        private String name;
        private long targetDate;
        private BigDecimal targetAmount;
        private BigDecimal collectedAmount;

        public void setTargetAmount(BigDecimal targetAmount) {
            this.targetAmount = targetAmount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
        }

        public void setCollectedAmount(BigDecimal collectedAmount) {
            this.collectedAmount = collectedAmount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
        }
    }
}
