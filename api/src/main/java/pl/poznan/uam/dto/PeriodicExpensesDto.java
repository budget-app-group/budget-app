package pl.poznan.uam.dto;

import lombok.Builder;
import lombok.Getter;

import java.math.BigDecimal;

public class PeriodicExpensesDto {

    @Getter
    @Builder
    public static class SumOfExpensesDto {
        private long date;
        public BigDecimal sumOfExpenses;
        public BigDecimal sumOfIncomes;
        public BigDecimal balance;

        public void setSumOfExpenses(BigDecimal sumOfExpenses) {
            this.sumOfExpenses = sumOfExpenses.setScale(2, BigDecimal.ROUND_HALF_EVEN);
        }

        public void setSumOfIncomes(BigDecimal sumOfIncomes) {
            this.sumOfIncomes = sumOfIncomes.setScale(2, BigDecimal.ROUND_HALF_EVEN);
        }

        public void setBalance(BigDecimal balance) {
            this.balance = balance.setScale(2, BigDecimal.ROUND_HALF_EVEN);
        }
    }
}
