package pl.poznan.uam.dto;

import lombok.Builder;
import lombok.Value;
import lombok.experimental.NonFinal;

import java.math.BigDecimal;
import java.util.List;

public class BudgetDto {

    @Value
    @Builder
    public static class FullDto {
        private long id;
        private String name;
        private UserAccountDto.FullDto admin;
        private List<UserAccountDto.FullDto> users;
    }

    @Value
    @Builder
    public static class PostDto {
        private long id;
        private String name;
        private UserAccountDto.FullDto admin;

    }

    @Value
    @Builder
    public static class MinimalDto {
        private String name;

    }

    @Value
    @Builder
    public static class ModifyDto {
        private String name;
        private String admin;

    }

    @Value
    @Builder
    public static class BalanceDto {
        private String name;
        @NonFinal
        private BigDecimal sumOfExpenses;
        @NonFinal
        private BigDecimal sumOfIncomes;
        @NonFinal
        private BigDecimal balance;

        public void setSumOfExpenses(BigDecimal sumOfExpenses) {
            this.sumOfExpenses = sumOfExpenses.setScale(2, BigDecimal.ROUND_HALF_EVEN);
        }

        public void setSumOfIncomes(BigDecimal sumOfIncomes) {
            this.sumOfIncomes = sumOfIncomes.setScale(2, BigDecimal.ROUND_HALF_EVEN);
        }

        public void setBalance(BigDecimal balance) {
            this.balance = balance.setScale(2, BigDecimal.ROUND_HALF_EVEN);
        }
    }

    @Value
    @Builder
    public static class AverageExpenditureDto {
        @NonFinal
        private BigDecimal dailyAverage;
        @NonFinal
        private BigDecimal weeklyAverage;
        @NonFinal
        private BigDecimal monthlyAverage;

        public void setDailyAverage(BigDecimal dailyAverage) {
            this.dailyAverage = dailyAverage.setScale(2, BigDecimal.ROUND_HALF_EVEN);
        }

        public void setWeeklyAverage(BigDecimal weeklyAverage) {
            this.weeklyAverage = weeklyAverage.setScale(2, BigDecimal.ROUND_HALF_EVEN);
        }

        public void setMonthlyAverage(BigDecimal monthlyAverage) {
            this.monthlyAverage = monthlyAverage.setScale(2, BigDecimal.ROUND_HALF_EVEN);
        }
    }
}
