package pl.poznan.uam.dto;

import lombok.Builder;
import lombok.Value;
import lombok.experimental.NonFinal;

import java.math.BigDecimal;

public class CategoryDto {

    @Value
    @Builder
    public static class FullDto {
        private long id;
        private String name;
        private String color;
        private MasterCategoryDto.FullDtoWithoutBudgetInfo budgetCategory;
        private BudgetDto.ModifyDto budgetData;

    }

    @Value
    @Builder
    public static class FullDtoWithoutBudgetInfo {
        private long id;
        private String name;
        private String color;
        private MasterCategoryDto.FullDtoWithoutBudgetInfo budgetCategory;

    }

    @Value
    @Builder
    public static class MinimalDto {
        private long id;
        private String name;
        private String color;

    }

    @Value
    @Builder
    public static class AnalysisCategoryDto{
        private long id;
        private String name;
        private String color;
        @NonFinal
        private BigDecimal sumOfExpenses;

        public void setSumOfExpenses(BigDecimal sumOfExpenses) {
            this.sumOfExpenses = sumOfExpenses.setScale(2, BigDecimal.ROUND_HALF_EVEN);
        }
    }
}
