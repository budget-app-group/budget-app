package pl.poznan.uam.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.FORBIDDEN)
public class PermissionDeniedException extends RuntimeException {

    public PermissionDeniedException() {
        super("You dont have sufficient rights to do that.");
    }

    public PermissionDeniedException(String message) {
        super(message);
    }
}
