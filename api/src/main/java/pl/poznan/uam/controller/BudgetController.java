package pl.poznan.uam.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.poznan.uam.dto.BudgetDto;
import pl.poznan.uam.dto.UserAccountDto;
import pl.poznan.uam.service.BudgetService;

import java.util.List;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class BudgetController {

    private final BudgetService budgetService;

    @GetMapping("/budget/{budgetId}")
    public ResponseEntity<BudgetDto.FullDto> getBudgetById(@PathVariable long budgetId) {
        return new ResponseEntity<>(budgetService.findBudgetById(budgetId), HttpStatus.OK);
    }

    @GetMapping("/budget")
    public ResponseEntity<List<BudgetDto.FullDto>> getAllBudgetsOfUser() {
        return new ResponseEntity<>(budgetService.findAllBudgets(), HttpStatus.OK);
    }

    @PostMapping("/budget")
    public ResponseEntity<BudgetDto.PostDto> addBudget(@RequestBody BudgetDto.MinimalDto userGrpDTO) {
        return new ResponseEntity<>(budgetService.createBudget(userGrpDTO), HttpStatus.CREATED);
    }

    @PutMapping("/budget/{budgetId}")
    public ResponseEntity<BudgetDto.FullDto> modifyBudget(@PathVariable long budgetId,
                                                          @RequestBody BudgetDto.ModifyDto userGrpDTO) {
        return new ResponseEntity<>(budgetService.modifyBudget(budgetId, userGrpDTO), HttpStatus.OK);
    }

    @DeleteMapping("/budget/{budgetId}")
    public ResponseEntity<BudgetDto.MinimalDto> deleteBudget(@PathVariable long budgetId) {
        return new ResponseEntity<>(budgetService.deleteBudget(budgetId), HttpStatus.OK);
    }

    @PutMapping("/budget/{budgetId}/{userId}")
    public ResponseEntity<BudgetDto.MinimalDto> addUserToBudget(@PathVariable long budgetId, @PathVariable long userId) {
        return new ResponseEntity<>(budgetService.addUserToBudget(budgetId, userId), HttpStatus.OK);
    }

    @PutMapping("/budget/{budgetId}/leave")
    public ResponseEntity<BudgetDto.MinimalDto> deleteLoggedUserFromBudget(@PathVariable long budgetId) {
        return new ResponseEntity<>(budgetService.deleteLoggedUserFromBudget(budgetId), HttpStatus.OK);
    }

    @GetMapping("/budget/{budgetId}/users")
    public ResponseEntity<List<UserAccountDto.FullDto>> getAllUsersOfBudget(@PathVariable long budgetId) {
        return new ResponseEntity<>(budgetService.findAllUsersOfBudget(budgetId), HttpStatus.OK);
    }

    @PutMapping("/budget/{budgetId}/remove/{userId}")
    public ResponseEntity<UserAccountDto.MinimalDto> removeUserFromBudget(@PathVariable long budgetId,
                                                                          @PathVariable long userId) {
        return new ResponseEntity<>(budgetService.removeUserFromBudget(budgetId, userId), HttpStatus.OK);
    }

    @PutMapping("/budget/{budgetId}/email/{email}")
    public ResponseEntity<BudgetDto.MinimalDto> addUserToBudgetByEmail(@PathVariable long budgetId, @PathVariable String email) {
        return new ResponseEntity<>(budgetService.addUserToBudgetByEmail(budgetId, email), HttpStatus.OK);
    }

    @PutMapping("/budget/{budgetId}/admin/{adminId}")
    public ResponseEntity<UserAccountDto.FullDto> addUserToBudgetByEmail(@PathVariable long budgetId,
                                                                         @PathVariable long adminId) {
        return new ResponseEntity<>(budgetService.changeBudgetAdmin(budgetId, adminId), HttpStatus.OK);
    }
}
