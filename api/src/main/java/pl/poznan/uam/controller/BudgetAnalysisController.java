package pl.poznan.uam.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.poznan.uam.dto.BudgetAnalysisDto;
import pl.poznan.uam.dto.BudgetDto;
import pl.poznan.uam.dto.MasterCategoryDto;
import pl.poznan.uam.dto.PeriodicExpensesDto;
import pl.poznan.uam.service.BudgetAnalysisService;

import java.util.List;

@RestController
@RequestMapping("/api/budget")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class BudgetAnalysisController {

    private final BudgetAnalysisService budgetAnalysisService;

    @GetMapping("/{budgetId}/summaryOfExpenses")
    public ResponseEntity<List<MasterCategoryDto.AnalysisMasterCatDto>> getSummaryOfExpensesInMasterCatsOfBudget(@PathVariable long budgetId,
                                                                                                                   @RequestParam("from") long from,
                                                                                                                   @RequestParam("to") long to){
        return new ResponseEntity<>(budgetAnalysisService.summaryOfExpensesInMasterCatsOfBudget(budgetId, from, to), HttpStatus.OK);
    }

    @GetMapping("/{budgetId}/summaryOfExpenses2")
    public ResponseEntity<List<BudgetAnalysisDto>> getSummaryOfExpensesInMasterCatsOfBudget2(@PathVariable long budgetId,
                                                                                                          @RequestParam("from") long from,
                                                                                                          @RequestParam("to") long to){
        return new ResponseEntity<>(budgetAnalysisService.summaryOfExpensesInMasterCatsOfBudget2(budgetId, from, to), HttpStatus.OK);
    }

    @GetMapping("/{budgetId}/budgetBalance")
    public ResponseEntity<BudgetDto.BalanceDto> getBudgetBalance(@PathVariable long budgetId,
                                                                 @RequestParam("from") long from,
                                                                 @RequestParam("to") long to) {
        return new ResponseEntity<>(budgetAnalysisService.budgetBalance(budgetId, from, to), HttpStatus.OK);
    }

    @GetMapping("/{budgetId}/averageExpenditure")
    public ResponseEntity<BudgetDto.AverageExpenditureDto> getAverageExpenditure(@PathVariable long budgetId,
                                                                                 @RequestParam("from") long from,
                                                                                 @RequestParam("to") long to) {
        return new ResponseEntity<>(budgetAnalysisService.averageExpenditure(budgetId, from, to), HttpStatus.OK);
    }

    @GetMapping("/{budgetId}/exceedingMasterCatAllocation")
    public ResponseEntity<List<MasterCategoryDto.ExceedingMasterCatAllocationDto>> getExceedingMasterCatAllocation(@PathVariable long budgetId) {
        return new ResponseEntity<>(budgetAnalysisService.checkIfMasterCatAllocationExceeded(budgetId), HttpStatus.OK);
    }

    @GetMapping("/{budgetId}/periodicExpenses")
    public ResponseEntity<List<PeriodicExpensesDto.SumOfExpensesDto>> getPeriodicExpensesSum(@PathVariable long budgetId,
                                                                              @RequestParam("from") long from,
                                                                              @RequestParam("to") long to,
                                                                              @RequestParam("mode") String mode) {
        return new ResponseEntity<>(budgetAnalysisService.periodicExpensesSum(budgetId, from, to, mode), HttpStatus.OK);
    }
}
