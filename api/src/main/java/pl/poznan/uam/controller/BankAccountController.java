package pl.poznan.uam.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.poznan.uam.dto.BankAccountDto;
import pl.poznan.uam.service.BankAccountService;

import java.util.List;

@RestController
@RequestMapping("/api/budget")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class BankAccountController {

    private final BankAccountService bankAccountService;

    @GetMapping("/{budgetId}/bankaccount")
    public ResponseEntity<List<BankAccountDto.FullDto>> getAllBankAccounts(@PathVariable long budgetId) {
        return new ResponseEntity<>(bankAccountService.findAllBankAccounts(budgetId), HttpStatus.OK);
    }

    @GetMapping("/{budgetId}/bankaccount/{id}")
    public ResponseEntity<BankAccountDto.FullDto> getBankAccount(@PathVariable long budgetId, @PathVariable long id) {
        return new ResponseEntity<>(bankAccountService.findBankAccount(budgetId, id), HttpStatus.OK);
    }

    @PostMapping("/{budgetId}/bankaccount")
    public ResponseEntity<BankAccountDto.MinimalDto> addBankAccount(@PathVariable long budgetId, @RequestBody BankAccountDto.MinimalDto bankAccountShortDTO) {
        return new ResponseEntity<>(bankAccountService.createBankAccount(budgetId, bankAccountShortDTO), HttpStatus.CREATED);
    }

    @DeleteMapping("/{budgetId}/bankaccount/{id}")
    public ResponseEntity<BankAccountDto.MinimalDto> deleteBankAccount(@PathVariable long budgetId, @PathVariable long id) {
        return new ResponseEntity<>(bankAccountService.deleteBankAccount(budgetId, id), HttpStatus.OK);
    }

    @PutMapping("/{budgetId}/bankaccount/{id}")
    public ResponseEntity<BankAccountDto.MinimalDto> modifyBankAccount(@PathVariable long budgetId, @PathVariable long id, @RequestBody BankAccountDto.MinimalDto bankAccountShortDTO) {
        return new ResponseEntity<>(bankAccountService.modifyBankAccount(budgetId, id, bankAccountShortDTO), HttpStatus.OK);
    }
}
