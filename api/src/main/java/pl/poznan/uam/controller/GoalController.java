package pl.poznan.uam.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.poznan.uam.dto.GoalDto;
import pl.poznan.uam.service.GoalService;

import java.util.List;

@RestController
@RequestMapping("/api/budget")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class GoalController {

    private final GoalService goalService;

    @GetMapping("/{budgetId}/goal")
    public ResponseEntity<List<GoalDto.FullDto>> getAllGoalsFromBudget(@PathVariable long budgetId) {
        return new ResponseEntity<>(goalService.findAllGoalsFromBudget(budgetId), HttpStatus.OK);
    }

    @GetMapping("/{budgetId}/goal/{goalId}")
    public ResponseEntity<GoalDto.FullDto> getGoalById(@PathVariable long budgetId, @PathVariable long goalId) {
        return new ResponseEntity<>(goalService.findGoal(budgetId, goalId), HttpStatus.OK);
    }

    @GetMapping("/{budgetId}/goal/{goalId}/donations")
    public ResponseEntity<GoalDto.GoalDonationDto> getAllDonationsToGoal(@PathVariable long budgetId, @PathVariable long goalId) {
        return new ResponseEntity<>(goalService.findAllDonationsToGoal(budgetId, goalId), HttpStatus.OK);
    }

    @PostMapping("/{budgetId}/goal")
    public ResponseEntity<GoalDto.PostDto> addGoal(@PathVariable long budgetId,
                                                   @RequestBody GoalDto.PostDto goalDto) {
        return new ResponseEntity<>(goalService.createGoal(budgetId, goalDto), HttpStatus.CREATED);
    }

    @PutMapping("/{budgetId}/goal/{goalId}")
    public ResponseEntity<GoalDto.PostDto> modifyGoal(@PathVariable long budgetId,
                                                      @PathVariable long goalId,
                                                      @RequestBody GoalDto.PostDto goalDto) {
        return new ResponseEntity<>(goalService.modifyGoal(budgetId, goalId, goalDto), HttpStatus.OK);
    }

    @DeleteMapping("/{budgetId}/goal/{goalId}")
    public ResponseEntity<GoalDto.FullDto> deleteGoal(@PathVariable long budgetId,
                                                      @PathVariable long goalId) {
        return new ResponseEntity<>(goalService.deleteGoal(budgetId, goalId), HttpStatus.OK);
    }
}
