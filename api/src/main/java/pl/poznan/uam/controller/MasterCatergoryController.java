package pl.poznan.uam.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.poznan.uam.dto.MasterCategoryDto;
import pl.poznan.uam.service.MasterCategoryService;

import java.util.List;

@RestController
@RequestMapping("/api/budget")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class MasterCatergoryController {

    private final MasterCategoryService masterCategoryService;

    // single mastercategory
    @PostMapping("/{budgetId}/master")
    public ResponseEntity<MasterCategoryDto.MinimalDto> addMasterCategory(@PathVariable long budgetId, @RequestBody MasterCategoryDto.MinimalDto masterCatDTO) {
        return new ResponseEntity<>(masterCategoryService.createMasterCategory(budgetId, masterCatDTO), HttpStatus.CREATED);
    }
// multiple mastercat
//    @PostMapping("/{budgetId}/master")
//    public ResponseEntity<List<MasterCategoryDto.MinimalDto>> addMasterCategoryList(@PathVariable long budgetId, @RequestBody List<MasterCategoryDto.MinimalDto> masterCatListDTO) {
//        return new ResponseEntity<>(masterCategoryService.createMasterCategoriesFromList(budgetId, masterCatListDTO), HttpStatus.CREATED);
//    }

    @GetMapping("/{budgetId}/master")
    public ResponseEntity<List<MasterCategoryDto.FullDto>> getAllUserMasterCategories(@PathVariable long budgetId) {
        return new ResponseEntity<>(masterCategoryService.findAllUserMasterCategories(budgetId), HttpStatus.OK);
    }

    @GetMapping("/{budgetId}/master/{mCatId}")
    public ResponseEntity<MasterCategoryDto.FullDto> getAllUserMasterCategories(@PathVariable long budgetId,
                                                                                @PathVariable long mCatId) {
        return new ResponseEntity<>(masterCategoryService.findMasterCategoryById(budgetId, mCatId), HttpStatus.OK);
    }

    @PutMapping("/{budgetId}/master/{catId}")
    public ResponseEntity<MasterCategoryDto.MinimalDto> modifyMasterCategory(@PathVariable long budgetId,
                                                                             @PathVariable long catId,
                                                                             @RequestBody MasterCategoryDto.MinimalDto masterCatDTO) {
        return new ResponseEntity<>(masterCategoryService.modifyMasterCategory(budgetId, catId, masterCatDTO), HttpStatus.OK);
    }

    @DeleteMapping("/{budgetId}/master/{catId}")
    public ResponseEntity<MasterCategoryDto.MinimalDto> deleteMasterCategory(@PathVariable long budgetId,
                                                                             @PathVariable long catId) {
        return new ResponseEntity<>(masterCategoryService.deleteMasterCategory(budgetId, catId), HttpStatus.OK);
    }
}
