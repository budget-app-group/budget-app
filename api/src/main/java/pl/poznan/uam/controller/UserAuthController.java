package pl.poznan.uam.controller;

import freemarker.template.TemplateException;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.poznan.uam.dto.LoginDto;
import pl.poznan.uam.dto.RegisterDto;
import pl.poznan.uam.dto.UserAccountDto;
import pl.poznan.uam.service.BudgetService;
import pl.poznan.uam.service.UserAuthService;

import javax.mail.MessagingException;
import javax.validation.Valid;
import java.io.IOException;
import java.util.Map;

@RestController
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class UserAuthController {

    private final UserAuthService userAuthService;
    private final BudgetService budgetService;

    @PostMapping("/auth/register")
    public ResponseEntity<UserAccountDto.FullDto> registerUser(@Valid @RequestBody RegisterDto registerDto)
            throws MessagingException, IOException, TemplateException {
        return new ResponseEntity<>(userAuthService.registerUser(registerDto), HttpStatus.CREATED);
    }

    @PostMapping("/auth/login")
    public ResponseEntity<Map<String, Object>> authenticateUser(@Valid @RequestBody LoginDto loginDto) {
        return new ResponseEntity<>(userAuthService.authenticateUser(loginDto), HttpStatus.OK);
    }

    @PostMapping("/confirm-email")
    public ResponseEntity<?> confirmEmail(@RequestParam("token") String verificationToken){
        return userAuthService.confirmEmail(verificationToken);
    }

    @PostMapping("/reset-password")
    public ResponseEntity<UserAccountDto.MinimalDto> resetPassword(@RequestBody LoginDto.EmailDto loginDto) {
        return new ResponseEntity<>(userAuthService.resetPassword(loginDto), HttpStatus.OK);
    }

    @PostMapping("/confirm-reset-password")
    public ResponseEntity<UserAccountDto.MinimalDto> confirmResetPassword(@RequestBody LoginDto.PasswordResetDto passwordResetDto,
                                                                          @RequestParam("token") String verificationToken) {
        return new ResponseEntity<>(userAuthService.confirmResetPassword(passwordResetDto, verificationToken), HttpStatus.OK);
    }

    //that shouldnt be here but frontend asked
    @PostMapping("/confirm-budget-join")
    public ResponseEntity<String> confirmBudgetJoin(@RequestParam("token") String verificationToken, @RequestParam long budgetId) {
        return budgetService.confirmBudgetJoin(verificationToken, budgetId);
    }

    @PostMapping("/resend-email")
    public ResponseEntity<?> resendEmail(@RequestBody LoginDto.EmailDto loginDto) {
        return userAuthService.resendEmail(loginDto);
    }
}
