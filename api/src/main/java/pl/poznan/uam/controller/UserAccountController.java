package pl.poznan.uam.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.poznan.uam.dto.RegisterDto;
import pl.poznan.uam.dto.UserAccountDto;
import pl.poznan.uam.service.UserAccountService;

import javax.validation.Valid;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class UserAccountController {

    private final UserAccountService userAccountService;

    @GetMapping("/useraccount")
    public ResponseEntity<UserAccountDto.FullDto> getLoggedUser() {
        return new ResponseEntity<>(userAccountService.findLoggedUser(), HttpStatus.OK);
    }

    @GetMapping("/useraccount/{id}")
    public ResponseEntity<UserAccountDto.FullDto> getUserById(@PathVariable long id) {
        return new ResponseEntity<>(userAccountService.findUserById(id), HttpStatus.OK);
    }

    @DeleteMapping("/useraccount")
    public ResponseEntity<UserAccountDto.FullDto> deleteUser() {
        return new ResponseEntity<>(userAccountService.deleteUserAccount(), HttpStatus.OK);
    }

    @PutMapping("/useraccount")
    public ResponseEntity<UserAccountDto.FullDto> modifyUserAccount(@Valid @RequestBody UserAccountDto.MinimalDto minimalDto) {
        return new ResponseEntity<>(userAccountService.modifyUserAccount(minimalDto), HttpStatus.OK);
    }

    @PutMapping("/passwordChange")
    public ResponseEntity<UserAccountDto.FullDto> modifyUserPassword(@RequestBody UserAccountDto.PasswordDto passwordDto) {
        return new ResponseEntity<>(userAccountService.modifyUserPassword(passwordDto), HttpStatus.OK);
    }
}
