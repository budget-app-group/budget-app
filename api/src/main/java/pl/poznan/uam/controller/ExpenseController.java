package pl.poznan.uam.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.poznan.uam.dto.ExpenseDto;
import pl.poznan.uam.service.ExpenseService;

import java.util.List;

@RestController
@RequestMapping("/api/budget")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ExpenseController {

    private final ExpenseService expenseService;

    @GetMapping("/{budgetId}/master/{mCatId}/category/{catId}/expense")
    public ResponseEntity<List<ExpenseDto.FullDto>> getAllExpensesFromMasterCategoryAndCategory(@PathVariable long budgetId,
                                                                                                @PathVariable long mCatId,
                                                                                                @PathVariable long catId) {
        return new ResponseEntity<>(expenseService.findAllExpensesFromMasterCategoryAndCategory(budgetId, mCatId, catId), HttpStatus.OK);
    }

    @GetMapping("/{budgetId}/master/{mCatId}/category/{catId}/expenseincome")
    public ResponseEntity<List<ExpenseDto.FullDto>> getAllExpensesAndIncomesFromMasterCategoryAndCategory(@PathVariable long budgetId,
                                                                                                          @PathVariable long mCatId,
                                                                                                          @PathVariable long catId) {
        return new ResponseEntity<>(expenseService.findAllExpensesAndIncomesFromMasterCategoryAndCategory(budgetId, mCatId, catId), HttpStatus.OK);
    }

    @PostMapping("/{budgetId}/master/{masterCatId}/category/{catId}/expense")
    public ResponseEntity<ExpenseDto.PostDto> addExpense(@PathVariable long budgetId,
                                                         @PathVariable long masterCatId,
                                                         @PathVariable long catId,
                                                         @RequestBody ExpenseDto.PostDto expenseDto) {
        return new ResponseEntity<>(expenseService.createExpense(budgetId, masterCatId, catId, expenseDto), HttpStatus.CREATED);
    }

    @PostMapping("/{budgetId}/expensecsvlist")
    public ResponseEntity<List<ExpenseDto.CSVDto>> addExpensesFromList(@PathVariable long budgetId,
                                                                       @RequestBody List<ExpenseDto.CSVDto> expenseDtoList) {
        return new ResponseEntity<>(expenseService.createExpensesFromCSVList(budgetId, expenseDtoList), HttpStatus.CREATED);
    }


//    @PostMapping("/{budgetId}/master/{masterCatId}/category/{catId}/bankaccount/{bAccid}")
//    public ResponseEntity<ExpenseDto.OCRPostDto> addExpenseFromPhoto(@PathVariable long budgetId,
//                                                                     @PathVariable long masterCatId,
//                                                                     @PathVariable long catId,
//                                                                     @PathVariable long bAccid,
//                                                                     @RequestParam("file") MultipartFile file) {
//        return new ResponseEntity<>(expenseService.createExpenseFromPhoto(budgetId, masterCatId, catId, bAccid, file), HttpStatus.CREATED);
//    }

    @GetMapping("/{budgetId}/expense")
    public ResponseEntity<List<ExpenseDto.FullDto>> getAllExpensesFromBudget(@PathVariable long budgetId) {
        return new ResponseEntity<>(expenseService.findAllExpensesFromBudget(budgetId), HttpStatus.OK);
    }

    @GetMapping("/{budgetId}/income")
    public ResponseEntity<List<ExpenseDto.FullDto>> getAllIncomesFromBudget(@PathVariable long budgetId) {
        return new ResponseEntity<>(expenseService.findAllIncomesFromBudget(budgetId), HttpStatus.OK);
    }

    @GetMapping("/{budgetId}/master/{masterCatId}/expense")
    public ResponseEntity<List<ExpenseDto.FullDto>> getExpensesFromMasterCategory(@PathVariable long budgetId,
                                                                                  @PathVariable long masterCatId) {
        return new ResponseEntity<>(expenseService.findAllExpensesFromMasterCategory(budgetId, masterCatId), HttpStatus.OK);
    }

    @GetMapping("/{budgetId}/category/{categoryId}/expense")
    public ResponseEntity<List<ExpenseDto.FullDto>> getExpensesFromCategory(@PathVariable long budgetId,
                                                                            @PathVariable long categoryId) {
        return new ResponseEntity<>(expenseService.findAllExpensesFromCategory(budgetId, categoryId), HttpStatus.OK);
    }

    @GetMapping("/{budgetId}/master/{masterCatId}/category/{catId}/expense/{expId}")
    public ResponseEntity<ExpenseDto.FullDto> getExpenseById(@PathVariable long budgetId,
                                                             @PathVariable long masterCatId,
                                                             @PathVariable long catId,
                                                             @PathVariable long expId) {
        return new ResponseEntity<>(expenseService.findExpenseById(budgetId, masterCatId, catId, expId), HttpStatus.OK);
    }

    @PutMapping("/{budgetId}/master/{masterCatId}/category/{catId}/expense/{expId}")
    public ResponseEntity<ExpenseDto.PutDto> modifyExpense(@PathVariable long budgetId,
                                                           @PathVariable long masterCatId,
                                                           @PathVariable long catId,
                                                           @PathVariable long expId,
                                                           @RequestBody ExpenseDto.PutDto expenseDto) {
        return new ResponseEntity<>(expenseService.modifyExpense(budgetId, masterCatId, catId, expId, expenseDto), HttpStatus.OK);
    }

    @DeleteMapping("/{budgetId}/master/{masterCatId}/category/{catId}/expense/{expId}")
    public ResponseEntity<ExpenseDto.MinimalDto> deleteExpense(@PathVariable long budgetId,
                                                               @PathVariable long masterCatId,
                                                               @PathVariable long catId,
                                                               @PathVariable long expId){
        return new ResponseEntity<>(expenseService.deleteExpense(budgetId, masterCatId, catId, expId), HttpStatus.OK);
    }


    @GetMapping("/{budgetId}/bankaccount/{bankId}/expense")
    public ResponseEntity<List<ExpenseDto.FullDto>> getAllExpensesFromBankAccount(@PathVariable long budgetId,
                                                                                  @PathVariable long bankId) {
        return new ResponseEntity<>(expenseService.findAllExpensesFromBankAccount(budgetId, bankId), HttpStatus.OK);
    }

    @GetMapping("/{budgetId}/expense/period")
    public ResponseEntity<List<ExpenseDto.FullDto>> getAllExpensesFromBudgetInGivenPeriod(@PathVariable long budgetId,
                                                                                          @RequestParam("from") long from,
                                                                                          @RequestParam("to") long to) {
        return new ResponseEntity<>(expenseService.findAllExpensesFromBudgetInGivenPeriod(budgetId, from, to), HttpStatus.OK);
    }

    @GetMapping("/{budgetId}/bankaccount/{bankId}/income")
    public ResponseEntity<List<ExpenseDto.FullDto>> getAllIncomesForBankAccount(@PathVariable long budgetId,
                                                                                @PathVariable long bankId) {
        return new ResponseEntity<>(expenseService.findAllIncomeFromBankAccount(budgetId, bankId), HttpStatus.OK);
    }
}
