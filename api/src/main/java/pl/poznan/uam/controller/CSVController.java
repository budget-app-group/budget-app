package pl.poznan.uam.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import pl.poznan.uam.dto.ExpenseDto;
import pl.poznan.uam.service.CSVService;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/api/budget")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CSVController {

    private final CSVService csvService;

    @PostMapping("/upload-csv")
    public ResponseEntity<List<ExpenseDto.CSVDto>> uploadFile(@RequestPart(value = "csvfile") MultipartFile file,
                                                              @RequestPart(value = "budgetId") String budgetId,
                                                              @RequestPart(value = "bankAccountId") String bankAccId) throws IOException {

        return new ResponseEntity<>(csvService.importExpensesFromCsv(file, Long.parseLong(budgetId), Long.parseLong(bankAccId)), HttpStatus.CREATED);
    }

}
