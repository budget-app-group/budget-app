package pl.poznan.uam.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import pl.poznan.uam.dto.ExpenseDto;
import pl.poznan.uam.service.OcrService;

@RestController
@RequestMapping("/api/ocr")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class OCRController {
    private final OcrService ocrService;

    @PostMapping
    public ResponseEntity<ExpenseDto.OCRPostDto> addExpenseFromPhoto(@RequestParam("file") MultipartFile file) {
        return new ResponseEntity<>(ocrService.getDataFomPhoto(file), HttpStatus.CREATED);
    }

}
