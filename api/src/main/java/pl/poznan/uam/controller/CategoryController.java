package pl.poznan.uam.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.poznan.uam.dto.CategoryDto;
import pl.poznan.uam.service.CategoryService;

import java.util.List;

@RestController
@RequestMapping("/api/budget")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CategoryController {

    private final CategoryService categoryService;

    @GetMapping("/{budgetId}/master/{masterCatId}/category")
    public ResponseEntity<List<CategoryDto.FullDto>> getAllUserMasterCategories(@PathVariable long budgetId,
                                                                                @PathVariable long masterCatId) {
        return new ResponseEntity<>(categoryService.findAllCategoriesFromMasterCat(budgetId, masterCatId), HttpStatus.OK);
    }

    @GetMapping("/{budgetId}/master/{masterCatId}/category/{catId}")
    public ResponseEntity<CategoryDto.FullDto> getAllUserMasterCategories(@PathVariable long budgetId,
                                                                          @PathVariable long masterCatId,
                                                                          @PathVariable long catId) {
        return new ResponseEntity<>(categoryService.findCategoryById(budgetId, masterCatId, catId), HttpStatus.OK);
    }


    @GetMapping("/{budgetId}/categories")
    public ResponseEntity<List<CategoryDto.FullDto>> getAllCategories(@PathVariable long budgetId) {
        return new ResponseEntity<>(categoryService.findAllCategoriesFromBudget(budgetId), HttpStatus.OK);
    }

    //list of category
//    @PostMapping("/{budgetId}/master/{masterCatId}")
//    public ResponseEntity<List<CategoryDto.MinimalDto>> addCategories(@PathVariable long budgetId,
//                                                                      @PathVariable long masterCatId,
//                                                                      @RequestBody List<CategoryDto.MinimalDto> categoryDTOList) {
//        return new ResponseEntity<>(categoryService.createCategoriesFromList(budgetId, masterCatId, categoryDTOList), HttpStatus.CREATED);
//    }

    @PostMapping("/{budgetId}/master/{masterCatId}/category")
    public ResponseEntity<CategoryDto.MinimalDto> addCategories(@PathVariable long budgetId,
                                                                @PathVariable long masterCatId,
                                                                @RequestBody CategoryDto.MinimalDto categoryDTO) {
        return new ResponseEntity<>(categoryService.createCategory(budgetId, masterCatId, categoryDTO), HttpStatus.CREATED);
    }

    @PutMapping("/{budgetId}/master/{masterCatId}/category/{catId}")
    public ResponseEntity<CategoryDto.MinimalDto> modifyCategory(@PathVariable long budgetId,
                                                                 @PathVariable long masterCatId,
                                                                 @PathVariable long catId,
                                                                 @RequestBody CategoryDto.MinimalDto categoryDTO) {
        return new ResponseEntity<>(categoryService.modifyCategory(budgetId, masterCatId, catId, categoryDTO), HttpStatus.OK);
    }

    @DeleteMapping("/{budgetId}/master/{masterCatId}/category/{catId}")
    public ResponseEntity<CategoryDto.MinimalDto> deleteCategory(@PathVariable long budgetId,
                                                                 @PathVariable long masterCatId,
                                                                 @PathVariable long catId) {
        return new ResponseEntity<>(categoryService.deleteCategory(budgetId, masterCatId, catId), HttpStatus.OK);
    }
}
