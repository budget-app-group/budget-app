package pl.poznan.uam.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pl.poznan.uam.model.MasterCategory;

import java.util.List;
import java.util.Optional;

@Repository
public interface MasterCategoryRepository extends CrudRepository <MasterCategory, Long> {
    Optional<List<MasterCategory>> findAllByBudget_Id(long id);

    @Query(value = "select mc from MasterCategory mc join mc.budget b join b.userAccounts ua where ua.email=:email and b.id=:budgetId and mc.id=:mCatId")
    Optional<MasterCategory> findMasterCategory(@Param("email") String email, @Param("budgetId") long budgetId, @Param("mCatId") long mCatId);

    @Query(value = "select mc from MasterCategory mc join mc.budget b join b.userAccounts ua where ua.email=:email and b.id=:budgetId")
    List<MasterCategory> findMasterCategories(@Param("email") String email, @Param("budgetId") long budgetId);

    @Query(value = "select mc from MasterCategory mc join mc.budget b join b.userAccounts ua where ua.email=:email and b.id=:budgetId and mc.name=:mCatName")
    List<MasterCategory> findFirstMasterCategoryByName(@Param("email") String email, @Param("budgetId") long budgetId, @Param("mCatName") String mCatName);
}
