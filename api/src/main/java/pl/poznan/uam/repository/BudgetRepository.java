package pl.poznan.uam.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import pl.poznan.uam.model.Budget;
import pl.poznan.uam.model.UserAccount;

import java.util.List;
import java.util.Optional;

public interface BudgetRepository extends JpaRepository<Budget, Long> {
    Optional<Budget> findById(long id);

    @Query(value = "select b from Budget b join b.userAccounts ua where ua.email=:email and b.id=:budgetId")
    Optional<Budget> findBudget(@Param("email") String email, @Param("budgetId") long budgetId);

    @Query(value = "select ua from Budget b join b.userAccounts ua where b.id=:budgetId")
    List<UserAccount> findBudgetUsers(@Param("budgetId") long budgetId);

    @Query(value = "select b from Budget b join b.userAccounts ua where ua.email=:email")
    List<Budget> findAllBudgets(@Param("email") String email);
}