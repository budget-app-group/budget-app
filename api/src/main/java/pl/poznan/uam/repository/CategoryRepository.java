package pl.poznan.uam.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pl.poznan.uam.model.Category;

import java.util.List;
import java.util.Optional;

@Repository
public interface CategoryRepository extends CrudRepository<Category, Long> {
    List<Category> findByMasterCategory_Id(long id);

    @Query(value = "select c from Category c join c.masterCategory mc join mc.budget b join b.userAccounts ua where ua.email=:email and b.id=:budgetId and mc.id=:mCatId and c.id=:catId")
    Optional<Category> findCategory(@Param("email") String email, @Param("budgetId") long budgetId, @Param("mCatId") long mCatId, @Param("catId") long catId);

    @Query(value = "select c from Category c join c.masterCategory mc join mc.budget b join b.userAccounts ua where ua.email=:email and b.id=:budgetId and c.id=:catId")
    Optional<Category> findCategory(@Param("email") String email, @Param("budgetId") long budgetId, @Param("catId") long catId);

    @Query(value = "select c from Category c join c.masterCategory mc join mc.budget b join b.userAccounts ua where ua.email=:email and b.id=:budgetId and mc.id=:mCatId")
    List<Category> findAllCategoriesFromMasterCat(@Param("email") String email, @Param("budgetId") long budgetId, @Param("mCatId") long mCatId);

    @Query(value = "select c from Category c join c.masterCategory mc join mc.budget b join b.userAccounts ua where ua.email=:email and b.id=:budgetId and c.name=:catName")
    List<Category> findFirstCategoryByName(@Param("email") String email, @Param("budgetId") long budgetId, @Param("catName") String catName);

    @Query(value = "select c from Category c join c.masterCategory mc join mc.budget b join b.userAccounts ua where ua.email=:email and b.id=:budgetId")
    List<Category> findAllCategoriesFromBudget(@Param("email") String email, @Param("budgetId") long budgetId);


}
