package pl.poznan.uam.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pl.poznan.uam.model.BankAccount;

import java.util.List;
import java.util.Optional;

@Repository
public interface BankAccountRepository extends CrudRepository<BankAccount, Long> {

    @Query(value = "select ba from BankAccount ba join ba.budget b join b.userAccounts ua where ua.email=:email and b.id=:budgetId")
    List<BankAccount> findAllBankAccounts(@Param("email") String email, @Param("budgetId") long budgetId);

    @Query(value = "select ba from BankAccount ba join ba.budget b join b.userAccounts ua where ua.email=:email and b.id=:budgetId and ba.id=:bAccId")
    Optional<BankAccount> findBankAccount(@Param("email") String email, @Param("budgetId") long budgetId, @Param("bAccId") long bAccId);

}
