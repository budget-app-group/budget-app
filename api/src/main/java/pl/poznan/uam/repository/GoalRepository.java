package pl.poznan.uam.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pl.poznan.uam.model.Goal;

import java.util.List;
import java.util.Optional;

@Repository
public interface GoalRepository extends CrudRepository<Goal, Long> {

    @Query(value = "select g from Goal g join g.budget b join b.userAccounts ua " +
            "where ua.email=:email and b.id=:budgetId ")
    List<Goal> findAllGoalsFromBudget(@Param("email") String email, @Param("budgetId") long budgetId);

    @Query(value = "select g from Goal g join g.budget b join b.userAccounts ua " +
            "where ua.email=:email and g.id=:goalId ")
    Optional<Goal> findGoal(@Param("email") String email, @Param("goalId") long goalId);

    @Query(value = "select g from Goal g join g.budget b join b.userAccounts ua " +
            "where ua.email=:email and g.id=:goalId and b.id=:budgetId ")
    Optional<Goal> findGoalInBudget(@Param("email") String email, @Param("budgetId") long budgetId,@Param("goalId") long goalId);
}
