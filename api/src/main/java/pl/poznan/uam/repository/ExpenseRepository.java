package pl.poznan.uam.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pl.poznan.uam.dto.BudgetAnalysisDto;
import pl.poznan.uam.model.Expense;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Repository
public interface ExpenseRepository extends CrudRepository<Expense, Long> {


    @Query(value = "select e from Expense e join e.budget b join b.userAccounts ua " +
            "where ua.email=:email and b.id=:budgetId")
    List<Expense> findAllExpenses(@Param("email") String email, @Param("budgetId") long budgetId);

    @Query(value = "select e from Expense e join e.budget b join b.userAccounts ua join e.category c " +
            "where ua.email=:email and b.id=:budgetId and c.id=:catId")
    List<Expense> findAllExpensesFromCategory(@Param("email") String email, @Param("budgetId") long budgetId, @Param("catId") long catId);

    @Query(value = "select e from Expense e join e.category c join c.masterCategory mc join mc.budget b join b.userAccounts ua " +
            "where ua.email=:email and b.id=:budgetId and mc.id=:mCatId and c.id=:catId")
    List<Expense> findAllExpensesFromMasterCategoryAndCategory(@Param("email") String email, @Param("budgetId") long budgetId, @Param("mCatId") long mCatId, @Param("catId") long catId);

    @Query(value = "select e from Expense e join e.category c join c.masterCategory mc join mc.budget b join b.userAccounts ua " +
            "where ua.email=:email and b.id=:budgetId and mc.id=:mCatId and c.id=:catId and e.id=:expId")
    Optional<Expense> findExpense(@Param("email") String email, @Param("budgetId") long budgetId, @Param("mCatId") long mCatId, @Param("catId") long catId, @Param("expId") long expId);

    @Query(value = "select e from Expense e join e.category c join c.masterCategory mc join mc.budget b join b.userAccounts ua " +
            "where ua.email=:email and b.id=:budgetId and mc.id=:mCatId")
    List<Expense> findAllExpensesFromMasterCategory(@Param("email") String email, @Param("budgetId") long budgetId, @Param("mCatId") long mCatId);

    @Query(value = "select e from Expense e join e.bankAccount ba join e.userAccount ua join e.budget b " +
            "where ua.email=:email and b.id=:bId and ba.id=:baId")
    List<Expense> findAllExpensesFromBankAccount(@Param("email") String email, @Param("bId") long bId, @Param("baId") long baId);

    @Query(value = "select e from Expense e join e.budget b join b.userAccounts ua " +
            "where ua.email=:email and b.id=:bId and e.expenseDate>:dateFrom and e.expenseDate<:dateTo")
    List<Expense> findExpensesFromTo(@Param("email") String email, @Param("bId") long bId, @Param("dateFrom") LocalDateTime dateFrom, @Param("dateTo") LocalDateTime dateTo);

    @Query(value = "select new pl.poznan.uam.dto.BudgetAnalysisDto(mc.id, mc.name, mc.budgetAllocation, sum(e.amount)) from Expense e join e.category c join c.masterCategory mc join mc.budget b join b.userAccounts ua " +
            "where ua.email=:email and b.id=:bId and e.expenseDate>:dateFrom and e.expenseDate<:dateTo and mc.id=:mCatId and e.isIncome= false " +
            "group by mc.id")
    BudgetAnalysisDto findSumOfExpenseInMasterCat2(@Param("email") String email, @Param("bId") long bId, @Param("mCatId") long mCatId, @Param("dateFrom") LocalDateTime dateFrom, @Param("dateTo") LocalDateTime dateTo);

    @Query(value = "select e from Expense e join e.budget b join b.userAccounts ua join e.goal g " +
            "where ua.email=:email and b.id=:budgetId and g.id=:goalId ")
    List<Expense> findAllDonationsToGoal(@Param("email") String email, @Param("budgetId") long budgetId, @Param("goalId") long goalId);
}
