package pl.poznan.uam.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.poznan.uam.model.Receipt;

@Repository
public interface ReceiptRepository extends CrudRepository<Receipt, Long> {

}
