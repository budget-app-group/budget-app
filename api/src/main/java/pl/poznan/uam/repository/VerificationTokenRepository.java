package pl.poznan.uam.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.poznan.uam.model.UserAccount;
import pl.poznan.uam.model.VerificationToken;

import java.util.Optional;

@Repository
public interface VerificationTokenRepository extends JpaRepository<VerificationToken, Long> {
    Optional<VerificationToken> findByVerificationToken(String verificationToken);

    Optional<VerificationToken> findByUserAccount(UserAccount userAccount);
}
