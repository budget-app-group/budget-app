package pl.poznan.uam.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pl.poznan.uam.model.UserAccount;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<UserAccount, Long> {
    Optional<UserAccount> findByEmailIgnoreCase(String email);
    Optional<UserAccount> findById(Long id);

    Boolean existsByEmail(String email);

    @Query("select ua from UserAccount ua join ua.budgets b where b.id=:budgetId")
    List<UserAccount> findUsersFromBudget(@Param("budgetId") long budgetId);

}
