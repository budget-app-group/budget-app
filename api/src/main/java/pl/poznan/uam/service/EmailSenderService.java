package pl.poznan.uam.service;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;
import pl.poznan.uam.model.Mail;

import javax.annotation.PostConstruct;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Map;

@Component
public class EmailSenderService {

    private Configuration cfg;

    @Autowired
    private Environment env;

    @Getter
    private JavaMailSender javaMailSender;

    @Autowired
    public EmailSenderService(JavaMailSender javaMailSender){
        this.javaMailSender = javaMailSender;
    }

    @PostConstruct
    public void initMailConfig() {
        cfg = new Configuration();
        cfg.setClassForTemplateLoading(this.getClass(), "/templates");
    }

    @Async
    public void sendMail(Mail mail) {

        try {
            Template template = cfg.getTemplate(mail.getTemplateName());

            MimeMessage mailMessage = javaMailSender.createMimeMessage();
            mailMessage.setFrom(new InternetAddress(env.getProperty("spring.mail.username")));
            mailMessage.addRecipient(Message.RecipientType.TO, new InternetAddress(mail.getRecipient()));
            mailMessage.setSubject("MoneyWatcher - " + mail.getTopic());

            Map<String, Object> input = mail.getTemplateOptions();

            Writer writer = new StringWriter();
            template.process(input, writer);

            mailMessage.setContent(writer.toString(), "text/html; charset=UTF-8");
            javaMailSender.send(mailMessage);
        } catch (IOException | MessagingException | TemplateException e1) {
            throw new ResponseStatusException(
                    HttpStatus.INTERNAL_SERVER_ERROR, "Something went wrong.");
        }
    }
}

