package pl.poznan.uam.service;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.poznan.uam.converters.MasterCategoryConverter;
import pl.poznan.uam.dto.MasterCategoryDto;
import pl.poznan.uam.exceptions.EntityNotFoundException;
import pl.poznan.uam.exceptions.PermissionDeniedException;
import pl.poznan.uam.model.Budget;
import pl.poznan.uam.model.MasterCategory;
import pl.poznan.uam.repository.BudgetRepository;
import pl.poznan.uam.repository.MasterCategoryRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static pl.poznan.uam.utils.UtilityFunctions.getLoggedUsername;

@Service
@Transactional
@RequiredArgsConstructor(onConstructor = @__(@Autowired))

public class MasterCategoryService {

    private final MasterCategoryConverter masterCategoryConverter;
    private final BudgetRepository budgetRepository;
    private final MasterCategoryRepository masterCategoryRepository;
    private final CategoryService categoryService;


    public List<MasterCategoryDto.FullDto> findAllUserMasterCategories(long budgetId) {
        Budget budget = budgetRepository.findBudget(getLoggedUsername(), budgetId)
                .orElseThrow(() -> new EntityNotFoundException(Budget.class));
        List<MasterCategory> masterCategoryList = masterCategoryRepository.findMasterCategories(getLoggedUsername(), budgetId);
        return masterCategoryConverter.listToFullDTO(budget, masterCategoryList);
    }

    public MasterCategoryDto.FullDto findMasterCategoryById(long budgetId, long mCatId) {
        Budget budget = budgetRepository.findBudget(getLoggedUsername(), budgetId)
                .orElseThrow(() -> new EntityNotFoundException(Budget.class));
        MasterCategory masterCategory = masterCategoryRepository.findMasterCategory(getLoggedUsername(), budgetId, mCatId)
                .orElseThrow(() -> new EntityNotFoundException(MasterCategory.class));
        return masterCategoryConverter.toFullDto(budget, masterCategory);
    }

//    public MasterCategoryDto.MinimalDto createMasterCategory(long budgetId, MasterCategoryDto.MinimalDto masterCatDTO) {
//        UserAccount user = getLoggedUser(userRepository);
//        Budget budget = user.getBudgets().stream().filter(usGp -> usGp.getId() == budgetId).findFirst()
//                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, BUDGET_NOT_FOUND.getMessage()));
//
//        MasterCategory masterCategoryToSave = MasterCategory.builder()
//                .name(masterCatDTO.getName())
//                .budgetAllocation(masterCatDTO.getBudgetAllocation())
//                .budget(budget)
//                .build();
//
//        masterCategoryRepository.save(masterCategoryToSave);
//        budget.addMasterCategory(masterCategoryToSave);
//        budgetRepository.save(budget);
//        return masterCatDTO;
//    }

    @Transactional
    public MasterCategoryDto.MinimalDto modifyMasterCategory(long budgetId, long catId, MasterCategoryDto.MinimalDto masterCatDTO) {
        MasterCategory masterCategoryToModify = masterCategoryRepository.findMasterCategory(getLoggedUsername(), budgetId, catId)
                .orElseThrow(() -> new EntityNotFoundException(MasterCategory.class));
        Budget budget = budgetRepository.findBudget(getLoggedUsername(), budgetId)
                .orElseThrow(() -> new EntityNotFoundException(Budget.class));
        if (budget.getAdmin().equals(getLoggedUsername())) {
            masterCategoryRepository.save(masterCategoryConverter.updateMasterCategory(masterCategoryToModify, masterCatDTO));
            return masterCatDTO;
        } else {
            throw new PermissionDeniedException();
        }
    }

    @Transactional
    public MasterCategoryDto.MinimalDto deleteMasterCategory(long budgetId, long catId) {
        MasterCategory masterCategory = masterCategoryRepository.findMasterCategory(getLoggedUsername(), budgetId, catId)
                .orElseThrow(() -> new EntityNotFoundException(MasterCategory.class));
        Budget budget = budgetRepository.findBudget(getLoggedUsername(), budgetId)
                .orElseThrow(() -> new EntityNotFoundException(Budget.class));
        if (budget.getAdmin().equals(getLoggedUsername())) {
            masterCategoryRepository.delete(masterCategory);
            return masterCategoryConverter.toMinimalDto(masterCategory);
        } else {
            throw new PermissionDeniedException();
        }
    }

    @Transactional
    public MasterCategoryDto.MinimalDto createMasterCategory(long budgetId, MasterCategoryDto.MinimalDto masterCatDTO) {
        Budget budget = budgetRepository.findBudget(getLoggedUsername(), budgetId)
                .orElseThrow(() -> new EntityNotFoundException(Budget.class));
        if (budget.getAdmin().equals(getLoggedUsername())) {
            MasterCategory masterCategoryToSave = new MasterCategory.Builder()
                    .name(masterCatDTO.getName())
                    .budgetAllocation(masterCatDTO.getBudgetAllocation())
                    .color(Optional.ofNullable(masterCatDTO.getColor()).orElse("#919191"))
                    .budget(budget)
                    .build();
            masterCategoryRepository.save(masterCategoryToSave);
            return masterCategoryConverter.toMinimalDto(masterCategoryToSave);
        } else {
            throw new PermissionDeniedException();
        }
    }

    @Transactional
    public List<MasterCategoryDto.MinimalDto> createMasterCategoriesFromList(long budgetId, List<MasterCategoryDto.MinimalDto> masterCatListDTO) {
        Budget budget = budgetRepository.findBudget(getLoggedUsername(), budgetId)
                .orElseThrow(() -> new EntityNotFoundException(Budget.class));
        for (MasterCategoryDto.MinimalDto masterCatDTO : masterCatListDTO) {
            MasterCategory masterCategoryToSave = new MasterCategory.Builder()
                    .name(masterCatDTO.getName())
                    .budgetAllocation(masterCatDTO.getBudgetAllocation())
                    .color(Optional.ofNullable(masterCatDTO.getColor()).orElse("#919191"))
                    .budget(budget)
                    .build();
            masterCategoryRepository.save(masterCategoryToSave);
        }
        return masterCatListDTO;
    }

    public void createDefaultMasterCategories(Budget budget) {
        List<MasterCategory> masterCategories = new ArrayList<>();
        MasterCategory masterCategoryPayments = new MasterCategory.Builder()
                .name("Opłaty stałe")
                .budgetAllocation(60)
                .budget(budget)
                .color("#F44336")
                .build();
        String[] paymentsCategories = new String[]{"Dom", "Rachunki", "Podatki i Opłaty"};
        categoryService.createDefaultCategories(masterCategoryPayments, paymentsCategories);
        masterCategories.add(masterCategoryPayments);

        MasterCategory masterCategorySavings = new MasterCategory.Builder()
                .name("Oszczędnosci i Inwestycje")
                .budgetAllocation(20)
                .budget(budget)
                .color("#2196F3")
                .build();
        String[] savingsCategories = new String[]{"Inwestycje emerytalne", "Oszczędności długoterminowe"};
        categoryService.createDefaultCategories(masterCategorySavings, savingsCategories);
        masterCategories.add(masterCategorySavings);

        MasterCategory masterCategoryEntertainment = new MasterCategory.Builder()
                .name("Rozrywka i Wypoczynek")
                .budgetAllocation(5)
                .budget(budget)
                .color("#FFEB3B")
                .build();
        String[] entertainmentCategories = new String[]{"Restauracje", "Sport", "Multimedia", "Kino i Teatr"};
        categoryService.createDefaultCategories(masterCategoryEntertainment, entertainmentCategories);
        masterCategories.add(masterCategoryEntertainment);

        MasterCategory masterCategoryCurrentExpenses = new MasterCategory.Builder()
                .name("Wydatki bieżące")
                .budgetAllocation(10)
                .budget(budget)
                .color("#4CAF50")
                .build();
        String[] currentExpensesCategories = new String[]{"Zakupy spożywcze", "Kosmetyki i Uroda"};
        categoryService.createDefaultCategories(masterCategoryCurrentExpenses, currentExpensesCategories);
        masterCategories.add(masterCategoryCurrentExpenses);

        MasterCategory masterCategoryIrregularExpenses = new MasterCategory.Builder()
                .name("Wydatki nieregularne")
                .budgetAllocation(5)
                .budget(budget)
                .color("#FF9800")
                .build();
        String[] irregularExpensesCategories = new String[]{"Zdrowie", "Odzież"};
        categoryService.createDefaultCategories(masterCategoryIrregularExpenses, irregularExpensesCategories);
        masterCategories.add(masterCategoryIrregularExpenses);

        for (MasterCategory masterCategory : masterCategories) {
            masterCategoryRepository.save(masterCategory);
        }
    }

    public void createOtherMasterCat(Budget budget) {
        MasterCategory masterCategoryOther = new MasterCategory.Builder()
                .name("Inne")
                .budgetAllocation(0)
                .color("#919191")
                .budget(budget)
                .build();
        categoryService.createOtherCat(masterCategoryOther);
        masterCategoryRepository.save(masterCategoryOther);
    }
}
