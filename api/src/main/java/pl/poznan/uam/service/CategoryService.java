package pl.poznan.uam.service;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.poznan.uam.converters.CategoryConverter;
import pl.poznan.uam.dto.CategoryDto;
import pl.poznan.uam.exceptions.EntityNotFoundException;
import pl.poznan.uam.exceptions.PermissionDeniedException;
import pl.poznan.uam.model.Budget;
import pl.poznan.uam.model.Category;
import pl.poznan.uam.model.MasterCategory;
import pl.poznan.uam.repository.BudgetRepository;
import pl.poznan.uam.repository.CategoryRepository;
import pl.poznan.uam.repository.MasterCategoryRepository;

import java.util.List;

import static pl.poznan.uam.utils.UtilityFunctions.getLoggedUsername;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CategoryService {

    private final CategoryConverter categoryConverter;
    private final CategoryRepository categoryRepository;
    private final MasterCategoryRepository masterCategoryRepository;
    private final BudgetRepository budgetRepository;

    public List<CategoryDto.FullDto> findAllCategoriesFromMasterCat(long budgetId, long masterCatId) {
        Budget budget = budgetRepository.findBudget(getLoggedUsername(), budgetId)
                .orElseThrow(() -> new EntityNotFoundException(Budget.class));

        return categoryConverter.listToFullDTO(budget, categoryRepository.findAllCategoriesFromMasterCat(getLoggedUsername(), budgetId, masterCatId));
    }

    public CategoryDto.FullDto findCategoryById(long budgetId, long masterCatId, long catId) {
        Budget budget = budgetRepository.findBudget(getLoggedUsername(), budgetId)
                .orElseThrow(() -> new EntityNotFoundException(Budget.class));

        Category category = categoryRepository.findCategory(getLoggedUsername(), budgetId, masterCatId, catId)
                .orElseThrow(() -> new EntityNotFoundException(Category.class));
        return categoryConverter.toFullDto(budget, category);
    }

    public List<CategoryDto.FullDto> findAllCategoriesFromBudget(long budgetId) {
        Budget budget = budgetRepository.findBudget(getLoggedUsername(), budgetId)
                .orElseThrow(() -> new EntityNotFoundException(Budget.class));

        return categoryConverter.listToFullDTO(budget, categoryRepository.findAllCategoriesFromBudget(getLoggedUsername(), budgetId));
    }

    @Transactional
    public CategoryDto.MinimalDto createCategory(long budgetId, long masterCatId, CategoryDto.MinimalDto categoryDTO) {
        Budget budget = budgetRepository.findBudget(getLoggedUsername(), budgetId)
                .orElseThrow(() -> new EntityNotFoundException(Budget.class));

        if (budget.getAdmin().equals(getLoggedUsername())) {
            MasterCategory masterCategory = masterCategoryRepository.findMasterCategory(getLoggedUsername(), budgetId, masterCatId)
                    .orElseThrow(() -> new EntityNotFoundException(Category.class));

            Category categoryToSave = new Category.Builder()
                    .name(categoryDTO.getName())
                    .color("#919191")
                    .masterCategory(masterCategory)
                    .build();
//            masterCategory.addCategory(categoryToSave);
//            masterCategoryRepository.save(masterCategory);
            categoryRepository.save(categoryToSave);
            return categoryConverter.toMinimalDto(categoryToSave);
        } else {
            throw new PermissionDeniedException();
        }

    }

    @Transactional
    public List<CategoryDto.MinimalDto> createCategoriesFromList(long budgetId, long masterCatId, List<CategoryDto.MinimalDto> categoryDTOList) {
        MasterCategory masterCategory = masterCategoryRepository.findMasterCategory(getLoggedUsername(), budgetId, masterCatId)
                .orElseThrow(() -> new EntityNotFoundException(Category.class));

        for (CategoryDto.MinimalDto categoryDTO : categoryDTOList) {
            Category categoryToSave = new Category.Builder()
                    .name(categoryDTO.getName())
                    .color("#919191")
                    .masterCategory(masterCategory)
                    .build();
            masterCategory.addCategory(categoryToSave);
        }
        masterCategoryRepository.save(masterCategory);
        return categoryDTOList;
    }

    @Transactional
    public CategoryDto.MinimalDto modifyCategory(long budgetId, long masterCatId, long categoryId, CategoryDto.MinimalDto categoryDTO) {
        Budget budget = budgetRepository.findBudget(getLoggedUsername(), budgetId)
                .orElseThrow(() -> new EntityNotFoundException(Budget.class));
        if (budget.getAdmin().equals(getLoggedUsername())) {
            Category categoryToModify = categoryRepository.findCategory(getLoggedUsername(), budgetId, masterCatId, categoryId)
                    .orElseThrow(() -> new EntityNotFoundException(Category.class));
            categoryRepository.save(categoryConverter.updateCategory(categoryToModify, categoryDTO));
            return categoryDTO;
        } else {
            throw new PermissionDeniedException();
        }
    }

    @Transactional
    public CategoryDto.MinimalDto deleteCategory(long budgetId, long masterCatId, long categoryId) {
        Budget budget = budgetRepository.findBudget(getLoggedUsername(), budgetId)
                .orElseThrow(() -> new EntityNotFoundException(Budget.class));
        if (budget.getAdmin().equals(getLoggedUsername())) {
            Category category = categoryRepository.findCategory(getLoggedUsername(), budgetId, masterCatId, categoryId)
                    .orElseThrow(() -> new EntityNotFoundException(Category.class));
            categoryRepository.deleteById(categoryId);
            return categoryConverter.toMinimalDto(category);
        } else {
            throw new PermissionDeniedException();
        }
    }

    public void createDefaultCategories(MasterCategory masterCategory, String[] Categories) {
        for (String name : Categories) {
            Category category = new Category.Builder()
                    .name(name)
                    .color("#919191")
                    .masterCategory(masterCategory)
                    .build();
            categoryRepository.save(category);
        }
    }

    public void createOtherCat(MasterCategory masterCategoryOther) {
        Category category = new Category.Builder()
                .name("Inne")
                .color("#919191")
                .masterCategory(masterCategoryOther)
                .build();
        categoryRepository.save(category);
    }
}


