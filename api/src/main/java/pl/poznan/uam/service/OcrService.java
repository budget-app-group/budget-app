package pl.poznan.uam.service;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import pl.poznan.uam.dto.ExpenseDto;
import pl.poznan.uam.model.Receipt;
import pl.poznan.uam.repository.ReceiptRepository;
import pl.poznan.uam.utils.ocr.OCRParser;
import pl.poznan.uam.utils.ocr.VisionOCR;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.util.Optional;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class OcrService {
    private final ReceiptRepository receiptRepository;
    private OCRParser ocrParser = new OCRParser();

    private static final String PARSE_ERROR = "ocrParseError";

    @Transactional
    public ExpenseDto.OCRPostDto getDataFomPhoto(MultipartFile file) {

        ExpenseDto.OCRPostDto expenseToAdd;
        String dataToParse;
        byte[] byteArr;
        try {
            byteArr = file.getBytes();
            VisionOCR ocr = new VisionOCR(VisionOCR.getVisionService());
            dataToParse = ocr.detectText(byteArr);
            expenseToAdd = ocrParser.parseOCR(dataToParse);
//            return ocr.detectText(byteArr);
        } catch (IOException | GeneralSecurityException e) {
            throw new RuntimeException(
                    "Call to google vision API failed", e);
        }

        Receipt.Builder builder = new Receipt.Builder();
        builder.orgReceipt(byteArr);
        builder.expense(expenseToAdd.toString());
        builder.afterOcr(Optional.ofNullable(dataToParse).orElse(""));

        if (expenseToAdd.getExpenseDate() == 0L) {
            builder.isDateIncorrect(true);
        }
        if (expenseToAdd.getShopName().contains(PARSE_ERROR) || expenseToAdd.getName().contains(PARSE_ERROR)) {
            builder.isShopNameIncorrect(true);
        }
        if (expenseToAdd.getAmount().equals(new BigDecimal(BigInteger.ZERO))) {
            builder.isPriceIncorrect(true);
        }

        Receipt receipt = builder.build();
        Receipt imageToRead = receiptRepository.save(receipt);

//            System.out.println("tujestem");
//            receiptRepository.findById(imageToRead.getId()).orElseThrow(() -> new EntityNotFoundException(Receipt.class));
//
//            try {
//                OutputStream os  = new FileOutputStream(new File("/home/zaqs/testfile.jpg"));
//                os.write(imageToRead.getOrgReceipt());
//                System.out.println("Success");
//                os.close();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
        return expenseToAdd;
    }
}
