package pl.poznan.uam.service;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;
import pl.poznan.uam.converters.UserAccountConverter;
import pl.poznan.uam.dto.UserAccountDto;
import pl.poznan.uam.exceptions.EntityNotFoundException;
import pl.poznan.uam.model.Budget;
import pl.poznan.uam.model.UserAccount;
import pl.poznan.uam.repository.BudgetRepository;
import pl.poznan.uam.repository.UserRepository;

import static pl.poznan.uam.utils.ExceptionEnums.EMAIL_EXISTS;
import static pl.poznan.uam.utils.UtilityFunctions.getLoggedUser;

@Service
@Transactional
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class UserAccountService {

    private final UserAccountConverter userAccountConverter;
    private final UserRepository userRepository;
    private final BudgetRepository budgetRepository;

    public UserAccountDto.FullDto findLoggedUser() {
        return userAccountConverter.toFullDTO(getLoggedUser(userRepository));
    }

    @Transactional
    public UserAccountDto.FullDto deleteUserAccount() {
        UserAccount user = getLoggedUser(userRepository);
        //Delete budget if user is last in this group
        for (Budget ug : user.getBudgets()) {
            if (ug.getUserAccounts().size() == 1) {
                budgetRepository.delete(ug);
            }
        }
        userRepository.delete(user);
        return userAccountConverter.toFullDTO(user);

    }

    public UserAccountDto.FullDto findUserById(long id) {
        UserAccount userAccount = userRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(UserAccount.class));
        return userAccountConverter.toFullDTO(userAccount);

    }

    @Transactional
    public UserAccountDto.FullDto modifyUserAccount(UserAccountDto.MinimalDto minimalDto) {
        UserAccount userAccountToModify = getLoggedUser(userRepository);

        if (userRepository.existsByEmail(minimalDto.getEmail())) {
            if (!userRepository.findByEmailIgnoreCase(minimalDto.getEmail()).get().equals(userAccountToModify)) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, EMAIL_EXISTS.getMessage());
            }
        }
        if (!userAccountToModify.getEmail().equals(minimalDto.getEmail())) {
            for (Budget ug : userAccountToModify.getBudgets()) {
                if (ug.getAdmin().equals(userAccountToModify.getEmail())) {
                    ug.setAdmin(minimalDto.getEmail());
                }
            }
        }

        userRepository.save(userAccountConverter.updateUserAccount(userAccountToModify, minimalDto));
        return userAccountConverter.toFullDTO(userAccountToModify);
    }

    public UserAccountDto.FullDto modifyUserPassword(UserAccountDto.PasswordDto passwordDto) {
        UserAccount userAccount = getLoggedUser(userRepository);
        userRepository.save(userAccountConverter.updateUserPassword(userAccount, passwordDto));
        return userAccountConverter.toFullDTO(userAccount);
    }
}
