package pl.poznan.uam.service;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.poznan.uam.converters.ExpenseConverter;
import pl.poznan.uam.dto.ExpenseDto;
import pl.poznan.uam.exceptions.EntityNotFoundException;
import pl.poznan.uam.exceptions.PermissionDeniedException;
import pl.poznan.uam.model.*;
import pl.poznan.uam.repository.*;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;
import java.util.stream.Collectors;

import static pl.poznan.uam.utils.UtilityFunctions.getLoggedUser;
import static pl.poznan.uam.utils.UtilityFunctions.getLoggedUsername;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ExpenseService {
    private final ExpenseConverter expenseConverter;
    private final ExpenseRepository expenseRepository;
    private final UserRepository userRepository;
    private final BudgetRepository budgetRepository;
    private final CategoryRepository categoryRepository;
    private final BankAccountRepository bankAccountRepository;
    private final GoalRepository goalRepository;

    public List<ExpenseDto.FullDto> findAllExpensesFromMasterCategoryAndCategory(long budgetId, long mCatId, long catId) {
        return expenseRepository.findAllExpensesFromMasterCategoryAndCategory(getLoggedUsername(), budgetId, mCatId, catId)
                .stream()
                .filter(exp -> !exp.isIncome())
                .map(expenseConverter::toFullDTO)
                .collect(Collectors.toList());
    }

    public List<ExpenseDto.FullDto> findAllExpensesAndIncomesFromMasterCategoryAndCategory(long budgetId, long mCatId, long catId) {
        return expenseRepository.findAllExpensesFromMasterCategoryAndCategory(getLoggedUsername(), budgetId, mCatId, catId)
                .stream()
                .map(expenseConverter::toFullDTO)
                .collect(Collectors.toList());
    }

    public List<ExpenseDto.FullDto> findAllExpensesFromBudget(long budgetId) {
        return expenseRepository.findAllExpenses(getLoggedUsername(), budgetId)
                .stream()
                .filter(expense -> !expense.isIncome())
                .map(expenseConverter::toFullDTO)
                .collect(Collectors.toList());
    }

    public List<ExpenseDto.FullDto> findAllExpensesFromMasterCategory(long budgetId, long mCatId) {
        return expenseRepository.findAllExpensesFromMasterCategory(getLoggedUsername(), budgetId, mCatId)
                .stream()
                .filter(expense -> !expense.isIncome())
                .map(expenseConverter::toFullDTO)
                .collect(Collectors.toList());
    }

    public List<ExpenseDto.FullDto> findAllExpensesFromCategory(long budgetId, long catId) {
        return expenseRepository.findAllExpensesFromCategory(getLoggedUsername(), budgetId, catId)
                .stream()
                .filter(expense -> !expense.isIncome())
                .map(expenseConverter::toFullDTO)
                .collect(Collectors.toList());
    }


    public ExpenseDto.FullDto findExpenseById(long budgetId, long masterCatId, long catId, long expId) {
        Expense expense = expenseRepository.findExpense(getLoggedUsername(), budgetId, masterCatId, catId, expId)
                .orElseThrow(() -> new EntityNotFoundException(Expense.class));
        return expenseConverter.toFullDTO(expense);
    }

    //todo: wszystkie wydatki bez uwagi na budżet?
    public List<ExpenseDto.FullDto> findAllExpensesFromBankAccount(long budgetId, long bankId) {
        return expenseRepository.findAllExpensesFromBankAccount(getLoggedUsername(), budgetId, bankId)
                .stream()
                .filter(expense -> !expense.isIncome())
                .map(expenseConverter::toFullDTO)
                .collect(Collectors.toList());
    }

    public List<ExpenseDto.FullDto> findAllExpensesFromBudgetInGivenPeriod(long budgetId, long from, long to) {
        LocalDateTime dateFrom = LocalDateTime.ofInstant(Instant.ofEpochMilli(from), ZoneId.of("Europe/Berlin"));
        LocalDateTime dateTo = LocalDateTime.ofInstant(Instant.ofEpochMilli(to), ZoneId.of("Europe/Berlin"));

        return expenseRepository.findExpensesFromTo(getLoggedUsername(), budgetId, dateFrom, dateTo)
                .stream()
//                .filter(expense -> !expense.isIncome())
                .map(expenseConverter::toFullDTO)
                .collect(Collectors.toList());
    }

    public List<ExpenseDto.FullDto> findAllIncomeFromBankAccount(long budgetId, long bankId) {
        return expenseRepository.findAllExpensesFromBankAccount(getLoggedUsername(), budgetId, bankId)
                .stream()
                .filter(Expense::isIncome)
                .map(expenseConverter::toFullDTO)
                .collect(Collectors.toList());
    }

    public List<ExpenseDto.FullDto> findAllIncomesFromBudget(long budgetId) {
        return expenseRepository.findAllExpenses(getLoggedUsername(), budgetId)
                .stream()
                .filter(Expense::isIncome)
                .map(expenseConverter::toFullDTO)
                .collect(Collectors.toList());
    }

    //todo: jak ułożyć ten link inaczej? masterCat jest zbędny
    @Transactional
    public ExpenseDto.PostDto createExpense(long budgetId, long masterCatId, long catId, ExpenseDto.PostDto expenseDto) {
        UserAccount user = getLoggedUser(userRepository);

        Budget budget = budgetRepository.findBudget(getLoggedUsername(), budgetId)
                .orElseThrow(() -> new EntityNotFoundException(Budget.class));

        Category category = categoryRepository.findCategory(getLoggedUsername(), budgetId, masterCatId, catId)
                .orElseThrow(() -> new EntityNotFoundException(Category.class));

        BankAccount bankAccount = bankAccountRepository.findBankAccount(getLoggedUsername(), budgetId, expenseDto.getBankAccountId())
                .orElseThrow(() -> new EntityNotFoundException(BankAccount.class));

//        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        LocalDateTime expenseDate = LocalDateTime.ofInstant(Instant.ofEpochMilli(expenseDto.getExpenseDate()),
                ZoneId.of("Europe/Berlin"));

        if (expenseDto.isIncome()) {
            bankAccount.setMeans(bankAccount.getMeans().add(expenseDto.getAmount()));
        } else {
            bankAccount.setMeans(bankAccount.getMeans().subtract(expenseDto.getAmount()));
        }
        bankAccountRepository.save(bankAccount);

        Goal goal = null;
        if (expenseDto.getGoalId() != 0) {
            goal = goalRepository.findGoal(getLoggedUsername(), expenseDto.getGoalId())
                    .orElseThrow(() -> new EntityNotFoundException(Goal.class));
            goal.setCollectedAmount(goal.getCollectedAmount().add(expenseDto.getAmount()));
            goalRepository.save(goal);
        }

        Expense expenseToSave = new Expense.Builder()
                .amount(expenseDto.getAmount())
                .itemName(expenseDto.getName())
                .expenseDate(expenseDate)
                .category(category)
                .userAccount(user)
                .budget(budget)
                .bankAccount(bankAccount)
                .shopName(expenseDto.getShopName())
                .isIncome(expenseDto.isIncome())
                .goal(goal)
                .build();

        expenseRepository.save(expenseToSave);

        return expenseConverter.toPostDto(expenseToSave);
    }

    public List<ExpenseDto.CSVDto> createExpensesFromCSVList(long budgetId, List<ExpenseDto.CSVDto> expenseDtoList) {
        UserAccount user = getLoggedUser(userRepository);

        Budget budget = budgetRepository.findBudget(getLoggedUsername(), budgetId)
                .orElseThrow(() -> new EntityNotFoundException(Budget.class));

        for (ExpenseDto.CSVDto expenseDto : expenseDtoList) {
            //        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
            LocalDateTime expenseDate = LocalDateTime.ofInstant(Instant.ofEpochMilli(expenseDto.getExpenseDate()),
                    ZoneId.of("Europe/Berlin"));

            Category category = categoryRepository.findCategory(getLoggedUsername(), budgetId, expenseDto.getCategoryId())
                    .orElseThrow(() -> new EntityNotFoundException(Category.class));

            BankAccount bankAccount = bankAccountRepository.findBankAccount(getLoggedUsername(), budgetId, expenseDto.getBankAccountId())
                    .orElseThrow(() -> new EntityNotFoundException(BankAccount.class));

            if (expenseDto.isIncome()) {
                bankAccount.setMeans(bankAccount.getMeans().add(expenseDto.getAmount()));
            } else {
                bankAccount.setMeans(bankAccount.getMeans().subtract(expenseDto.getAmount()));
            }
            bankAccountRepository.save(bankAccount);
            Expense expenseToSave = new Expense.Builder()
                    .amount(expenseDto.getAmount())
                    .itemName(expenseDto.getName())
                    .expenseDate(expenseDate)
                    .category(category)
                    .userAccount(user)
                    .budget(budget)
                    .bankAccount(bankAccount)
                    .shopName(expenseDto.getShopName())
                    .isIncome(expenseDto.isIncome())
                    .build();
            expenseRepository.save(expenseToSave);
        }
        return expenseDtoList;
    }

    //todo co zrobićz linkiem bo znowu niewykorzystane rzeczy
    @Transactional
    public ExpenseDto.PutDto modifyExpense(long budgetId, long masterCatId, long catId, long expId, ExpenseDto.PutDto expenseDto) {

        Budget newBudget = budgetRepository.findBudget(getLoggedUsername(), budgetId)
                .orElseThrow(() -> new EntityNotFoundException(Budget.class));

        Category newCategory = categoryRepository.findCategory(getLoggedUsername(), budgetId, expenseDto.getMastCatId(), expenseDto.getCategoryId())
                .orElseThrow(() -> new EntityNotFoundException(Category.class));

        Expense expenseToModify = expenseRepository.findExpense(getLoggedUsername(), budgetId, masterCatId, catId, expId)
                .orElseThrow(() -> new EntityNotFoundException(Expense.class));

        UserAccount expenseOwner = expenseToModify.getUserAccount();

        BankAccount bankAccount = bankAccountRepository.findBankAccount(getLoggedUsername(), budgetId, expenseDto.getBankAccountId())
                .orElseThrow(() -> new EntityNotFoundException(BankAccount.class));

        LocalDateTime expenseDate = LocalDateTime.ofInstant(Instant.ofEpochMilli(expenseDto.getExpenseDate()),
                ZoneId.of("Europe/Berlin"));

        if (expenseOwner.getEmail().equals(getLoggedUsername())) {
            Expense modifiedExpense = new Expense.Builder()
                    .id(expenseToModify.getId())
                    .amount(expenseDto.getAmount())
                    .itemName(expenseDto.getName())
                    .expenseDate(expenseDate)
                    .shopName(expenseDto.getShopName())
                    .bankAccount(bankAccount)
                    .category(newCategory)
                    .budget(newBudget)
                    .userAccount(expenseOwner)
                    .isIncome(expenseDto.isIncome())
                    .goal(expenseToModify.getGoal())
                    .build();

            expenseRepository.save(modifiedExpense);
            return expenseDto;
        } else {
            throw new PermissionDeniedException();
        }
    }

    //todo: napisac test na usuwanie wydatku i na usuwanie przychodu
    @Transactional
    public ExpenseDto.MinimalDto deleteExpense(long budgetId, long masterCatId, long catId, long expId) {
        Expense expenseToDelete = expenseRepository.findExpense(getLoggedUsername(), budgetId, masterCatId, catId, expId)
                .orElseThrow(() -> new EntityNotFoundException(Expense.class));

        BankAccount bankAccount = bankAccountRepository.findBankAccount(getLoggedUsername(), budgetId, expenseToDelete.getBankAccount().getId())
                .orElseThrow(() -> new EntityNotFoundException(BankAccount.class));

        UserAccount expenseOwner = expenseToDelete.getUserAccount();

        if (expenseOwner.getEmail().equals(getLoggedUsername())) {
            if (expenseToDelete.isIncome()) {
                bankAccount.setMeans(bankAccount.getMeans().subtract(expenseToDelete.getAmount()));
            } else {
                bankAccount.setMeans(bankAccount.getMeans().add(expenseToDelete.getAmount()));
            }

            bankAccountRepository.save(bankAccount);
            expenseRepository.delete(expenseToDelete);
            return expenseConverter.toMinimalDTO(expenseToDelete);
        } else {
            throw new PermissionDeniedException();
        }
    }
}
