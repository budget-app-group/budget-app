package pl.poznan.uam.service;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;
import pl.poznan.uam.converters.UserAccountConverter;
import pl.poznan.uam.dto.LoginDto;
import pl.poznan.uam.dto.RegisterDto;
import pl.poznan.uam.dto.UserAccountDto;
import pl.poznan.uam.exceptions.EntityNotFoundException;
import pl.poznan.uam.model.*;
import pl.poznan.uam.repository.BankAccountRepository;
import pl.poznan.uam.repository.BudgetRepository;
import pl.poznan.uam.repository.UserRepository;
import pl.poznan.uam.repository.VerificationTokenRepository;
import pl.poznan.uam.security.JwtAuthenticationResponse;
import pl.poznan.uam.security.JwtTokenProvider;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.HashMap;
import java.util.Map;

import static pl.poznan.uam.utils.ExceptionEnums.*;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class UserAuthService {

    private final AuthenticationManager authenticationManager;
    private final UserRepository userRepository;
    private final BudgetRepository budgetRepository;
    private final BankAccountRepository bankAccountRepository;
    private final PasswordEncoder passwordEncoder;
    private final JwtTokenProvider tokenProvider;
    private final VerificationTokenRepository verificationTokenRepository;
    private final EmailSenderService emailSenderService;
    private final UserAccountConverter userAccountConverter;
    private final MasterCategoryService masterCategoryService;
    private final Environment env;

    @Transactional
    public UserAccountDto.FullDto registerUser(RegisterDto registerDto) {
        if (userRepository.existsByEmail(registerDto.getEmail().toLowerCase())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, EMAIL_EXISTS.getMessage());
        }

        UserAccount user = new UserAccount.Builder()
                .name(registerDto.getName())
                .surname(registerDto.getSurname())
                .email(registerDto.getEmail().toLowerCase())
                .password(passwordEncoder.encode(registerDto.getPassword()))
                .isEnabled(false)
                .firstTime(true)
                .build();

        //create personal budget for new user
        Budget budget = new Budget.Builder()
                .name("Budżet osobisty")
                .admin(user.getEmail())
                .build();
        userRepository.save(user);
        budget.addUserAccount(user);
        budgetRepository.save(budget);
        masterCategoryService.createDefaultMasterCategories(budget);
        BankAccount defaultBankAcc1 = new BankAccount.Builder()
                .name("Gotówka")
                .bankName(BankAccountBankName.CASH)
                .type("Gotówka")
                .means(BigDecimal.ZERO)
                .startingAmount(BigDecimal.ZERO)
                .budget(budget)
                .build();
        bankAccountRepository.save(defaultBankAcc1);
        generateVerificationToken(user);
        return userAccountConverter.toFullDTO(user);
    }

    public void generateVerificationToken(UserAccount user) {
        VerificationToken verificationToken = new VerificationToken(user, 1);
        verificationTokenRepository.save(verificationToken);

        Map<String, Object> templateInterpolation = new HashMap<>();
        templateInterpolation.put("name", user.getName());
        templateInterpolation.put("surname", user.getSurname());
        templateInterpolation.put("url", "https://" + env.getProperty("app.IP-Address")
                + "/confirm-email?token=" + verificationToken.getVerificationToken());

        Mail mailToSend = new Mail.Builder()
                .templateName("confirmationMail.html")
                .recipient(user.getEmail())
                .topic("Potwierdź adres email")
                .templateOptions(templateInterpolation)
                .build();

        emailSenderService.sendMail(mailToSend);
    }

    @Transactional
    public ResponseEntity<?> resendEmail(LoginDto.EmailDto loginDto) {
        UserAccount user = userRepository.findByEmailIgnoreCase(loginDto.getEmail())
                .orElseThrow(() -> new EntityNotFoundException(UserAccount.class));
        if (!user.isEnabled()) {
            VerificationToken oldToken = verificationTokenRepository.findByUserAccount(user)
                    .orElseThrow(() -> new EntityNotFoundException(UserAccount.class));
            verificationTokenRepository.delete(oldToken);
            generateVerificationToken(user);
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ACCOUNT_VERIFIED.getMessage());
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Transactional
    public ResponseEntity<?> confirmEmail(String verificationToken){
        VerificationToken token = verificationTokenRepository.findByVerificationToken(verificationToken)
                .orElseThrow(() -> new EntityNotFoundException(VerificationToken.class));
        if (LocalDateTime.now().isBefore(token.getExpirationDate())){
            UserAccount userAccount = userRepository.findByEmailIgnoreCase(token.getUserAccount().getEmail())
                    .orElseThrow(() -> new EntityNotFoundException(UserAccount.class));
            userAccount.setEnabled(true);
            userRepository.save(userAccount);
            verificationTokenRepository.delete(token);
        }else{
            verificationTokenRepository.delete(token);
            throw new EntityNotFoundException(VerificationToken.class);
        }
        return new ResponseEntity<>(EMAIL_VERIFIED.getMessage(), HttpStatus.OK);
    }

    @Transactional
    public Map<String, Object> authenticateUser(LoginDto loginDto) {
        UserAccount userAccount = userRepository.findByEmailIgnoreCase(loginDto.getEmail())
                .orElseThrow(() -> new EntityNotFoundException(UserAccount.class));
        if (userAccount.isEnabled()) {
            Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(loginDto.getEmail(), loginDto.getPassword()));

            SecurityContextHolder.getContext().setAuthentication(authentication);
            String jwt = tokenProvider.generateToken(authentication);
            Map<String, Object> authenticatedUserObjectList = new HashMap<>();
            authenticatedUserObjectList.put("user", userAccountConverter.toFullDTO(userAccount));
            authenticatedUserObjectList.put("firstTime", userAccount.isFirstTime());
            authenticatedUserObjectList.put("token", new JwtAuthenticationResponse(jwt));

            if (userAccount.isFirstTime()) {
                userAccount.setFirstTime(false);
                userRepository.save(userAccount);
            }

            userAccount.setLastLogin(LocalDateTime.now().atZone(ZoneId.of("Europe/Berlin")).toLocalDateTime());
            userRepository.save(userAccount);

            return authenticatedUserObjectList;
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ACCOUNT_NOT_VERIFIED.getMessage());
        }
    }

    public UserAccountDto.MinimalDto resetPassword(LoginDto.EmailDto loginDto) {
        UserAccount user = userRepository.findByEmailIgnoreCase(loginDto.getEmail())
                .orElseThrow(() -> new EntityNotFoundException(UserAccount.class));

        VerificationToken verificationToken = new VerificationToken(user, 1);
        verificationTokenRepository.save(verificationToken);

        Map<String, Object> templateInterpolation = new HashMap<>();
        templateInterpolation.put("url", "https://" + env.getProperty("app.IP-Address")
                + "/confirm-reset-password?token=" + verificationToken.getVerificationToken());

        Mail mailToSend = new Mail.Builder()
                .templateName("passwordReset.html")
                .recipient(user.getEmail())
                .topic("Zmień swoje hasło")
                .templateOptions(templateInterpolation)
                .build();

        emailSenderService.sendMail(mailToSend);

        return userAccountConverter.toMinimalDTO(user);
    }

    @Transactional
    public UserAccountDto.MinimalDto confirmResetPassword(LoginDto.PasswordResetDto passwordResetDto, String verificationToken) {

        VerificationToken token = verificationTokenRepository.findByVerificationToken(verificationToken)
                .orElseThrow(() -> new EntityNotFoundException(VerificationToken.class));

        if (LocalDateTime.now().isBefore(token.getExpirationDate())) {
            UserAccount userAccount = userRepository.findByEmailIgnoreCase(token.getUserAccount().getEmail())
                    .orElseThrow(() -> new EntityNotFoundException(UserAccount.class));

            UserAccount updatedUser = new UserAccount.Builder()
                    .id(userAccount.getId())
                    .email(userAccount.getEmail())
                    .name(userAccount.getName())
                    .isEnabled(userAccount.isEnabled())
                    .surname(userAccount.getSurname())
                    .password(passwordEncoder.encode(passwordResetDto.getPassword()))
                    .budgets(userAccount.getBudgets())
                    .expenses(userAccount.getExpenses())
                    .build();
            userRepository.save(updatedUser);
            verificationTokenRepository.delete(token);
            return userAccountConverter.toMinimalDTO(userAccount);
        } else {
            verificationTokenRepository.delete(token);
            throw new EntityNotFoundException(VerificationToken.class);
        }
    }
}
