package pl.poznan.uam.service;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.poznan.uam.converters.*;
import pl.poznan.uam.dto.*;
import pl.poznan.uam.exceptions.EntityNotFoundException;
import pl.poznan.uam.model.Budget;
import pl.poznan.uam.model.Category;
import pl.poznan.uam.model.MasterCategory;
import pl.poznan.uam.repository.BudgetRepository;
import pl.poznan.uam.repository.CategoryRepository;
import pl.poznan.uam.repository.ExpenseRepository;
import pl.poznan.uam.repository.MasterCategoryRepository;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static pl.poznan.uam.utils.UtilityFunctions.getLoggedUsername;

@Service
@Transactional
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class BudgetAnalysisService {

    private final CategoryRepository categoryRepository;
    private final MasterCategoryRepository masterCategoryRepository;
    private final BudgetRepository budgetRepository;
    private final ExpenseRepository expenseRepository;
    private final ExpenseConverter expenseConverter;
    private final MasterCategoryConverter masterCategoryConverter;
    private final CategoryConverter categoryConverter;
    private final BudgetConverter budgetConverter;
    private final PeriodicExpensesConverter periodicExpensesConverter;

    public List<ExpenseDto.MinimalDto> getFilteredExpenses(long budgetId, MasterCategory masterCategory, LocalDateTime dateFrom, LocalDateTime dateTo) {
        return expenseRepository.findAllExpensesFromMasterCategory(getLoggedUsername(), budgetId, masterCategory.getId())
                .stream()
                .map(expenseConverter::toMinimalDTO)
                .filter(exp -> LocalDateTime.ofInstant(Instant.ofEpochMilli(exp.getExpenseDate()), ZoneId.of("Europe/Berlin")).isBefore(dateTo) &&
                        LocalDateTime.ofInstant(Instant.ofEpochMilli(exp.getExpenseDate()), ZoneId.of("Europe/Berlin")).isAfter(dateFrom))
                .collect(Collectors.toList());
    }

    public BigDecimal getSumOf(List<ExpenseDto.MinimalDto> expenses, boolean income, String mode){
        BigDecimal sum = BigDecimal.ZERO;
        BigDecimal tmpSum = BigDecimal.ZERO;

        if (mode.equals("balance")){
            BigDecimal tmpSumOfIncomes = expenses.stream()
                    .filter(exp -> exp.isIncome() == !income)
                    .map(ExpenseDto.MinimalDto::getAmount)
                    .reduce(BigDecimal.ZERO, BigDecimal::add);
            BigDecimal tmpSumOfExpenses = expenses.stream()
                    .filter(exp -> exp.isIncome() == income)
                    .map(ExpenseDto.MinimalDto::getAmount)
                    .reduce(BigDecimal.ZERO, BigDecimal::add);
            tmpSum = tmpSumOfIncomes.subtract(tmpSumOfExpenses);
        } else {
            tmpSum = expenses.stream()
                .filter(exp -> exp.isIncome() == income)
                .map(ExpenseDto.MinimalDto::getAmount)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
        }
        sum = sum.add(tmpSum);
        return sum.setScale(2, BigDecimal.ROUND_HALF_UP);
    }

    public List<MasterCategoryDto.AnalysisMasterCatDto> summaryOfExpensesInMasterCatsOfBudget(long budgetId, long from, long to) {
        LocalDateTime dateFrom = LocalDateTime.ofInstant(Instant.ofEpochMilli(from), ZoneId.of("Europe/Berlin"));
        LocalDateTime dateTo = LocalDateTime.ofInstant(Instant.ofEpochMilli(to), ZoneId.of("Europe/Berlin"));
        List<MasterCategory> masterCategories = masterCategoryRepository.findMasterCategories(getLoggedUsername(), budgetId);
        List<MasterCategoryDto.AnalysisMasterCatDto> retList = new ArrayList<>();

        for (MasterCategory masterCategory : masterCategories){
            List<CategoryDto.AnalysisCategoryDto> categoryDtoList = new ArrayList<>();
            List<Category> categories = categoryRepository.findByMasterCategory_Id(masterCategory.getId());

            List<ExpenseDto.MinimalDto> expensesFiltered =
                    expenseRepository.findAllExpensesFromMasterCategory(getLoggedUsername(), budgetId, masterCategory.getId())
                            .stream()
                            .map(expenseConverter::toMinimalDTO)
                            .filter(exp -> LocalDateTime.ofInstant(Instant.ofEpochMilli(exp.getExpenseDate()), ZoneId.of("Europe/Berlin")).isBefore(dateTo) &&
                                    LocalDateTime.ofInstant(Instant.ofEpochMilli(exp.getExpenseDate()), ZoneId.of("Europe/Berlin")).isAfter(dateFrom) && !exp.isIncome())
                            .collect(Collectors.toList());
            BigDecimal sumInMasterCat = expensesFiltered.stream()
                    .map(ExpenseDto.MinimalDto::getAmount)
                    .reduce(BigDecimal.ZERO, BigDecimal::add);

            for (Category category : categories){
                List<ExpenseDto.MinimalDto> expensesInCatFiltered =
                        expenseRepository.findAllExpensesFromCategory(getLoggedUsername(), budgetId, category.getId())
                                .stream()
                                .map(expenseConverter::toMinimalDTO)
                                .filter(exp -> LocalDateTime.ofInstant(Instant.ofEpochMilli(exp.getExpenseDate()), ZoneId.of("Europe/Berlin")).isBefore(dateTo) &&
                                        LocalDateTime.ofInstant(Instant.ofEpochMilli(exp.getExpenseDate()), ZoneId.of("Europe/Berlin")).isAfter(dateFrom) && !exp.isIncome())
                                .collect(Collectors.toList());
                BigDecimal sumInCat = expensesInCatFiltered.stream().map(ExpenseDto.MinimalDto::getAmount).reduce(BigDecimal.ZERO, BigDecimal::add);
                categoryDtoList.add(categoryConverter.toAnalysisCategoryDto(category, sumInCat));
            }
            retList.add(masterCategoryConverter.toAnalysisMasterCatDto(masterCategory, sumInMasterCat, categoryDtoList));
        }
        return retList;
    }

    public  List<BudgetAnalysisDto> summaryOfExpensesInMasterCatsOfBudget2(long budgetId, long from, long to) {
        LocalDateTime dateFrom = LocalDateTime.ofInstant(Instant.ofEpochMilli(from), ZoneId.of("Europe/Berlin"));
        LocalDateTime dateTo = LocalDateTime.ofInstant(Instant.ofEpochMilli(to), ZoneId.of("Europe/Berlin"));
        List<MasterCategory> masterCategories = masterCategoryRepository.findMasterCategories(getLoggedUsername(), budgetId);
        List<BudgetAnalysisDto> resutlList = new ArrayList<>();

        for( MasterCategory masterCategory : masterCategories){
            resutlList.add(expenseRepository.findSumOfExpenseInMasterCat2(getLoggedUsername(),budgetId, masterCategory.getId(), dateFrom, dateTo));
        }
        return resutlList;
    }

    public BudgetDto.BalanceDto budgetBalance(long budgetId, long from, long to) {
        LocalDateTime dateFrom = LocalDateTime.ofInstant(Instant.ofEpochMilli(from), ZoneId.of("Europe/Berlin"));
        LocalDateTime dateTo = LocalDateTime.ofInstant(Instant.ofEpochMilli(to), ZoneId.of("Europe/Berlin"));
        Budget budget = budgetRepository.findBudget(getLoggedUsername(), budgetId)
                .orElseThrow(() -> new EntityNotFoundException(Budget.class));
        List<MasterCategory> masterCategories = masterCategoryRepository.findMasterCategories(getLoggedUsername(), budgetId);

        BigDecimal sumOfExpenses = BigDecimal.ZERO;
        BigDecimal sumOfIncomes = BigDecimal.ZERO;
        BigDecimal balance = BigDecimal.ZERO;

        for (MasterCategory masterCategory : masterCategories){
            List<ExpenseDto.MinimalDto> expensesFiltered = getFilteredExpenses(budgetId, masterCategory, dateFrom, dateTo);
            sumOfIncomes = sumOfIncomes.add(getSumOf(expensesFiltered, true, null))
                    .setScale(2, BigDecimal.ROUND_HALF_UP);
            sumOfExpenses = sumOfExpenses.add(getSumOf(expensesFiltered, false, null))
                    .setScale(2, BigDecimal.ROUND_HALF_UP);
        }

        balance = balance.add(sumOfIncomes).setScale(2, BigDecimal.ROUND_HALF_UP);
        balance = balance.subtract(sumOfExpenses).setScale(2, BigDecimal.ROUND_HALF_UP);
        return budgetConverter.toBalanceDto(budget.getName(), sumOfExpenses, sumOfIncomes, balance);
    }

    public BudgetDto.AverageExpenditureDto averageExpenditure(long budgetId, long from, long to) {
        LocalDateTime dateFrom = LocalDateTime.ofInstant(Instant.ofEpochMilli(from), ZoneId.of("Europe/Berlin"));
        LocalDateTime dateTo = LocalDateTime.ofInstant(Instant.ofEpochMilli(to), ZoneId.of("Europe/Berlin"));
        List<MasterCategory> masterCategories = masterCategoryRepository.findMasterCategories(getLoggedUsername(), budgetId);

        BigDecimal dailyAverage;
        BigDecimal weeklyAverage;
        BigDecimal monthlyAverage;
        BigDecimal sumOfExpenses = BigDecimal.ZERO;

        for (MasterCategory masterCategory : masterCategories){
            List<ExpenseDto.MinimalDto> expensesFiltered = getFilteredExpenses(budgetId, masterCategory, dateFrom, dateTo);

            sumOfExpenses = sumOfExpenses.add(expensesFiltered.stream()
                    .filter(exp -> !exp.isIncome())
                    .map(ExpenseDto.MinimalDto::getAmount)
                    .reduce(BigDecimal.ZERO, BigDecimal::add));
        }
        Period between = Period.between(dateFrom.toLocalDate(), dateTo.toLocalDate());
        dailyAverage = sumOfExpenses.divide(new BigDecimal(
                (between.getDays() == 0 ? 1 : between.getDays())
        ), 2);
        weeklyAverage = sumOfExpenses.divide(new BigDecimal(
                ChronoUnit.WEEKS.between(dateFrom, dateTo) == 0 ? 1 : ChronoUnit.WEEKS.between(dateFrom, dateTo)
        ), 2);
        monthlyAverage = sumOfExpenses.divide(new BigDecimal(
                (between.getYears() == 0 ? 1 : between.getYears())
        ), 2);
        return budgetConverter.toAverageExpenditureDto(dailyAverage, weeklyAverage, monthlyAverage);
    }

    public List<MasterCategoryDto.ExceedingMasterCatAllocationDto> checkIfMasterCatAllocationExceeded(long budgetId) {
        List<MasterCategory> masterCategories = masterCategoryRepository.findMasterCategories(getLoggedUsername(), budgetId);
        List<MasterCategoryDto.ExceedingMasterCatAllocationDto> retList = new ArrayList<>();

        BigDecimal sumOfExpenses = BigDecimal.ZERO;
        BigDecimal sumOfIncomes = BigDecimal.ZERO;
        HashMap<MasterCategoryDto.MinimalDto, BigDecimal> sumInMaster = new HashMap<>();

        for (MasterCategory masterCategory : masterCategories) {
            List<ExpenseDto.MinimalDto> expensesFiltered = expenseConverter.listToMinimalDTO(
                    expenseRepository.findAllExpensesFromMasterCategory(getLoggedUsername(), budgetId, masterCategory.getId()));
            BigDecimal masterCatSum = getSumOf(expensesFiltered, false, null);
            sumOfIncomes = sumOfIncomes.add(getSumOf(expensesFiltered, true, null))
                    .setScale(2, BigDecimal.ROUND_HALF_UP);
            sumOfExpenses = sumOfExpenses.add(masterCatSum)
                    .setScale(2, BigDecimal.ROUND_HALF_UP);
            sumInMaster.put(masterCategoryConverter.toMinimalDto(masterCategory), masterCatSum);
//            BigDecimal masterCatSum = expensesFiltered.stream()
//                    .filter(exp -> !exp.isIncome())
//                    .map(expenseConverter::toMinimalDTO)
//                    .map(ExpenseDto.MinimalDto::getAmount)
//                    .reduce(BigDecimal.ZERO, BigDecimal::add);
//            sumInMaster.put(masterCategoryConverter.toMinimalDto(masterCategory), masterCatSum);
//            sumOfExpenses = sumOfExpenses.add(masterCatSum);
        }

        for (Map.Entry<MasterCategoryDto.MinimalDto, BigDecimal> entry : sumInMaster.entrySet()){
            boolean exceeded = false;
            BigDecimal allocationAmount = sumOfIncomes
                    .multiply(new BigDecimal(entry.getKey().getBudgetAllocation()))
                    .divide(new BigDecimal(100), BigDecimal.ROUND_HALF_UP)
                    .setScale(2, BigDecimal.ROUND_HALF_UP);
            BigDecimal balance = allocationAmount.subtract(entry.getValue()).setScale(4);
            if (entry.getValue().intValue() > allocationAmount.intValue()){
                exceeded = true;
            }
            retList.add(masterCategoryConverter.toExceedingMasterCatAllocationDto(
                    entry.getKey().getName(),
                    entry.getKey().getBudgetAllocation(),
                    allocationAmount.intValue(),
                    balance.intValue(),
                    entry.getValue().intValue(),
                    exceeded));
        }
        return retList;
    }

    public List<PeriodicExpensesDto.SumOfExpensesDto> periodicExpensesSum(long budgetId, long from, long to, String mode) {
        boolean flg = false;
        if  (mode.equals("income")){
            flg = true;
        }

        LocalDateTime dateFrom = LocalDateTime.ofInstant(Instant.ofEpochMilli(from), ZoneId.of("Europe/Berlin"));
        LocalDateTime dateTo = LocalDateTime.ofInstant(Instant.ofEpochMilli(to), ZoneId.of("Europe/Berlin"));
        List<PeriodicExpensesDto.SumOfExpensesDto> resultList = new ArrayList<>();

        List<ExpenseDto.MinimalDto> expenses = expenseRepository.findAllExpenses(getLoggedUsername(), budgetId)
                .stream()
                .map(expenseConverter::toMinimalDTO)
                .collect(Collectors.toList());

        long daysBetween = ChronoUnit.DAYS.between(dateFrom, dateTo);
        LocalDateTime tmpDate = dateFrom;
        List<LocalDateTime> dates = new ArrayList<>();

        if (daysBetween <= 31) {
            while (tmpDate.isBefore(dateTo) || tmpDate.toLocalDate().isEqual(dateTo.toLocalDate())) {
                dates.add(tmpDate);
                tmpDate = tmpDate.plusDays(1);
            }
            for (LocalDateTime day : dates) {
                resultList.add(periodicExpensesConverter.toDailyExpensesDto(day,
                        getSumOf(expenses.stream()
                                .filter(exp -> LocalDateTime.ofInstant(Instant.ofEpochMilli(exp.getExpenseDate()), ZoneId.of("Europe/Berlin")).toLocalDate().isEqual(day.toLocalDate()))
                                .collect(Collectors.toList()), flg, mode), mode));
            }
        } else {
            dateTo = dateTo.with(TemporalAdjusters.lastDayOfMonth());
            while (tmpDate.isBefore(dateTo)) {
                dates.add(tmpDate);
                tmpDate = tmpDate.plusMonths(1);
            }
            for (LocalDateTime month : dates) {
                resultList.add(periodicExpensesConverter.toDailyExpensesDto(month,
                        getSumOf(expenses.stream()
                                .filter(exp -> (LocalDateTime.ofInstant(Instant.ofEpochMilli(exp.getExpenseDate()), ZoneId.of("Europe/Berlin")).toLocalDate().getMonth().equals(month.getMonth()) &&
                                        LocalDateTime.ofInstant(Instant.ofEpochMilli(exp.getExpenseDate()), ZoneId.of("Europe/Berlin")).toLocalDate().getYear() == month.getYear()))
                                .collect(Collectors.toList()), flg, mode), mode));
            }
        }
        return resultList;
    }
}
