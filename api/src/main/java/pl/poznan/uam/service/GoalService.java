package pl.poznan.uam.service;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.poznan.uam.converters.ExpenseConverter;
import pl.poznan.uam.converters.GoalConverter;
import pl.poznan.uam.dto.ExpenseDto;
import pl.poznan.uam.dto.GoalDto;
import pl.poznan.uam.exceptions.EntityNotFoundException;
import pl.poznan.uam.exceptions.PermissionDeniedException;
import pl.poznan.uam.model.Budget;
import pl.poznan.uam.model.Goal;
import pl.poznan.uam.repository.BudgetRepository;
import pl.poznan.uam.repository.ExpenseRepository;
import pl.poznan.uam.repository.GoalRepository;
import pl.poznan.uam.repository.UserRepository;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;
import java.util.stream.Collectors;

import static pl.poznan.uam.utils.UtilityFunctions.getLoggedUsername;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class GoalService {
    private final GoalRepository goalRepository;
    private final GoalConverter goalConverter;
    private final ExpenseRepository expenseRepository;
    private final ExpenseConverter expenseConverter;
    private final BudgetRepository budgetRepository;
    private final UserRepository userRepository;


    public List<GoalDto.FullDto> findAllGoalsFromBudget(long budgetId) {
        Budget budget = budgetRepository.findBudget(getLoggedUsername(), budgetId)
                .orElseThrow(() -> new EntityNotFoundException(Budget.class));
        return goalRepository.findAllGoalsFromBudget(getLoggedUsername(), budgetId)
                .stream()
                .map(goal -> goalConverter.toFullDto(budget, goal))
                .collect(Collectors.toList());
    }

    public GoalDto.FullDto findGoal(long budgetId, long goalId) {
        Budget budget = budgetRepository.findBudget(getLoggedUsername(), budgetId)
                .orElseThrow(() -> new EntityNotFoundException(Budget.class));

        Goal goal = goalRepository.findGoalInBudget(getLoggedUsername(), budgetId, goalId)
                .orElseThrow(() -> new EntityNotFoundException(Goal.class));

        return goalConverter.toFullDto(budget, goal);
    }

    public GoalDto.GoalDonationDto findAllDonationsToGoal(long budgetId, long goalId) {
        Goal goal = goalRepository.findGoalInBudget(getLoggedUsername(), budgetId, goalId)
                .orElseThrow(() -> new EntityNotFoundException(Goal.class));
        List<ExpenseDto.FullDto> donations = expenseRepository.findAllDonationsToGoal(getLoggedUsername(), budgetId, goalId)
                .stream()
                .map(expenseConverter::toFullDTO)
                .collect(Collectors.toList());
        return goalConverter.toGoalDonationDto(goal, donations);
    }

    public GoalDto.PostDto createGoal(long budgetId, GoalDto.PostDto goalDto) {
        Budget budget = budgetRepository.findBudget(getLoggedUsername(), budgetId)
                .orElseThrow(() -> new EntityNotFoundException(Budget.class));
        if (budget.getAdmin().equals(getLoggedUsername())) {
            LocalDateTime targetDate = LocalDateTime.ofInstant(Instant.ofEpochMilli(goalDto.getTargetDate()),
                    ZoneId.of("Europe/Berlin"));

            Goal goalToSave = new Goal.Builder()
                    .name(goalDto.getName())
                    .collectedAmount(goalDto.getCollectedAmount())
                    .targetAmount(goalDto.getTargetAmount())
                    .targetDate(targetDate)
                    .budget(budget)
                    .build();

            goalRepository.save(goalToSave);
            return goalConverter.toPostDto(goalToSave);
        } else {
            throw new PermissionDeniedException();
        }
    }

    public GoalDto.PostDto modifyGoal(long budgetId, long goalId, GoalDto.PostDto goalDto) {
        Budget budget = budgetRepository.findBudget(getLoggedUsername(), budgetId)
                .orElseThrow(() -> new EntityNotFoundException(Budget.class));
        Goal goalToModify = goalRepository.findGoal(getLoggedUsername(), goalId)
                .orElseThrow(() -> new EntityNotFoundException(Budget.class));
        if (budget.getAdmin().equals(getLoggedUsername())) {
            LocalDateTime targetDate = LocalDateTime.ofInstant(Instant.ofEpochMilli(goalDto.getTargetDate()),
                    ZoneId.of("Europe/Berlin"));
            Goal modifiedGoal = new Goal.Builder()
                    .id(goalToModify.getId())
                    .name(goalDto.getName())
                    .targetDate(targetDate)
                    .targetAmount(goalDto.getTargetAmount())
                    .collectedAmount(goalDto.getCollectedAmount())
                    .budget(budget)
                    .build();
            goalRepository.save(modifiedGoal);

            return goalDto;
        } else {
            throw new PermissionDeniedException();
        }
    }

    public GoalDto.FullDto deleteGoal(long budgetId, long goalId) {
        Budget budget = budgetRepository.findBudget(getLoggedUsername(), budgetId)
                .orElseThrow(() -> new EntityNotFoundException(Budget.class));
        if (budget.getAdmin().equals(getLoggedUsername())) {
            Goal goalToDelete = goalRepository.findGoalInBudget(getLoggedUsername(), budgetId, goalId)
                    .orElseThrow(() -> new EntityNotFoundException(Goal.class));
            goalRepository.delete(goalToDelete);
            return goalConverter.toFullDto(budget, goalToDelete);
        } else {
            throw new PermissionDeniedException();
        }
    }
}
