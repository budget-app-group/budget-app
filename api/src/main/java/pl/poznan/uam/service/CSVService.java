package pl.poznan.uam.service;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import pl.poznan.uam.dto.ExpenseDto;
import pl.poznan.uam.exceptions.EntityNotFoundException;
import pl.poznan.uam.model.BankAccount;
import pl.poznan.uam.model.Budget;
import pl.poznan.uam.model.Category;
import pl.poznan.uam.model.MasterCategory;
import pl.poznan.uam.repository.BankAccountRepository;
import pl.poznan.uam.repository.BudgetRepository;
import pl.poznan.uam.repository.CategoryRepository;
import pl.poznan.uam.repository.MasterCategoryRepository;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static pl.poznan.uam.utils.UtilityFunctions.getLoggedUsername;
import static pl.poznan.uam.utils.csv.INGParser.parseING;
import static pl.poznan.uam.utils.csv.PKOParser.parsePKO;
import static pl.poznan.uam.utils.csv.SantanderParser.parseSANTANDER;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CSVService {

    private final BudgetRepository budgetRepository;
    private final BankAccountRepository bankAccountRepository;
    private final CategoryRepository categoryRepository;
    private final MasterCategoryRepository masterCategoryRepository;


    public List<ExpenseDto.CSVDto> importExpensesFromCsv(MultipartFile file, long budgetId, long bankAccId) throws IOException {

        Budget budget = budgetRepository.findBudget(getLoggedUsername(), budgetId)
                .orElseThrow(() -> new EntityNotFoundException(Budget.class));

        BankAccount bankAccount = bankAccountRepository.findBankAccount(getLoggedUsername(), budgetId, bankAccId)
                .orElseThrow(() -> new EntityNotFoundException(BankAccount.class));

        Category category;
        try {
            List<Category> categories = categoryRepository.findFirstCategoryByName(getLoggedUsername(), budgetId, "Inne");
            category = categories.get(0);

        } catch (IndexOutOfBoundsException e1) {
            try {
                List<MasterCategory> masterCategories = masterCategoryRepository.findFirstMasterCategoryByName(getLoggedUsername(), budgetId, "Inne");
                MasterCategory masterCategory = masterCategories.get(0);
                category = new Category.Builder()
                        .name("Inne")
                        .color("#919191")
                        .masterCategory(masterCategory)
                        .build();
                category = categoryRepository.save(category);
            } catch (IndexOutOfBoundsException e2) {
                MasterCategory masterCategory = new MasterCategory.Builder()
                        .budget(budget)
                        .budgetAllocation(0)
                        .name("Inne")
                        .color("#919191")
                        .build();
                category = new Category.Builder()
                        .name("Inne")
                        .color("#919191")
                        .masterCategory(masterCategory)
                        .build();
                masterCategoryRepository.save(masterCategory);
                category = categoryRepository.save(category);
            }
        }

        MasterCategory masterCategory = category.getMasterCategory();

        List<ExpenseDto.CSVDto> expenseList = new ArrayList<>();

        switch (bankAccount.getBankName()) {
            case PKO:
                expenseList = parsePKO(file);
                break;
            case SANTANDER:
                expenseList = parseSANTANDER(file);
                break;
            case ING:
                expenseList = parseING(file);
                break;
            default:
                throw new ResponseStatusException(
                        HttpStatus.BAD_REQUEST, "Sorry. We don't support CSV import for this bank yet");
        }

        for (ExpenseDto.CSVDto expense : expenseList) {
            expense.setCategoryId(category.getId());
            expense.setMasterCatId(masterCategory.getId());
            expense.setBankAccountId(bankAccount.getId());
        }

        return expenseList;
    }

}
