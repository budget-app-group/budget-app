package pl.poznan.uam.service;


import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.poznan.uam.converters.BankAccountConverter;
import pl.poznan.uam.converters.BudgetConverter;
import pl.poznan.uam.dto.BankAccountDto;
import pl.poznan.uam.exceptions.EntityNotFoundException;
import pl.poznan.uam.exceptions.PermissionDeniedException;
import pl.poznan.uam.model.BankAccount;
import pl.poznan.uam.model.Budget;
import pl.poznan.uam.repository.BankAccountRepository;
import pl.poznan.uam.repository.BudgetRepository;

import javax.transaction.Transactional;
import java.util.List;

import static pl.poznan.uam.utils.UtilityFunctions.getLoggedUsername;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class BankAccountService {

    private final BankAccountConverter bankAccountConverter;
    private final BudgetRepository budgetRepository;
    private final BudgetConverter budgetConverter;
    private final BankAccountRepository bankAccountRepository;

    @Transactional
    public BankAccountDto.MinimalDto createBankAccount(long budgetId, BankAccountDto.MinimalDto bankAccMinimalDto) {
        Budget budget = budgetRepository.findBudget(getLoggedUsername(), budgetId)
                .orElseThrow(() -> new EntityNotFoundException(Budget.class));
        if (budget.getAdmin().equals(getLoggedUsername())) {
            BankAccount bankAccount = new BankAccount.Builder()
                    .name(bankAccMinimalDto.getName())
                    .bankName(bankAccMinimalDto.getBankName())
                    .type(bankAccMinimalDto.getType())
                    .means(bankAccMinimalDto.getStartingAmount())
                    .startingAmount(bankAccMinimalDto.getStartingAmount())
                    .budget(budget)
                    .build();

            bankAccountRepository.save(bankAccount);
            return bankAccountConverter.toMinimalDTO(bankAccount);
        } else {
            throw new PermissionDeniedException();
        }
    }

    public BankAccountDto.FullDto findBankAccount(long budgetId, long id) {
        Budget budget = budgetRepository.findBudget(getLoggedUsername(), budgetId)
                .orElseThrow(() -> new EntityNotFoundException(Budget.class));

        return bankAccountConverter.toFullDTO(budget, bankAccountRepository.findBankAccount(getLoggedUsername(), budgetId, id)
                .orElseThrow(() -> new EntityNotFoundException(BankAccount.class)));
    }

    public List<BankAccountDto.FullDto> findAllBankAccounts(long budgetId) {
        Budget budget = budgetRepository.findBudget(getLoggedUsername(), budgetId)
                .orElseThrow(() -> new EntityNotFoundException(Budget.class));

        return bankAccountConverter.listToFullDTO(budget, bankAccountRepository.findAllBankAccounts(getLoggedUsername(), budgetId));
    }

    @Transactional
    public BankAccountDto.MinimalDto deleteBankAccount(long budgetId, long id) {
        BankAccount bankAccountToDelete = bankAccountRepository.findBankAccount(getLoggedUsername(), budgetId, id)
                .orElseThrow(() -> new EntityNotFoundException(BankAccount.class));
        if (bankAccountToDelete.getBudget().getAdmin().equals(getLoggedUsername())) {
            bankAccountRepository.delete(bankAccountToDelete);
            return bankAccountConverter.toMinimalDTO(bankAccountToDelete);
        } else {
            throw new PermissionDeniedException();
        }
    }

    @Transactional
    public BankAccountDto.MinimalDto modifyBankAccount(long budgetId, long id, BankAccountDto.MinimalDto bankAccMinimalDto) {
        BankAccount bankAccountToModify = bankAccountRepository.findBankAccount(getLoggedUsername(), budgetId, id)
                .orElseThrow(() -> new EntityNotFoundException(BankAccount.class));
        if (bankAccountToModify.getBudget().getAdmin().equals(getLoggedUsername())) {
            bankAccountRepository.save(bankAccountConverter.updateBankAcc(bankAccountToModify, bankAccMinimalDto));
            return bankAccMinimalDto;
        } else {
            throw new PermissionDeniedException();
        }

    }

}
