package pl.poznan.uam.service;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import pl.poznan.uam.converters.BudgetConverter;
import pl.poznan.uam.converters.UserAccountConverter;
import pl.poznan.uam.dto.BudgetDto;
import pl.poznan.uam.dto.UserAccountDto;
import pl.poznan.uam.exceptions.EntityNotFoundException;
import pl.poznan.uam.exceptions.PermissionDeniedException;
import pl.poznan.uam.model.*;
import pl.poznan.uam.repository.BankAccountRepository;
import pl.poznan.uam.repository.BudgetRepository;
import pl.poznan.uam.repository.UserRepository;
import pl.poznan.uam.repository.VerificationTokenRepository;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static pl.poznan.uam.utils.ExceptionEnums.ADMIN_CANNOT_LEAVE;
import static pl.poznan.uam.utils.UtilityFunctions.getLoggedUser;
import static pl.poznan.uam.utils.UtilityFunctions.getLoggedUsername;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class BudgetService {

    private final UserAccountConverter userAccountConverter;
    private final BudgetConverter budgetConverter;
    private final BudgetRepository budgetRepository;
    private final UserRepository userRepository;
    private final BankAccountRepository bankAccountRepository;
    private final MasterCategoryService masterCategoryService;
    private final VerificationTokenRepository verificationTokenRepository;
    private final EmailSenderService emailSenderService;
    private final Environment env;


    public BudgetDto.FullDto findBudgetById(long budgetId) {
        //todo: to można zapisać lepiej - lepsza sqlka?
        Budget budget = budgetRepository.findBudget(getLoggedUsername(), budgetId)
                .orElseThrow(() -> new EntityNotFoundException(Budget.class));
        UserAccount adminAccount = userRepository.findByEmailIgnoreCase(budget.getAdmin())
                .orElseThrow(() -> new EntityNotFoundException(Budget.class));
        List<UserAccount> usersInBudget = userRepository.findUsersFromBudget(budgetId);

        return budgetConverter.toFullDto(budget, adminAccount, userAccountConverter.listToFullDTO(usersInBudget));
    }


    public List<BudgetDto.FullDto> findAllBudgets() {
        List<Budget> budgetsOfUser = budgetRepository.findAllBudgets(getLoggedUsername());
        return budgetsOfUser.stream()
                .map(ug ->
                        budgetConverter.toFullDto(ug,
                                userRepository.findByEmailIgnoreCase(ug.getAdmin())
                                        .orElseThrow(() -> new EntityNotFoundException(UserAccount.class)),
                                budgetRepository.findBudgetUsers(ug.getId())
                                        .stream()
                                        .map(userAccountConverter::toFullDTO).collect(Collectors.toList())))
                .collect(Collectors.toList());
    }

    @Transactional
    public BudgetDto.PostDto createBudget(BudgetDto.MinimalDto budgetDTO) {
        UserAccount userAccount = getLoggedUser(userRepository);
        Budget budgetToSave = new Budget.Builder()
                .name(budgetDTO.getName())
                .admin(userAccount.getEmail())
                .build();

        budgetToSave.addUserAccount(userAccount);
        budgetRepository.save(budgetToSave);

        BankAccount bankAccount = new BankAccount.Builder()
                .name("Gotówka")
                .bankName(BankAccountBankName.CASH)
                .means(new BigDecimal(0.00))
                .startingAmount(new BigDecimal(0.00))
                .type("Konto główne")
                .budget(budgetToSave)
                .build();
        bankAccountRepository.save(bankAccount);
        masterCategoryService.createOtherMasterCat(budgetToSave);
        return budgetConverter.toPostDto(budgetToSave, userRepository.findByEmailIgnoreCase(budgetToSave.getAdmin())
                .orElseThrow(() -> new EntityNotFoundException(UserAccount.class)));
    }

    @Transactional
    public BudgetDto.FullDto modifyBudget(long budgetId, BudgetDto.ModifyDto budgetDTO) {
        Budget budgetToModify = budgetRepository.findBudget(getLoggedUsername(), budgetId)
                .orElseThrow(() -> new EntityNotFoundException(Budget.class));

        if (budgetToModify.getAdmin().equals(getLoggedUsername())) {
            if (userRepository.existsByEmail(budgetDTO.getAdmin())) {
                budgetToModify.setAdmin(budgetDTO.getAdmin());
                budgetRepository.save(budgetConverter.updateBudget(budgetToModify, budgetDTO));
            } else {
                throw new EntityNotFoundException(UserAccount.class);
            }
        } else {
            throw new PermissionDeniedException();
        }

        UserAccount adminAccount = userRepository.findByEmailIgnoreCase(budgetToModify.getAdmin())
                .orElseThrow(() -> new EntityNotFoundException(Budget.class));
        List<UserAccount> usersInBudget = userRepository.findUsersFromBudget(budgetId);
        return budgetConverter.toFullDto(budgetToModify, adminAccount, userAccountConverter.listToFullDTO(usersInBudget));

    }

    @Transactional
    public BudgetDto.MinimalDto deleteBudget(long budgetId) {
        Budget budgetToDelete = budgetRepository.findBudget(getLoggedUsername(), budgetId)
                .orElseThrow(() -> new EntityNotFoundException(Budget.class));
        if (budgetToDelete.getAdmin().equals(getLoggedUsername())) {
            for (UserAccount ua : budgetToDelete.getUserAccounts()) {
                ua.removeBudget(budgetToDelete);
                userRepository.save(ua);
            }
            budgetRepository.delete(budgetToDelete);
        } else {
            throw new PermissionDeniedException();
        }
        return budgetConverter.toMinimalDto(budgetToDelete);
    }

    @Transactional
    public BudgetDto.MinimalDto addUserToBudget(long budgetId, long userId) {
        Budget budget = budgetRepository.findBudget(getLoggedUsername(), budgetId)
                .orElseThrow(() -> new EntityNotFoundException(Budget.class));

        if (getLoggedUsername().equals(budget.getAdmin())) {
            UserAccount userAccountToAdd = userRepository.findById(userId)
                    .orElseThrow(() -> new EntityNotFoundException(UserAccount.class));

            budget.addUserAccount(userAccountToAdd);
            budgetRepository.save(budget);
        } else {
            throw new PermissionDeniedException();
        }
        return budgetConverter.toMinimalDto(budget);
    }

    @Transactional
    public BudgetDto.MinimalDto deleteLoggedUserFromBudget(long budgetId) {
        Budget budget = budgetRepository.findBudget(getLoggedUsername(), budgetId)
                .orElseThrow(() -> new EntityNotFoundException(Budget.class));
        UserAccount userAccount = userRepository.findByEmailIgnoreCase(getLoggedUsername())
                .orElseThrow(() -> new EntityNotFoundException(UserAccount.class));
        if (getLoggedUsername().equals(budget.getAdmin())) {
            throw new PermissionDeniedException("You cant leave as admin");
        } else if (budget.getUserAccounts().size() == 1) {
            throw new PermissionDeniedException("You cant leave as last person.");
        } else {
            budget.removeUserAccount(userAccount);
        }
        budgetRepository.save(budget);
        return budgetConverter.toMinimalDto(budget);
    }

    public List<UserAccountDto.FullDto> findAllUsersOfBudget(long budgetId) {
        UserAccount loggedUser = getLoggedUser(userRepository);
        List<UserAccount> usersOfBudget = userRepository.findUsersFromBudget(budgetId);

        if (usersOfBudget.contains(loggedUser)) {
            return userAccountConverter.listToFullDTO(usersOfBudget);
        } else {
            throw new PermissionDeniedException();
        }
//        return userAccountConverter.listToFullDTO(userRepository.findUsersFromBudget(budgetId));
    }

    @Transactional
    public UserAccountDto.MinimalDto removeUserFromBudget(long budgetId, long userId) {
        Budget budget = budgetRepository.findBudget(getLoggedUsername(), budgetId)
                .orElseThrow(() -> new EntityNotFoundException(Budget.class));
        if (budget.getAdmin().equals(getLoggedUsername())) {
            UserAccount userAccountToRemove = userRepository.findById(userId)
                    .orElseThrow(() -> new EntityNotFoundException(UserAccount.class));
            if (budget.getAdmin().equals(userAccountToRemove.getEmail())) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ADMIN_CANNOT_LEAVE.getMessage());
            }
            budget.removeUserAccount(userAccountToRemove);
            budgetRepository.save(budget);
            return userAccountConverter.toMinimalDTO(userAccountToRemove);

        } else {
            throw new PermissionDeniedException();
        }
    }

    @Transactional
    public BudgetDto.MinimalDto addUserToBudgetByEmail(long budgetId, String email) {
        Budget budget = budgetRepository.findBudget(getLoggedUsername(), budgetId)
                .orElseThrow(() -> new EntityNotFoundException(Budget.class));
        if (budget.getAdmin().equals(getLoggedUsername())) {
            UserAccount userAccountToAdd = userRepository.findByEmailIgnoreCase(email)
                    .orElseThrow(() -> new EntityNotFoundException(UserAccount.class));

            if (budget.getUserAccounts().contains(userAccountToAdd)) {
                throw new ResponseStatusException(
                        HttpStatus.BAD_REQUEST, "User already in this budget.");
            } else if (userAccountToAdd.isEnabled()) {
                VerificationToken verificationToken = new VerificationToken(userAccountToAdd, 7);
                verificationTokenRepository.save(verificationToken);

                Map<String, Object> templateInterpolation = new HashMap<>();
                templateInterpolation.put("name", userAccountToAdd.getName());
                templateInterpolation.put("surname", userAccountToAdd.getSurname());
                templateInterpolation.put("admin", budget.getAdmin());
                templateInterpolation.put("budgetName", budget.getName());
                templateInterpolation.put("url", "https://" + env.getProperty("app.IP-Address")
                        + "/confirm-budget-join?token="
                        + verificationToken.getVerificationToken() + "&budgetId=" + budgetId);

                Mail mailToSend = new Mail.Builder()
                        .templateName("budgetInvMail.html")
                        .recipient(userAccountToAdd.getEmail())
                        .topic("You have been invited to new budget!")
                        .templateOptions(templateInterpolation)
                        .build();

                emailSenderService.sendMail(mailToSend);

            } else {
                throw new EntityNotFoundException(UserAccount.class);
            }
        } else {
            throw new PermissionDeniedException();
        }
        return budgetConverter.toMinimalDto(budget);
    }

    @Transactional
    public UserAccountDto.FullDto changeBudgetAdmin(long budgetId, long adminId) {
        Budget budget = budgetRepository.findBudget(getLoggedUsername(), budgetId)
                .orElseThrow(() -> new EntityNotFoundException(Budget.class));
        UserAccount newAdminUserAccount = userRepository.findById(adminId)
                .orElseThrow(() -> new EntityNotFoundException(UserAccount.class));
        if (budget.getAdmin().equals(getLoggedUsername())) {
            budget.setAdmin(newAdminUserAccount.getEmail());
        } else {
            throw new PermissionDeniedException();
        }
        budgetRepository.save(budget);
        return userAccountConverter.toFullDTO(newAdminUserAccount);
    }

    @Transactional
    public ResponseEntity<String> confirmBudgetJoin(String verificationToken, long budgetId) {
        VerificationToken token = verificationTokenRepository.findByVerificationToken(verificationToken)
                .orElseThrow(() -> new EntityNotFoundException(VerificationToken.class));
        if (LocalDateTime.now().isBefore(token.getExpirationDate())) {
            UserAccount userAccountToAdd = userRepository.findByEmailIgnoreCase(token.getUserAccount().getEmail())
                    .orElseThrow(() -> new EntityNotFoundException(UserAccount.class));
            Budget budget = budgetRepository.findById(budgetId)
                    .orElseThrow(() -> new EntityNotFoundException(Budget.class));

            budget.addUserAccount(userAccountToAdd);
            budgetRepository.save(budget);
            verificationTokenRepository.delete(token);
        } else {
            verificationTokenRepository.delete(token);
            throw new EntityNotFoundException(VerificationToken.class);
        }
        return new ResponseEntity<>("User succesfully added to budget!", HttpStatus.OK);
    }
}