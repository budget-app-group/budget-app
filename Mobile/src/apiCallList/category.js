import axios from 'axios'
import { protocol, origin, getHeaderList, errorHandler } from '../common/js/auth'
import AsyncStorage from '@react-native-community/async-storage'

export const getCategoryList = async (budgetId, masterCategoryId) => {
  try {
    const response = await axios.get(`${protocol}://${origin}/api/budget/${budgetId}/master/${masterCategoryId}/category`, await getHeaderList())
    return response.data
  } catch (error) {
    errorHandler(error)
    return []
  }
}

export const getCategory = async (budgetId, masterCategoryId, id) => {
  try {
    const response = await axios.get(`${protocol}://${origin}/api/budget/${budgetId}/master/${masterCategoryId}/category/${id}`, await getHeaderList())
    return response.data
  } catch (error) {
    errorHandler(error)
    return null
  }
}

export const addCategory = async (budgetId, masterCategoryId, data) => {
  try {
    await axios.post(`${protocol}://${origin}/api/budget/${budgetId}/master/${masterCategoryId}/category`, data, await getHeaderList())
    return true
  } catch (error) {
    errorHandler(error)
    return false
  }
}

export const updateCategory = async (budgetId, masterCategoryId, id, data) => {
  try {
    await axios.put(`${protocol}://${origin}/api/budget/${budgetId}/master/${masterCategoryId}/category/${id}`, data, await getHeaderList())
    return true
  } catch (error) {
    errorHandler(error)
    return false
  }
}

export const deleteCategory = async (budgetId, masterCategoryId, id) => {
  try {
    const check = await AsyncStorage.getItem('categoryId')
    if (check === `${id}`) { await AsyncStorage.removeItem('categoryId') }
    await axios.delete(`${protocol}://${origin}/api/budget/${budgetId}/master/${masterCategoryId}/category/${id}`, await getHeaderList())
    return true
  } catch (error) {
    errorHandler(error)
    return false
  }
}

export const getAllCategories = async (budgetId = -1) => {
  try {
    const response = await axios.get(`${protocol}://${origin}/api/budget/${budgetId}/categories`, await getHeaderList())
    return response.data
  } catch (error) {
    errorHandler(error)
    return []
  }
}