import axios from 'axios'
import { protocol, origin, getHeaderList, errorHandler } from '../common/js/auth'

export const getTextFromImage = async (uri = '') => {
  try {
    let formData = new FormData()
    formData.append('file', { uri: uri, type: 'image/jpeg', name: 'file' })
    const response = await axios.post(`${protocol}://${origin}/api/ocr`, formData, await getHeaderList())
    return response.data
  } catch (error) {
    errorHandler(error)
    return null
  }
}