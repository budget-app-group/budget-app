import axios from 'axios'
import { protocol, origin, getHeaderList } from '../common/js/auth'

export const updateAccount = async (data) => {
    try {
      const response = await axios.put(`${protocol}://${origin}/api/useraccount`, data, await getHeaderList())
      return response.data
    }
    catch(error) {
      return false
    }
  }

export const changePassword = async (data) => {
    try {
      await axios.put(`${protocol}://${origin}/api/passwordChange`, data, await getHeaderList())
      return true
    }
    catch(error) {
      return false
    }
  }