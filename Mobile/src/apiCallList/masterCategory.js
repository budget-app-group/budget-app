import axios from 'axios'
import { protocol, origin, getHeaderList, errorHandler } from '../common/js/auth'
import AsyncStorage from '@react-native-community/async-storage'

export const getMasterCategoryList = async (budgetId) => {
  try {
    const response = await axios.get(`${protocol}://${origin}/api/budget/${budgetId}/master`, await getHeaderList())
    return response.data
  } catch (error) {
    errorHandler(error)
    return []
  }
}

export const getMasterCategory = async (budgetId, id) => {
  try {
    const response = await axios.get(`${protocol}://${origin}/api/budget/${budgetId}/master/${id}`, await getHeaderList())
    return response.data
  } catch (error) {
    errorHandler(error)
    return null
  }
}

export const addMasterCategory = async (budgetId, data) => {
  try {
    await axios.post(`${protocol}://${origin}/api/budget/${budgetId}/master`, data, await getHeaderList())
    return true
  } catch (error) {
    errorHandler(error)
    return false
  }
}

export const updateMasterCategory = async (budgetId, id, data) => {
  try {
    await axios.put(`${protocol}://${origin}/api/budget/${budgetId}/master/${id}`, data, await getHeaderList())
    return true
  } catch (error) {
    errorHandler(error)
    return false
  }
}

export const deleteMasterCategory = async (budgetId, id) => {
  try {
    const check = await AsyncStorage.getItem('masterCategoryId')
    if (check === `${id}`) { await AsyncStorage.removeItem('masterCategoryId') }
    await axios.delete(`${protocol}://${origin}/api/budget/${budgetId}/master/${id}`, await getHeaderList())
    return true
  } catch (error) {
    errorHandler(error)
    return false
  }
}