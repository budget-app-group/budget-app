import axios from 'axios'
import { protocol, origin, getHeaderList, errorHandler } from '../common/js/auth'

export const getBudgetIncomes = async (budgetId = -1) => {
  try {
    const response = await axios.get(`${protocol}://${origin}/api/budget/${budgetId}/income`, await getHeaderList())
    return response.data
  } catch (error) {
    errorHandler(error)
    return []
  }
}