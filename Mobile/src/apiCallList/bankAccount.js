import axios from 'axios'
import { protocol, origin, getHeaderList, errorHandler } from '../common/js/auth'

export const getBankAccountList = async (budgetId) => {
  try {
    const response = await axios.get(`${protocol}://${origin}/api/budget/${budgetId}/bankaccount`, await getHeaderList())
    return response.data
  } catch (error) {
    errorHandler(error)
    return []
  }
}

export const getBankAccount = async (budgetId, id) => {
  try {
    const response = await axios.get(`${protocol}://${origin}/api/budget/${budgetId}/bankaccount/${id}`, await getHeaderList())
    return response.data
  } catch (error) {
    errorHandler(error)
    return null
  }
}

export const addBankAccount = async (budgetId, data) => {
  try {
    await axios.post(`${protocol}://${origin}/api/budget/${budgetId}/bankaccount`, data, await getHeaderList())
    return true
  } catch (error) {
    errorHandler(error)
    return false
  }
}

export const updateBankAccount = async (budgetId, id, data) => {
  try {
    await axios.put(`${protocol}://${origin}/api/budget/${budgetId}/bankaccount/${id}`, data, await getHeaderList())
    return true
  } catch (error) {
    errorHandler(error)
    return false
  }
}

export const deleteBankAccount = async (budgetId, id) => {
  try {
    await axios.delete(`${protocol}://${origin}/api/budget/${budgetId}/bankaccount/${id}`, await getHeaderList())
    return true
  } catch (error) {
    errorHandler(error)
    return false
  }
}