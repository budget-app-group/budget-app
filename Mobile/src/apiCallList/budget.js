import axios from 'axios'
import { protocol, origin, getHeaderList, errorHandler } from '../common/js/auth'
import AsyncStorage from '@react-native-community/async-storage'

export const getBudgetList = async () => {
  try {
    const response = await axios.get(`${protocol}://${origin}/api/budget`, await getHeaderList())
    return response.data
  } catch (error) {
    errorHandler(error)
    return []
  }
}

export const getBudget = async (id = -1) => {
  try {
    const response = await axios.get(`${protocol}://${origin}/api/budget/${id}`, await getHeaderList())
    return response.data
  } catch (error) {
    errorHandler(error)
    return null
  }
}

export const getBudgetUsers = async (id = -1) => {
  try {
    const response = await axios.get(`${protocol}://${origin}/api/budget/${id}/users`, await getHeaderList())
    return response.data
  } catch (error) {
    errorHandler(error)
    return []
  }
}

export const addBudget = async (data) => {
  try {
    await axios.post(`${protocol}://${origin}/api/budget`, data, await getHeaderList())
    return true
  } catch (error) {
    errorHandler(error)
    return false
  }
}

export const updateBudget = async (id, data) => {
  try {
    await axios.put(`${protocol}://${origin}/api/budget/${id}`, data, await getHeaderList())
    return true
  } catch (error) {
    errorHandler(error)
    return false
  }
}

export const deleteBudget = async (id = -1) => {
  try {
    const check = await AsyncStorage.getItem('budgetId')
    if (check === `${id}`) { await AsyncStorage.removeItem('budgetId') }
    await axios.delete(`${protocol}://${origin}/api/budget/${id}`, await getHeaderList())
    return true
  } catch (error) {
    errorHandler(error)
    return false
  }
}

export const deleteUserFromBudget = async (id = -1, uId = 1) => {
  try{
    await axios.put(`${protocol}://${origin}/api/budget/${id}/remove/${uId}`, null, await getHeaderList())
    return true
  } catch (error) {
    errorHandler(error)
    return false
  }
}

export const addUserToBudget = async (id = -1, email = '') => {
  try {
    await axios.put(`${protocol}://${origin}/api/budget/${id}/email/${email}`, null, await getHeaderList())
    return true
  } catch (error) {
    errorHandler(error)
    return false
  }
}

export const changeBudgetAdmin = async (id = -1, uId = 1) => {
  try {
    await axios.put(`${protocol}://${origin}/api/budget/${id}/admin/${uId}`, null, await getHeaderList())
    return true
  } catch (error) {
    errorHandler(error)
    return false
  }
}

export const deleteLoggedUserFromBudget = async (id = -1) => {
  try {
    await axios.put(`${protocol}://${origin}/api/budget/${id}/leave`, null, await getHeaderList())
    return true
  } catch (error) {
    errorHandler(error)
    return false
  }
}
