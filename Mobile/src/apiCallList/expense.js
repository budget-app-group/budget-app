import axios from 'axios'
import { protocol, origin, getHeaderList, errorHandler } from '../common/js/auth'

export const getExpense = async (budgetId = -1, masterCategoryId = -1, categoryId = -1, expenseId = -1) => {
  try {
    const response = await axios.get(`${protocol}://${origin}/api/budget/${budgetId}/master/` +
    `${masterCategoryId}/category/${categoryId}/expense/${expenseId}`, await getHeaderList())
    return response.data
  } catch (error) {
    errorHandler(error)
    return null
  }
}

export const getSumaryOfExpenseList = async (budgetId = -1, startDate = null, endDate = null) => {
  try {
    const response = await axios.get(`${protocol}://${origin}/api/budget/${budgetId}/summaryOfExpenses?from=${startDate}&to=${endDate}`, await getHeaderList())
    return response.data
  } catch (error) {
    errorHandler(error)
    return []
  }
}

export const getBudgetExpenseList = async (budgetId = -1) => {
  try {
    const response = await axios.get(`${protocol}://${origin}/api/budget/${budgetId}/expense`, await getHeaderList())
    return response.data
  } catch (error) {
    errorHandler(error)
    return []
  }
}

export const getBudgetPeriodExpense = async (budgetId = -1, startDate = null, endDate = null) => {
  try {
    const response = await axios.get(`${protocol}://${origin}/api/budget/${budgetId}/expense/period?from=${startDate}&to=${endDate}`, await getHeaderList())
    return response.data
  } catch (error) {
    errorHandler(error)
    return []
  }
}

//modes: expense, income, balance
export const getExpensesAnalysis = async (budgetId = -1, startDate = null, endDate = null, mode) => {
  try {
    const response = await axios.get(`${protocol}://${origin}/api/budget/${budgetId}/periodicExpenses?from=${startDate}&to=${endDate}&mode=${mode}`, await getHeaderList())
    return response.data
  } catch (error) {
    errorHandler(error)
    return []
  }
}

export const addExpense = async (budgetId = -1, masterCategoryId = -1, categoryId = -1, data = null) => {
  try {
    await axios.post(`${protocol}://${origin}/api/budget/${budgetId}/master/${masterCategoryId}` +
    `/category/${categoryId}/expense`, data, await getHeaderList())
    return true
  } catch (error) {
    errorHandler(error)
    return false
  }
}

export const updateExpense = async (budgetId = -1, masterCategoryId = -1, categoryId = -1, expenseId = -1, data = null) => {
  try {
    await axios.put(`${protocol}://${origin}/api/budget/${budgetId}/master/${masterCategoryId}` +
    `/category/${categoryId}/expense/${expenseId}`, data, await getHeaderList())
    return true
  } catch (error) {
    errorHandler(error)
    return false
  }
}

export const deleteExpense = async (budgetId = -1, masterCategoryId = -1, categoryId = -1, expenseId = -1) => {
  try {
    await axios.delete(`${protocol}://${origin}/api/budget/${budgetId}/master/${masterCategoryId}` +
    `/category/${categoryId}/expense/${expenseId}`, await getHeaderList())
    return true
  } catch (error) {
    errorHandler(error)
    return false
  }
}

export const getExpensesFromBankAccount = async (budgetId = -1, bankAccountId = -1) => {
  try {
    const response = await axios.get(`${protocol}://${origin}/api/budget/${budgetId}/bankaccount/${bankAccountId}/expense`, await getHeaderList())
    return response.data
  } catch (error) {
    errorHandler(error)
    return false
  }
}

export const getExpensesFromCategory = async (budgetId = -1, masterCategoryId = -1, categoryId = -1) => {
  try {
    const response = await axios.get(`${protocol}://${origin}/api/budget/${budgetId}/master/${masterCategoryId}/category/${categoryId}/expense`, await getHeaderList())
    return response.data
  } catch (error) {
    errorHandler(error)
    return []
  }
}