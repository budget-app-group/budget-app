import axios from 'axios'
import { protocol, origin, getHeaderList, errorHandler } from '../common/js/auth'
import AsyncStorage from '@react-native-community/async-storage'

export const signIn = async (data) => {
  try {
    const response = await axios.post(`${protocol}://${origin}/auth/login`, data)
    return response.data
  } catch (error) {
    errorHandler(error)
    return null
  }
}

export const signOut = async () => {
  try {
    await AsyncStorage.clear()
    return true
  } catch (error) {
    return false
  }
}

export const signUp = async (data) => {
  try {
    await axios.post(`${protocol}://${origin}/auth/register`, data)
    return true
  } catch (error) {
    errorHandler(error)
    return false
  }
}

export const resetPassword = async (data) => {
  try {
    await axios.post(`${protocol}://${origin}/reset-password`, data)
    return true
  }
  catch(error) {
    // errorHandler(error)
    return false
  }
}

export const deleteAccount = async () => {
  try {
    await axios.delete(`${protocol}://${origin}/api/useraccount`, await getHeaderList())
    return true
  } catch (error) {
    errorHandler(error)
    return false
  }
}