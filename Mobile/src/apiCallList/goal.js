import axios from 'axios'
import { protocol, origin, getHeaderList, errorHandler } from '../common/js/auth'

export const getGoalsList = async (budgetId = -1) => {
  try {
    const response = await axios.get(`${protocol}://${origin}/api/budget/${budgetId}/goal`, await getHeaderList())
    return response.data
  } catch (error) {
    errorHandler(error)
    return null
  }
}

export const getGoal = async (budgetId = -1, goalId = -1) => {
  try {
    const response = await axios.get(`${protocol}://${origin}/api/budget/${budgetId}/goal/${goalId}`, await getHeaderList())
    return response.data
  } catch (error) {
    errorHandler(error)
    return []
  }
}

export const addGoal = async (budgetId = -1, data = null) => {
  try {
    await axios.post(`${protocol}://${origin}/api/budget/${budgetId}/goal`, data, await getHeaderList())
    return true
  } catch (error) {
    errorHandler(error)
    return false
  }
}

export const updateGoal = async (budgetId = -1, goalId = -1, data = null) => {
  try {
    await axios.put(`${protocol}://${origin}/api/budget/${budgetId}/goal/${goalId}`, data, await getHeaderList())
    return true
  } catch (error) {
    errorHandler(error)
    return false
  }
}

export const deleteGoal = async (budgetId = -1, goalId = -1) => {
  try {
    await axios.delete(`${protocol}://${origin}/api/budget/${budgetId}/goal/${goalId}`, await getHeaderList())
    return true
  } catch (error) {
    errorHandler(error)
    return false
  }
}


export const postDonation = async (budgetId = -1, masterCategoryId = -1, categoryId = -1, data = null) => {
  try {
    await axios.post(`${protocol}://${origin}/api/budget/${budgetId}/master/${masterCategoryId}` +
    `/category/${categoryId}/expense`, data, await getHeaderList())
    return true
  } catch (error) {
    errorHandler(error)
    return false
  }
}