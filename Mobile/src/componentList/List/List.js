import React from 'react'
import ListItem from '../ListItem/ListItem'
import { FlatList } from 'react-native'
import { style } from './ListStyle'

const List = props => {
  return (
    <FlatList
      style={style.itemList}
      data={props.itemList}
      renderItem={info => (
        <ListItem
          item={info.item}
          isEditable={props.isEditable ? item => props.isEditable(item) : undefined}
          isDeletable={item => props.isDeletable(item)}
          isLeavable={props.isLeavable ? item => props.isLeavable(item) : undefined}
          itemAction={object => props.itemAction(object)}
          selectedItemId={props.selectedItemId}
          onItemPress={id => props.onItemPress(id)}
          onItemLongPress={id => props.onItemLongPress(id)}
        />
      )}
    />
  )
}

export default List