import { Button, Icon } from 'native-base'
import React from 'react'
import { Image, Text, TouchableWithoutFeedback, View } from 'react-native'
import { style } from './ListItemStyle'


const ListItem = props => {
  return (
    <View style={style.listItem}>

      <TouchableWithoutFeedback
        onPress={() => props.onItemPress(props.item.id)}
        onLongPress={() => props.onItemLongPress(props.item.id) }>
          <View style={typeof props.isLeavable === 'undefined' ? style.leftBigContainer : style.leftContainer}>
            <Image resizeMode='cover' source={props.item.image} style={style.image} />
            <Text>{props.selectedItemId === props.item.id ? 'XXX' : props.item.name}</Text>
          </View>
      </TouchableWithoutFeedback>

      <View style={ typeof props.isLeavable === 'undefined'  ? style.rightSmallContainer : style.rightContainer}>
        {typeof props.isLeavable !== 'undefined' &&
          <Button
            disabled={!props.isLeavable(props.item)}
            onPress={() => props.itemAction({ id: props.item.id, action: 'leave' })}
            transparent
          >
            <Icon name='exit'
              style={ props.isLeavable(props.item) ? { color: '#0033cc'} : { color: 'grey'}}
            />
          </Button>
        }
        { typeof props.isEditable !== 'undefined' ? (
          <Button
            disabled={!props.isEditable(props.item)}
            onPress={() => props.itemAction({ id: props.item.id, action: 'edit' })}
            transparent
          >
            <Icon name='create'
              style={ props.isEditable(props.item) ? { color: '#009900'} : { color: 'grey'}}
            />
          </Button>
        ) : (
          <View style={{ flex: 1 }} />
        )
        }
        <Button
          disabled={!props.isDeletable(props.item)}
          onPress={() => props.itemAction({ id: props.item.id, action: 'delete' })}
          transparent
        >
          <Icon name='trash'
            style={ props.isDeletable(props.item) ? { color: '#F44336'} : { color: 'grey'}}
          />
        </Button>
      </View>

    </View>
  )
}

const areEqual = (previous, next) => {
  return next.selectedItemId !== '' && (previous.selectedItemId === next.selectedItemId)
}

export default React.memo(ListItem, areEqual)