import { StyleSheet } from 'react-native'

export const style = StyleSheet.create({
  description: {
    width: '70%',
    fontSize: 20
  },
  center: {
    flex: 1,
    fontSize: 28,
    justifyContent: 'center',
    alignItems: 'center'
  },
  button: {
    width: 150,
    height: 50,
    backgroundColor:'#29ABE2',
    borderRadius: 50,
  },
  emptyListContainer: {
    padding: 10,
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center'
  },
  buttontext: {
    flex: 1, 
    color: '#fff', 
    fontSize: 18, 
    marginTop: 12,
    justifyContent: 'center', 
    textAlign: 'center'
  },
  headertext: {
    fontSize: 30,
    color: '#29ABE2',
    padding: 15,
  },
  buttonOutlinedNarrow: {
    width:150,
    borderColor:'#29ABE2',
    borderWidth: 1,
    borderRadius: 25,
    marginVertical: 10,
    paddingVertical: 13
  },
  buttonOutlinedText: {
    fontSize:16,
    fontWeight:'500',
    color:'#29ABE2',
    textAlign:'center'
  },
})