import React from 'react'
import { View, Text, Button } from 'react-native'
import * as dict from '../../common/dictionary/polish.json'
import { style } from './EmptyListStyle'
import { TouchableOpacity } from 'react-native-gesture-handler';

const EmptyList = (props) => {
  return (
    <View style={style.center}>
      <Text style={style.headertext}>{props.description}</Text>
      {props.onButtonClick &&
      <TouchableOpacity style={style.buttonOutlinedNarrow}
                        onPress={() => props.onButtonClick()}>
        <Text style={style.buttonOutlinedText}>{dict.header.back}</Text>
      </TouchableOpacity>}
    </View>
  )
}

export default EmptyList