import { StyleSheet } from 'react-native'

export const style = StyleSheet.create({
  container: {
    flex: 1
  },
  itemList: {
    width: '100%'
  },
  listItem: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    padding: 10,
    borderBottomWidth: 0.2,
    borderColor: '#29ABE2',
    backgroundColor: '#ffffff'
  },
  leftContainer: {
    width: '75%',
    flexDirection: 'row',
    alignItems: 'center',
  },
  leftBigContainer: {
    width: '75%',
    flexDirection: 'row',
    alignItems: 'center',
  },
  rightContainer: {
    width: '25%',
    flexDirection: 'row',
    alignItems: 'center',
  },
  rightSmallContainer: {
    width: '25%',
    flexDirection: 'row',
    alignItems: 'center',
  },
  image: {
    height: 30,
    width: 30,
    marginLeft: 8,
    marginRight: 8
  }
})