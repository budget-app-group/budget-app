import { Button, Icon } from 'native-base'
import React from 'react'
import { Image, Text, TouchableWithoutFeedback, View } from 'react-native'
import { style } from './UsersStyle'

const ListItem = props => {
  return (
    <View style={style.listItem}>
      <TouchableWithoutFeedback
        onPress={() => props.onItemPress(props.item.id)}
        onLongPress={() => props.onItemLongPress(props.item.id) }>
          <View style={style.leftContainer}>
            <Text style={style.text}>{props.selectedItemId === props.item.id ? '' : props.item.name}</Text>
          </View>
      </TouchableWithoutFeedback>

      <View style={style.rightContainer}>
        { typeof props.adminPermission !== 'undefined' ? (
          <Button
            disabled={!props.adminPermission(props.item)}
            onPress={() => props.itemAction({ id: props.item.id, action: 'changeAdmin' })}
            transparent
          >
            <Icon name='ribbon'
              style={ props.adminPermission(props.item) ? { color: '#009900'} : { color: 'grey'}}
            />
          </Button>
        ) : (
          <View style={{ flex: 1 }} />
        )
        }
        <Button
          disabled={!props.isDeletable(props.item)}
          onPress={() => props.itemAction({ id: props.item.id, action: 'delete' })}
          transparent
        >
          <Icon name='trash'
            style={ props.isDeletable(props.item) ? { color: '#F44336'} : { color: 'grey'}}
          />
        </Button>
      </View>
    </View>
  )
}

const areEqual = (previous, next) => {
  return next.selectedItemId !== '' && (previous.selectedItemId === next.selectedItemId)
}

export default React.memo(ListItem, areEqual)