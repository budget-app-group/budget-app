import React from 'react'
import ListItem from './UserItem'
import { FlatList } from 'react-native'
import { style } from './UsersStyle'

const List = props => {
    return (
      <FlatList
        style={style.itemList}
        data={props.itemList}
        renderItem={info => (
          <ListItem
            item={info.item}
            isDeletable={item => props.isDeletable(item)}
            adminPermission={item => props.adminPermission(item)}
            itemAction={object => props.itemAction(object)}
            selectedItemId={props.selectedItemId}
            onItemPress={id => props.onItemPress(id)}
            onItemLongPress={id => props.onItemLongPress(id)}
          />
        )}
      />
    )
  }
  
  export default List