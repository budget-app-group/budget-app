import { StyleSheet } from 'react-native'

export const style = StyleSheet.create({
  itemList: {
    width: '100%'
  },
  listItem: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    padding: 10,
    borderBottomWidth: 0.2,
    borderColor: '#29ABE2',
    backgroundColor: '#ffffff'
  },
  leftContainer: {
    width: '75%',
    flexDirection: 'column',
    alignItems: 'flex-start',
  },
  rightContainer: {
    width: '25%',
    flexDirection: 'row',
    alignItems: 'center'
  },
  image: {
    height: 30,
    width: 30,
    marginLeft: 8,
    marginRight: 8
  },
  noteText: {
    fontFamily: 'Lato-Regular', 
    color: '#999999', 
    fontSize: 13
  },
  text: {
    fontSize: 15, 
    color: '#404040'
  }
})