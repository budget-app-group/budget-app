import React from 'react'
import ExpenseListItem from '../ExpenseListComponent/ExpenseListItem'
import { FlatList } from 'react-native'
import { style } from './ExpenseListStyle'

const ExpenseListComponent = props => {
  return (
    <FlatList
      style={style.itemList}
      data={props.itemList}
      renderItem={info => (
        <ExpenseListItem
          item={info.item}
          isEditable={props.isEditable ? item => props.isEditable(item) : undefined}
          isDeletable={item => props.isDeletable(item)}
          itemAction={object => props.itemAction(object)}
          selectedItemId={props.selectedItemId}
          onItemPress={id => props.onItemPress(id)}
          onItemLongPress={id => props.onItemLongPress(id)}
        />
      )}
    /> 
  )
}

export default ExpenseListComponent
