import { Button, Icon, Text } from 'native-base'
import React from 'react'
import { TouchableWithoutFeedback, View } from 'react-native'
import { style } from './ExpenseListStyle'

const ExpenseListItem = props => {
    return (
      <View style={style.listItem}>
        <TouchableWithoutFeedback
          onPress={() => props.onItemPress(props.item.id)}
          onLongPress={() => props.onItemLongPress(props.item.id) }>
            <View style={style.leftContainer}>
              <Text style={style.text}>{props.selectedItemId === props.item.id ? '' : props.item.name}</Text>
              <Text style={style.noteText}>{props.selectedItemId === props.item.id ? '' : new Date(props.item.expenseDate).toLocaleDateString()}  {props.selectedItemId === props.item.id ? '' : props.item.amount + "zł"}</Text>
            </View>
        </TouchableWithoutFeedback>
  
        <View style={style.rightContainer}>
          { typeof props.isEditable !== 'undefined' ? (
            <Button
              disabled={!props.isEditable(props.item)}
              onPress={() => props.itemAction({ id: props.item.id, action: 'edit' })}
              transparent
            >
              <Icon name='create'
                style={ props.isEditable(props.item) ? { color: '#009900'} : { color: 'grey'}}
              />
            </Button>
          ) : (
            <View style={{ flex: 1 }} />
          )
          }
          <Button
            disabled={!props.isDeletable(props.item)}
            onPress={() => props.itemAction({ id: props.item.id, action: 'delete' })}
            transparent
          >
            <Icon name='trash'
              style={ props.isDeletable(props.item) ? { color: '#F44336'} : { color: 'grey'}}
            />
          </Button>
        </View>
      </View>
    )
  }
  
  const areEqual = (previous, next) => {
    return next.selectedItemId !== '' && (previous.selectedItemId === next.selectedItemId)
  }
  
  export default React.memo(ExpenseListItem, areEqual)