import { StyleSheet } from 'react-native'

export const style = StyleSheet.create({
  center: {
    flex: 1,
    fontSize: 28,
    justifyContent: 'center',
    alignItems: 'center'
  },
  container: {
    flex: 1,
    flexDirection: 'row',
  },
  container1: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
  },
  welcomeContainer: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    flexWrap: 'wrap'
  },
  spinner: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center'
  },
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  capture: {
    flex: 0,
    backgroundColor: '#fff',
    borderRadius: 5,
    color: '#000',
    padding: 10,
    margin: 40,
  },
  menuheader: {
    height: 100, 
    backgroundColor: '#29ABE2',
    flexDirection: 'row'
  },
  menuemail: {
    color: '#fff', 
    fontFamily: 'Lato-Regular'
  },
  menuNameSurname: {
    color: '#fff',
    fontWeight: 'bold',
    fontFamily: 'Lato-Regular'
  },
  menuProfileImage: {
    height: 40,
    width: 40,
    marginLeft: 8,
    marginRight: 10,
    marginTop: 10,
    marginBottom: 10
  },
  menuSettingsButton: {
    paddingLeft: 15
  },
  menuSettingsButton2: {
    color: '#fff',
    fontSize: 35,
  },
  menuText: {
    fontSize: 18,
    padding: 5,
    marginTop: 3,
    fontFamily: 'Lato-Regular',
  },
  menuHeaderColumn: {
    flexDirection: 'column',
    marginBottom: 3
  },
  menuButton: {
    flexDirection: 'row',
    paddingLeft: 5,
    paddingBottom: 15,
    paddingTop: 15,
    paddingRight: 15
  },
  menuImage: {
    height: 36,
    width: 36,
    marginLeft: 8,
    marginRight: 8
  },
  menuItems: {
    justifyContent: 'center',
    flexDirection: 'row',
    padding: 5,
    alignItems: 'center'
  },
  active: {
    backgroundColor: '#ace3fa'
  },
  menuButtonImage: {
    width: 50,
    height: 50,
    padding: 3,
    marginRight: 10
  },
  topname: {
    color: '#29ABE2',
    marginTop: 20,
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: 35
  },
  homeheader: {
    backgroundColor: '#0071BC', 
    height: 65
  },
  homeheadermargin: {
    flexDirection: 'row', 
    marginLeft: "-38%"
  },
  homeheadermenuicon: {
    fontSize: 35, 
    color: '#fff'
  },
  pieStyle: {
    width: 330,
    height: 330,
    flex: 1,
  },
  actionButtonIcon: {
    fontSize: 20,
    height: 22,
    color: 'white',
  },
  button: {
    width:300,
    backgroundColor:'#29ABE2',
    borderRadius: 25,
    marginVertical: 10,
    paddingVertical: 13
  },
  headertext: {
    fontSize: 30,
    color: '#0071BC',
    padding: 15,
  },
  headertext2: {
    fontSize: 25,
    color: '#29ABE2',
    padding: 10,
    fontWeight:'500'
  },
  text2: {
    fontSize: 16,
    color: '#29ABE2',
    fontWeight:'300',
    textAlign:'center'
  },
  buttonText: {
    fontSize:16,
    fontWeight:'500',
    color:'#ffffff',
    textAlign:'center'
  },
  backgroundDrawer: {
    backgroundColor: '#d3d3d3'
  },
  buttonOutlined: {
    width:300,
    borderColor:'#29ABE2',
    borderWidth: 1,
    borderRadius: 25,
    marginVertical: 10,
    paddingVertical: 13
  },
  buttonOutlined2: {
    width:300,
    borderColor:'#29ABE2',
    borderWidth: 1,
    borderRadius: 25,
    marginVertical: 20,
    paddingVertical: 13
  },
  buttonOutlinedText: {
    fontSize:16,
    fontWeight:'500',
    color:'#29ABE2',
    textAlign:'center'
  },
  text: {
    fontFamily: 'Lato-Regular',
    fontSize: 16
  },
  monthsButton: {
    backgroundColor:'#ffffff',
    borderColor:'#29ABE2',
    borderWidth: 0.5
  }
})
