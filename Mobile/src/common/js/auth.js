import AsyncStorage from '@react-native-community/async-storage'
import { signOut } from '../../apiCallList/auth'
import { Actions } from 'react-native-router-flux'

export const origin = 'mybudget-wmi.herokuapp.com'
export const protocol = 'https'

export const getHeaderList = async () => {
  try {
    const result = await AsyncStorage.getItem('token')
    if (result !== null) { return { headers: { 'Authorization': result } } }
    else {
      const check = signOut()
      if (check) { Actions.SignIn() }
      return ''
    }
  } catch (error) {
    const check = signOut()
    if (check) { Actions.SignIn() }
    return ''
  }
}

export const errorHandler = error => {
  if (error.response && error.response.status && (error.response.status === 401)) {
    const check = signOut()
    if (check) { Actions.SignIn() }
  }
}