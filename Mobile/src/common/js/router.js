import React from 'react'
import { Icon } from 'native-base'
import { BackHandler } from 'react-native'
import { Router, Scene, Drawer, Actions } from 'react-native-router-flux'

import SignInScreen from '../../screenList/SignIn/SignInScreen'
import SignUpScreen from '../../screenList/SignUp/SignUpScreen'
import Home from '../../screenList/Home/Home'
import BudgetListScreen from '../../screenList/BudgetList/BudgetListScreen'
import BudgetScreen from '../../screenList/Budget/BudgetScreen'
import BankAccountListScreen from '../../screenList/BankAccountList/BankAccountListScreen'
import AnalysisListScreen from '../../screenList/AnalysisList/AnalysisListScreen'
import MasterCategoryListScreen from '../../screenList/MasterCategoryList/MasterCategoryListScreen'
import CategoryListScreen from '../../screenList/CategoryList/CategoryListScreen'
import GoalListScreen from '../../screenList/GoalList/GoalListScreen'
import GoalScreen from '../../screenList/Goal/GoalScreen'
import Donation from '../../screenList/Goal/DonationScreen'
import SettingsScreen from '../../screenList/Settings/SettingsScreen'
import Camera from '../../screenList/Camera/Camera'
import ExpenseListScreen from '../../screenList/ExpenseList/ExpenseListScreen'
import HomeScreen from '../../screenList/Home/HomeScreen'
import CategoryScreen from '../../screenList/Category/CategoryScreen'
import MasterCategoryScreen from '../../screenList/MasterCategory/MasterCategoryScreen'
import ExpenseScreen from '../../screenList/Expense/ExpenseScreen'
import BankAccountScreen from '../../screenList/BankAccount/BankAccountScreen'
import Photo from '../../screenList/Photo/Photo'
import PasswordResetScreen from '../../screenList/PasswordReset/PasswordResetScreen'
import ChangePasswordScreen from '../../screenList/Settings/ChangePasswordScreen'
import Gallery from '../../screenList/Gallery/Gallery'
import IncomeList from '../../screenList/IncomeList/IncomeListScreen'
import AsyncStorage from '@react-native-community/async-storage'

var backButtonPressedOnceToExit = false

const onBackPress = async () => {
	if (backButtonPressedOnceToExit && Actions.currentScene === 'SignIn') {
		BackHandler.exitApp()
	} else {
		if (Actions.currentScene !== 'SignIn') {
			backButtonPressedOnceToExit && (backButtonPressedOnceToExit = false)
			if (Actions.currentScene === 'Expense') {
				const prev = await getPrev()
				if (prev === 'Camera') { 
					Actions.popTo('Camera')
				} else if (prev === 'Gallery') { 
					Actions.popTo('Gallery')
				} else {
					Actions.popTo('HomeScreen')
				}
			} else {Actions.pop()}
			return true
		} else {
			backButtonPressedOnceToExit = true
			return true
		}
	}
}

getPrev = async () => {
	const prev = await AsyncStorage.getItem('prev')
	await AsyncStorage.removeItem('prev')
	return prev
}

const Navigator = () => {
	return (
		<Router
			navigationBarStyle={styles.navBar}
			titleStyle={styles.navBarTitle}
			barButtonTextStyle={styles.barButtonTextStyle}
			barButtonIconStyle={styles.barButtonIconStyle}
			rightButtonStyle={styles.rightButton}
			rightButtonTextStyle={styles.rightButtonText}
			backAndroidHandler={onBackPress}
			tintColor='white'>
				<Drawer
					key='drawerMenu'
					onOpen={() => Actions.refresh({key: 'drawerMenu'})}
					contentComponent={Home}
					drawerWidth={280}
					drawerIcon={<Icon name="menu" style={styles.hamburger}></Icon>}
					drawerPosition='left'>
					<Scene key='root'>
						<Scene key='HomeScreen' component={HomeScreen} title='Menu Główne' statusBarStyle='light-content'/>
						<Scene key='BankAccount' component={BankAccountScreen} title='Konto'/>
						<Scene key='BankAccountList' component={BankAccountListScreen} title='Konta'/>
						<Scene key='Budget' component={BudgetScreen} title='Budżet'/>
						<Scene key='BudgetList' component={BudgetListScreen} title='Budżety'/>
						<Scene key='Home' component={Home} title='Home'/>
						<Scene key='AnalysisList' component={AnalysisListScreen} title='Analiza'/>
						<Scene key='CategoryList' component={CategoryListScreen} title='Kategorie'/>
						<Scene key='MasterCategoryList' component={MasterCategoryListScreen} title='Kategorie główne'/>
						<Scene key='ExpenseList' component={ExpenseListScreen} title='Wydatki'/>
						<Scene key='IncomeList' component={IncomeList} title='Przychody'/>
						<Scene key='GoalList' component={GoalListScreen} title='Cele'/>
						<Scene key='Goal' component={GoalScreen} title='Cel'/>
						<Scene key='Settings' component={SettingsScreen} title='Ustawienia'/>
						<Scene key='Category' component={CategoryScreen} title='Kategoria'/>
						<Scene key='ChangePassword' component={ChangePasswordScreen} title='Zmiana hasła'/>
						<Scene key='MasterCategory' component={MasterCategoryScreen} title='Kategoria główna'/>
						<Scene key='Expense' component={ExpenseScreen} title='Wydatek'/>
						<Scene key='Gallery' component={Gallery} title='Galeria'/>
						<Scene key="Donation" component={Donation} title='Dotacja'/>
						<Scene wrap={false} key='Camera' component={Camera} title='Aparat' hideNavBar/>
						<Scene wrap={false} key='Photo' component={Photo} title='Zdjęcie' hideNavBar/>
						<Scene key='PasswordReset' component={PasswordResetScreen} title='Resetowanie hasła' hideNavBar/>
						<Scene key='SignUp' type="reset" component={SignUpScreen} title='Rejestracja' hideNavBar/>
						<Scene key='SignIn' type="reset" component={SignInScreen} title='Logowanie' initial={true} hideNavBar gesturesEnabled={false}/>
					</Scene>
				</Drawer>
			</Router>
		)
}

const styles = {
	navBar: {
		backgroundColor: '#0071BC',
		height: 70
	},
	navBarTitle: {
		color: '#FFFFFF',
		fontSize: 20
	},
	barButtonTextStyle: {
		color: 'white',
		size: 50,
		fontSize: 50
	},
	barButtonIconStyle: {
		
	},
	rightButton: {
		marginTop: 5,
		marginRight: 5,
		backgroundColor: 'white',
		borderRadius: 5,
		borderWidth: 1,
		borderColor: '#00b8ff'
	},
	rightButtonText: {
		color: '#00b8ff'
	},
	hamburger: {
		marginLeft: 10,
		fontSize: 30,
		color: '#fff'
	}
}

export default Navigator
