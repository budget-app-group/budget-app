import React, { Component } from 'react'
import {decode as atob, encode as btoa} from 'base-64'
import {
  Text, View, TouchableOpacity,
  TouchableWithoutFeedback, Keyboard, TextInput, Image, ToastAndroid
} from 'react-native'
import { Actions } from 'react-native-router-flux'
import * as dict from '../../common/dictionary/polish.json'
import { styles } from './SignInStyle'
import Logo from '../../common/iconList/Logo.png'
import { signIn } from '../../apiCallList/auth'
import AsyncStorage from '@react-native-community/async-storage'
import { Container, Spinner} from 'native-base'
import { ScrollView } from 'react-native-gesture-handler'

export default class SignInScreen extends Component {
  constructor() {
    super()
    this.state = {
      email: '',
      password: '',
      emailValidation: false,
      passwordValidation: false,
      isInputOneFocus: false,
      isInputTwoFocus: false,
      currentDate: new Date(),
      isSingedIn: true,

      isSingInButtonDisabled: false
    }
  }

  componentDidMount = async () => {
    const token = await AsyncStorage.getItem('token')
    if (token) {
      isExpired = await this.checkIfTokenExpired(token)
      if (isExpired) {
        this.setState({
          isSingedIn: false
        })
      } else {
        Actions.HomeScreen()
      }
    } else {
      this.setState({
        isSingedIn: false
      })
    }
  }

  checkIfTokenExpired (token) {
    let payload = JSON.parse(atob(token.split('.')[1]))
    if (payload.exp < this.state.currentDate.getTime()) {
      return false
    } else {
      return true
    }
  }

  async componentDidMount () {
    const email = await AsyncStorage.getItem('email')
    if (email) { this.setState({ email: email })}
  }

  async signIn () {
    this.setState({ isSingInButtonDisabled: true })
    const response = await signIn({ email: this.state.email, password: this.state.password })

    if (response) {
      await AsyncStorage.setItem('token', `Bearer ${response.token.accessToken}`)
      await AsyncStorage.setItem('user', JSON.stringify(response.user))
      this.setState({ isSingInButtonDisabled: false })
      Actions.HomeScreen()
    } else {
      this.setState({ isSingInButtonDisabled: false })
      ToastAndroid.show(dict.error_list.auth.error_signin, ToastAndroid.SHORT)
    }
  }

  validate(type, text) {
    var alph = /^[a-zA-Z]+$/
    var email = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{1,}))$/
    var pass = /^.{8,16}$/
    if (type == 'email') {
      if(email.test(text)){
        this.setState({
          emailValidation: false
        })
      }
      else {
        this.setState({
          emailValidation: true
        })
      }
    }
    if (type == 'password') {
      if(pass.test(text)) {
        this.setState({
          passwordValidation: false
        })
      }
      else {
        this.setState({
          passwordValidation: true
        })
      }
    }

  }

  signUp () { Actions.SignUp() }

  onChange (stateName, text) {
    this.setState({
      [stateName]: text
    })
  }

  handlerFocusBlur = (input, value) => {
    this.setState({[input]:value})
}

  passwordReset() { Actions.PasswordReset() }

  render() {
    return (
      <Container>
      <ScrollView contentContainerStyle={{flexGrow: 1}}>
        { this.state.isSingedIn === false ? (
        <TouchableWithoutFeedback style={{ flex: 1 }} onPress={Keyboard.dismiss} accessible={false}>
          <View style={styles.container}>
            <View style={{flex: 0.5}}/>
              <Text style={styles.logoText}>{dict.app_name}</Text>
              <Image  style={styles.logoimage} source={Logo}/>
              <View style={styles.containerAuth}>
                <TextInput
                  autoCapitalize='none'
                  style={[styles.inputBox, this.state.emailValidation ? styles.inputBoxValidation : null, this.state.isInputOneFocus ? styles.textInputFocus : '']} 
                  underlineColorAndroid='rgba(0,0,0,0)' 
                  placeholder={dict.placeholder.login}
                  value={this.state.email}
                  onChangeText={(text) => {this.validate('email', text); this.onChange('email', text); }}
                  placeholderTextColor = '#29ABE2'
                  selectionColor='#fff'
                  onFocus={this.handlerFocusBlur.bind(this, 'isInputOneFocus', true)}
                  onBlur={this.handlerFocusBlur.bind(this, 'isInputOneFocus', false)}
                  keyboardType='email-address'
                  onSubmitEditing={()=> this.password.focus()}
                />
                <TextInput
                  style={[styles.inputBox, this.state.passwordValidation ? styles.inputBoxValidation : null, this.state.isInputTwoFocus ? styles.textInputFocus : '']}
                  underlineColorAndroid='rgba(0,0,0,0)' 
                  placeholder={dict.placeholder.password}
                  onChangeText={(text) => {this.validate('password', text); this.onChange('password', text); }}
                  value={this.state.password}
                  secureTextEntry={true}
                  selectionColor='#fff'
                  placeholderTextColor = '#29ABE2'
                  onFocus={this.handlerFocusBlur.bind(this, 'isInputTwoFocus', true)}
                  onBlur={this.handlerFocusBlur.bind(this, 'isInputTwoFocus', false)}
                  ref={(input) => { this.password = input }}
                  onSubmitEditing={()=> this.signIn()}
                />
                <View style={{alignItems: 'center'}}>
                  <TouchableOpacity 
                    disabled={this.state.isSingInButtonDisabled}
                    onPress={() => {try {this.signIn()} catch {ToastAndroid.show(dict.error_list.auth.error_signin, ToastAndroid.SHORT)}}} style={styles.button}>
                    <Text style={styles.buttonText}>{dict.header.sign_in}</Text>
                  </TouchableOpacity>
                    <TouchableOpacity 
                      onPress={this.passwordReset}
                      style={styles.signinPasswordRecCont}>
                      <Text style={styles.signupText}>{dict.password_retrieval}</Text>
                    </TouchableOpacity>
                </View>
              </View>
            <View style={styles.signupTextCont}>
              <Text style={styles.signupText}>{dict.bottom_tag_register}</Text>
              <TouchableOpacity onPress={this.signUp}><Text style={styles.signInButton}>{dict.header.sign_up}</Text></TouchableOpacity>
            </View>
          </View>
        </TouchableWithoutFeedback>
        ) : (
          <Container style={styles.container}>
              <Spinner color='blue'/>
          </Container>
        )}
        </ScrollView>
      </Container>
    )
  }
}