import { StyleSheet } from 'react-native'

export const styles = StyleSheet.create({
    container: {
      backgroundColor: '#fff',
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
    },
    signupTextCont: {
      flexGrow: 1,
      alignItems: 'flex-end',
      justifyContent: 'center',
      paddingVertical: 16,
      flexDirection: 'row',
    },
    signinPasswordRecCont: {
      justifyContent: 'center',
    },
    signupText: {
        color: 'rgba(41, 171, 226,0.6)',
        fontSize: 16,
    },
    signInButton: {
        color: '#29ABE2',
        fontSize: 16,
        fontWeight: '500',
    },
    /// from form
    container1 : {
      flexGrow: 1,
      paddingTop: 100,
      justifyContent:'center',
      alignItems: 'center'
    },
    containerAuth : {
      flexGrow: 1,
      flex: 0.5,
      paddingTop: 100,
      justifyContent:'center',
      alignItems: 'center'
    },
    inputBox: {
      width:300,
      backgroundColor:'#ffffff',
      borderRadius: 25,
      borderColor:'#29ABE2',
      borderWidth: 1,
      paddingHorizontal:16,
      fontSize:16,
      color:'#29ABE2',
      marginVertical: 10
    },
    inputBox2: {
      width:300,
      backgroundColor:'#D4EEF9',
      borderRadius: 25,
      paddingHorizontal:16,
      fontSize:16,
      color:'#ffffff',
      marginVertical: 10
    },
    inputBoxValidation: {
      borderWidth: 2,
      borderColor: '#ff0000'
    },
    button: {
      width:300,
      backgroundColor:'#29ABE2',
       borderRadius: 25,
        marginVertical: 10,
        paddingVertical: 13
    },
    buttonText: {
      fontSize:16,
      fontWeight:'500',
      color:'#ffffff',
      textAlign:'center'
    },
    // from logo
    container2: {
      flexGrow: 1,
      justifyContent:'center',
      alignItems: 'center'
    },
    logoText : {
        marginVertical: 15,
        fontSize:35,
        color:'rgba(41, 171, 226, 0.7)'
    },
    logoimage: {
      alignItems: 'center',
      height: 70,
      width: 70,
      justifyContent: 'center'
    },
    textInputFocus: {
      borderWidth: 2,
      borderColor:'#26A0D4',
    }
  })
  