import React, { Component } from 'react'
import { View, Dimensions, TouchableOpacity, CameraRoll, PermissionsAndroid } from 'react-native'
import { RNCamera } from 'react-native-camera'
import { Icon } from 'native-base'
import * as dict from '../../common/dictionary/polish.json'
import { style } from '../../common/style/main'
import { cameraStyle } from './CameraStyle'
import { Actions } from 'react-native-router-flux'
import { errorHandler } from '../../common/js/auth'
import AsyncStorage from '@react-native-community/async-storage'


const { height, width } = Dimensions.get('window')
export default class Camera extends Component {
  constructor () {
    super()
    this.camera = React.createRef()
    this.state = {
      orientation: height > width ? 'Portrait' : 'Landscape',
      pictureTaken: false
    }
  }

  componentDidMount = async () => {
    await this.requestCameraPermission()
  }

  requestCameraPermission = async () => {
    try {
      const granted = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        {
          title: dict.permission_list.external_storage.title,
          message: dict.permission_list.external_storage.message,
          buttonNeutral: dict.permission_list.later,
          buttonNegative: dict.permission_list.no,
          buttonPositive: dict.permission_list.yes
        }
      )
      // #TODO zabezpieczenie jak uytkownik się nie zgodzi, jak narazie cisnę dalej
      // te console logi są tylko do czasu obsłuzenia powyzszego punktu
    } catch (error) {
      errorHandler(error)
    }
  }

  takePicture = async () => {
    if (this.camera) {
      try {
        this.setState({pictureTaken: true})
        const data = await this.camera.takePictureAsync({ quality: 1, base64: true })
        this.camera.pausePreview()
        CameraRoll.saveToCameraRoll(data.uri)
        this.setState({pictureTaken: false})
        await AsyncStorage.setItem('prev', 'Camera')
        Actions.Photo({ uri: data.uri, width: data.width, height: data.height })
      } catch (error) {
        errorHandler(error)
      }
    } else {
      alert(dict.error + dict.header.camera)
    }
  }

  render() {
    return (
      <View style={style.container}>
        <RNCamera
          ref={cameraRef => this.camera = cameraRef}
          captureAudio={false}
          style={cameraStyle.container}
          type={RNCamera.Constants.Type.back}
          flashMode={RNCamera.Constants.FlashMode.off} >

          <View style={this.state.orientation === 'Portrait'
            ? cameraStyle.buttonContainerPortrait : cameraStyle.buttonContainerLandscape} >

            <TouchableOpacity
              onPress={Actions.pop}
              style={this.state.orientation === 'Portrait' ? cameraStyle.buttonPortrait
                : cameraStyle.buttonLandscape} >
              <Icon
                type='EvilIcons'
                name='chevron-left'
                style={{ fontSize: 60, color: 'white' }} />
            </TouchableOpacity>

            <TouchableOpacity
              disabled={this.state.pictureTaken}
              onPress={() => this.takePicture()}
              style={this.state.orientation === 'Portrait' ? cameraStyle.buttonPortrait
                : cameraStyle.buttonLandscape} >
              <Icon type='EvilIcons' name='camera' style={{ fontSize: 60, color: 'white' }} />
            </TouchableOpacity>

            <TouchableOpacity
              onPress={Actions.Gallery}
              style={this.state.orientation === 'Portrait' ? cameraStyle.buttonPortrait
                : cameraStyle.buttonLandscape} >
              <Icon
                type='EvilIcons'
                name='image'
                style={{ fontSize: 60, color: 'white' }} />
            </TouchableOpacity>

          </View>

        </RNCamera>
      </View>
    )
  }
}
