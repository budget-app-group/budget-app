import React, { Component } from 'react'
import { View, ToastAndroid } from 'react-native'
import { Container, Icon, Spinner } from 'native-base'
import { style } from '../../common/style/main'
import List from '../../componentList/List/List'
import Logo from '../../common/iconList/Logo.png'
import { Actions } from 'react-native-router-flux'
import EmptyList from '../../componentList/EmptyList/EmptyList'
import * as dict from '../../common/dictionary/polish.json'
import ActionButton from 'react-native-action-button'
import AsyncStorage from '@react-native-community/async-storage'
import { getBankAccountList } from '../../apiCallList/bankAccount'

export default class BankAccountListScreen extends Component {
    constructor () {
        super ()
        this.state = {
            budgetId: '',
            data: [],
            isDataEmpty: false,
            selectedBankAccountId: '',
            isDataReady: false,
            userEmail: '',
            budgetAdminEmail: ''
        }
    }

    async componentDidMount() {
        const id = await AsyncStorage.getItem('budgetId')
        const admin = await AsyncStorage.getItem('budgetAdminEmail')
        let user = await AsyncStorage.getItem('user')
        user = JSON.parse(user)
        this.setState({userEmail: user.email, budgetAdminEmail: admin})
        if (id) {
            const response = await getBankAccountList(id)
            this.setState({
                data: response.map(element => {
                    return Object.assign(element, { image: Logo, key: `${element.id}` })
                }),
                isDataEmpty: !response.length,
                budgetId: id
            })
        }
        else {
            this.setState({
                isDataEmpty: true,
                budgetId: '',
                data: []
            })
        }
        this.setState({isDataReady: true})
    }

    async previewBankAccount (id) {
        await AsyncStorage.setItem('bankAccountId', `${id}`)
        await AsyncStorage.setItem('action', 'preview')
        this.setState({ selectedBankAccountId: this.state.selectedBankAccountId === id ? '' : id }, () => {
            Actions.BankAccount()
        })
    }

    async selectBankAccount (id) {
        await AsyncStorage.setItem('bankAccountId', `${id}`)
        let item = this.state.data.find(i => i.id === id)
        ToastAndroid.show(`Wybrano konto bankowe: ${item.name}`, ToastAndroid.SHORT)
        this.setState({ selectedBankAccountId: this.state.selectedBankAccountId === id ? '' : id }, () => {
            Actions.HomeScreen()
        })
    }
    
    async viewExpenses (id) {
        let bankAccount = await this.state.data.filter(el => el.id == id)[0]
        await AsyncStorage.setItem('bankAccount', JSON.stringify(bankAccount))
        await AsyncStorage.setItem('expenseListType', 'bankAccExp')
        this.setState({ selectedBankAccountId: this.state.selectedBankAccountId === id ? '' : id }, () => {
            Actions.ExpenseList()
        })
    }

    render () {
        return (
            <Container>
                { this.state.isDataReady ? (
                <View style={style.container}>
                    {this.state.isDataEmpty ?
                    <EmptyList
                        description={this.state.budgetId !== '' ? dict.error_list.empty.bankAccountList : dict.error_list.no_budget_id}
                        onButtonClick={() => this.state.budgetId !== '' ? Actions.HomeScreen() : Actions.BudgetList()}
                    />
                    :
                    <List
                        itemList={this.state.data}
                        onItemPress={id => this.viewExpenses(id)}
                        onItemLongPress={id => this.previewBankAccount(id)}
                        isEditable={item => { return (this.state.userEmail == item.budgetData.admin) }}
                        isDeletable={item => { return (this.state.userEmail == item.budgetData.admin) }}
                        itemAction={async props => {
                            await AsyncStorage.setItem('bankAccountId', `${props.id}`)
                            await AsyncStorage.setItem('action', `${props.action}`)
                            Actions.BankAccount()
                        }}
                        selectedItemId={this.state.selectedBankAccountId}
                    />}
                    {this.state.userEmail == this.state.budgetAdminEmail ? 
                    (<ActionButton buttonColor='#29ABE2' position="center" onPress={async () => {
                        await AsyncStorage.setItem('action', 'add')
                        Actions.BankAccount()
                    }}>
                        <Icon name='add' style={style.actionButtonIcon} />
                    </ActionButton>) : (<View></View>)
                    }
                </View>
                ) : (
                    <Container style={style.spinner}>
                        <Spinner color='blue'/>
                    </Container>
                )}
            </Container>
        )
    }
}