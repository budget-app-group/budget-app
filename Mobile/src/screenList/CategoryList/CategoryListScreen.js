import React, { Component } from 'react'
import { View } from 'react-native'
import { Container, Icon, Spinner } from 'native-base'
import { style } from '../../common/style/main'
import ActionButton from 'react-native-action-button'
import List from '../../componentList/List/List'
import Logo from '../../common/iconList/Logo.png'
import { Actions } from 'react-native-router-flux'
import EmptyList from '../../componentList/EmptyList/EmptyList'
import * as dict from '../../common/dictionary/polish.json'
import AsyncStorage from '@react-native-community/async-storage'
import { getCategoryList } from '../../apiCallList/category'
import { errorHandler } from '../../common/js/auth'

export default class CategoryListScreen extends Component {
    constructor () {
        super ()
        this.state = {
            budgetId: '',
            data: [],
            isDataEmpty: false,
            masterCategoryId: '',
            selectedCategoryId: '',
            userEmail: '',
            isDataReady: false,
            budgetAdminEmail: ''
        }
    }

    async componentDidMount() {
        try {
            const budgetId = await AsyncStorage.getItem('budgetId')
            const masterCategoryId = await AsyncStorage.getItem('masterCategoryId')
            const admin = await AsyncStorage.getItem('budgetAdminEmail')
            let user = await AsyncStorage.getItem('user')
            user = JSON.parse(user)
            this.setState({userEmail: user.email, budgetAdminEmail: admin})
            if (masterCategoryId && budgetId) {
                const response = await getCategoryList(budgetId, masterCategoryId)
                this.setState({
                    data: response.map(element => {
                        return Object.assign(element, { image: Logo, key: `${element.id}` })
                    }),
                    isDataEmpty: !response.length,
                    budgetId: budgetId,
                    masterCategoryId: masterCategoryId
                })
            }
            else { this.setState({ isDataEmpty: true }) }
        } catch (error) {
            errorHandler(error)
            this.setState({ isDataEmpty: true })
        }
        this.setState({isDataReady: true})
    }

    async viewExpenses (id) {
        let category = await this.state.data.filter(el => el.id === id)[0]
        await AsyncStorage.setItem('category', JSON.stringify(category))
        await AsyncStorage.setItem('categoryId', `${id}`)
        await AsyncStorage.setItem('expenseListType', 'categoryExp')
        Actions.ExpenseList()
    }

    async previewCategory (id) {
        await AsyncStorage.setItem('categoryId', `${id}`)
        await AsyncStorage.setItem('action', 'preview')
        Actions.Category()
    }


    render () {
        return (
            <Container>
                { this.state.isDataReady ? (
                <View style={style.container}>
                    {this.state.isDataEmpty ?
                    <EmptyList
                        description={this.state.budgetId !== '' ? dict.error_list.empty.categoryList : dict.error_list.no_budget_id}
                        onButtonClick={this.state.budgetId !== '' ? () => Actions.MasterCategoryList() : () => Actions.BudgetList()}
                    />
                    :
                    <List
                        itemList={this.state.data}
                        onItemPress={id => this.viewExpenses(id)}
                        onItemLongPress={id => this.previewCategory(id)}
                        isEditable={item => { return (this.state.userEmail == item.budgetData.admin) }}
                        isDeletable={item => { return (this.state.userEmail == item.budgetData.admin) }}
                        itemAction={async props => {
                            await AsyncStorage.setItem('categoryId', `${props.id}`)
                            await AsyncStorage.setItem('action', `${props.action}`)
                            Actions.Category()
                        }}
                        selectedItemId={this.state.selectedCategoryId}
                    />}
                    {this.state.userEmail == this.state.budgetAdminEmail ? 
                    (<ActionButton buttonColor='#29ABE2' position="center" onPress={async () => {
                        await AsyncStorage.setItem('action', 'add')
                        Actions.Category()
                    }}>
                        <Icon name='add' style={style.actionButtonIcon} />
                    </ActionButton>) : (<View></View>)
                    }
                </View>
                ) : (
                    <Container style={style.spinner}>
                        <Spinner color='blue'/>
                    </Container>
                )}
            </Container>
        )
    }
}