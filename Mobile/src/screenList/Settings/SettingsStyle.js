import { StyleSheet } from 'react-native'

export const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
    //   justifyContent: 'center',
    },
    containerChangePassword: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center'
    },
    divider:{ 
      borderBottomEndRadius: 0.8,
      borderBottomColor: '#A5A5A5',
      borderBottomWidth: 1,
    },
    listItems: {
      flexDirection: 'column'
    },
    signinPasswordRecCont: {
      paddingBottom: 200
    },
      signupText: {
      color: 'rgba(41, 171, 226,0.6)',
      fontSize: 16,
    },
    inputBox: {
      width:300,
      backgroundColor:'rgba(41, 171, 226,0.5)',
      borderRadius: 25,
      paddingHorizontal:16,
      fontSize:16,
      color:'#ffffff',
      marginVertical: 10
    },
    inputBoxValidation: {
      borderWidth: 2,
      borderColor: '#ff0000'
    },
    button: {
      width:300,
      backgroundColor:'#29ABE2',
      borderRadius: 25,
      marginVertical: 10,
      paddingVertical: 13
    },
    buttonSignOut: {
      width:300,
      borderColor:'#29ABE2',
      borderWidth: 1,
      borderRadius: 25,
      marginVertical: 10,
      paddingVertical: 13
    },
    buttonTextSignOut: {
      fontSize:16,
      fontWeight:'500',
      color:'#29ABE2',
      textAlign:'center'
    },
    buttonText: {
      fontSize:16,
      fontWeight:'500',
      color:'#ffffff',
      textAlign:'center'
    },
    buttonContent: {
      alignItems: 'center',
      paddingTop: 25
    },
    settingsText: {
      paddingTop: 10,
      color: '#0080FF'
    },
    textInputFocus: {
      borderWidth: 2,
      borderColor:'#26A0D4',
    }
  })