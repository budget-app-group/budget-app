import React, { Component } from 'react'
import { View, StyleSheet, TouchableNativeFeedback, TouchableOpacity, ToastAndroid, Alert } from 'react-native'
import { Container, Content, Button, ListItem, Text, Icon, Left, Body, Right, Switch, Toast } from 'native-base'
import { TextInput, TouchableWithoutFeedback } from 'react-native-gesture-handler'
import * as dict from '../../common/dictionary/polish.json'
import { updateAccount } from '../../apiCallList/settings'
import { styles } from './SettingsStyle'
import AsyncStorage from '@react-native-community/async-storage'
import { Actions } from 'react-native-router-flux'
import { signOut, deleteAccount } from '../../apiCallList/auth'

export default class SettingsScreen extends Component {
    constructor () {
        super ()
        this.state = {
          newsletterSwitch: false,
          notificationsSwitch: false,
          user: {
            email: '',
            name: '',
            surname: ''
          },
          email: '',
          name: '',
          surname: '',
          data: []
        }
      }
    async componentDidMount () {
      const user = await AsyncStorage.getItem('user')
      this.setState({
        user: JSON.parse(user)
      })
    } 

    async updateAccount() {
      try {
        response = await updateAccount({
          name: this.state.name == '' ? this.state.user.name : this.state.name,
          surname: this.state.surname == '' ? this.state.user.surname : this.state.surname,
          email: this.state.email == '' ? this.state.user.email : this.state.email
        })
        if (response) { ToastAndroid.show(dict.toast.changesSaved, ToastAndroid.SHORT)}
        await AsyncStorage.setItem('user', JSON.stringify(response))
        Actions.HomeScreen()
      }
      catch {
        ToastAndroid.show(dict.toast.settings_error, ToastAndroid.SHORT)
      }
    }

    onChange (stateName, text) {
      this.setState({
        [stateName]: text
      })
    }

    userSignOut () {
      const check = signOut()
      if (check) {
        Actions.SignIn() 
      }
    }

    async deleteUserAccount () {
      await deleteAccount()
      ToastAndroid.show(`${dict.settings.accountDeleted}`, ToastAndroid.SHORT)
      this.userSignOut()
    }
    
    render() {
        return(
            <Container>
              <Content>
              <ListItem style={styles.listItems}>
                <View>
                  <Text>{dict.settings.email}</Text>
                </View>
                <View>
                  <TextInput 
                    placeholder={this.state.user.email}
                    value={this.state.email}
                    onChangeText={text => this.onChange('email', text)}></TextInput>
                </View>
              </ListItem>
              <ListItem style={styles.listItems}>
                <View>
                  <Text>{dict.settings.name}</Text>
                </View>
                <View>
                  <TextInput 
                    placeholder={this.state.user.name}
                    value={this.state.name}
                    onChangeText={text => this.onChange('name', text)}></TextInput>
                </View> 
              </ListItem> 
              <ListItem style={styles.listItems}>    
                <View>
                  <Text>{dict.settings.surename}</Text>
                </View>
                <View>
                    <TextInput 
                      placeholder={this.state.user.surname}
                      value={this.state.surname}
                      onChangeText={text => this.onChange('surname', text)}></TextInput>
                </View>
              </ListItem>
              <ListItem style={styles.listItems}>    
                <View>
                  <Text>{dict.settings.password}</Text>
                </View>
                <View>
                  <TouchableOpacity onPress={Actions.ChangePassword}>
                    <Text style={styles.settingsText}>{dict.placeholder.change_password}</Text>
                  </TouchableOpacity>
                </View>
              </ListItem>    
              <View style={styles.divider}></View>
                {/* <ListItem>
                  <Left>
                    <Text>{dict.settings.notifications}</Text>
                  </Left>
                  <Right>
                    <Switch value={this.state.notificationsSwitch} />
                  </Right>
                </ListItem>
                <ListItem>
                  <Left>
                    <TouchableNativeFeedback>
                      <Text>{dict.settings.help}</Text>
                    </TouchableNativeFeedback>
                  </Left>
                </ListItem> */}
                <ListItem>
                  <Left>
                    <TouchableOpacity onPress={ () => {
                      Alert.alert(
                        `${dict.header.delete}`,
                        `${dict.user_delete_question}`,
                        [ {text: `${dict.permission_list.no}`, style: 'destructive'},
                          {text: `${dict.permission_list.yes}`, onPress: () => this.deleteUserAccount(), style: 'destructive'},
                        ],
                        {cancelable: false},
                      )
                    }}>
                      <Text style={{color: "#ff0000"}}>{dict.settings.deleteAccount}</Text>
                    </TouchableOpacity>
                  </Left>
                </ListItem>
                <View style={styles.buttonContent}>
                  <TouchableOpacity onPress={() => this.updateAccount()} style={styles.button}>
                    <Text style={styles.buttonText}>{dict.header.save}</Text>
                  </TouchableOpacity>
                  <TouchableOpacity onPress={this.userSignOut} style={styles.buttonSignOut}>
                    <Text style={styles.buttonTextSignOut}>{dict.header.signout}</Text>
                  </TouchableOpacity>
                </View>
              </Content>
            </Container>
        )
    }
}