import React, { Component } from 'react'
import { View, StyleSheet, TouchableNativeFeedback, TouchableOpacity, ToastAndroid } from 'react-native'
import { Container, Content, Button, ListItem, Text, Icon, Left, Body, Right, Switch } from 'native-base'
import { TextInput, TouchableWithoutFeedback } from 'react-native-gesture-handler'
import * as dict from '../../common/dictionary/polish.json'
import { changePassword } from '../../apiCallList/settings'
import { styles } from './SettingsStyle'
import { Actions } from 'react-native-router-flux'
import AsyncStorage from '@react-native-community/async-storage'

export default class ChangePasswordScreen extends Component {
    constructor () {
    super ()
      this.state = {
        password: '',
        passwordRepeat: '',
        passwordValidation: false,
        passwordRepeatValidation: false,
        isInputOneFocus: false,
        isInputTwoFocus: false
      }
    }

async changePass(){
    try {
        if (this.state.password == this.state.passwordRepeat) {
          response = await changePassword({
            password: this.state.password
          })
          Actions.Settings()
        }
        else{
          ToastAndroid.show(dict.toast.password_change_error, ToastAndroid.SHORT)
        }
    }
    catch (error) {
        errorHandler(error)
        ToastAndroid.show(dict.toast.password_change_error, ToastAndroid.SHORT)
    }
}

    validate(type, text) {
      var pass = /^.{8,16}$/
      var passRepeat = this.state.password
      if (type == 'password') {
        if(pass.test(text)) {
          this.setState({
            passwordValidation: false
          })
        }
        else {
          this.setState({
            passwordValidation: true
          })
        }
      }
      if (type == 'passwordRepeat') {
        if (this.state.password === text) {
          this.setState({
            passwordRepeatValidation: false
          })
        } else {
          this.setState({
            passwordRepeatValidation: true
          })
        }
      }
    }

    onChange (stateName, text) {
        this.setState({
          [stateName]: text
        })
      }

      handlerFocusBlur = (input, value) => {
        this.setState({[input]:value})
    }

    render() {
        return(
          <Container>
            <View style={styles.containerChangePassword}>
              <TextInput
                style={[styles.inputBox, this.state.passwordValidation ? styles.inputBoxValidation : null, this.state.isInputOneFocus ? styles.textInputFocus : '']} 
                underlineColorAndroid='rgba(0,0,0,0)' 
                placeholder={dict.placeholder.password}
                value={this.state.password}
                onChangeText={(text) => {this.validate('password', text); this.onChange('password', text); }}
                placeholderTextColor = '#ffffff'
                selectionColor='#fff'
                onFocus={this.handlerFocusBlur.bind(this, 'isInputOneFocus', true)}
                onBlur={this.handlerFocusBlur.bind(this, 'isInputOneFocus', false)}
                secureTextEntry={true}
              />
              <TextInput
                style={[styles.inputBox, this.state.passwordRepeatValidation ? styles.inputBoxValidation : null, this.state.isInputTwoFocus ? styles.textInputFocus : '']}
                underlineColorAndroid='rgba(0,0,0,0)' 
                placeholder={dict.placeholder.passwordRepeat}
                value={this.state.passwordRepeat}
                onChangeText={(text) => {this.validate('passwordRepeat', text); this.onChange('passwordRepeat', text); }}
                placeholderTextColor = '#ffffff'
                selectionColor='#fff'
                onFocus={this.handlerFocusBlur.bind(this, 'isInputTwoFocus', true)}
                onBlur={this.handlerFocusBlur.bind(this, 'isInputTwoFocus', false)}
                secureTextEntry={true}
              />
              <View style={styles.buttonContent}>
                <TouchableOpacity onPress={() => this.changePass()} style={styles.button}>
                  <Text style={styles.buttonText}>{dict.header.save}</Text>
                </TouchableOpacity>
              </View>
            </View>
          </Container>
          )
    }
}