import React, { Component } from 'react'
import { View } from 'react-native'
import { Container, Icon, Spinner } from 'native-base'
import { style } from '../../common/style/main'
import ActionButton from 'react-native-action-button'
import { Actions } from 'react-native-router-flux'
import List from '../../componentList/List/List'
import EmptyList from '../../componentList/EmptyList/EmptyList'
import * as dict from '../../common/dictionary/polish.json'
import { styles } from './BudgetListScreenStyle'
import AsyncStorage from '@react-native-community/async-storage'
import { getBudgetList } from '../../apiCallList/budget'
import Logo from '../../common/iconList/Logo.png'

export default class BudgetListScreen extends Component {
    constructor () {
        super ()
        this.state = {
            data: [],
            isDataEmpty: false,
            selectedBudgetId: '',
            user: null,
            active: false,

            isDataReady: false
        }
    }

    async componentDidMount () {
        let user = await AsyncStorage.getItem('user')
        user = JSON.parse(user)
        let response = await getBudgetList()
        if (response) {
            this.setState({
                data: response.map(element => {
                    return Object.assign(element, { image: Logo, key: `${element.id}` })
                }),
                isDataEmpty: !response.length,
                user: user || null
            })
        }
        this.setState({isDataReady: true})
    }

    async selectBudget (id) {
        let budget = await this.state.data.filter(el => el.id === id)[0]
        await AsyncStorage.setItem('budgetName', `${budget.name}`)
        await AsyncStorage.setItem('budgetAdminEmail', `${budget.admin.email}`)
        await AsyncStorage.setItem('budgetId', `${id}`)
        this.setState({ selectedBudgetId: this.state.selectedBudgetId === id ? '' : id }, () => {
            Actions.HomeScreen()
        })
    }

    async preview (id) {
        await AsyncStorage.setItem('budgetId', `${id}`)
        await AsyncStorage.setItem('action', 'preview')
        Actions.Budget()
    }

    render () {
        return (
            <Container>
                { this.state.isDataReady ? (
                <View style={styles.container}>
                    {this.state.isDataEmpty ?
                    <EmptyList description={dict.error_list.empty.bugdetList} />
                    :
                    <List
                        itemList={this.state.data}
                        onItemPress={id => this.selectBudget(id)}
                        onItemLongPress={id => this.preview(id)}
                        isEditable={budget => { return (this.state.user && this.state.user.id === budget.admin.id) } }
                        isDeletable={budget => { return (this.state.user && this.state.user.id === budget.admin.id) } }
                        itemAction={async props => {
                            await AsyncStorage.setItem('budgetId', `${props.id}`)
                            await AsyncStorage.setItem('action', `${props.action}`)
                            Actions.Budget()
                        }}
                        selectedItemId={this.state.selectedBudgetId}
                    />}
                    <ActionButton buttonColor='#29ABE2' position="center" onPress={async () => {
                        await AsyncStorage.setItem('action', 'add')
                        Actions.Budget()
                    }}>
                        <Icon name='add' style={style.actionButtonIcon} />
                    </ActionButton>
                </View>
                ) : (
                    <Container style={styles.container}>
                        <Spinner color='blue'/>
                    </Container>
                )}
            </Container>
        )
    }
}