import { StyleSheet } from 'react-native'

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  bottomButton: {
      position: 'absolute',
      bottom: 0,
      right: 0,
      borderWidth: 1,
      resizeMode: 'contain',
      borderRadius: 50,
      borderColor: '#432db7',
      borderWidth: 2,
      backgroundColor: '#5542bc',
      height: 60,
      width: 60,
      marginBottom: 30,
      marginRight: 30,
      justifyContent: 'center',
      alignItems: 'center'
  },
  image: {
    height: 30,
    width: 30
  }
})