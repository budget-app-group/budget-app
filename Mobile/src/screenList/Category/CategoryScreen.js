import AsyncStorage from '@react-native-community/async-storage'
import { Container, Spinner } from 'native-base'
import React, { Component } from 'react'
import { Alert, Image, Keyboard, ScrollView, Text, TextInput, ToastAndroid, TouchableOpacity, TouchableWithoutFeedback, View } from 'react-native'
import { TouchableNativeFeedback } from 'react-native-gesture-handler'
import { Actions } from 'react-native-router-flux'
import { addCategory, deleteCategory, getCategory, updateCategory } from '../../apiCallList/category'
import * as dict from '../../common/dictionary/polish.json'
import Logo from '../../common/iconList/Logo.png'
import { styles } from './CategoryScreenStyle'

export default class CategoryScreen extends Component {
    constructor () {
        super ()
        this.state = {
            categoryId: '',
            budgetId: '',
            masterCategoryId: '',
            name: '',
            image: Logo,
            action: '',
            isEditable: true,
            isDataReady: false,
            isInputOneFocus: false,
        }
    }

    async componentDidMount () {
        const budgetId = await AsyncStorage.getItem('budgetId')
        const action = await AsyncStorage.getItem('action')
        const masterCategoryId = await AsyncStorage.getItem('masterCategoryId')
        const categoryId = await AsyncStorage.getItem('categoryId')

        if (action === 'add') {
            this.setState({
                budgetId: budgetId,
                masterCategoryId: masterCategoryId,
                action: 'add'
            })
        } else if (categoryId && masterCategoryId && action && budgetId) {
            const response = await getCategory(budgetId, masterCategoryId, categoryId)
            if (response) {
                this.setState({
                    categoryId: categoryId,
                    name: response.name,
                    budgetId: budgetId,
                    masterCategoryId: masterCategoryId,
                    action: action
                })
                if (action === 'preview') {
                    this.setState({
                        isEditable: false
                    })
                }
            }
            // #TODO validation
        }
        this.setState({isDataReady: true})
    }

    saveData = async () => {
        // #TODO validation
        let response = false
        if (this.state.action === 'add') {
            response = await addCategory(this.state.budgetId, this.state.masterCategoryId, { name: this.state.name })
            ToastAndroid.show(dict.toast[response ? 'categoryCreated' : 'operationFailed'], ToastAndroid.SHORT)
        } else if (this.state.action === 'edit') {
            response = await updateCategory(this.state.budgetId, this.state.masterCategoryId, this.state.categoryId, {
                name: this.state.name })
            ToastAndroid.show(dict.toast[response ? 'categoryEdited' : 'operationFailed'], ToastAndroid.SHORT)
        } else if (this.state.action === 'delete') {
            response = await deleteCategory(this.state.budgetId, this.state.masterCategoryId, this.state.categoryId)
            if (response) {
                ToastAndroid.show(dict.category_deleted, ToastAndroid.SHORT)
            }
        }
        if (response) {
            await AsyncStorage.removeItem('action')
            Actions.CategoryList()
        }
    }

    onChange (stateName, text) {
        this.setState({
            [stateName]: text
        })
    }

    showDeleteAlert() {
        Alert.alert(
            `${dict.header.delete}`,
            `${dict.delete_category}`,
            [ {text: `${dict.permission_list.no}`, onPress: () => Actions.CategoryList() ,style: 'destructive'},
              {text: `${dict.permission_list.yes}`, onPress: () => this.saveData(), style: 'destructive'},
            ],
            {cancelable: false},
        )
    }

    handlerFocusBlur = (input, value) => {
        this.setState({[input]:value})
    }

    render () {
        return (
            <Container style={{flex: 1}}>
                <ScrollView contentContainerStyle={{flexGrow: 1}}>
                    { this.state.isDataReady ? (
                    <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
                        <View style={styles.container}>
                            {this.state.action === 'delete' ? (
                                <View style={styles.container}>
                                    {this.showDeleteAlert()}
                                </View>
                            ) : (
                                    <View style={styles.container}>
                                        <TouchableNativeFeedback>
                                            <Image resizeMode='cover' source={Logo} style={styles.image}/>
                                        </TouchableNativeFeedback>  
                                        <Text style={styles.headertext}>{dict.header.name}</Text>
                                        <TextInput
                                            style={[styles.inputBox, this.state.isInputOneFocus ? styles.textInputFocus : '']}
                                            underlineColorAndroid='rgba(0,0,0,0)'
                                            placeholder={dict.placeholder.category_name}
                                            value={this.state.name}
                                            placeholderTextColor = '#29ABE2'
                                            selectionColor='#fff'
                                            onFocus={this.handlerFocusBlur.bind(this, 'isInputOneFocus', true)}
                                            onBlur={this.handlerFocusBlur.bind(this, 'isInputOneFocus', false)}
                                            // onSubmitEditing={()=> focus.this.saveData()}
                                            onChangeText={text => this.onChange('name', text)}
                                            editable={this.state.isEditable}
                                        />
                                        { this.state.isEditable ? (
                                            <TouchableOpacity onPress={this.saveData} style={styles.button}>
                                                <Text style={styles.buttonText}>{dict.header.save}</Text>
                                            </TouchableOpacity>
                                        ) : (
                                            <TouchableOpacity onPress={Actions.MasterCategoryList} style={styles.buttonOutlined}>
                                                <Text style={styles.buttonOutlinedText}>{dict.header.return}</Text>
                                            </TouchableOpacity>
                                        )}
                                    </View>
                            )}
                        </View>
                    </TouchableWithoutFeedback>
                    ) : (
                        <Container style={styles.container}>
                            <Spinner color='blue'/>
                        </Container>
                    )}
                </ScrollView>
            </Container>
        )
    }
}