import React, { Component } from 'react'
import { View } from 'react-native'
import { Container, Icon, Spinner } from 'native-base'
import AsyncStorage from '@react-native-community/async-storage'
import { getGoalsList } from '../../apiCallList/goal'
import Logo from '../../common/iconList/Logo.png'
import { style } from '../../common/style/main'
import EmptyList from '../../componentList/EmptyList/EmptyList'
import * as dict from '../../common/dictionary/polish.json'
import List from '../../componentList/List/List'
import { Actions } from 'react-native-router-flux'
import ActionButton from 'react-native-action-button'

export default class GoalListScreen extends Component {
    constructor () {
        super ()
        this.state = {
            budgetId: '',
            data: [],
            isDataEmpty: false,
            selectedGoalId: '',
            userEmail: '',
            isDataReady: false,
            budgetAdminEmail: ''
        }
    }

    async componentDidMount() {
        const budgetId = await AsyncStorage.getItem('budgetId')
        const admin = await AsyncStorage.getItem('budgetAdminEmail')
        let user = await AsyncStorage.getItem('user')
        user = JSON.parse(user)
        this.setState({userEmail: user.email, budgetAdminEmail: admin})
        if (budgetId) {
            const response = await getGoalsList(budgetId)
            this.setState({
                data: response.map(el => {
                    return Object.assign(el, { image: Logo, key: `${el.id}` })
                }),
                isDataEmpty: !response.length,
                budgetId: budgetId
            })
        } else {
            this.setState({
                isDataEmpty: true
            })
        }
        this.setState({isDataReady: true})
    }

    async selectGoal (id) {
        await AsyncStorage.setItem('goalId', `${id}`)
        await AsyncStorage.setItem('action', 'preview')
        this.setState({ selectedGoalId: this.state.selectedGoalId === id ? '': id}, () => {
            Actions.Goal()
        })
    }

    addDonation () {
        Actions.HomeScreen()
    }

    render() {
        return(
            <Container>
                { this.state.isDataReady ? (
                <View style={style.container}>
                    { this.state.isDataEmpty ?
                    <EmptyList
                        description = { this.state.budgetId !== '' ? dict.error_list.empty.goalList : dict.error_list.no_budget_id }
                        onButtonClick={() => this.state.budgetId !== '' ? Actions.HomeScreen() : Actions.BudgetList()}
                    /> 
                    :
                    <List
                        itemList={this.state.data}
                        onItemPress={id => this.selectGoal(id)}
                        onItemLongPress={id => this.selectGoal(id)}
                        isEditable={item => { return (this.state.userEmail == item.budgetData.admin) }}
                        isDeletable={item => { return (this.state.userEmail == item.budgetData.admin) }}
                        itemAction={async props => {
                            await AsyncStorage.setItem('goalId', `${props.id}`)
                            await AsyncStorage.setItem('action', `${props.action}`)
                            Actions.Goal()
                        }}
                        selectedItemId={this.state.selectedGoalId}
                        />}
                    {this.state.userEmail == this.state.budgetAdminEmail ? 
                    (<ActionButton buttonColor="#2BAAE0" position="center" onPress={async () => {
                            await AsyncStorage.setItem('action', 'add')
                            Actions.Goal()
                        }}>
                        <Icon name="add" style={style.actionButtonIcon} />
                    </ActionButton>) : (<View></View>)
                    }
                </View>
                ) : (
                    <Container style={style.spinner}>
                        <Spinner color='blue'/>
                    </Container>
                )}
            </Container>
        )
    }
}