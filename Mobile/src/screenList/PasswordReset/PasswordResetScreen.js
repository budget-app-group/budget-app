import React, { Component } from 'react'
import { View, StyleSheet, ToastAndroid,
   TouchableWithoutFeedback, Keyboard, Image, TextInput, TouchableOpacity } from 'react-native'
import { Container, Content, Button, ListItem, Text, Icon, Left, Body, Right, Switch } from 'native-base'
import { Actions } from 'react-native-router-flux'
import { styles } from './PasswordResetStyle'
import * as dict from '../../common/dictionary/polish.json'
import { resetPassword } from '../../apiCallList/auth'
import Logo from '../../common/iconList/Logo.png'

export default class PasswordResetScreen extends Component {
  constructor () {
    super ()
      this.state = {
        email: '',
        isInputOneFocus: false,
        }
      }

  async resetPassword() {
    const response = await resetPassword({
      email: this.state.email
    })
    if (response) {
      ToastAndroid.show(dict.toast.mail_sent, ToastAndroid.SHORT)
      Actions.SignIn()
    }
    else {
      ToastAndroid.show(dict.toast.mail_send_error, ToastAndroid.SHORT)
    }
  }

  onChange (stateName, text) {
    this.setState({
      [stateName]: text
    })
  }

  handlerFocusBlur = (input, value) => {
    this.setState({[input]:value})
}
    
    render() {
        return(
          <TouchableWithoutFeedback style={{ flex: 1 }} onPress={Keyboard.dismiss} accessible={false}>
            <View style={styles.container}>
              <View style={styles.container2}>
                <Text style={styles.logoText}>{dict.app_name}</Text>
                <Image style={styles.logoimage} source={Logo}/>
              </View>
                  <Text style={styles.mailText}>{dict.placeholder.mail}</Text>
                  <TextInput style={[styles.inputBox, this.state.isInputOneFocus ? styles.textInputFocus : '']}
                    underlineColorAndroid='rgba(0,0,0,0)'
                    placeholder={dict.placeholder.login}
                    onChangeText={text => this.onChange('email', text)} 
                    placeholderTextColor = "#ffffff"
                    selectionColor='#fff'
                    onFocus={this.handlerFocusBlur.bind(this, 'isInputOneFocus', true)}
                    onBlur={this.handlerFocusBlur.bind(this, 'isInputOneFocus', false)}
                    selectionColor="#fff"
                    keyboardType="email-address"
                  />
                  <TouchableOpacity onPress={() => this.resetPassword()} style={styles.button}>
                  <Text style={styles.buttonText}>{dict.header.reset}</Text>
                  </TouchableOpacity>
            </View>
          </TouchableWithoutFeedback>
        )
    }
}