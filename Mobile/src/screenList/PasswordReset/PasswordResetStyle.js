import { StyleSheet } from 'react-native'

export const styles = StyleSheet.create({
    container: {
      backgroundColor: '#fff',
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
    },
    signupTextCont: {
        flexGrow: 1,
      alignItems: 'flex-end',
      justifyContent: 'center',
      paddingVertical: 16,
      flexDirection: 'row',
    },
    signupText: {
        color: 'rgba(41, 171, 226,0.6)',
        fontSize: 16,
    },
    signupButton: {
        color: '#29ABE2',
        fontSize: 16,
        fontWeight: '500',
    },
    container1 : {
      flexGrow: 1,
      justifyContent:'center',
      alignItems: 'center'
    },
    inputBox: {
      width:300,
      backgroundColor:'#D4EEF9',
      borderRadius: 25,
      paddingHorizontal:16,
      fontSize:16,
      color:'#ffffff',
      marginVertical: 10
    },
    button: {
      width:300,
      backgroundColor:'#29ABE2',
       borderRadius: 25,
        marginVertical: 10,
        paddingVertical: 13
    },
    buttonText: {
      fontSize:16,
      fontWeight:'500',
      color:'#ffffff',
      textAlign:'center'
    },
    container2 : {
      marginBottom: 50,
      alignItems: 'center'
    },
    logoText : {
        marginVertical: 15,
        fontSize:35,
        color:'rgba(41, 171, 226,0.7)'
    },
    mailText: {
        // marginVertical: 15,
        margin: 23,
        fontSize:20,
        color:'rgba(41, 171, 226, 0.9)'
    },
    logoimage: {
      alignItems: 'center',
      height: 70,
      width: 75,
      justifyContent: 'center'
    },
    textInputFocus: {
      borderWidth: 2,
      borderColor:'#26A0D4',
    }
  })