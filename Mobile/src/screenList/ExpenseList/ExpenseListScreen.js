import AsyncStorage from '@react-native-community/async-storage'
import { Container, Icon, Spinner } from 'native-base'
import React, { Component } from 'react'
import { View } from 'react-native'
import ActionButton from 'react-native-action-button'
import { Actions } from 'react-native-router-flux'
import { getBudgetExpenseList, getExpensesFromBankAccount, getExpensesFromCategory } from '../../apiCallList/expense'
import * as dict from '../../common/dictionary/polish.json'
import Logo from '../../common/iconList/Logo.png'
import { style } from '../../common/style/main'
import EmptyList from '../../componentList/EmptyList/EmptyList'
import ExpenseListComponent from '../../componentList/ExpenseListComponent/ExpenseListComponent'
export default class ExpenseListScreen extends Component {
    constructor () {
        super ()
        this.state = {
            budgetId: '',
            data: [],
            isDataEmpty: false,
            selectedExpenseId: '',
            listType: '',
            isDataReady: false,
            userEmail: null
        }
    }

    async componentDidMount() {
        const budgetId = await AsyncStorage.getItem('budgetId')
        const bankAccount = JSON.parse(await AsyncStorage.getItem('bankAccount'))
        const expenseListType = await AsyncStorage.getItem('expenseListType')
        const category = JSON.parse(await AsyncStorage.getItem('category'))
        let user = await AsyncStorage.getItem('user')
        user = JSON.parse(user)
        this.setState({userEmail: user.email})
        let response = ''
        if (budgetId) {
            if (expenseListType === 'bankAccExp') {
                response = await getExpensesFromBankAccount(budgetId, bankAccount.id)
                Actions.refresh({key: 'ExpenseList', title: `Wydatki z konta: ${bankAccount.name}`})
            } else if (expenseListType === 'expenses'){
                response = await getBudgetExpenseList(budgetId)
            } else if (expenseListType === 'categoryExp') {
                response = await getExpensesFromCategory(budgetId, category.budgetCategory.id, category.id)
                Actions.refresh({key: 'ExpenseList', title: `${dict.header.categoryExpenses}: ${category.name}`})
            }
            this.setState({
                data: response.map(element => {
                    return Object.assign(element, { name: element.name, image: Logo, key: `${element.id}`})
                }),
                isDataEmpty: !response.length,
                budgetId: budgetId,
                listType: expenseListType
                
            })
        }
        else {
            this.setState({
                isDataEmpty: true
            })
        }
        this.setState({isDataReady: true})
    }

    async previewExpense (id) {
        let expense = await this.state.data.filter(element => element.id == id)[0]
        await AsyncStorage.setItem('expenseId', `${expense.id}`)
        await AsyncStorage.setItem('masterCategoryId', `${expense.category.budgetCategory.id}`)
        await AsyncStorage.setItem('categoryId', `${expense.category.id}`)
        await AsyncStorage.setItem('bankAccountId', `${expense.bankAccount.id}`)
        await AsyncStorage.setItem('action', 'preview')
        await AsyncStorage.setItem('screenType', 'expense')
        await AsyncStorage.setItem('prev', 'ExpenseList')
        this.setState({ selectedExpenseId: this.state.selectedExpenseId === id ? '' : id }, () => {
            Actions.Expense()
        })
    }

    render () {
        return (
            <Container>
                { this.state.isDataReady ? (
                <View style={style.container}>
                    {this.state.isDataEmpty ?
                    <EmptyList
                        description={this.state.budgetId !== '' ? dict.error_list.empty.expenseList : dict.error_list.no_budget_id}
                        onButtonClick={() => this.state.budgetId !== '' ? Actions.HomeScreen() : Actions.BudgetList()}
                    />
                    :
                    <ExpenseListComponent
                        itemList={this.state.data}
                        onItemPress={id => this.previewExpense(id)}
                        onItemLongPress={id => this.previewExpense(id)}
                        isEditable={item => { return ((this.state.userEmail == item.expenseOwner.email) || (this.state.userEmail == item.budgetData.admin)) }}
                        isDeletable={item => { return ((this.state.userEmail == item.expenseOwner.email) || (this.state.userEmail == item.budgetData.admin)) }}
                        itemAction={async props => {
                            let expense = await this.state.data.filter(element => element.id == props.id)[0]
                            await AsyncStorage.setItem('expenseId', `${props.id}`)
                            await AsyncStorage.setItem('action', `${props.action}`)
                            await AsyncStorage.setItem('masterCategoryId', `${expense.category.budgetCategory.id}`)
                            await AsyncStorage.setItem('categoryId', `${expense.category.id}`)
                            await AsyncStorage.setItem('bankAccountId', `${expense.bankAccount.id}`)
                            await AsyncStorage.setItem('screenType', 'expense')
                            Actions.Expense()
                        }}
                        selectedItemId={this.state.selectedExpenseId}
                    />}
                    <ActionButton buttonColor="#29ABE2" position="center" onPress={async () => {
                        await AsyncStorage.setItem('action', 'add')
                        await AsyncStorage.setItem('screenType', 'expense')
                        Actions.Expense()
                    }}>
                        <Icon name="add" style={style.actionButtonIcon} />
                    </ActionButton>
                </View>
                ) : (
                    <Container style={style.spinner}>
                        <Spinner color='blue'/>
                    </Container>
                )}
            </Container>
        )
    }
}