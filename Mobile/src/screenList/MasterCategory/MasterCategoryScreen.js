import AsyncStorage from '@react-native-community/async-storage'
import { Container, Spinner } from 'native-base'
import React, { Component } from 'react'
import { Alert, Image, Keyboard, ScrollView, Text, TextInput, ToastAndroid, TouchableOpacity, TouchableWithoutFeedback, View } from 'react-native'
import ColorPalette from 'react-native-color-palette'
import Dialog from "react-native-dialog"
import { TouchableNativeFeedback } from 'react-native-gesture-handler'
import { Actions } from 'react-native-router-flux'
import { addMasterCategory, deleteMasterCategory, getMasterCategory, updateMasterCategory } from '../../apiCallList/masterCategory'
import * as dict from '../../common/dictionary/polish.json'
import Logo from '../../common/iconList/Logo.png'
import { styles } from './MasterCategoryScreenStyle'

export default class MasterCategoryScreen extends Component {
  constructor() {
    super()
    this.onColorChange = this.onColorChange.bind(this)
    this.state = {
      budgetId: '',
      id: '',
      name: '',
      budgetAllocation: '',
      image: Logo,
      action: '',
      isEditable: true,
      color: '#919191',
      newColor: '#919191',
      colorPickerDialog: false,
      isDataReady: false,
      isInputOneFocus: false,
      isInputTwoFocus: false
    }
  }


  async componentDidMount() {
    const budgetId = await AsyncStorage.getItem('budgetId')
    const action = await AsyncStorage.getItem('action')
    const id = await AsyncStorage.getItem('masterCategoryId')

    if (action === 'add') {
      this.setState({
        budgetId: budgetId,
        action: 'add'
      })
    } else if (budgetId && id) {
      const response = await getMasterCategory(budgetId, id)
      if (response) {
        this.setState({
          id: id,
          budgetId: budgetId,
          name: response.name,
          color: response.color,
          budgetAllocation: `${response.budgetAllocation}`,
          action: action
        })
        if (action === 'preview') {
          this.setState({
            isEditable: false
          })
        }
      }
      // #TODO validation
    }
    this.setState({ isDataReady: true })
  }

showDeleteAlert() {
  Alert.alert(
      `${dict.header.delete}`,
      `${dict.delete_master_cat}`,
      [ {text: `${dict.permission_list.no}`, onPress: () => Actions.MasterCategoryList() ,style: 'destructive'},
        {text: `${dict.permission_list.yes}`, onPress: () => this.saveData(), style: 'destructive'},
      ],
      {cancelable: false},
  )
}

  saveData = async () => {
    // #TODO validation
    const budgetAllocation = this.state.budgetAllocation.replace(',', '.')
    let response = false
    if (this.state.action === 'add') {
      if (this.state.budgetAllocation === '') {
       ToastAndroid.show(dict.error_list.empty.fullfilData, ToastAndroid.SHORT)
      } else {
      response = await addMasterCategory(this.state.budgetId, {
        name: this.state.name,
        budgetAllocation: JSON.parse(budgetAllocation),
        color: this.state.color
      })
      ToastAndroid.show(dict.toast[response ? 'masterCatCreated' : 'operationFailed'], ToastAndroid.SHORT)
    }
    } else if (this.state.action === 'edit') {
      response = await updateMasterCategory(this.state.budgetId, this.state.id, {
        name: this.state.name,
        budgetAllocation: JSON.parse(budgetAllocation),
        color: this.state.color
      })
      ToastAndroid.show(dict.toast[response ? 'masterCatEdited' : 'operationFailed'], ToastAndroid.SHORT)
    } else if (this.state.action === 'delete') {
      response = await deleteMasterCategory(this.state.budgetId, this.state.id)
      if (response) {
        ToastAndroid.show(dict.master_cat_deleted, ToastAndroid.SHORT)
      }
    }
    if (response) {
      await AsyncStorage.removeItem('action')
      Actions.MasterCategoryList()
    }
  }

  onChange(stateName, text) {
    this.setState({
      [stateName]: text
    })
  }

  onColorChange(newColor) {
    this.setState({
      color: newColor,
      colorPickerDialog: false
    })
  }

  showDialog = () => {
    if (this.state.isEditable) { this.setState({ colorPickerDialog: true }) }
  }

  handleCancel = () => {
    this.setState({ colorPickerDialog: false })
  }

  handlerFocusBlur = (input, value) => {
    this.setState({[input]:value})
}

  render() {
    // let selectedColor = this.state.color
    return (
      <Container style={{flex: 1}}>
          <ScrollView contentContainerStyle={{flexGrow: 1}}>
            {this.state.isDataReady ? (
              <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
                <View>
                  {this.state.action === 'delete' ? (
                    <View style={styles.container}>
                      {this.showDeleteAlert()}
                    </View>
                  ) : (
                      <View style={styles.container}>
                        <TouchableNativeFeedback>
                          <Image resizeMode='cover' source={Logo} style={styles.image} />
                        </TouchableNativeFeedback>
                        <Text style={styles.headertext}>{dict.header.name}</Text>
                        <TextInput
                          style={[styles.inputBox, this.state.isInputOneFocus ? styles.textInputFocus : '']}
                          underlineColorAndroid='rgba(0,0,0,0)'
                          placeholder={dict.placeholder.category_name}
                          value={this.state.name}
                          placeholderTextColor='#29ABE2'
                          selectionColor='#fff'
                          onFocus={this.handlerFocusBlur.bind(this, 'isInputOneFocus', true)}
                          onBlur={this.handlerFocusBlur.bind(this, 'isInputOneFocus', false)}
                          onChangeText={text => this.onChange('name', text)}
                          editable={this.state.isEditable}
                        />

                        <Text style={styles.headertext}>{dict.header.categoryColor}</Text>
                        <TouchableNativeFeedback onPress={this.showDialog}>
                          <View style={[styles.dot, { backgroundColor: this.state.color }]}></View>
                        </TouchableNativeFeedback>
                        <View>
                          <Dialog.Container visible={this.state.colorPickerDialog}>
                            <Dialog.Title style={styles.text}>{dict.categoryLabels.chooseColor}</Dialog.Title>
                            <ColorPalette
                              onChange={color => this.onColorChange(color)}
                              value={this.state.color}
                              colors={['#980000', '#CC4125','#ff0000', '#ff9900', '#ffff00', '#00ff00', '#38761d', '#00ffff', '#4A86E8', '#0000ff', '#9900ff', '#351C75', '#FF00FF']}
                              title={""}
                              icon={
                                <Text>✔</Text>
                              }
                            />
                            <Dialog.Button style={styles.text} label="Anuluj" onPress={this.handleCancel} />
                          </Dialog.Container>
                        </View>
                        <Text style={styles.headertext}>{dict.header.budget_allocation}</Text>
                        <TextInput
                          keyboardType="numeric"
                          style={[styles.inputBox, this.state.isInputTwoFocus ? styles.textInputFocus : '']}
                          underlineColorAndroid='rgba(0,0,0,0)'
                          placeholder={dict.placeholder.budget_allocation}
                          value={this.state.budgetAllocation}
                          placeholderTextColor='#29ABE2'
                          selectionColor='#fff'
                          onFocus={this.handlerFocusBlur.bind(this, 'isInputTwoFocus', true)}
                          onBlur={this.handlerFocusBlur.bind(this, 'isInputTwoFocus', false)}
                          onChangeText={text => this.onChange('budgetAllocation', text)}
                          editable={this.state.isEditable}
                        />
                        {this.state.isEditable ? (
                          <TouchableOpacity onPress={this.saveData} style={styles.button}>
                            <Text style={styles.buttonText}>{dict.header.save}</Text>
                          </TouchableOpacity>
                        ) : (
                            <TouchableOpacity onPress={Actions.MasterCategoryList} style={styles.buttonOutlined}>
                              <Text style={styles.buttonOutlinedText}>{dict.header.return}</Text>
                            </TouchableOpacity>
                          )}
                        </View>
                    )}
                  {/* <View style={{ flex: 0.2 }} /> */}
                </View>
              </TouchableWithoutFeedback>
            ) : (
              <Container style={styles.container}>
                  <Spinner color='blue' />
                </Container>
              )}
          </ScrollView>
      </Container>
    )
  }
}