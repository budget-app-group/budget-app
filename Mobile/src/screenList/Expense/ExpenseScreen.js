import AsyncStorage from '@react-native-community/async-storage'
import { Container, DatePicker, Spinner } from 'native-base'
import React, { Component } from 'react'
import { Alert, Keyboard, ScrollView, Text, TextInput, ToastAndroid, TouchableOpacity, TouchableWithoutFeedback, View } from 'react-native'
import RNPickerSelect from 'react-native-picker-select'
import { Actions } from 'react-native-router-flux'
import { Chevron } from 'react-native-shapes'
import { getBankAccountList } from '../../apiCallList/bankAccount'
import { getAllCategories } from '../../apiCallList/category'
import { addExpense, deleteExpense, getExpense, updateExpense } from '../../apiCallList/expense'
import * as dict from '../../common/dictionary/polish.json'
import { styles } from './ExpenseScreenStyle'

export default class ExpenseScreen extends Component {
    constructor () {
        super ()
        this.state = {
            id: '',
            budgetId: -1,
            masterCategoryId: -1,
            categoryId: -1,
            action: '',
            isEditable: true,

            isCategoryPickedColor: '#ff0000',
            isBankAccPickedColor: '#ff0000',
            
            // data to send below
            amount: '',
            name: '',
            expenseDate: new Date(),
            bankAccountId: -1,
            shopName: '',
            isIncome: false,
            categoryList: [],
            bankAccountList: [],
            isDataReady: false,
            isInputOneFocus: false,
            isInputTwoFocus: false,
            isInputThreeFocus: false,
        }
    }

    async componentDidMount () {
        const budgetId = await AsyncStorage.getItem('budgetId')
        const masterCategoryId = await AsyncStorage.getItem('masterCategoryId')
        const categoryId = await AsyncStorage.getItem('categoryId')
        const bankAccountId = await AsyncStorage.getItem('bankAccountId')
        const expenseId = await AsyncStorage.getItem('expenseId')
        const incomeId = await AsyncStorage.getItem('incomeId')
        const action = await AsyncStorage.getItem('action')
        const screenType = await AsyncStorage.getItem('screenType')
        let expenseObject = await AsyncStorage.getItem('expenseObject')
        if (screenType === 'income') {
            await this.setState({
                isIncome: true
            })
            await Actions.refresh({key: 'Expense', title: 'Przychód'})
        }
        const getId = this.state.isIncome ? incomeId : expenseId
        if (expenseObject) {
            expenseObject = JSON.parse(expenseObject)
            this.setState({
                action: 'add',
                budgetId,
                amount: isNaN(expenseObject.amount) ? '0' : `${expenseObject.amount}`,
                name: expenseObject.name === 'ocrParseError' ? '' : expenseObject.name,
                expenseDate: expenseObject.expenseDate <= 1420070400 ? new Date() : new Date(expenseObject.expenseDate),
                shopName: expenseObject.shopName === 'ocrParseError' ? '' : expenseObject.shopName
            }, async () => await AsyncStorage.removeItem('expenseObject'))
        } else {
            if (action === 'add') {
                this.setState({
                    action: 'add',
                    budgetId,
                    masterCategoryId,
                    categoryId,
                })
            } else if (getId) {
                const response = await getExpense(budgetId, masterCategoryId, categoryId, getId)
                if (response) {
                    this.setState({
                        budgetId,
                        masterCategoryId,
                        categoryId: JSON.parse(response.category.id),
                        bankAccountId: JSON.parse(bankAccountId),
                        id: response.id,
                        action: action,
                        amount: `${response.amount}`,
                        name: response.name,
                        expenseDate: new Date(response.expenseDate),
                        bankAccount: response.bankAccount,
                        category: response.category,
                        shopName: response.shopName || '',
                        isIncome: response.income,
                        isCategoryPickedColor: '#29ABE2',
                        isBankAccPickedColor: '#29ABE2'
                    })
                }
                // #TODO validation
            }
        }

        const bankAccounts = await getBankAccountList(budgetId)
        if (bankAccounts.length > 0) {
            this.setState({
                bankAccountList: bankAccounts.map( ba => ({ label: ba.name, value: ba.id }))
            })
            if (bankAccounts.length === 1) this.setState({bankAccountId: bankAccounts[0].id})
        }

        const categories = await getAllCategories(budgetId)
        if (categories.length > 0) {
            this.setState({
                categoryList: categories.map( cat => Object.assign(cat, { label: cat.name, value: cat.id }))
            })
           if (categories.length === 1)  this.setState({categoryId: categories[0].id})
        }

        if (action === 'preview') {
            this.setState({
                isEditable: false
            })
        }
        this.setState({isDataReady: true})
    }

    setMasterCat = async () => {
        let category = this.state.categoryList.find( cat => {
            return cat.id === this.state.categoryId
        })
        category ? await this.setState({
            masterCategoryId: JSON.parse(category.budgetCategory.id)
        }) : (
            ToastAndroid.show(dict.error_list.empty.noCategory, ToastAndroid.SHORT)
        )
    }

    saveData = async () => {
        // #TODO validation and onLeave
        const masterCategoryId = await AsyncStorage.getItem('masterCategoryId')
        const categoryId = await AsyncStorage.getItem('categoryId')
        const expenseAmount = this.state.amount.replace(',', '.')
        let response = false
        if (this.state.action === 'add') {
            if (this.state.bankAccountId === -1) {
                ToastAndroid.show(dict.error_list.empty.noBankAcc, ToastAndroid.SHORT)
            } else {
                if (this.state.amount === '' || this.state.name === '') {
                    ToastAndroid.show(dict.error_list.empty.fullfilData, ToastAndroid.SHORT)
                } else {
                response = await addExpense(this.state.budgetId, this.state.masterCategoryId,
                    this.state.categoryId, {
                        amount: JSON.parse(expenseAmount),
                        name: this.state.name,
                        expenseDate: this.state.expenseDate.getTime(),
                        bankAccountId: this.state.bankAccountId,
                        shopName: this.state.shopName,
                        income: this.state.isIncome
                    })
                    if (response) { 
                        ToastAndroid.show(this.state.isIncome ?  dict.toast.incomeCreated : dict.toast.expenseCreated, ToastAndroid.SHORT)
                    } else {
                        ToastAndroid.show(`${dict.toast.operationFailed}`, ToastAndroid.SHORT)
                    }
                }
            }
        } else if (this.state.action === 'edit') {
            response = await updateExpense(this.state.budgetId, masterCategoryId,
                categoryId, this.state.id, {
                    amount: JSON.parse(expenseAmount),
                    name: this.state.name,
                    expenseDate: this.state.expenseDate.getTime(),
                    bankAccountId: this.state.bankAccountId,
                    shopName: this.state.shopName,
                    income: this.state.isIncome,
                    mastCatId: this.state.masterCategoryId,
                    categoryId: this.state.categoryId,

                })
                if (response) { 
                    ToastAndroid.show(this.state.isIncome ?  dict.toast.incomeEdited : dict.toast.expenseEdited, ToastAndroid.SHORT)
                } else {
                    ToastAndroid.show(dict.toast.operationFailed, ToastAndroid.SHORT)
                }
        } else if (this.state.action === 'delete') {
            response = await deleteExpense(this.state.budgetId, this.state.masterCategoryId,
                this.state.categoryId, this.state.id)
            if (response) {
                ToastAndroid.show(`${dict.expense_deleted}`, ToastAndroid.SHORT)
            }
        }
        if (response) {
            this.state.isIncome ? Actions.IncomeList() : Actions.ExpenseList()
        }
    }

    onChange (stateName, text) {
        this.setState({
            [stateName]: text
        })
    }

    showDeleteAlert() {
        Alert.alert(
            `${dict.header.delete}`,
            `${dict.delete_expense}`,
            [ {text: `${dict.permission_list.no}`, onPress: () => this.state.isIncome ? Actions.IncomeList() : Actions.ExpenseList() ,style: 'destructive'},
              {text: `${dict.permission_list.yes}`, onPress: () => this.saveData(), style: 'destructive'},
            ],
            {cancelable: false},
        )
    }

    handlerFocusBlur = (input, value) => {
        this.setState({[input]:value})
    }

    render () {
        const bankAccPlaceholder = {
            label: `${dict.placeholder.bankAccountPlaceholder}`,
            value: null,
            color: '#9EA0A4',
        }
        const categoryPlaceHolder = {
            label: `${dict.placeholder.categoryPlaceholder}`,
            value: null,
            color: '#9EA0A4',
        }
        return (
            <Container style={{flex: 1}}>
                <ScrollView contentContainerStyle={{flexGrow: 1}}>
                { this.state.isDataReady ? (
                <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
                    <View style={styles.container}>
                        {this.state.action === 'delete'? (
                            <View>
                                {this.showDeleteAlert()}
                            </View>
                        ) : (
                            <View style={styles.container}>
                                <Text style={styles.inputHeader}>{this.state.isIncome ? dict.header.amount : dict.header.price}</Text>
                                <TextInput
                                    keyboardType="numeric"
                                    style={[styles.inputBox, this.state.isInputOneFocus ? styles.textInputFocus : '']}
                                    underlineColorAndroid='rgba(0,0,0,0)'
                                    placeholder={ this.state.isIncome ? dict.placeholder.income_amount : dict.placeholder.amount}
                                    value={this.state.amount}
                                    placeholderTextColor = '#29ABE2'
                                    selectionColor='#fff'
                                    onFocus={this.handlerFocusBlur.bind(this, 'isInputOneFocus', true)}
                                    onBlur={this.handlerFocusBlur.bind(this, 'isInputOneFocus', false)}
                                    onChangeText={text => this.onChange('amount', text)}
                                    editable={this.state.isEditable}
                                />
                                <Text style={styles.inputHeader}>{dict.header.name}</Text>
                                <TextInput
                                    style={[styles.inputBox, this.state.isInputTwoFocus ? styles.textInputFocus : '']}
                                    underlineColorAndroid='rgba(0,0,0,0)'
                                    placeholder={ this.state.isIncome ? dict.placeholder.income_name : dict.placeholder.expense_name}
                                    value={this.state.name}
                                    placeholderTextColor = '#29ABE2'
                                    selectionColor='#fff'
                                    onFocus={this.handlerFocusBlur.bind(this, 'isInputTwoFocus', true)}
                                    onBlur={this.handlerFocusBlur.bind(this, 'isInputTwoFocus', false)}
                                    onChangeText={text => this.onChange('name', text)}
                                    editable={this.state.isEditable}
                                />
                                <View style={styles.containerCenter}>
                                    <Text style={styles.buttonOutlinedText}>Data: </Text>
                                    <DatePicker
                                        defaultDate={this.state.expenseDate}
                                        minimumDate={new Date(this.state.expenseDate.getFullYear() - 10,
                                            this.state.expenseDate.getMonth(), this.state.expenseDate.getDay() + 1)}
                                        maximumDate={new Date(this.state.expenseDate.getFullYear() + 10,
                                            this.state.expenseDate.getMonth(), this.state.expenseDate.getDay() + 1)}
                                        locale={'pl'}
                                        timeZoneOffsetInMinutes={undefined}
                                        modalTransparent={false}
                                        animationType={'fade'}
                                        androidMode={'default'}
                                        textStyle={{ color: '#29ABE2' }}
                                        placeHolderTextStyle={{ color: '#ffffff' }}
                                        onDateChange={newDate => this.setState({ expenseDate: newDate })}
                                        disabled={!this.state.isEditable} />
                                </View>
                                
                                <Text style={styles.inputHeader}>{dict.header.bankAccount}</Text>
                                <RNPickerSelect
                                    disabled={!this.state.isEditable}
                                    placeholder={bankAccPlaceholder}
                                    items={this.state.bankAccountList}
                                    onValueChange={value => {
                                        this.setState({
                                            bankAccountId: value,
                                            isBankAccPickedColor: value ? '#29ABE2' : '#ff0000'
                                        });
                                    }}
                                    style={{
                                    inputAndroid: {
                                        width: 300,
                                        height: 50,
                                        backgroundColor: '#ffffff',
                                        borderRadius: 50,
                                        paddingHorizontal: 16,
                                        marginTop: 7,
                                        fontSize: 16,
                                        color: '#29ABE2',
                                        borderColor: this.state.isBankAccPickedColor,
                                        borderWidth: 1,
                                    },
                                    iconContainer: {
                                        top:28,
                                        right: 20,
                                    },
                                    placeholder: {
                                        color: this.state.isBankAccPickedColor,
                                        fontSize: 16
                                    }
                                    }}
                                    value={this.state.bankAccountId}
                                    useNativeAndroidPickerStyle={false}
                                    Icon={() => {
                                    return <Chevron size={1.2} color='#29ABE2' />;
                                    }}
                                />
                                <View style={{ flex: 0.08 }} />
                                <Text style={styles.inputHeader}>{dict.header.category}</Text>
                                <RNPickerSelect
                                    disabled={!this.state.isEditable}
                                    placeholder={categoryPlaceHolder}
                                    items={this.state.categoryList}
                                    onValueChange={value => {
                                        this.setState({
                                            categoryId: value,
                                            isCategoryPickedColor: value ? '#29ABE2' : '#ff0000'
                                        })
                                        // this.setMasterCat()
                                    }}
                                    style={{
                                    inputAndroid: {
                                        width: 300,
                                        height: 50,
                                        backgroundColor: '#ffffff',
                                        borderRadius: 50,
                                        paddingHorizontal: 16,
                                        fontSize: 16,
                                        marginTop: 7,
                                        color: '#29ABE2',
                                        borderColor: this.state.isCategoryPickedColor,
                                        borderWidth: 1,
                                    },
                                    iconContainer: {
                                        top:28,
                                        right: 20,
                                    },
                                    placeholder: {
                                        color: this.state.isCategoryPickedColor,
                                        fontSize: 16
                                      }
                                    }}
                                    value={this.state.categoryId}
                                    useNativeAndroidPickerStyle={false}
                                    Icon={() => {
                                    return <Chevron size={1.2} color='#29ABE2' />;
                                    }}
                                />
                                { !this.state.isIncome && <Text style={styles.inputHeader}>{dict.header.shopName}</Text>}
                                { !this.state.isIncome &&
                                <TextInput
                                    style={[styles.inputBox, this.state.isInputThreeFocus ? styles.textInputFocus : '']}
                                    underlineColorAndroid='rgba(0,0,0,0)'
                                    placeholder={dict.placeholder.shop_name}
                                    value={this.state.shopName}
                                    placeholderTextColor = '#29ABE2'
                                    selectionColor='#fff'
                                    onFocus={this.handlerFocusBlur.bind(this, 'isInputThreeFocus', true)}
                                    onBlur={this.handlerFocusBlur.bind(this, 'isInputThreeFocus', false)}
                                    onChangeText={text => this.onChange('shopName', text)}
                                    editable={this.state.isEditable}
                                />}
                                { this.state.isEditable ? (
                                    <TouchableOpacity onPress={async () => {
                                            await this.setMasterCat()
                                            this.saveData()
                                        }} style={styles.button}>
                                        <Text style={styles.buttonText}>{dict.header.save}</Text>
                                    </TouchableOpacity>
                                ) : (
                                    <TouchableOpacity onPress={this.state.isIncome ? Actions.IncomeList : Actions.ExpenseList} style={styles.buttonOutlined}>
                                        <Text style={styles.buttonOutlinedText}>{dict.header.return}</Text>
                                    </TouchableOpacity>
                                )}
                            </View>
                        )}
                    </View>
                </TouchableWithoutFeedback>
                ) : (
                    <Container style={styles.container}>
                        <Spinner color='blue'/>
                    </Container>
                )}
                </ScrollView>
            </Container>
        )
    }
}