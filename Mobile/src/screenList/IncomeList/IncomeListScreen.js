import AsyncStorage from '@react-native-community/async-storage'
import { Container, Icon, Spinner } from 'native-base'
import React, { Component } from 'react'
import { View } from 'react-native'
import ActionButton from 'react-native-action-button'
import { Actions } from 'react-native-router-flux'
import { getBudgetIncomes } from '../../apiCallList/income'
import * as dict from '../../common/dictionary/polish.json'
import Logo from '../../common/iconList/Logo.png'
import { style } from '../../common/style/main'
import EmptyList from '../../componentList/EmptyList/EmptyList'
import ExpenseListComponent from '../../componentList/ExpenseListComponent/ExpenseListComponent'

export default class IncomeListScreen extends Component {
  constructor () {
    super ()
    this.state = {
      budgetId: '',
      data: [],
      isDataEmpty: false,
      isDataReady: false,

      selectedIncomeId: ''
    }
  }

  async componentDidMount() {
    const budgetId = await AsyncStorage.getItem('budgetId')

    if (budgetId) {
      const response = await getBudgetIncomes(budgetId)
      this.setState({
        data: response.map(el => {
          return Object.assign(el, {name: el.name, image: Logo, key: `${el.id}` })
        }),
        isDataEmpty: !response.length,
        budgetId: budgetId
      })
    } else {
      this.setState({
        isDataEmpty: true
      })
    }
    this.setState({ isDataReady: true })
  }

  async previewIncome (id) {
    let income = await this.state.data.filter(element => element.id == id)[0]
    await AsyncStorage.setItem('incomeId', `${income.id}`)
    await AsyncStorage.setItem('masterCategoryId', `${income.category.budgetCategory.id}`)
    await AsyncStorage.setItem('categoryId', `${income.category.id}`)
    await AsyncStorage.setItem('bankAccountId', `${income.bankAccount.id}`)
    await AsyncStorage.setItem('action', 'preview')
    await AsyncStorage.setItem('screenType', 'income')
    Actions.Expense()
  }

  render () {
    return (
      <Container>
        { this.state.isDataReady ? (
          <View style={style.container}>
            { this.state.isDataEmpty ?
              <EmptyList
                description={this.state.budgetId !== '' ? dict.error_list.empty.incomeList : dict.error_list.no_budget_id}
                onButtonClick={() => this.state.budgetId !== '' ? Actions.HomeScreen() : Actions.BudgetList()}
              /> 
              :
              <ExpenseListComponent
                itemList={this.state.data}
                onItemPress={id => this.previewIncome(id)}
                onItemLongPress={id => this.previewIncome(id)}
                isEditable={() => { return true }}
                isDeletable={() => { return true }}
                itemAction={async props => {
                  let income = await this.state.data.filter(element => element.id == props.id)[0]
                  await AsyncStorage.setItem('incomeId', `${props.id}`)
                  await AsyncStorage.setItem('action', `${props.action}`)
                  await AsyncStorage.setItem('masterCategoryId', `${income.category.budgetCategory.id}`)
                  await AsyncStorage.setItem('categoryId', `${income.category.id}`)
                  await AsyncStorage.setItem('bankAccountId', `${income.bankAccount.id}`)
                  await AsyncStorage.setItem('screenType', 'income')
                  Actions.Expense()
                }}
                selectedItemId={this.state.selectedIncomeId}
            />}
            <ActionButton buttonColor="#29ABE2" position="center" onPress={async () => {
              await AsyncStorage.setItem('action', 'add')
              await AsyncStorage.setItem('screenType', 'income')
              Actions.Expense()
            }}>
              <Icon name="add" style={style.actionButtonIcon} />
            </ActionButton>
          </View>
        ) : (
          <Container style={style.spinner}>
              <Spinner color='blue'/>
          </Container>
        )}
      </Container>
    )
  }
}