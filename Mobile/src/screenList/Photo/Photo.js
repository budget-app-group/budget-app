import React, { Component } from 'react'
import { Text, View, Image, TouchableOpacity, CameraRoll } from 'react-native'
import { Actions } from 'react-native-router-flux'
import { Icon, Spinner } from 'native-base'
import { PhotoStyle } from './PhotoStyle'

import ImagePicker from 'react-native-image-crop-picker'

import { getTextFromImage } from '../../apiCallList/ocr'
import { errorHandler } from '../../common/js/auth'
import AsyncStorage from '@react-native-community/async-storage'

export default class Photo extends Component {
  constructor (props) {
    super()
    this.state = {
      isUriDelivered: !!props.uri
    }
  }

  componentDidMount = () => {
    ImagePicker.openCropper({
      path: this.props.uri,
      width: 300,
      height: 400,
      enableRotationGesture: true,
      showCropGuidelines: false,
      hideBottomControls: true
    }).then(image => {
      this.sendPicture(image.path)
    })
    .catch(error => {
      Actions.pop()
      errorHandler(error)
    })
  }

  sendPicture = async (newPicture) => {
    try {
      let newExpenseData = await getTextFromImage(newPicture)
      if (!newExpenseData) {
        alert('OCR parse error')
      } else {
        await AsyncStorage.setItem('expenseObject', JSON.stringify(newExpenseData))
        await AsyncStorage.setItem('screenType', 'expense')
        await AsyncStorage.setItem('action', 'add')
        ImagePicker.clean()
        Actions.Expense()
      }
    } catch (error) {
      alert('OCR error')
      ImagePicker.clean()
      errorHandler(error)
    }
  }

  render() {
    return (
      <View style={{ flex: 1, justifyContent: 'center', backgroundColor: 'black' }} >
        <Spinner color='blue'/>
      </View>
    )
  }
}
