import { StyleSheet } from 'react-native'

export const PhotoStyle = StyleSheet.create({
  box: {
    backgroundColor: 'gray',
    opacity: 0.2,
    borderWidth: 1,
    borderColor: 'black',
    width: '70%',
    height: '100%',
    position: 'absolute',
    left: '15%'
  },
  container: {
    flex: 1
  },
  buttonContainerPortrait: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    flexDirection: 'row',
    justifyContent: 'center'
  },
  buttonContainerLandscape: {
    position: 'absolute',
    bottom: 0,
    top: 0,
    right: 0,
    flexDirection: 'column',
    justifyContent: 'center'
  },
  buttonPortrait: {
    backgroundColor: 'transparent',
    padding: 5,
    marginHorizontal: 20
  },
  buttonLandscape: {
    backgroundColor: 'transparent',
    padding: 5,
    marginVertical: 20
  }
})