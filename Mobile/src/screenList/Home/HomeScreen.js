import AsyncStorage from '@react-native-community/async-storage'
import { ActionSheet, Button, Container, Fab, Icon, Root, Text } from 'native-base'
import React, { Component } from 'react'
import { Dimensions, TouchableOpacity, View } from 'react-native'
import Pie from 'react-native-fab-pie'
import { Actions } from 'react-native-router-flux'
import { getSumaryOfExpenseList } from '../../apiCallList/expense'
import * as dict from '../../common/dictionary/polish.json'
import { errorHandler } from '../../common/js/auth'
import { style } from '../../common/style/main'
import EmptyList from '../../componentList/EmptyList/EmptyList'
import LabelList from './LabelList'

const months = ['styczeń', 'luty', 'marzec', 'kwiecień', 'maj', 'czerwiec', 
                'lipiec', 'sierpień', 'wrzesień', 'październik', 'listopad', 'grudzień', 'Anuluj'] 
var CANCEL_INDEX = 12
const current_date = new Date()
var { height, width } = Dimensions.get('window');

export default class HomeScreen extends Component {
  constructor () {
      super ()
      this.state = {
        data: [],
        isDataEmpty: false,
        budgetId: '',
        chosenDate: Date.now()/1000,
        active: false,
        month_name: months[current_date.getMonth()]
      }
      this.pie = React.createRef()
      this.setDate = this.setDate.bind(this)
    }

    setDate(newDate) {
      newDate = this.timestampConvert(newDate)
      this.setState({ chosenDate: newDate })
      var dateNow = Date.now()
      this.getPieChart(this.state.chosenDate, dateNow)
    }

    async setMonthInPie() {
      var date = new Date()
      if (date.getMonth() < (months.indexOf(this.state.month_name) && (months.indexOf(this.state.month_name) !== 13))){
        date.setFullYear(date.getFullYear() - 1)
      }
      if (this.state.month_name !== 'Anuluj') {
        date.setMonth(months.indexOf(this.state.month_name))
      } else {
        date.setMonth(date.getMonth())
      }
      let startDate = await this.beginningOfMonth(date)
      let endDate = await this.endOfMonth(date)
      this.getPieChart(startDate, endDate) 
    }

    beginningOfMonth(myDate){    
      let date = new Date(myDate)
      date.setDate(1)
      date.setHours(0)
      date.setMinutes(0)
      date.setSeconds(0)
      date = this.timestampConvert(date)
      return date
    }

    endOfMonth(myDate){
      let date = new Date(myDate)
      date.setMonth(date.getMonth() +1)
      date.setDate(0)
      date.setHours(23)
      date.setMinutes(59)
      date.setSeconds(59)
      date = this.timestampConvert(date)
      return date
    }
    
    timestampConvert(date) {
      var timestamp = Date.parse(date)
      return timestamp
    }

    animate = () => {
      this.pie.current.reset().then(this.pie.current.animate);
    }

    async getPieChart(date1, date2) {
      const id = await AsyncStorage.getItem('budgetId')
      try {
        if (id) {
          const response = await getSumaryOfExpenseList(id, date1, date2)
          this.setState({
            data: response.map((element, index) => {
              return {
                value: element.sumOfExpenses < 0 ? (-1) * element.sumOfExpenses : element.sumOfExpenses,
                title: element.name,
                key: `pie-${index}`,
                color: element.color
              }
            }),
            isDataEmpty: !response.length,
            budgetId: id
          }, () => { this.pie && this.pie.current && this.pie.current.animate() })
        }
        else { this.setState({ isDataEmpty: true }) }
      } catch (error) {
        errorHandler(error)
        this.setState({ isDataEmpty: true })
      }
    }

    async componentDidUpdate(prevProps, prevState) {
      if(this.state.month_name == 'Anuluj') {
        this.forceUpdate(() => { this.setState({ month_name: months[current_date.getMonth()] })}, this.pie.current.animate())
      }
      if(prevState.month_name !== this.state.month_name) {
        await this.setMonthInPie(),
        this.pie.current.reset().then(this.pie.current.animate),
        () => { this.pie && this.pie.current }
      }
    }

    componentDidMount () {
      var dateNow = new Date()
      let startDate = this.beginningOfMonth(dateNow)
      let endDate = this.endOfMonth(dateNow)
      this.getPieChart(startDate, endDate)
    }

    expenseScreen = async () => {
      await AsyncStorage.setItem('action', 'add')
      await AsyncStorage.setItem('screenType', 'expense')
      Actions.Expense()
    }
    render () {
      return (
        <Container>
          { this.state.isDataEmpty ? (
            <View style={style.welcomeContainer}>
              <Text style={style.headertext2}>{dict.placeholder.welcomeMsg}</Text>
              <Text style={style.text2}>{dict.placeholder.welcomeDesc}</Text>
              <TouchableOpacity onPress={Actions.BudgetList} style={style.buttonOutlined2}>
                  <Text style={style.buttonOutlinedText}>{dict.header.chooseBudget}</Text>
              </TouchableOpacity>
            </View>
          ) : (
          <Root>
          <View>
            <Button 
              style={style.monthsButton}
              full light onPress={() => ActionSheet.show({
              options: months,
              cancelButtonIndex: CANCEL_INDEX,
              title: "Wybierz miesiąc: "
            },
            buttonIndex => {
              this.setState({ month_name: months[buttonIndex] })
            }
            )}>
              <Text>{this.state.month_name}</Text>
            </Button>
          </View>
          
                <View
                    style={{
                      marginVertical: 5,
                      justifyContent: 'center',
                      alignItems: 'center',
                      flex: 1
                    }}
                  >
                    {this.state.isDataEmpty ?
                    <EmptyList
                        description={this.state.budgetId !== '' ? dict.error_list.empty.masterCategoryList : dict.error_list.no_budget_id}
                        onButtonClick={() => Actions.BudgetList()}
                    />
                    :
                    <Pie
                      ref={this.pie}
                      containerStyle={{ justifyContent: 'space-between' }}
                      pieStyle={style.pieStyle}
                      outerRadius={height/5.5}
                      innerRadius={height/18}
                      data={this.state.data}
                      animate>
                      <LabelList/>
                    </Pie>}
                  </View>
                  <View style={{ flex: 0.08 }} />
                  <View>
                    <Fab
                      active={this.state.active}
                      direction="up"
                      containerStyle={{ }}
                      style={{ backgroundColor: '#29ABE2', flexDirection: "row-reverse" }}
                      position="bottomRight"
                      onPress={() => this.setState({ active: !this.state.active })}>
                      <Icon name="add" />
                      <Button style={{ backgroundColor: '#34A34F' }} onPress={this.expenseScreen}>
                        <Icon type="MaterialIcons" name="add-shopping-cart" />
                      </Button>
                      <Button style={{ backgroundColor: '#6e6eff' }} onPress={Actions.Camera}>
                        <Icon name="camera" />
                      </Button>
                    </Fab>
                  </View>
                </Root>
          )}
                </Container>
      )
    }
}