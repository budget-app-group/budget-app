import { StyleSheet } from 'react-native'

export const styles = StyleSheet.create({
    container: {
      justifyContent: 'center',
      alignItems: 'center',
      flex: 1,
    },
    labelsContainer: {
      flex: 1,
      marginTop: 10,
      justifyContent: 'center',
      padding: 2
    },
    listItem: {
      flexDirection: 'row',
      flex: 1,
      justifyContent: 'space-between',
      padding: 2

    },
    dot: {
      width: 18,
      height: 18,
      borderRadius: 15,
      marginHorizontal: 10,
    },
    text: {
      fontFamily: 'Lato-Regular',
      fontSize: 16
    },
    labelContainer: {
      flex: 1,
    },
    headerText: {
      fontFamily: 'Lato-Regular',
      fontSize: 20,
      color: '#29ABE2'
    },
    labelsHeader: {
      margin: 5,
      flexDirection: 'row',
      justifyContent: 'space-between'
    }
  });
  