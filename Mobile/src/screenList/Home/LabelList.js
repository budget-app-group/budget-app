import React from 'react'
import { Text, View, TouchableNativeFeedback, ScrollView } from 'react-native'
import { styles } from './HomeStyle'
import * as dict from '../../common/dictionary/polish.json'
import { Container } from 'native-base'

const LabelList = (props) => {
  return (
    <Container>
      <View style={styles.labelsHeader}>
        <Text style={styles.headerText}>{dict.categoryLabels.categoryType}</Text>
        <Text style={styles.headerText}>{dict.categoryLabels.amount}</Text>
      </View>
      <View style={styles.labelsContainer}>
        <ScrollView contentContainerStyle={{flexGrow: 1}}>
        {props.data.map((arc, index) => (
          <TouchableNativeFeedback onPress={() => props.focus(index)} key={index}>
                <View style={styles.listItem}>
                  <View style={[styles.dot, { backgroundColor: arc.color }]}></View>
                  <Text style={styles.text}>{arc.title}</Text>
                  <Text style={styles.text}>{arc.value} PLN</Text>
                </View>
          </TouchableNativeFeedback>
        ))}
        </ScrollView>
      </View>
    </Container>
  )
}

export default LabelList