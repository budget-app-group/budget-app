import React, { Component } from 'react'
import { Text, View, ScrollView, Image } from 'react-native'
import { Container, Icon } from 'native-base'
import { style } from '../../common/style/main'
import * as dict from '../../common/dictionary/polish.json'
import { Actions } from 'react-native-router-flux'
import { TouchableOpacity } from 'react-native-gesture-handler'
import AsyncStorage from '@react-native-community/async-storage'
import { errorHandler } from '../../common/js/auth'

import Profile from '../../common/iconList/profile.png'
import MenuHome from '../../common/iconList/homeMenu.png'
import AccountMenu from '../../common/iconList/bank.png'
import BudgetMenu from '../../common/iconList/budget.png'
import CategoryMenu from '../../common/iconList/category.png'
import ExpenseMenu from '../../common/iconList/expense.png'
import AnalysisMenu from '../../common/iconList/analysis.png'
import GoalMenu from '../../common/iconList/goal.png'
import CameraMenu from '../../common/iconList/camera.png'
import SettingsMenu from '../../common/iconList/settings.png'
import IncomeMenu from '../../common/iconList/income.png'
import {getBudget} from '../../apiCallList/budget'


export default class Home extends Component {
  constructor () {
    super()
    this.state = {
      user: null,
      prev: '',
      budgetName: ''
    }
  }

  componentDidMount = async () => {
    try {
      const user = await AsyncStorage.getItem('user')
      const budgetName = await AsyncStorage.getItem('budgetName')
      this.setState({ user: JSON.parse(user),  budgetName: budgetName })
    } catch (error) {
      errorHandler(error)
    }
  }
  
  expenseList = async () => {
    await AsyncStorage.setItem('expenseListType', 'expenses')
    await this.setState({
      prev: 'expense'
    })
    Actions.ExpenseList()
  }

  incomeList = async () => {
    await this.setState({
      prev: 'income'
    })
    Actions.IncomeList()
  }
  
  async shouldUpdate() {
    let user = await AsyncStorage.getItem('user')
    let budgetName = await AsyncStorage.getItem('budgetName')
    user = JSON.parse(user)
      if (this.state.user === null) { this.setState({ user: user }) }
      else if (user !== null && (user.name !== this.state.user.name || user.surname !== this.state.user.surname || user.email !== this.state.user.email || this.state.user === null)) {
      this.setState({ user: user })
      }
      if (budgetName !== this.state.budgetName) {
        this.setState({ budgetName: budgetName })
      }
  }

  render() {
    this.shouldUpdate()
    return (
      <Container>
        <TouchableOpacity onPress={Actions.Settings}>
          <View style={style.menuheader}>
            <View style={style.menuItems}>
              <Image source={Profile} style={style.menuProfileImage}></Image>
              <View style={style.menuHeaderColumn}>
              {this.state.user && (
                <View>
                  <Text style={style.menuNameSurname}>{this.state.user && this.state.user.name ? this.state.user.name : 'Error'} {this.state.user && this.state.user.surname ? this.state.user.surname : 'Error'}</Text>
                  <Text style={style.menuemail}>{this.state.user && this.state.user.email ? this.state.user.email : 'Error'}</Text>
                  <Text style={style.menuemail}>{dict.placeholder.choosenBudget}: {this.state.budgetName ? this.state.budgetName : 'Brak wybranego budżetu'}</Text>
                </View>
              )
              }
              </View>
                {/* <TouchableOpacity
                  style={style.menuSettingsButton}
                  onPress={Actions.Settings}>
                  <Icon name='settings' style={style.menuSettingsButton2}></Icon>
              </TouchableOpacity> */}
            </View>
          </View>
        </TouchableOpacity>
        <ScrollView>
          <TouchableOpacity
            style={[style.menuButton, Actions.currentScene === 'HomeScreen' ? style.active : null]}
            onPress={Actions.HomeScreen}>
            <Image source={MenuHome} style={style.menuImage}></Image>
            <Text style={style.menuText}>{dict.drawerNames.home}</Text>
          </TouchableOpacity>

          <TouchableOpacity
             style={[style.menuButton, Actions.currentScene === 'AnalysisList' ? style.active : null]}
            onPress={Actions.AnalysisList}>
            <Image source={AnalysisMenu} style={style.menuImage}></Image>
            <Text style={style.menuText}>{dict.drawerNames.analysisList}</Text>
          </TouchableOpacity>

          <TouchableOpacity
             style={[style.menuButton, (Actions.currentScene === 'BankAccountList' || Actions.currentScene === 'BankAccount') ? style.active : null]}
            onPress={Actions.BankAccountList}>
            <Image source={AccountMenu} style={style.menuImage}></Image>
            <Text style={style.menuText}>{dict.drawerNames.bankAcountList}</Text>
          </TouchableOpacity>


          <TouchableOpacity
            style={[style.menuButton, (Actions.currentScene === 'BudgetList' || Actions.currentScene === 'Budget') ? style.active : null]}
            onPress={Actions.BudgetList}>
            <Image source={BudgetMenu} style={style.menuImage}></Image>
            <Text style={style.menuText}>{dict.drawerNames.budgetList}</Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={[style.menuButton, (Actions.currentScene === 'MasterCategoryList' || Actions.currentScene === 'CategoryList') ? style.active : null]}
            onPress={Actions.MasterCategoryList}>
            <Image source={CategoryMenu} style={style.menuImage}></Image>
            <Text style={style.menuText}>{dict.drawerNames.categoryList}</Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={[style.menuButton, (Actions.currentScene === 'ExpenseList' || (Actions.currentScene === 'Expense' && this.state.prev === 'expense')) ? style.active : null]}
            onPress={this.expenseList}>
             <Image source={ExpenseMenu} style={style.menuImage}></Image>
            <Text style={style.menuText}>{dict.header.expense_list}</Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={[style.menuButton, (Actions.currentScene === 'IncomeList'  || (Actions.currentScene === 'Expense' && this.state.prev === 'income')) ? style.active : null]}
            onPress={this.incomeList}>
             <Image source={IncomeMenu} style={style.menuImage}></Image>
            <Text style={style.menuText}>{dict.header.incomes}</Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={[style.menuButton, (Actions.currentScene === 'GoalList' || Actions.currentScene === 'Goal' || Actions.currentScene === 'Donation') ? style.active : null]}
            onPress={Actions.GoalList}>
            <Image source={GoalMenu} style={style.menuImage}></Image>
            <Text style={style.menuText}>{dict.drawerNames.goalList}</Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={[style.menuButton, (Actions.currentScene === 'Camera' || Actions.currentScene === 'Photo' || Actions.currentScene === 'Gallery') ? style.active : null]}
            onPress={Actions.Camera}>
            <Image source={CameraMenu} style={style.menuImage}></Image>
            <Text style={style.menuText}>{dict.drawerNames.camera}</Text>
          </TouchableOpacity> 
        </ScrollView>
      </Container>
    )
  }
}

