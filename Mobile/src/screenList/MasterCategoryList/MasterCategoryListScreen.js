import React, { Component } from 'react'
import { View } from 'react-native'
import { Container, Icon, Spinner } from 'native-base'
import { style } from '../../common/style/main'
import ActionButton from 'react-native-action-button'
import List from '../../componentList/List/List'
import Logo from '../../common/iconList/Logo.png'
import { Actions } from 'react-native-router-flux'
import EmptyList from '../../componentList/EmptyList/EmptyList'
import * as dict from '../../common/dictionary/polish.json'
import AsyncStorage from '@react-native-community/async-storage'
import { getMasterCategoryList } from '../../apiCallList/masterCategory'
import { errorHandler } from '../../common/js/auth'

export default class MasterCategoryListScreen extends Component {
    constructor () {
        super ()
        this.state = {
            budgetId: '',
            data: [],
            isDataEmpty: false,
            selectedMasterCategoryId: '',
            userEmail: '',
            isDataReady: false,
            budgetAdminEmail: ''
        }
    }

    async componentDidMount() {
        try {
            const id = await AsyncStorage.getItem('budgetId')
            const admin = await AsyncStorage.getItem('budgetAdminEmail')
            let user = await AsyncStorage.getItem('user')
            user = JSON.parse(user)
            this.setState({userEmail: user.email, budgetAdminEmail: admin})
            if (id) {
                const response = await getMasterCategoryList(id)
                this.setState({
                    data: response.map(element => {
                        return Object.assign(element, { image: Logo, key: `${element.id}` })
                    }),
                    isDataEmpty: !response.length,
                    budgetId: id
                })
            }
            else { this.setState({ isDataEmpty: true }) }
        } catch (error) {
            errorHandler(error)
            this.setState({ isDataEmpty: true })
        }
        this.setState({isDataReady: true})
    }


    async selectMasterCategory (id) {
        await AsyncStorage.setItem('masterCategoryId', `${id}`)
        this.setState({ selectedMasterCategoryId: this.state.selectedMasterCategoryId === id ? '' : id }, () => {
            Actions.CategoryList()
        })
    }

    async previewMasterCategory (id) {
        await AsyncStorage.setItem('masterCategoryId', `${id}`)
        await AsyncStorage.setItem('action', 'preview')
        Actions.MasterCategory()
    }

    render () {
        return (
            <Container>
                { this.state.isDataReady ? (
                <View style={style.container}>
                    {this.state.isDataEmpty ?
                    <EmptyList
                        description={this.state.budgetId !== '' ? dict.error_list.empty.masterCategoryList : dict.error_list.no_budget_id}
                        onButtonClick={() => this.state.budgetId !== '' ? Actions.HomeScreen() : Actions.BudgetList()}
                    />
                    :
                    <List
                        itemList={this.state.data}
                        onItemPress={id => this.selectMasterCategory(id)} // #TODO what will happen?
                        onItemLongPress={id => this.previewMasterCategory(id)}
                        isEditable={item => { return (this.state.userEmail == item.budgetData.admin)}}
                        isDeletable={item => { return (this.state.userEmail == item.budgetData.admin)}}
                        itemAction={async props => {
                            await AsyncStorage.setItem('masterCategoryId', `${props.id}`)
                            await AsyncStorage.setItem('action', `${props.action}`)
                            Actions.MasterCategory()
                        }}
                        selectedItemId={this.state.selectedMasterCategoryId}
                    />}
                    {this.state.userEmail == this.state.budgetAdminEmail ? 
                    (<ActionButton buttonColor="#29ABE2" position="center" onPress={async () => {
                        await AsyncStorage.setItem('action', 'add')
                        Actions.MasterCategory()
                    }}>
                        <Icon name="add" style={style.actionButtonIcon} />
                     </ActionButton>) : (<View></View>)
                    }
                </View>
                ) : (
                    <Container style={style.spinner}>
                        <Spinner color='blue'/>
                    </Container>
                )}
            </Container>
        )
    }
}