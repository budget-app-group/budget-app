import AsyncStorage from '@react-native-community/async-storage'
import { Container } from 'native-base'
import React, { Component } from 'react'
import { Image, Keyboard, ScrollView, Text, TextInput, ToastAndroid, TouchableOpacity, TouchableWithoutFeedback, View } from 'react-native'
import { Actions } from 'react-native-router-flux'
import { signUp } from '../../apiCallList/auth'
import * as dict from '../../common/dictionary/polish.json'
import Logo from '../../common/iconList/Logo.png'
import { styles } from './SignUpStyle'

export default class SignUpScreen extends Component {
  constructor() {
    super()
    this.state = {
      email: '',
      name: '',
      surname: '',
      password: '',
      passwordRepeated: '',
      emailValidation: false,
      passwordValidation: false,
      nameValidation: false,
      surnameValidation: false,
      passwordMatchValidation: false,
      isInputOneFocus: false,
      isInputTwoFocus: false,
      isInputThreeFocus: false,
      isInputFourFocus: false,
      isInputFiveFocus: false,

      isSingUpButtonDisabled: false
    }
  }

  async signUp () {
    if (this.state.email !== '' && this.state.password !== '' && this.state.name !== '' && this.state.surname){
      if (!this.state.emailValidation && !this.state.nameValidation && !this.state.surnameValidation && !this.state.passwordValidation
        && !this.state.passwordMatchValidation) {
          this.setState({isSingUpButtonDisabled: true})
          const response = await signUp({
            email: this.state.email,
            name: this.state.name,
            surname: this.state.surname,
            password: this.state.password
          })
          if (response) {
            await AsyncStorage.clear()
            await AsyncStorage.setItem('email', this.state.email)
            ToastAndroid.show(`${dict.toast.sentActivationLink}`, ToastAndroid.SHORT)
            this.setState({isSingUpButtonDisabled: false})
            Actions.SignIn()
          }
      } else {
        ToastAndroid.show(`${dict.toast.invalid}`, ToastAndroid.SHORT)
      }
    } else {
      ToastAndroid.show(`${dict.toast.notEmpty}`, ToastAndroid.SHORT)
    }
  }

  validate(type, text) {
    var alph = /^[A-Za-zżźćńółęąśŻŹĆĄŚĘŁÓŃ]+$/
    var email = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{1,}))$/
    var pass = /^.{7,30}$/
    if (type == 'email') {
      if(email.test(text)){
        this.setState({
          emailValidation: false
        })
      }
      else {
        this.setState({
          emailValidation: true
        })
      }
    }
    if (type == 'password') {
      if(pass.test(text)) {
        this.setState({
          passwordValidation: false
        })
      }
      else {
        this.setState({
          passwordValidation: true
        })
      }
    }
    if (type == 'name') {
      if(alph.test(text)) {
        this.setState({
          nameValidation: false
        })
      }
      else {
        this.setState({
          nameValidation: true
        })
      }
    }
    if (type == 'surname') {
      if(alph.test(text)) {
        this.setState({
          surnameValidation: false
        })
      }
      else {
        this.setState({
          surnameValidation: true
        })
      }
    }
    if (type == 'passwordRepeat') {
      if (this.state.password === text) {
        this.setState({
          passwordMatchValidation: false
        })
      } else {
        this.setState({
          passwordMatchValidation: true
        })
      }
    }
  }

  signIn() {Actions.SignIn()}

  onChange (stateName, text) {
    this.setState({
      [stateName]: text
    })
  }

  handlerFocusBlur = (input, value) => {
    this.setState({[input]:value})
}

  render() {
    return (
      <Container style={{flex: 1}}>
        <ScrollView contentContainerStyle={{flexGrow: 1}}>
      <TouchableWithoutFeedback style={{ flex: 1 }} onPress={Keyboard.dismiss} accessible={false}>
        <View style={styles.container}>
        <View style={styles.container2}>
          <Text style={styles.logoText}>{dict.app_name}</Text>
          <Image style={styles.logoimage} source={Logo}/>
        </View>
          <View style={styles.container1}>
            <TextInput
              autoCapitalize='none'
              style={[styles.inputBox, this.state.emailValidation ? styles.inputBoxValidation: null, this.state.isInputOneFocus ? styles.textInputFocus : '']}
              underlineColorAndroid='rgba(0,0,0,0)' 
              placeholder={dict.placeholder.login}
              placeholderTextColor = "#29ABE2"
              onChangeText={(text) => {this.validate('email', text); this.onChange('email', text) }}
              selectionColor="#fff"
              onFocus={this.handlerFocusBlur.bind(this, 'isInputOneFocus', true)}
              onBlur={this.handlerFocusBlur.bind(this, 'isInputOneFocus', false)}
              keyboardType="email-address"
              onSubmitEditing={()=> this.name.focus()}
            />
            <TextInput
              style={[styles.inputBox, this.state.nameValidation ? styles.inputBoxValidation: null, this.state.isInputTwoFocus ? styles.textInputFocus : '']} 
              underlineColorAndroid='rgba(0,0,0,0)' 
              placeholder={dict.placeholder.name}
              placeholderTextColor = "#29ABE2"
              onChangeText={(text) => {this.validate('name', text); this.onChange('name', text)}}
              selectionColor="#fff"
              onFocus={this.handlerFocusBlur.bind(this, 'isInputTwoFocus', true)}
              onBlur={this.handlerFocusBlur.bind(this, 'isInputTwoFocus', false)}
              keyboardType="default"
              ref={(input) => { this.name = input }}
              onSubmitEditing={()=> this.surname.focus()}
            />
            <TextInput
              style={[styles.inputBox, this.state.surnameValidation ? styles.inputBoxValidation: null, this.state.isInputThreeFocus ? styles.textInputFocus : '']} 
              underlineColorAndroid='rgba(0,0,0,0)' 
              placeholder={dict.placeholder.surename}
              placeholderTextColor = "#29ABE2"
              onChangeText={(text) => {this.validate('surname', text); this.onChange('surname', text) }}
              selectionColor="#fff"
              onFocus={this.handlerFocusBlur.bind(this, 'isInputThreeFocus', true)}
              onBlur={this.handlerFocusBlur.bind(this, 'isInputThreeFocus', false)}
              keyboardType="default"
              ref={(input) => { this.surname = input }}
              onSubmitEditing={()=> this.password.focus()}
            />
            <TextInput
              style={[styles.inputBox, this.state.passwordValidation ? styles.inputBoxValidation: null, this.state.isInputFourFocus ? styles.textInputFocus : '']} 
              underlineColorAndroid='rgba(0,0,0,0)' 
              placeholder={dict.placeholder.password}
              onChangeText={(text) => {this.validate('password', text); this.onChange('password', text) }}
              secureTextEntry={true}
              selectionColor="#fff"
              onFocus={this.handlerFocusBlur.bind(this, 'isInputFourFocus', true)}
              onBlur={this.handlerFocusBlur.bind(this, 'isInputFourFocus', false)}
              placeholderTextColor = "#29ABE2"
              ref={(input) => { this.password = input }}
            />
            { this.state.passwordValidation && 
              (<Text style={{color: '#ff0000'}}>{dict.placeholder.password8Chars}</Text>
            )}
            <TextInput
              style={[styles.inputBox, this.state.passwordMatchValidation ? styles.inputBoxValidation: null, this.state.isInputFiveFocus ? styles.textInputFocus : '']} 
              underlineColorAndroid='rgba(0,0,0,0)' 
              placeholder={dict.placeholder.passwordRepeat}
              onChangeText={ async (text) => {
                await this.onChange('passwordRepeated', text)
                this.validate('passwordRepeat', text)
              }}
              secureTextEntry={true}
              selectionColor="#fff"
              placeholderTextColor = "#29ABE2"
              onFocus={this.handlerFocusBlur.bind(this, 'isInputFiveFocus', true)}
              onBlur={this.handlerFocusBlur.bind(this, 'isInputFiveFocus', false)}
              ref={(input) => { this.password = input }}
              onSubmitEditing={()=> this.signUp()}
            /> 
            { this.state.passwordMatchValidation && 
              (<Text style={{color: '#ff0000'}}>{dict.placeholder.passwordMatch}</Text>
            )}
            <TouchableOpacity
              disabled={this.state.isSingUpButtonDisabled} 
              onPress={() => this.signUp()} style={styles.button}>
              <Text style={styles.buttonText}>{dict.header.sign_up}</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.signupTextCont}>
            <Text style={styles.signupText}>{dict.bottom_tag_login}</Text>
            <TouchableOpacity 
              onPress={() => this.signIn()}>
                <Text style={styles.signupButton}>{dict.header.sign_in}</Text>
            </TouchableOpacity>
          </View>
        </View>
      </TouchableWithoutFeedback>
      </ScrollView>
      </Container>
    )
  }
}