import { StyleSheet, Dimensions } from 'react-native'

const { width } = Dimensions.get('window')

const GalleryStyle = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'black'
  },
  image: {
    width: Math.round(width / 3) - 3,
    height: Math.round(width / 3) - 3,
    margin: 1
  }
})

export default GalleryStyle