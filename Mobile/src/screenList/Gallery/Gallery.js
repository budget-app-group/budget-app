import React, { Component } from 'react'
import GalleryStyle from './GalleryStyle'
import { FlatList, CameraRoll, Image, TouchableOpacity, View, Text } from 'react-native'
import { Actions } from 'react-native-router-flux'
import * as dict from '../../common/dictionary/polish.json'
import AsyncStorage from '@react-native-community/async-storage'

export default class Gallery extends Component {
  constructor() {
    super()
    this.state = {
      photoList: [],
      page: 0,
      isLoadingActive: true
    }
  }
  
  componentDidMount = () => {
    this.updatePhotoList()
  }

  updatePhotoList = async () => {
    CameraRoll.getPhotos({ first: (this.state.page + 1) * 20 })
    .then(response => {
      let isEndReached = (this.state.page + 1) * 20 === response.edges.length
      let newPhotoList = response.edges.slice(-20).map((element, index) =>
        Object.assign(element, { key: `${this.state.page * 20 + index}` }))
      this.state.isLoadingActive && this.setState({
        page: this.state.page + 1,
        isLoadingActive: isEndReached,
        photoList: this.state.photoList.concat(newPhotoList)
      })
    })
  }

  renderFooter = () => {
    if (!this.state.isLoadingActive) {
      return <View><Text>{dict.error_list.empty.gallery}</Text></View>
    } else return null
  }

  photoCrop = async (info) => {
    await AsyncStorage.setItem('prev', 'Gallery')
    Actions.Photo({
      uri: info.item.node.image.uri,
      width: info.item.node.image.width,
      height: info.item.node.image.height
    })
  }
  
  render () {
    return (
      <FlatList
        style={GalleryStyle.container}
        data={this.state.photoList}
        numColumns={3}
        onEndReached={this.updatePhotoList}
        onEndReachedThreshold={0.5}
        initialNumToRender={20}
        ListFooterComponent={this.renderFooter}
        renderItem={info => (
          <TouchableOpacity
            onPress={ () => this.photoCrop(info)}
            style={GalleryStyle.image}
          >
            <Image source={{ uri: info.item.node.image.uri }} style={GalleryStyle.image} />
          </TouchableOpacity>
        )}
      />
    )
  }
}
