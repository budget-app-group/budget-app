import AsyncStorage from '@react-native-community/async-storage'
import { Container, Root, Spinner } from 'native-base'
import React, { Component } from 'react'
import { Alert, Image, Keyboard, ScrollView, Text, TextInput, ToastAndroid, TouchableOpacity, TouchableWithoutFeedback, View } from 'react-native'
import { TouchableNativeFeedback } from 'react-native-gesture-handler'
import { Actions } from 'react-native-router-flux'
import { addBudget, addUserToBudget, deleteBudget, deleteUserFromBudget, getBudget, getBudgetUsers, updateBudget, changeBudgetAdmin, deleteLoggedUserFromBudget } from '../../apiCallList/budget'
import * as dict from '../../common/dictionary/polish.json'
import Logo from '../../common/iconList/Logo.png'
import List from '../../componentList/UsersList/UsersList'
import { styles } from './BudgetScreenStyle'

export default class BudgetScreen extends Component {
    constructor () {
        super ()
        this.state = {
            id: '',
            name: '',
            email: '',
            admin: '',
            adminMail: '',
            image: Logo,
            action: '',
            emailValidation: false,
            isEditable: true,
            user: null,
            users: [],
            isUserListEmpty: false,
            isDataReady: false,
            isModalVisible: false,
            selectedUserId: '',
            budgetId: null,
            userId: null,
            isInputOneFocus: false,
            isInputTwoFocus: false,
            isInputThreeFocus: false,
            isInputFourFocus: false,
        }
      }

  async componentDidMount() {
    const id = await AsyncStorage.getItem('budgetId')
    const action = await AsyncStorage.getItem('action')
    let user = await AsyncStorage.getItem('user')
    user = JSON.parse(user)
    this.setState({'budgetId': id, userId: user.id})

    if (action === 'add') {
      this.setState({
        action: 'add'
      })
    } else if (id) {
      const response = await getBudget(id)
      if (response) {
        this.setState({
          id: response.id,
          name: response.name,
          adminMail: response.admin.email,
          admin: response.admin.id,
          action: action,
          user: user || null
        })
        const responseUsers = await getBudgetUsers(this.state.id)
        if (responseUsers.length > 0) {
          this.setState({
            users: responseUsers.map(element => {
              return Object.assign(element, { image: Logo, key: `${element.id}` })
            }),
            isUserListEmpty: !responseUsers.length
          })
        }
        if (action === 'preview') {
          this.setState({
            isEditable: false
          })
        }
      }
      // #TODO validation
    }
    this.setState({ isDataReady: true })
  }

  validate(type, text) {
    var email = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{1,}))$/
    if (type == 'email') {
      if (email.test(text)) {
        this.setState({
          emailValidation: false
        })
      }
      else {
        this.setState({
          emailValidation: true
        })
      }
    }
  }
  
  async saveData() {
    // #TODO validation and onLeave
    let response = false
    if (this.state.action === 'add') {
      response = await addBudget({ name: this.state.name })
      ToastAndroid.show(dict.toast[response ? 'budgetCreated' : 'operationFailed'], ToastAndroid.SHORT)
    } else if (this.state.action === 'edit') {
      response = await updateBudget(this.state.id, {
        name: this.state.name,
        admin: this.state.adminMail
      })
      ToastAndroid.show(dict.toast[response ? 'budgetEdited' : 'operationFailed'], ToastAndroid.SHORT)
    } else if (this.state.action === 'delete') {
      response = await deleteBudget(this.state.id)
      if (response) {
        ToastAndroid.show(dict.budget_deleted, ToastAndroid.SHORT)
      }
    }
    if (response) {
      await AsyncStorage.removeItem('action')
      Actions.BudgetList()
    }
  }
  
  onChange(stateName, text) {
    this.setState({
      [stateName]: text
    })
  }
  
  async removeUser(uId) {
    await deleteUserFromBudget(this.state.budgetId, uId)
    this.updateComponent()
    ToastAndroid.show(`${dict.user_removed}`, ToastAndroid.SHORT)
  }
  
  async changeAdmin(id) {
    await changeBudgetAdmin(this.state.budgetId, id)
    ToastAndroid.show(`${dict.toast.budgetAdminChanged}`, ToastAndroid.SHORT)
  }
  
  async addUserToBudget() {
    const id = await AsyncStorage.getItem('budgetId')
    if ((!this.state.emailValidation) && (this.state.email !== '')) {
      await addUserToBudget(id, this.state.email)
      this.setState({
        email: ''
      })
      ToastAndroid.show('Wysłano zaproszenie', ToastAndroid.SHORT)
    } else {
      ToastAndroid.show('Nie podano emaila', ToastAndroid.SHORT)
    }
  }

  async deleteUser (id) {
      if (await deleteLoggedUserFromBudget(id)) {
        ToastAndroid.show( `${dict.toast.budgetLeaved}`, ToastAndroid.SHORT)
        Actions.BudgetList()
      }
    }

  showDeleteAlert() {
      Alert.alert(
          `${dict.header.delete}`,
          `${dict.delete_budget}`,
          [ {text: `${dict.permission_list.no}`, onPress: () => Actions.BudgetList() ,style: 'destructive'},
            {text: `${dict.permission_list.yes}`, onPress: () => this.saveData(), style: 'destructive'},
          ],
          {cancelable: false},
      )
  }

  handlerFocusBlur = (input, value) => {
    this.setState({[input]:value})
  }

  updateComponent = async () => {
    const responseUsers = await getBudgetUsers(this.state.id)
      if (responseUsers.length > 0) {
        this.setState({
          users: responseUsers.map(element => {
            return Object.assign(element, { image: Logo, key: `${element.id}` })
          }),
          isUserListEmpty: !responseUsers.length
        })
      }
  }

  render() {
    return (
      <Container style={{ flex: 1 }}>
        <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
          <Root>
            {this.state.isDataReady ? (
              <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
                <View style={styles.container}>
                  {this.state.action === 'delete' ? (
                    <View style={styles.container}>
                        {this.state.action === 'delete' ? (
                            <View style={styles.container}>
                                {this.showDeleteAlert()}
                            </View>
                        ) : (
                            <View style={styles.container}>
                                <TouchableNativeFeedback>
                                    <Image resizeMode='cover' source={Logo} style={styles.image}/>
                                </TouchableNativeFeedback>  
                                <Text style={styles.headertext}>{dict.header.name}</Text>
                                <TextInput
                                    editable={this.state.isEditable}
                                    style={[styles.inputBox, this.state.isInputOneFocus ? styles.textInputFocus : '']}
                                    underlineColorAndroid='rgba(0,0,0,0)'
                                    placeholder={dict.placeholder.budget_name}
                                    value={this.state.name}
                                    placeholderTextColor = '#29ABE2'
                                    selectionColor='#fff'
                                    onFocus={this.handlerFocusBlur.bind(this, 'isInputOneFocus', true)}
                                    onBlur={this.handlerFocusBlur.bind(this, 'isInputOneFocus', false)}
                                    // onSubmitEditing={()=> focus.this.saveData()}
                                    onChangeText={text => this.onChange('name', text)}
                                />
                                { this.state.action != 'add' && 
                                    <Text style={styles.headertext}>{dict.header.user}</Text>
                                }
                                { (this.state.isEditable && this.state.action != 'add') && 
                                <View style={styles.useraddview}>
                                    <TextInput
                                        style={[styles.inputBoxBudgetUser, this.state.emailValidation ? styles.inputBoxValidation: null, this.state.isInputTwoFocus ? styles.textInputFocus : '']}
                                        underlineColorAndroid='rgba(0,0,0,0)'
                                        value={this.state.email}
                                        onChangeText={(text) => {this.validate('email', text); this.onChange('email', text); }}
                                        selectionColor='#fff'
                                        onFocus={this.handlerFocusBlur.bind(this, 'isInputTwoFocus', true)}
                                        onBlur={this.handlerFocusBlur.bind(this, 'isInputTwoFocus', false)}
                                        placeholder={dict.placeholder.add_group}
                                        placeholderTextColor = '#29ABE2'
                                    />
                                    <TouchableOpacity style={styles.adduserbutton}
                                        onPress={() => this.addUserToBudget()}>
                                        <Text style={styles.addbuttontext}>+</Text>
                                    </TouchableOpacity>
                                </View>
                                }
                                <View style={styles.container1}>
                                <ScrollView style={styles.scrollView} showsHorizontalScrollIndicator="true">
                                    <List
                                        itemList={this.state.users}
                                        onItemPress={ () => {} }
                                        onItemLongPress={ () => {} }
                                        isDeletable={(user) => { return (this.state.user.id == this.state.admin && this.state.isEditable && user.id !== this.state.admin) }}
                                        itemAction={async props => {
                                            await AsyncStorage.setItem('userId', `${props.id}`)
                                            this.setState({selectedUserId: `${props.id}`})
                                            Alert.alert(
                                                `${dict.header.delete}`,
                                                `${dict.user_delete_question}`,
                                                [ {text: `${dict.permission_list.no}`, style: 'destructive'},
                                                {text: `${dict.permission_list.yes}`, onPress: () => this.removeUser(`${props.id}`), style: 'destructive'},
                                                ],
                                                {cancelable: false},
                                            );
                                        }}
                                    />
                                </ScrollView>
                                </View>
                                { this.state.isEditable ? (
                                    <TouchableOpacity onPress={() => this.saveData()} style={styles.button}>
                                        <Text style={styles.buttonText}>{dict.header.save}</Text>
                                    </TouchableOpacity>
                                ) : (
                                    this.state.admin !== this.state.userId && <TouchableOpacity onPress={() => {
                                      Alert.alert(
                                              `${dict.header.delete}`,
                                              `${dict.leave_budget}`,
                                              [ {text: `${dict.permission_list.no}` ,style: 'destructive'},
                                                {text: `${dict.permission_list.yes}`, onPress: () => this.deleteUser(this.state.budgetId), style: 'destructive'},
                                              ],
                                              {cancelable: false},
                                            )
                                    }} style={styles.buttonOutlined}>
                                        <Text style={styles.buttonOutlinedText}>{dict.header.leaveBudget}</Text>
                                    </TouchableOpacity>
                                )}
                            </View>
                        )}
                        {/* <View style={{ flex: 0.2 }} /> */}
                    </View>
                  ) : (
                      <View style={styles.container}>
                        <TouchableNativeFeedback>
                          <Image resizeMode='cover' source={Logo} style={styles.image} />
                        </TouchableNativeFeedback>
                        <Text style={styles.headertext}>{dict.header.name}</Text>
                        <TextInput
                          editable={this.state.isEditable}
                          style={[styles.inputBox, this.state.isInputThreeFocus ? styles.textInputFocus : '']}
                          underlineColorAndroid='rgba(0,0,0,0)'
                          placeholder={dict.placeholder.budget_name}
                          value={this.state.name}
                          placeholderTextColor='#29ABE2'
                          selectionColor='#fff'
                          onFocus={this.handlerFocusBlur.bind(this, 'isInputThreeFocus', true)}
                          onBlur={this.handlerFocusBlur.bind(this, 'isInputThreeFocus', false)}
                          // onSubmitEditing={()=> focus.this.saveData()}
                          onChangeText={text => this.onChange('name', text)}
                        />
                        {this.state.action != 'add' &&
                          <Text style={styles.headertext}>{dict.header.user}</Text>
                        }
                        {(this.state.isEditable && this.state.action != 'add') &&
                          <View style={styles.useraddview}>
                            <TextInput
                              style={[styles.inputBoxBudgetUser, this.state.emailValidation ? styles.inputBoxValidation : null, this.state.isInputFourFocus ? styles.textInputFocus : '']}
                              underlineColorAndroid='rgba(0,0,0,0)'
                              onChangeText={(text) => {this.validate('email', text); this.onChange('email', text); }}
                              value={this.state.email}
                              placeholder={dict.placeholder.add_group}
                              placeholderTextColor='#29ABE2'
                              selectionColor='#fff'
                              onFocus={this.handlerFocusBlur.bind(this, 'isInputFourFocus', true)}
                              onBlur={this.handlerFocusBlur.bind(this, 'isInputFourFocus', false)}
                              selectionColor='#fff'
                            />
                            <TouchableOpacity style={styles.adduserbutton}
                              onPress={() => this.addUserToBudget()}>
                              <Text style={styles.addbuttontext}>+</Text>
                            </TouchableOpacity>
                          </View>
                        }
                        <View style={styles.container1}>
                          <ScrollView style={styles.scrollView} showsHorizontalScrollIndicator="true">
                            <List
                              itemList={this.state.users}
                              onItemPress={() => {}}
                              onItemLongPress={() => {}}
                              adminPermission={() => { return (this.state.user.id == this.state.admin && this.state.isEditable) }}
                              isDeletable={(user) => { return (this.state.user.id == this.state.admin && this.state.isEditable && user.id !== this.state.admin) }}
                              itemAction={async props => {
                                await AsyncStorage.setItem('userId', `${props.id}`)
                                this.setState({ selectedUserId: `${props.id}` })
                                {props.action === 'delete' ? (
                                  Alert.alert(
                                  `${dict.header.delete}`,
                                  `${dict.user_delete_question}`,
                                  [{ text: `${dict.permission_list.no}`, style: 'destructive' },
                                  { text: `${dict.permission_list.yes}`, onPress: () => this.removeUser(`${props.id}`), style: 'destructive' },
                                  ],
                                  { cancelable: false },
                                )
                               ) : (
                                  Alert.alert(
                                  `${dict.header.delete}`,
                                  `${dict.change_budget_admin}`,
                                  [{ text: `${dict.permission_list.no}`, style: 'destructive' },
                                  { text: `${dict.permission_list.yes}`, onPress: () => this.changeAdmin(`${props.id}`), style: 'destructive' },
                                  ],
                                  { cancelable: false },
                                )
                                ) }
                              }}
                              />
                          </ScrollView>
                        </View>
                        { this.state.isEditable ? (
                             <TouchableOpacity onPress={() => this.saveData()} style={styles.button}>
                                 <Text style={styles.buttonText}>{dict.header.save}</Text>
                             </TouchableOpacity>
                         ) : (
                             this.state.admin !== this.state.userId && <TouchableOpacity onPress={() => {
                               Alert.alert(
                                       `${dict.header.delete}`,
                                       `${dict.leave_budget}`,
                                       [ {text: `${dict.permission_list.no}` ,style: 'destructive'},
                                         {text: `${dict.permission_list.yes}`, onPress: () => this.deleteUser(this.state.budgetId), style: 'destructive'},
                                       ],
                                       {cancelable: false},
                                     )
                             }} style={styles.buttonOutlined}>
                                 <Text style={styles.buttonOutlinedText}>{dict.header.leaveBudget}</Text>
                             </TouchableOpacity>
                         )}
                      </View>
                    )}
                  {/* <View style={{ flex: 0.2 }} /> */}
                </View>
              </TouchableWithoutFeedback>
            ) : (
                <Container style={styles.container}>
                  <Spinner color='blue' />
                </Container>
              )}
          </Root>
        </ScrollView>
      </Container>
    )
  }
}