import React, { Component } from 'react'
import { View, Text, ScrollView, TouchableOpacity, Dimensions, SafeAreaView } from 'react-native'
import { LineChart } from 'react-native-chart-kit'
import * as dict from '../../common/dictionary/polish.json'
import AsyncStorage from '@react-native-community/async-storage'
import { getBudgetPeriodExpense, getExpensesAnalysis } from '../../apiCallList/expense'
import { styles } from './AnalysisListStyle'
import { errorHandler } from '../../common/js/auth.js'

export default class AnalysisListScreen extends Component {
  constructor () {
    super ()
    this.state = {
      choose: 'day',
      isDataEmpty: false,
      labels: [],
      sumOfExpenses: 0,
      budgetId: '',
      mode: 'expense',
      data: [ 0 ]
    }
  }

  componentDidMount = async () => {
    this.setState({ budgetId: await AsyncStorage.getItem('budgetId') }, () => {
      this.checkPeriod(this.state.choose)
    })
  }

  getLabels(timestamp, choose){
    let a = new Date(timestamp)
    if (choose == 'week') {
      let days = ['Ni','Pn', 'Wt', 'Sr', 'Cz', 'Pt', 'So', 'Ni']
      let dayOfWeek = days[a.getDay()]
      return dayOfWeek
    }
    else if (choose = 'month'){
      let months = ['Sty', 'Lut', 'Mar', 'Kwi', 'Maj', 'Cze', 
      'Lip', 'Sie', 'Wrz', 'Paź', 'Lis', 'Gru']
      let monthofYear = months[a.getMonth()]
      return monthofYear
    }
    else {
      (timestamp = 0) => {
        return (new Date(timestamp)).getDate()
      }
    }
  }

  isLeapYear = (year = -1) => year % 400 === 0 || (year % 100 !== 0 && year % 4 === 0)

  getDayAmountInYear = (year = -1) => this.isLeapYear(year) ? 366 : 365

  getDayAmountInMonth = (year = -1, month = -1) => {
    if (month === 0 || month === 2 || month === 4 || month === 6 || month === 7 || month === 9 || month === 11) {
      return 31
    } else if (month === 1) {
      return this.isLeapYear(year) ? 29 : 28
    } else { return 30 }
  }

  checkPeriod = async choose => {
    let response
    let newData
    let sumOfExpenses = 0
    let end = new Date()
    let start = new Date()
    start.setHours(0, 0, 0, 0)
    end.setHours(23, 59, 59, 999)
    
    switch (choose) {
      case 'week': {
        start.setDate(start.getDate() - 7)
        break
      }
      case 'month': {
        start.setMonth(start.getMonth() - 1)
        break
      }
      case 'year': {
        start.setFullYear(start.getFullYear() - 1)
        break
      }
      case 'day': {
        start = start.getTime()
        end = end.getTime()
        response = await getBudgetPeriodExpense(this.state.budgetId, start, end)
        if (response.length) {
          if(this.state.mode == 'expense') {
            this.setState({
              data: response.map((element, index) => {
                return !element.income ? element.amount : 0
              }),
              isDataEmpty: false,
              })
            }
          else if(this.state.mode == 'income') {
            this.setState({
              data: response.map((element, index) => {
                return element.income ? element.amount : 0
              }),
              isDataEmpty: false,
              })
          }
          else if(this.state.mode == 'balance') {
            this.setState({
              data: response.map((element, index) => {
                return element.income ? element.amount : element.amount * (-1)
              }),
              isDataEmpty: false,
              })
            }
        }
        else {
          this.setState({
            sumOfExpenses: 0,
            labels: [],
            data: [0]
          })
        }
        for (index in this.state.data) {
          sumOfExpenses += this.state.data[index]
        }
        this.setState({ sumOfExpenses })
        return 0
      }
    }

    start = start.getTime()
    end = end.getTime()

    try {
      response = await getExpensesAnalysis(this.state.budgetId, start, end, this.state.mode)
      if (response.length) {
        if(this.state.mode == 'expense') {
          if (choose !== 'month') {
            this.setState({
              data: response.map(element => element.sumOfExpenses ? element.sumOfExpenses : 0 ),
              labels: response.map(element => {return this.getLabels(element.date, this.state.choose)}),
              isDataEmpty: false,
            })
          }
          else {
            this.setState({
              data: response.map(element => element.sumOfExpenses ? element.sumOfExpenses : 0 ),
              labels: ['1', '5', '12', '16', '19', '24', '28'],
              isDataEmpty: false,
            })
          }
        } else if(this.state.mode == 'income') {
          if (choose !== 'month') {
            this.setState({
              data: response.map(element => element.sumOfIncomes ? element.sumOfIncomes : 0 ),
              labels: response.map(element => {return this.getLabels(element.date, this.state.choose)}),
              isDataEmpty: false,
            })
          }
          else {
            this.setState({
              data: response.map(element => element.sumOfIncomes ? element.sumOfIncomes : 0 ),
              labels: ['1', '5', '12', '16', '19', '24', '28'],
              isDataEmpty: false,
            })
          }
        }
        else if(this.state.mode == 'balance') {
          if (choose !== 'month') {
            this.setState({
              data: response.map(element => element.balance ? element.balance : 0 ),
              labels: response.map(element => {return this.getLabels(element.date, this.state.choose)}),
              isDataEmpty: false,
            })
          }
          else {
            this.setState({
              data: response.map(element => element.balance ? element.balance : 0 ),
              labels: ['1', '5', '12', '16', '19', '24', '28'],
              isDataEmpty: false,
            })
          }
        }
        for (index in this.state.data) {
          sumOfExpenses += this.state.data[index]
        }
        this.setState({ sumOfExpenses })
      } else {
        this.setState({
          sumOfExpenses: 0,
          labels: [],
          data: [0]
        })
      }
    } catch (error) {
      errorHandler(error)
      this.setState({
        labels: [],
        sumOfExpenses: 0,
        data: [0]
      })
    }
  }

  setProperties = (stateName = '', newValue) => {
    if((this.state[stateName] !== newValue) && this.state.budgetId) {
      this.setState({
        [stateName]: newValue,
        labels: stateName === 'choose' ?
          newValue === 'day' ? []
            : newValue === 'week' ? dict.day_name_list
            : newValue === 'month' ? []
            : dict.month_list
            : this.state.labels
      }, () => {
        this.checkPeriod(this.state.choose)
      })
    }
  }
  
  render() {
    return(
      <SafeAreaView style={styles.container}>
        <ScrollView style={styles.container}>
          <View style={styles.padding}>
            <View style={styles.choose}>
              <TouchableOpacity style={[styles.button, this.state.choose === 'day' ? styles.active : null]}
                onPress={this.setProperties.bind(this, 'choose', 'day')}>
                <Text style={[styles.text, this.state.choose === 'day' ? styles.activeText : null]}>{dict.analysis.today}</Text>
              </TouchableOpacity>

              <TouchableOpacity style={[styles.button, this.state.choose === 'week' ? styles.active : null]}
                onPress={this.setProperties.bind(this, 'choose', 'week')}>
                <Text style={[styles.text, this.state.choose === 'week' ? styles.activeText : null]}>{dict.analysis.week}</Text>
              </TouchableOpacity>

              <TouchableOpacity style={[styles.button, this.state.choose === 'month' ? styles.active : null]}
                onPress={this.setProperties.bind(this, 'choose', 'month')}>
                <Text style={[styles.text, this.state.choose === 'month' ? styles.activeText : null]}>{dict.analysis.month}</Text>
              </TouchableOpacity>

              <TouchableOpacity style={[styles.button, this.state.choose === 'year' ? styles.active : null]}
                onPress={this.setProperties.bind(this, 'choose', 'year')}>
                <Text style={[styles.text, this.state.choose === 'year' ? styles.activeText : null]}>{dict.analysis.year}</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.spacer}></View>

            <View style={styles.choose}>
              <TouchableOpacity style={[styles.button, this.state.mode === 'expense' ? styles.active : null]}
                onPress={this.setProperties.bind(this, 'mode', 'expense')}>
                <Text style={[styles.text, this.state.mode === 'expense' ? styles.activeText : null]}>{dict.analysis.expenses}</Text>
              </TouchableOpacity>

              <TouchableOpacity style={[styles.button, this.state.mode === 'income' ? styles.active : null]}
                onPress={this.setProperties.bind(this, 'mode', 'income')}>
                <Text style={[styles.text, this.state.mode === 'income' ? styles.activeText : null]}>{dict.analysis.incomes}</Text>
              </TouchableOpacity>

              <TouchableOpacity style={[styles.button, this.state.mode === 'balance' ? styles.active : null]}
                onPress={this.setProperties.bind(this, 'mode', 'balance')}>
                <Text style={[styles.text, this.state.mode === 'balance' ? styles.activeText : null]}>{dict.analysis.balance}</Text>
              </TouchableOpacity>
            </View>
          </View>
          <View style={styles.balanceView}>
            <Text style={styles.balanceText}>{ this.state.sumOfExpenses.toFixed(2) } zł</Text>
          </View>
          <View style={{paddingTop: 20}}>
            <LineChart
              data={{
                labels: this.state.labels,
                datasets: [
                  {
                    data: this.state.data,
                  },
                ],
              }}
              width={Dimensions.get('window').width}
              height={340}
              yAxisLabel={''}
              chartConfig={{
                backgroundColor: '#1cc910',
                backgroundGradientFrom: '#eff3ff',
                backgroundGradientTo: '#efefef',
                decimalPlaces: 2, // optional, defaults to 2dp
                color: (opacity = 255) => `rgba(0, 0, 0, ${opacity})`,
                style: {
                  borderRadius: 20,
                  marginHorizontal: 14,
                },
              }}
              bezier
              style={{
                borderRadius: 16,
              }}
            />
          </View>
        </ScrollView>
      </SafeAreaView>
    )
  }
}
