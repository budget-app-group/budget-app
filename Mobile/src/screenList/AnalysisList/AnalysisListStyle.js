import { StyleSheet } from 'react-native'

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
      },
      title: {
        fontSize: 18,
        marginVertical: 7,
        fontFamily: 'Lato-Regular'
      },
      padding: {
        marginTop: 30
      },
      section: {
        paddingTop: 15,
        flexDirection: 'column',
        marginHorizontal: 14,
        marginBottom: 14,
        paddingBottom: 24,
        borderBottomEndRadius: 0.8,
        borderBottomColor: '#A5A5A5',
        borderBottomWidth: 1,
        alignItems: 'center'
      },
      active: {
        backgroundColor: '#29ABE2'
      },
      activeText: {
        color: '#FFF'
      },
      choose: {
        flexDirection: 'row',
        borderRadius: 20,
        borderColor: '#29ABE2',
        borderWidth: 1,
        justifyContent: 'space-around',
        overflow: 'hidden',
        marginHorizontal: 14,
      },
      spacer: {
        height: 5
      },
      button: {
        padding: 14,
        alignContent: 'center',
        flex: 1,
        alignItems: 'center',
      },
      text: {
        textAlign: 'center',
        fontWeight: '500',
        fontFamily: 'Lato-Regular'
      },
      balanceText: {
        fontWeight: '500',
        textAlign: 'center',
        justifyContent: 'center',
        fontSize: 22,
        fontFamily: 'Lato-Regular'
      },
      balanceView: {
        flexDirection: "row",
        paddingTop: 20,
        alignItems: 'center',
        justifyContent: 'center'
      }
})