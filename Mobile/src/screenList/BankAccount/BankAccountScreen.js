import AsyncStorage from '@react-native-community/async-storage'
import { Container, Spinner } from 'native-base'
import React, { Component } from 'react'
import { Alert, Image, Keyboard, ScrollView, Text, TextInput, ToastAndroid, TouchableOpacity, TouchableWithoutFeedback, View } from 'react-native'
import { TouchableNativeFeedback } from 'react-native-gesture-handler'
import RNPickerSelect from 'react-native-picker-select'
import { Actions } from 'react-native-router-flux'
import { Chevron } from 'react-native-shapes'
import { addBankAccount, deleteBankAccount, getBankAccount, updateBankAccount } from '../../apiCallList/bankAccount'
import * as dict from '../../common/dictionary/polish.json'
import Logo from '../../common/iconList/Logo.png'
import { bankAccountTypeList } from '../../common/js/enum'
import { styles } from './BankAccountScreenStyle'

export default class BankAccountScreen extends Component {
    constructor () {
        super ()
        this.state = {
            id: '',
            name: '',
            budgetId: '',
            image: Logo,
            action: '',
            bankName: '',
            bankAccTypeList: [],
            type: '',
            startingAmount: 0,
            means: '',
            isEditable: true,
            isDataReady: false,
            isInputOneFocus: false,
            isInputTwoFocus: false,
            isInputThreeFocus: false,
            isInputFourFocus: false,

            isBankAccTypePickedColor: '#ff0000'
        }
    }

  async componentDidMount() {
    const budgetId = await AsyncStorage.getItem('budgetId')
    const action = await AsyncStorage.getItem('action')
    const id = await AsyncStorage.getItem('bankAccountId')

        if (action === 'add') {
            this.setState({
                budgetId: budgetId,
                action: 'add'
            })
        } else if (id) {
            const response = await getBankAccount(budgetId, id)
            if (response) {
                this.setState({
                    id: id,
                    name: response.name,
                    bankName: response.bankName,
                    type: response.type,
                    budgetId: budgetId,
                    means: `${response.means}`,
                    action: action,
                    startingAmount: `${response.startingAmount}`,
                    isBankAccTypePickedColor: '#29ABE2'
                })
            }
            if (action === 'preview') {
                this.setState({
                    isEditable: false,
                    isBankAccTypePickedColor: '#29ABE2'
                })
            }
            // #TODO validation
        }
        
        this.setState({
            bankAccTypeList: bankAccountTypeList.map( item => ({ label: item, value: item })),
            isDataReady: true
        })
    }

    saveData = async () => {
        // #TODO validation and onLeave
        const startingAmount = this.state.startingAmount.replace(',', '.')
        let response = false
        if (this.state.action === 'add') {
            response = await addBankAccount(this.state.budgetId, {
                name: this.state.name,
                bankName: this.state.bankName,
                type: this.state.type,
                startingAmount: JSON.parse(startingAmount)
            })
            ToastAndroid.show(`${dict.toast[response ? 'bankAccCreated' : 'operationFailed']}`, ToastAndroid.SHORT)
        } else if (this.state.action === 'edit') {
            response = await updateBankAccount(this.state.budgetId, this.state.id, {
                name: this.state.name,
                bankName: this.state.bankName,
                type: this.state.type,
                startingAmount: JSON.parse(startingAmount)
            })
            ToastAndroid.show(`${dict.toast[response ? 'bankAccEdited' : 'operationFailed']}`, ToastAndroid.SHORT)
        } else if (this.state.action === 'delete') {
            response = await deleteBankAccount(this.state.budgetId, this.state.id)
            if (response) {
                ToastAndroid.show(`${dict.bank_acc_deleted}`, ToastAndroid.SHORT)
            }
        }
        if (response) {
            await AsyncStorage.removeItem('action')
            Actions.BankAccountList()
        }
    }

    onChange (stateName, text) {
        this.setState({
            [stateName]: text
        })
      }
      // #TODO validation

    formatCurrency (value) {
        return `${value} PLN`
    }

    showDeleteAlert() {
        Alert.alert(
            `${dict.header.delete}`,
            `${dict.delete_bank_acc}`,
            [ {text: `${dict.permission_list.no}`, onPress: () => Actions.BankAccountList() ,style: 'destructive'},
              {text: `${dict.permission_list.yes}`, onPress: () => this.saveData(), style: 'destructive'},
            ],
            {cancelable: false},
        )
    }

    handlerFocusBlur = (input, value) => {
        this.setState({[input]:value})
    }

    render () {
        const bankTypePlaceholder = {
            label: `${dict.placeholder.bankTypePlaceholder}`,
            value: null,
            color: '#9EA0A4',
        }
        return (
            <Container style={{flex: 1}}>
                <ScrollView contentContainerStyle={{flexGrow: 1}}>
                { this.state.isDataReady ? (
                <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
                    <View style={styles.container}>
                        {this.state.action === 'delete' ? (
                            <View>
                                {this.showDeleteAlert()}
                            </View>
                        ) : (
                                <View style={styles.container}>
                                    <TouchableNativeFeedback>
                                        <Image resizeMode='cover' source={Logo} style={styles.image}/>
                                    </TouchableNativeFeedback>
                                    <Text style={styles.inputHeader}>{dict.header.name}</Text>
                                    <TextInput
                                        editable={this.state.isEditable}
                                        style={[styles.inputBox, this.state.isInputOneFocus ? styles.textInputFocus : '']}
                                        underlineColorAndroid='rgba(0,0,0,0)'
                                        placeholder={dict.placeholder.bank_account_name}
                                        value={this.state.name}
                                        placeholderTextColor = '#29ABE2'
                                        selectionColor='#fff'
                                        onFocus={this.handlerFocusBlur.bind(this, 'isInputOneFocus', true)}
                                        onBlur={this.handlerFocusBlur.bind(this, 'isInputOneFocus', false)}
                                        // onSubmitEditing={()=> focus.this.saveData()}
                                        onChangeText={text => this.onChange('name', text)}
                                    />
                                    <Text style={styles.inputHeader}>{dict.header.bank_name}</Text>
                                    <TextInput
                                        editable={this.state.isEditable}
                                        style={[styles.inputBox, this.state.isInputTwoFocus ? styles.textInputFocus : '']}
                                        underlineColorAndroid='rgba(0,0,0,0)'
                                        placeholder={dict.placeholder.bank_name}
                                        value={this.state.type}
                                        placeholderTextColor = '#29ABE2'
                                        selectionColor='#fff'
                                        onFocus={this.handlerFocusBlur.bind(this, 'isInputTwoFocus', true)}
                                        onBlur={this.handlerFocusBlur.bind(this, 'isInputTwoFocus', false)}
                                        // onSubmitEditing={()=> focus.this.saveData()}
                                        onChangeText={text => this.onChange('type', text)}
                                    />
                                    <Text style={styles.inputHeader}>{dict.header.startingAmount}</Text>
                                    {this.state.action === 'preview' ? (
                                        <TextInput
                                        style={[styles.inputBox, this.state.isInputThreeFocus ? styles.textInputFocus : '']}
                                        underlineColorAndroid='rgba(0,0,0,0)'
                                        placeholder={dict.placeholder.means}
                                        value={this.formatCurrency(this.state.means)}
                                        placeholderTextColor = '#29ABE2'
                                        selectionColor='#fff'
                                        onFocus={this.handlerFocusBlur.bind(this, 'isInputThreeFocus', true)}
                                        onBlur={this.handlerFocusBlur.bind(this, 'isInputThreeFocus', false)}
                                        onChangeText={text => this.onChange('means', text)}
                                        editable={this.state.isEditable}
                                    />) : (
                                        <TextInput
                                            keyboardType="numeric"
                                            style={[styles.inputBox, this.state.isInputFourFocus ? styles.textInputFocus : '']}
                                            underlineColorAndroid='rgba(0,0,0,0)'
                                            placeholder={dict.placeholder.startingAmount}
                                            value={this.state.startingAmount}
                                            placeholderTextColor = '#29ABE2'
                                            selectionColor='#fff'
                                            onFocus={this.handlerFocusBlur.bind(this, 'isInputFourFocus', true)}
                                            onBlur={this.handlerFocusBlur.bind(this, 'isInputFourFocus', false)}
                                            onChangeText={text => this.onChange('startingAmount', text)}
                                            editable={this.state.isEditable}
                                    />)}
                                    <Text style={styles.inputHeader}>{dict.header.bankType}</Text>
                                    <RNPickerSelect
                                        disabled={!this.state.isEditable}
                                        placeholder={bankTypePlaceholder}
                                        items={this.state.bankAccTypeList}
                                        onValueChange={value => {
                                            this.setState({
                                                bankName: value,
                                                isBankAccTypePickedColor: value ? '#29ABE2' : '#ff0000'
                                            });
                                        }}
                                        style={{
                                        inputAndroid: {
                                            width: 300,
                                            height: 50,
                                            backgroundColor: '#ffffff',
                                            borderRadius: 50,
                                            paddingHorizontal: 16,
                                            fontSize: 16,
                                            marginTop: 7,
                                            color: '#29ABE2',
                                            borderColor: this.state.isBankAccTypePickedColor,
                                            borderWidth: 1,
                                        },
                                        iconContainer: {
                                            top:28,
                                            right: 20,
                                        },
                                        placeholder: {
                                            color: this.state.isBankAccTypePickedColor,
                                            fontSize: 16
                                        }
                                        }}
                                        value={this.state.bankName}
                                        useNativeAndroidPickerStyle={false}
                                        Icon={() => {
                                        return <Chevron size={1.2} color='#29ABE2' />;
                                        }}
                                    />
                                    { this.state.isEditable ? (
                                        <TouchableOpacity onPress={this.saveData} style={styles.button}>
                                            <Text style={styles.buttonText}>{dict.header.save}</Text>
                                        </TouchableOpacity>
                                    ) : (
                                        <TouchableOpacity onPress={Actions.BankAccountList} style={styles.buttonOutlined}>
                                            <Text style={styles.buttonOutlinedText}>{dict.header.return}</Text>
                                        </TouchableOpacity>
                                    )}
                            </View>
                        )}
                    </View>
                </TouchableWithoutFeedback>
                ) : (
                    <Container style={styles.container}>
                        <Spinner color='blue'/>
                    </Container>
                )}
                </ScrollView>
            </Container>
        )
    }
}