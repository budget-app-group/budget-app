import AsyncStorage from '@react-native-community/async-storage'
import { Container, Spinner } from 'native-base'
import React, { Component } from 'react'
import { ScrollView, Text, TextInput, TouchableOpacity, View, ToastAndroid } from 'react-native'
import RNPickerSelect from 'react-native-picker-select'
import { Actions } from 'react-native-router-flux'
import { Chevron } from 'react-native-shapes'
import { getBankAccountList } from '../../apiCallList/bankAccount'
import { getAllCategories } from '../../apiCallList/category'
import { getGoal, postDonation } from '../../apiCallList/goal'
import * as dict from '../../common/dictionary/polish.json'
import { styles } from './GoalScreenStyle'

export default class DonationScreen extends Component {
    constructor () {
        super ()
        this.state = {
            id: '',
            budgetId: -1,
            masterCategoryId: -1,
            categoryId: -1,
            action: '',

            categoryList: [],
            bankAccountList: [],
            
            // data to send below
            amount: '',
            name: '',
            expenseDate: new Date(),
            bankAccountId: -1,
            shopName: '',
            isIncome: false,

            // goal data
            goalName: '',
            targetAmount: '',
            targetDate: new Date(),
            collectedAmount: '',

            isDataRead: false,
            isInputOneFocus: false,

            isCategoryPickedColor: '#29ABE2',
            isBankAccPickedColor: '#ff0000',
        }
    }

    async componentDidMount () {
        const budgetId = await AsyncStorage.getItem('budgetId')
        const masterCategoryId = await AsyncStorage.getItem('masterCategoryId')
        const categoryId = await AsyncStorage.getItem('categoryId')
        const bankAccountId = await AsyncStorage.getItem('bankAccountId')
        const action = await AsyncStorage.getItem('action')
        const goalId = await AsyncStorage.getItem('goalId')

        const response = await getGoal(budgetId, goalId)
        if (response) {
            this.setState({
                goalName: response.name,
                targetAmount: `${response.targetAmount}`,
                targetDate: new Date(response.targetDate),
                collectedAmount: `${response.collectedAmount}`
            })
        }
        if (action === 'donate') {
            this.setState({
                goalId,
                budgetId,
                masterCategoryId,
                categoryId,
                bankAccountId: JSON.parse(bankAccountId)
            })
        } else {
            Actions.GoalList()
        }

        const bankAccounts = await getBankAccountList(budgetId)
        if (bankAccounts.length > 0) {
            this.setState({
                bankAccountList: bankAccounts.map( ba => ({ label: ba.name, value: ba.id }))
            })
            if (bankAccounts.length === 1) this.setState({bankAccountId: bankAccounts[0].id})
        }

        const categories = await getAllCategories(budgetId)
        if (categories.length > 0) {
            this.setState({
                categoryList: categories.map( cat => Object.assign(cat, { label: cat.name, value: cat.id }))
            })
            if (categories.length === 1)  this.setState({categoryId: categories[0].id})
        }
        this.setState({isDataReady: true})
    }

    setMasterCat = async () => {
        let category = this.state.categoryList.find( cat => {
            return cat.id === this.state.categoryId
        })
        category ? await this.setState({masterCategoryId: JSON.parse(category.budgetCategory.id)}) : (
            ToastAndroid.show(dict.error_list.empty.noCategory, ToastAndroid.SHORT)
        )
    }

    save = async () => {
        if (this.state.bankAccountId === null) {
            ToastAndroid.show(dict.error_list.empty.noBankAcc, ToastAndroid.SHORT)
        } else {
            const response = await postDonation(this.state.budgetId, this.state.masterCategoryId,
                this.state.categoryId, {
                    amount: JSON.parse(this.state.amount),
                    name: this.state.goalName,
                    expenseDate: this.timestampConvert(this.state.expenseDate),
                    bankAccountId: this.state.bankAccountId,
                    shopName: this.state.shopName,
                    isIncome: this.state.isIncome,
                    goalId: this.state.goalId
                })
                if (response) {
                    ToastAndroid.show(dict.donation, ToastAndroid.SHORT)
                    Actions.GoalList()
                }
        }
    }
    onChange (stateName, text) {
        this.setState({
            [stateName]: text
        })
    }

    timestampConvert(date) {
        var timestamp = Date.parse(date)
        return timestamp
    }

    handlerFocusBlur = (input, value) => {
        this.setState({[input]:value})
    }

    render () {
        const bankAccPlaceholder = {
            label: `${dict.placeholder.bankAccountPlaceholder}`,
            value: null,
            color: '#9EA0A4',
        }
        const categoryPlaceHolder = {
            label: `${dict.placeholder.categoryPlaceholder}`,
            value: null,
            color: '#9EA0A4',
        }
        return (
            <Container style={{flex: 1}}>
                <ScrollView contentContainerStyle={{flexGrow: 1}}>
                    { this.state.isDataReady ? (
                    <View style={styles.container2}>
                        <View style={styles.containerCenter}>
                            <Text style={styles.headertext}>{this.state.goalName}</Text>
                        </View>
                        <View style={styles.containerCenter}>
                            <TextInput
                                keyboardType="numeric"
                                style={[styles.inputBox, this.state.isInputOneFocus ? styles.textInputFocus : '']}
                                underlineColorAndroid='rgba(0,0,0,0)'
                                placeholder={dict.placeholder.donationAmount}
                                value={this.state.amount}
                                placeholderTextColor = '#29ABE2'
                                selectionColor='#fff'
                                onFocus={this.handlerFocusBlur.bind(this, 'isInputOneFocus', true)}
                                onBlur={this.handlerFocusBlur.bind(this, 'isInputOneFocus', false)}
                                onChangeText={text => this.onChange('amount', text)}
                            />
                            <Text style={styles.inputHeader}>{dict.header.bankAccount}</Text>
                            <RNPickerSelect
                                    placeholder={bankAccPlaceholder}
                                    items={this.state.bankAccountList}
                                    onValueChange={value => {
                                        this.setState({
                                            bankAccountId: value,
                                            isBankAccPickedColor: value ? '#29ABE2' : '#ff0000'
                                        });
                                    }}
                                    style={{
                                    inputAndroid: {
                                        width: 300,
                                        height: 50,
                                        backgroundColor: '#ffffff',
                                        borderRadius: 50,
                                        paddingHorizontal: 16,
                                        marginTop: 7,
                                        fontSize: 16,
                                        color: '#29ABE2',
                                        borderColor: this.state.isBankAccPickedColor,
                                        borderWidth: 1,
                                    },
                                    iconContainer: {
                                        top:28,
                                        right: 20,
                                    },
                                    placeholder: {
                                        color: this.state.isBankAccPickedColor,
                                        fontSize: 16
                                      }
                                    }}
                                    value={this.state.bankAccountId}
                                    useNativeAndroidPickerStyle={false}
                                    Icon={() => {
                                    return <Chevron size={1.2} color='#29ABE2' />;
                                    }}
                                />
                            <View style={{ flex: 0.08 }} />
                            <Text style={styles.inputHeader}>{dict.header.category}</Text>
                            <RNPickerSelect
                                placeholder={categoryPlaceHolder}
                                items={this.state.categoryList}
                                onValueChange={value => {
                                    this.setState({
                                        categoryId: value,
                                        isCategoryPickedColor: value ? '#29ABE2' : '#ff0000'
                                    })
                                    // this.setMasterCat()
                                }}
                                style={{
                                inputAndroid: {
                                    width: 300,
                                    height: 50,
                                    backgroundColor: '#ffffff',
                                    borderRadius: 50,
                                    paddingHorizontal: 16,
                                    fontSize: 16,
                                    marginTop: 7,
                                    color: '#29ABE2',
                                    borderColor: this.state.isCategoryPickedColor,
                                    borderWidth: 1,
                                },
                                iconContainer: {
                                    top:28,
                                    right: 20,
                                },
                                placeholder: {
                                    color: this.state.isCategoryPickedColor,
                                    fontSize: 16
                                    }
                                }}
                                value={this.state.categoryId}
                                useNativeAndroidPickerStyle={false}
                                Icon={() => {
                                return <Chevron size={1.2} color='#29ABE2' />;
                                }}
                            />
                            <TouchableOpacity onPress={async () => {
                                            await this.setMasterCat()
                                            this.save()
                                        }} style={styles.button}>
                                <Text style={styles.buttonText}>{dict.header.donate}</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                    ) : (
                        <Container style={styles.container}>
                            <Spinner color='blue'/>
                        </Container>
                    )}
            </ScrollView>
        </Container>
        )
    }
}