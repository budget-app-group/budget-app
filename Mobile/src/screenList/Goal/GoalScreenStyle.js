import { StyleSheet } from 'react-native'

export const styles = StyleSheet.create({
  container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
  },
  container1: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    // justifyContent: 'center',
  },
  container2: {
    flex: 1,
    flexDirection: "column",
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center'
  },
  containerCenter: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  image: {
    width: 120,
    height: 120,
  },
  headertext: {
    fontSize: 30,
    color: '#29ABE2',
    padding: 15,
  },
  inputBox: {
    width:300,
    backgroundColor:'#ffffff',
    borderRadius: 25,
    borderColor:'#29ABE2',
    borderWidth: 1,
    paddingHorizontal:16,
    fontSize:16,
    color:'#29ABE2',
    marginVertical: 10
  },
  inputBox2: {
    width:300,
    backgroundColor:'#D4EEF9',
    borderRadius: 25,
    paddingHorizontal:16,
    fontSize:16,
    color:'#ffffff',
    marginVertical: 10
  },
  button: {
    width:300,
    backgroundColor:'#29ABE2',
     borderRadius: 25,
      marginVertical: 10,
      paddingVertical: 13
  },
  buttonText: {
    fontSize:16,
    fontWeight:'500',
    color:'#ffffff',
    textAlign:'center'
  },
  useraddview: {
    justifyContent: 'center',
    flexDirection: 'row'
  },
  adduserbutton: {
    width:50,
    height: 50,
    marginLeft: 10,
    backgroundColor:'#29ABE2',
    borderRadius: 50,
    marginVertical: 10,
    paddingVertical: 4
  },
  addbuttontext: {
    flex: 1, color: '#fff', 
    fontSize: 30, 
    justifyContent: 'center', 
    textAlign: 'center'
  },
  buttonOutlined: {
    width:300,
    borderColor:'#29ABE2',
    borderWidth: 1,
    borderRadius: 25,
    marginVertical: 10,
    paddingVertical: 13
  },
  buttonOutlinedText: {
    fontSize:16,
    fontWeight:'500',
    color:'#29ABE2',
    textAlign:'center'
  },
  inputHeader: {
    fontSize: 16,
    color: '#29ABE2',
    paddingRight: 20 
  },
  textInputFocus: {
    borderWidth: 2,
    borderColor:'#26A0D4',
  }
})