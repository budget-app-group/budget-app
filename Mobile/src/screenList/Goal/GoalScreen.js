import AsyncStorage from '@react-native-community/async-storage'
import { Container, DatePicker, Spinner } from 'native-base'
import React, { Component } from 'react'
import { Alert, Image, Keyboard, ScrollView, Text, TextInput, ToastAndroid, TouchableOpacity, TouchableWithoutFeedback, View } from 'react-native'
import { TouchableNativeFeedback } from 'react-native-gesture-handler'
import { Actions } from 'react-native-router-flux'
import { addGoal, deleteGoal, getGoal, updateGoal } from '../../apiCallList/goal'
import * as dict from '../../common/dictionary/polish.json'
import Logo from '../../common/iconList/Logo.png'
import { styles } from './GoalScreenStyle'


export default class GoalScreen extends Component {
    constructor () {
        super()
        this.state = {
            id: '',
            budgetId: -1,
            image: Logo,
            action: '',
            name: '',
            targetAmount: '',
            targetDate: new Date(),
            collectedAmount: '',
            isEditable: true,
            donateable: false,
            isDataReady: false,
            isInputOneFocus: false,
            isInputTwoFocus: false,
            isInputThreeFocus: false
        }
    }

    async componentDidMount () {
        const budgetId = await AsyncStorage.getItem('budgetId')
        const goalId = await AsyncStorage.getItem('goalId')
        const action = await AsyncStorage.getItem('action')
        const bankAccountId = await AsyncStorage.getItem('bankAccountId')

        if (action === 'add') {
            this.setState({
                action: 'add',
                budgetId,
                bankAccountId: JSON.parse(bankAccountId)
            })
        } else if (goalId) {
            const response = await getGoal(budgetId, goalId)
            if (response) {
                this.setState({
                    budgetId,
                    id: response.id,
                    action: action,
                    name: response.name,
                    targetAmount: `${response.targetAmount}`,
                    targetDate: new Date(response.targetDate),
                    collectedAmount: `${response.collectedAmount}`
                })
                if (action === 'preview') {
                    this.setState({
                        isEditable: false,
                        donateable: true
                    })
                }
            }
        }

        this.setState({isDataReady: true})
    }

    save = async () => {
        let response = false
        if (this.state.action === 'add') {
            if (this.state.targetAmount === '' || this.state.name === '' || this.state.collectedAmount === '') {
                ToastAndroid.show(dict.error_list.empty.fullfilData, ToastAndroid.SHORT)
            } else {
                response = await addGoal(this.state.budgetId, {
                    name: this.state.name,
                    targetAmount: JSON.parse(this.state.targetAmount),
                    collectedAmount: JSON.parse(this.state.collectedAmount),
                    targetDate: this.state.targetDate.getTime()
                })
                ToastAndroid.show(dict.toast[response ? 'goalCreated' : 'operationFailed'], ToastAndroid.SHORT)
            }
        } else if (this.state.action === 'edit') {
            response = await updateGoal(this.state.budgetId, this.state.id, {
                name: this.state.name,
                targetAmount: JSON.parse(this.state.targetAmount),
                collectedAmount: JSON.parse(this.state.collectedAmount),
                targetDate: this.state.targetDate.getTime()
            })
            ToastAndroid.show(dict.toast[response ? 'goalEdited' : 'operationFailed'], ToastAndroid.SHORT)
        } else if (this.state.action === 'delete') {
            response = await deleteGoal(this.state.budgetId, this.state.id)
            if (response) {
                ToastAndroid.show(dict.goal_deleted, ToastAndroid.SHORT)
            }
        }
        if (response) {
            Actions.GoalList()
        }
    }

    onChange (stateName, text) {
        this.setState({
            [stateName]: text
        })
    }
    
    donate = async () => {
        await AsyncStorage.setItem('action', 'donate')
        Actions.Donation()
    }

    showDeleteAlert() {
        Alert.alert(
            `${dict.header.delete}`,
            `${dict.delete_goal}`,
            [ {text: `${dict.permission_list.no}`, onPress: () => Actions.GoalList() ,style: 'destructive'},
              {text: `${dict.permission_list.yes}`, onPress: () => this.save(), style: 'destructive'},
            ],
            {cancelable: false},
        )
    }

    handlerFocusBlur = (input, value) => {
        this.setState({[input]:value})
    }

    render () {
        return (
            <Container style={{flex: 1}}>
                <ScrollView contentContainerStyle={{flexGrow: 1}}>
                    { this.state.isDataReady ? (
                    <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
                        <View style={styles.container}>
                            {this.state.action === 'delete'? (
                                    <View>
                                        {this.showDeleteAlert()}
                                    </View>
                                ) : (
                                    <View style={styles.container}>
                                        <TouchableNativeFeedback>
                                            <Image resizeMode='cover' source={Logo} style={styles.image}/>
                                        </TouchableNativeFeedback>
                                        <Text style={styles.inputHeader}>{dict.header.name}</Text>
                                        <TextInput
                                            style={[styles.inputBox, this.state.isInputOneFocus ? styles.textInputFocus : '']}
                                            underlineColorAndroid='rgba(0,0,0,0)'
                                            placeholder={dict.placeholder.goalName}
                                            value={this.state.name}
                                            placeholderTextColor = '#29ABE2'
                                            selectionColor='#fff'
                                            onFocus={this.handlerFocusBlur.bind(this, 'isInputOneFocus', true)}
                                            onBlur={this.handlerFocusBlur.bind(this, 'isInputOneFocus', false)}
                                            onChangeText={text => this.onChange('name', text)}
                                            editable={this.state.isEditable}
                                        />
                                        <Text style={styles.inputHeader}>{dict.placeholder.targetAmount}</Text>
                                        <TextInput
                                            keyboardType="numeric"
                                            style={[styles.inputBox, this.state.isInputTwoFocus ? styles.textInputFocus : '']}
                                            underlineColorAndroid='rgba(0,0,0,0)'
                                            placeholder={dict.placeholder.targetAmount}
                                            value={this.state.targetAmount}
                                            placeholderTextColor = '#29ABE2'
                                            selectionColor='#fff'
                                            onFocus={this.handlerFocusBlur.bind(this, 'isInputTwoFocus', true)}
                                            onBlur={this.handlerFocusBlur.bind(this, 'isInputTwoFocus', false)}
                                            onChangeText={text => this.onChange('targetAmount', text)}
                                            editable={this.state.isEditable}
                                        />
                                        <Text style={styles.inputHeader}>{dict.placeholder.collectedAmount}</Text>
                                        <TextInput
                                            keyboardType="numeric"
                                            style={[styles.inputBox, this.state.isInputThreeFocus ? styles.textInputFocus : '']}
                                            underlineColorAndroid='rgba(0,0,0,0)'
                                            placeholder={dict.placeholder.collectedAmount}
                                            value={this.state.collectedAmount}
                                            placeholderTextColor = '#29ABE2'
                                            selectionColor='#fff'
                                            onFocus={this.handlerFocusBlur.bind(this, 'isInputThreeFocus', true)}
                                            onBlur={this.handlerFocusBlur.bind(this, 'isInputThreeFocus', false)}
                                            onChangeText={text => this.onChange('collectedAmount', text)}
                                            editable={this.state.isEditable}
                                        />
                                        <DatePicker
                                            style={styles.inputBox}
                                            defaultDate={this.state.targetDate}
                                            minimumDate={new Date(this.state.targetDate.getFullYear() - 10,
                                                this.state.targetDate.getMonth(), this.state.targetDate.getDay() + 1)}
                                            maximumDate={new Date(this.state.targetDate.getFullYear() + 10,
                                                this.state.targetDate.getMonth(), this.state.targetDate.getDay() + 1)}
                                            locale={'pl'}
                                            timeZoneOffsetInMinutes={undefined}
                                            modalTransparent={false}
                                            animationType={'fade'}
                                            androidMode={'default'}
                                            textStyle={{ color: '#29ABE2' }}
                                            placeHolderTextStyle={{ color: '#ffffff' }}
                                            onDateChange={newDate => this.setState({ targetDate: newDate })}
                                            disabled={!this.state.isEditable}
                                        />
                                        { this.state.isEditable ? (
                                            <TouchableOpacity onPress={this.save} style={styles.button}>
                                                <Text style={styles.buttonText}>{dict.header.save}</Text>
                                            </TouchableOpacity>
                                        ) : (
                                        <View>
                                            { this.state.donateable &&
                                                <TouchableOpacity onPress={this.donate} style={styles.button}>
                                                <Text style={styles.buttonText}>{dict.header.addDonation}</Text>
                                            </TouchableOpacity>
                                            }
                                            <TouchableOpacity onPress={ () => Actions.GoalList() } style={styles.buttonOutlined}>
                                                <Text style={styles.buttonOutlinedText}>{dict.header.return}</Text>
                                            </TouchableOpacity>
                                        </View>
                                        )}
                                    </View>
                                )}
                        </View>
                    </TouchableWithoutFeedback>
                    ) : (
                        <Container style={styles.container}>
                            <Spinner color='blue'/>
                        </Container>
                    )}
                    </ScrollView>
                </Container>
        )
    }
}
