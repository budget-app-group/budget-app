import React, { Component } from 'react'
import { View } from 'react-native'
import Navigator from './src/common/js/router'
console.disableYellowBox = true

export default class App extends Component {  
  render() {
    return (
      <View style={{ flex: 1 }}>
        <Navigator />
      </View>
    )
  }
}
