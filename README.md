# Welcome
This is Engineer's Thesis enabling polish people to handle their home budget in easy way.

# Manuals

## Backend
... #TODO

## Mobile
... #TODO

## Web
1. Go to 'web' folder
2. Enter 'yarn install'
3. Now you have few possibilities:
  - yarn start - starts the development server
  - yarn build - bundles the app into static production build
  - yarn test - starts the test runner
  - yarn eject - removes this tool and copies build dependencies, configuration files
    and scripts into the app directory. If you do this, you can’t go back!

# Contributor List
1. Łuksz Siwocha - mobile + backend 
2. Konrad Sobaniec - mobile + graphics
3. Hubert Staszek - backend + devops
4. Jakub Stefko - mobile + web

# Special thanks to
1. Our supervisor [Patryk Żywica](https://min.wmi.amu.edu.pl/en/staff/patryk-zywica/)
2. Friend of Hubert who made all the mobile views for us
3. [Freepik](https://www.flaticon.com/authors/freepik) from [flaticon](https://www.flaticon.com/) for all the icons
4. Everyone who was supportive to us, but we have forgotten to mention (we are sorry...)
