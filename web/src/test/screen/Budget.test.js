import React from 'react'
import ReactDOM from 'react-dom'
import Budget from '../../screen/Budget'
import { Route, BrowserRouter } from 'react-router-dom'
import { Provider } from 'react-redux'
import createStore from '../../store/createStore'

const store = createStore(window.__INITIAL_STATE__)

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <Route exact path='/budget' component={Budget} />
    </BrowserRouter>
  </Provider>, div)
  ReactDOM.unmountComponentAtNode(div)
})
