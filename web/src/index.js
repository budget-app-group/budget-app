import React from 'react'
import ReactDOM from 'react-dom'
import './style/normalize.css'
import './style/style.scss'
import App from './screen'
import createStore from './store/createStore'

const store = createStore(window.__INITIAL_STATE__)
window.store = store

ReactDOM.render(<App store={store} />, document.getElementById('root'))
