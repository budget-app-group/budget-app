export const isEmpty = value => value === ''

export const passwordRegex = new RegExp("^.{7,30}$")

export const floatRegex = new RegExp('^-?[0-9]+(.[0-9]{1,2})?$')

export const isPercent = value => !isNaN(value) && Number(value) <= 100 && Number(value) >= 0