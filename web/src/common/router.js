import React from 'react'
import { Route, BrowserRouter, Switch } from 'react-router-dom'
import PrivateRoute from './privateRoute'

import BankAccount from '../screen/BankAccount'
import Budget from '../screen/Budget'
import Category from '../screen/Category'
import Expense from '../screen/Expense'
import Home from '../screen/Home'
import Login from '../screen/Login'
import MasterCategory from '../screen/MasterCategory'
import NewPassword from '../screen/NewPassword'
import NotFound from '../screen/NotFound'
import Register from '../screen/Register'
import ResetPassword from '../screen/ResetPassword'
import Confirm from '../screen/Confirm'

const Router = (
  <BrowserRouter>
    <Switch>
      <PrivateRoute exact path='/bankAccount'><BankAccount /></PrivateRoute>
      <PrivateRoute exact path='/budget'><Budget /></PrivateRoute>
      <PrivateRoute exact path='/category'><Category /></PrivateRoute>
      <PrivateRoute exact path='/expense'><Expense /></PrivateRoute>
      <PrivateRoute exact path='/'><Home /></PrivateRoute>
      <PrivateRoute exact path='/masterCategory'><MasterCategory /></PrivateRoute>
      <Route exact path='/confirm-budget-join' component={Confirm} />
      <Route exact path='/confirm-email' component={Confirm} />
      <Route exact path='/confirm-reset-password' component={NewPassword} />
      <Route exact path='/login' component={Login} />
      <Route exact path='/register' component={Register} />
      <Route exact path='/reset-password' component={ResetPassword} />
      <Route component={NotFound} />
    </Switch>
  </BrowserRouter>
)

export default Router