import React from 'react'
import { Route, Redirect } from 'react-router-dom'
import TopBar from '../component/TopBar/TopBar'
import Navigation from '../component/Navigation'
import { Grid } from 'semantic-ui-react'

const PrivateRoute = ({ children, ...rest }) => {
  return (
    <Route
      {...rest}
      render={({ location }) =>
        !!localStorage.getItem('token') ? (
          <section style={{ height: '100%' }}>
            <TopBar />
            <Grid>
              <Grid.Column width={2} stretched className='no-padding full-screen-height'>
                <Navigation />
              </Grid.Column>
              <Grid.Column width={13} stretched className='full-screen-height'>
                {children}
              </Grid.Column>
            </Grid>
          </section>
        ) : (
          <Redirect
            to={{
              pathname: '/login',
              state: { from: location }
            }}
          />
        )
      }
    />
  )
}

export default PrivateRoute
