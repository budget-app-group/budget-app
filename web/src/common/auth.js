export const protocol = 'https'
export const origin = 'mybudget-wmi.herokuapp.com'


export const errorHandler = error => {
  console.log('error: ', error)

  if (error.response && error.response.status && error.response.status === 401) {
    localStorage.clear()
    window.location.replace(window.location.origin)
  }
}


export const getHeaderList = (token = '') => {
  if (token === '') {
    localStorage.clear()
    window.location.replace(window.location.origin)
  } else {
    return { headers: { 'Authorization': token } }
  }
}