export const formDate = (token = null, isEuropeanFormat = false) => {
  let data = token ? new Date(token) : new Date()

  return isEuropeanFormat
    ? `${('0' + data.getDate()).slice(-2)}-${('0' + (data.getMonth() + 1)).slice(-2)}-${data.getFullYear()}`
    : `${data.getFullYear()}-${('0' + (data.getMonth() + 1)).slice(-2)}-${('0' + data.getDate()).slice(-2)}`
}