export const bankList = [
  {
    index: 0,
    key: 0,
    text: '-',
    value: 'CASH'
  },
  {
    index: 1,
    key: 1,
    text: 'ING Bank Śląski',
    value: 'ING'
  },
  {
    index: 2,
    key: 2,
    text: 'PKO Bank Polski',
    value: 'PKO'
  },
  {
    index: 3,
    key: 3,
    text: 'Bank Pekao S.A.',
    value: 'PEKAO'
  },
  {
    index: 4,
    key: 4,
    text: 'mBank',
    value: 'MBANK'
  },
  {
    index: 5,
    key: 5,
    text: 'Santander Bank Polska',
    value: 'SANTANDER'
  }
]