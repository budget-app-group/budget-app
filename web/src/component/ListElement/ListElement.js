import React from 'react'
import PropTypes from 'prop-types'
import './ListElement.scss'
import _ from 'lodash'
import { Button, Table, Icon } from 'semantic-ui-react'
import { formDate } from '../../common/dates'

const renderCell = (property, item) => {
  if (typeof property.influenceList !== 'undefined') {
    return property.influenceList.filter(element => element.value === _.get(item, property.field))[0].text
  } else if (typeof property.isColor !== 'undefined') {
    return <Icon name='circle' style={{ color: _.get(item, property.field) }} />
  } else if (typeof property.isDate !== 'undefined') {
    return formDate(_.get(item, property.field), true)
  } else {
    return _.get(item, property.field)
  }
}

const ListElement = props => {
  const isAdmin = props.isAdmin(_.get(props.element, props.adminElementProperty))
  return (
    <Table.Row
      className={`list-element ${props.selectedId === props.element.id ? 'list-element--active' : ''}`}
      index={`${props.index}`}
      key={`${props.index}`}
      onDoubleClick={props.editHandler.bind(this, props.element.id)}
      onClick={props.setIdHandler.bind(this, props.element.id)}>
      <Table.Cell className='list-element-property' key='index'>{props.index}</Table.Cell>

      {props.propertyList.map((property, index) =>
        <Table.Cell className='list-element-property' key={`${index}`}>
          {renderCell(property, props.element)}
        </Table.Cell>)
      }

      {props.isLeavable &&
        <Table.Cell textAlign='center' className='list-element-property' key='leave'>
          <Button basic
            onClick={props.leaveHandler.bind(this, props.element.name)}
            disabled={isAdmin}
          ><Icon name='sign out'/>Opuść</Button>
        </Table.Cell>
      }

      <Table.Cell textAlign='center' className='list-element-property' key='edit'>
        <Button basic color='green' style={{width: 120}}
          onClick={props.editHandler.bind(this, props.element.id)}
        ><Icon name={isAdmin ? 'pencil alternate' : 'eye'}/>{isAdmin ? 'Edycja' : 'Podgląd'}</Button>
      </Table.Cell>

      <Table.Cell textAlign="center" key='delete'>
        <Button color='red' size='small' basic className='list-element-button'
          onClick={props.deleteHandler.bind(this, props.element.name)}
          disabled={!isAdmin}
        ><Icon name='trash alternate outline'/>Usuń</Button>
      </Table.Cell>

    </Table.Row>
  )
}

ListElement.propTypes = {
  element: PropTypes.object.isRequired,
  index: PropTypes.number.isRequired,
  setIdHandler: PropTypes.func.isRequired,
  editHandler: PropTypes.func.isRequired,
  deleteHandler: PropTypes.func.isRequired,
  user: PropTypes.object.isRequired,
  selectedId: PropTypes.number.isRequired,
  propertyList: PropTypes.array.isRequired,
  leaveHandler: PropTypes.func,
  isLeavable: PropTypes.bool,
  isAdmin: PropTypes.func.isRequired,
  adminElementProperty: PropTypes.string.isRequired
}

export default ListElement