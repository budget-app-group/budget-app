import PropTypes from 'prop-types'
import React from 'react'
import { Container, Table } from 'semantic-ui-react'
import ListElement from '../ListElement/ListElement'
import ListHeader from '../ListHeader/ListHeader'
import './List.scss'

const List = props => {
  return (
    <Container fluid className='list'>
      <ListHeader
        name={props.headerName}
        searchHandler={props.searchHandler}
        addHandler={props.addHandler}
        isAddEnabled={props.isAddEnabled}
        dropdownList={props.dropdownList}
        isCSVenabled={props.isCSVenabled}
        isCSVshown={props.isCSVshown}
        addCSVhandler={props.addCSVhandler}
      />
      <Table basic className='list-data'>
        <Table.Header className='list-data-header'>
          <Table.Row>
            <Table.HeaderCell className='header-element' key='id'>Id</Table.HeaderCell>

            {props.propertyList.map(property =>
              <Table.HeaderCell className='header-element' key={`${property.label}`}>
                {property.label}
              </Table.HeaderCell>)
            }

            {props.isLeavable && <Table.HeaderCell width={2} textAlign='center' className='header-element' key='leave'>Opuść</Table.HeaderCell>}

            <Table.HeaderCell width={2} textAlign='center' className='header-element' key='edit'>
              {props.adminElementProperty === '' ? 'Edycja' : 'Edycja / Podgląd'}
            </Table.HeaderCell>

            <Table.HeaderCell width={2} textAlign='center' className='header-element' key='delete'>Usuń</Table.HeaderCell>

          </Table.Row>
        </Table.Header>
        <Table.Body>
          {props.data.map((element, index) => 
            <ListElement
              element={element}
              index={index + 1}
              key={index}
              setIdHandler={props.setIdHandler}
              editHandler={props.editHandler}
              deleteHandler={props.deleteHandler}
              propertyList={props.propertyList}
              user={props.user}
              selectedId={props.selectedId}
              isLeavable={props.isLeavable}
              isAdmin={props.isAdmin}
              adminElementProperty={props.adminElementProperty}
              leaveHandler={props.leaveHandler}
            />
          )}
        </Table.Body>
      </Table>
    </Container>
  )
}

List.propTypes = {
  headerName: PropTypes.string.isRequired,
  searchHandler: PropTypes.func.isRequired,
  addHandler: PropTypes.func.isRequired,
  data: PropTypes.array.isRequired,
  setIdHandler: PropTypes.func.isRequired,
  editHandler: PropTypes.func.isRequired,
  deleteHandler: PropTypes.func.isRequired,
  propertyList: PropTypes.array.isRequired,
  user: PropTypes.object.isRequired,
  selectedId: PropTypes.number.isRequired,
  isLeavable: PropTypes.bool,
  leaveHandler: PropTypes.func,
  isAdmin: PropTypes.func.isRequired,
  adminElementProperty: PropTypes.string.isRequired,
  isAddEnabled: PropTypes.bool.isRequired,
  dropdownList: PropTypes.array.isRequired,
  isCSVenabled: PropTypes.bool,
  isCSVshown: PropTypes.bool,
  addCSVhandler: PropTypes.func
}

export default List