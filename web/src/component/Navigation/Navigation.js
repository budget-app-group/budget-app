import React from 'react'
import './Navigation.scss'
import { Menu, Icon } from 'semantic-ui-react'
import * as dict from '../../common/dictionary/polish.json'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'

const Navigation = props => {
  return (
    <Menu color='blue' fluid vertical className='navigation-container'>
      <Menu.Item
        as={Link}
        to='/'
        name={dict.header.home}
        active={props.location.pathname === '/'}>
        {dict.header.home}
        <Icon name='home' />
      </Menu.Item>

      <Menu.Item
        as={Link}
        to='/bankAccount'
        name={dict.header.bank_account_list}
        active={props.location.pathname === '/bankAccount'}>
        {dict.header.bank_account_list}
        <Icon name='credit card outline' />
      </Menu.Item>

      <Menu.Item
        as={Link}
        to='/budget'
        name={dict.header.budget_list}
        active={props.location.pathname === '/budget'}>
        {dict.header.budget_list}
        <Icon name='calculator' />
      </Menu.Item>

      <Menu.Item
        as={Link}
        to='/masterCategory'
        name={dict.header.master_category_list}
        active={props.location.pathname === '/masterCategory'}>
        {dict.header.master_category_list}
        <Icon name='folder' />
      </Menu.Item>

      <Menu.Item
        as={Link}
        to='/category'
        name={dict.header.category_list}
        active={props.location.pathname === '/category'}>
        {dict.header.category_list}
        <Icon name='folder open' />
      </Menu.Item>

      <Menu.Item
        as={Link}
        to='/expense'
        name={dict.header.expense_list}
        active={props.location.pathname === '/expense'}>
        {dict.header.expense_list}
        <Icon name='money' />
      </Menu.Item>
      
    </Menu>
  )
}

Navigation.propTypes = {
  location: PropTypes.object.isRequired
}

export default Navigation
