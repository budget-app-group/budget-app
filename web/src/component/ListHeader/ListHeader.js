import React from 'react'
import PropTypes from 'prop-types'
import { Header, Input, Button, Dropdown, Container, Grid, Icon } from 'semantic-ui-react'

const ListHeader = props => {
  return (
    <Grid padded columns='13'>
        <Grid.Row centered>
          <Header as='h1'>{props.name}</Header>
        </Grid.Row>
        <Grid.Row style={{ justifyContent: 'space-between'}}>
          <Button
            basic
            color='blue'
            className='add-button'
            disabled={!props.isAddEnabled}
            onClick={props.addHandler}>
            <Icon name='add'/>
            Dodaj
          </Button>
          {props.isCSVshown && <Button
            basic
            color='blue'
            className='add-button'
            disabled={!props.isCSVenabled}
            onClick={props.addCSVhandler}>
            <Icon name='table'/>
            Zaimportuj CSV
          </Button>}
          {props.dropdownList.length !== 0 &&
            <Container className='line dropdown-line'>
              {props.dropdownList.map((element, index) =>
                  <Dropdown
                    index={index} key={index}
                    search
                    selection
                    disabled={!element.optionList.length}
                    placeholder={element.placeholder}
                    onChange={(event, { value }) => element.onChange(value)}
                    options={element.optionList}
                    value={element.value}
                  />
              )}
            </Container>
          }
          <Input
            icon='search'
            placeholder='Szukaj...'
            disabled={!props.isAddEnabled}
            onChange={event => props.searchHandler(event.target.value)} />
        </Grid.Row>
      </Grid>
  )
}

ListHeader.propTypes = {
  name: PropTypes.string.isRequired,
  searchHandler: PropTypes.func.isRequired,
  addHandler: PropTypes.func.isRequired,
  isAddEnabled: PropTypes.bool.isRequired,
  dropdownList: PropTypes.array.isRequired,
  isCSVshown: PropTypes.bool,
  isCSVenabled: PropTypes.bool,
  addCSVhandler: PropTypes.func
}

export default ListHeader