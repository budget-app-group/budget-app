import React from 'react'
import { Link } from 'react-router-dom'
import { Dropdown, Icon } from 'semantic-ui-react'
import { logout } from '../../store/thunk/auth'
import './TopBar.scss'

const TopBar = () => {
  return (
    <section className='top-bar-container'>
      <div>
        <Link to='/login'>
          <span className='page-title'>Money Watcher</span>
        </Link>
      </div>
      <div>
        <Dropdown
        direction='left'
        trigger={
          <span>
            <Icon name='user circle' />
            {` ${localStorage.getItem('user') && JSON.parse(localStorage.getItem('user')).name} ` +
              `${localStorage.getItem('user') && JSON.parse(localStorage.getItem('user')).surname}`}
          </span>
        }>
          <Dropdown.Menu>
            <Dropdown.Item text={'wyloguj'} icon={'power'} onClick={logout} as={Link} to='/' />
          </Dropdown.Menu>
        </Dropdown>
      </div>
    </section>
  )
}

export default TopBar