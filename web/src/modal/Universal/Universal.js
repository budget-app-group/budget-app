import React from 'react'
import PropTypes from 'prop-types'
import { Modal, Icon, Button } from 'semantic-ui-react'
import '../Universal/Universal.scss'

const UniversalModal = props => {
  return (
    <Modal open={props.isOpen} size='small'>
      <Modal.Header className='modal-header'>
        <Icon size='large' name={props.mode === 'leave' ? 'sign-out' : 'trash alternate outline'} className='modal-header-icon'/>
        <span className='modal-header-text'>{props.mode === 'leave' ? 'Opuść' : 'Usuń'}</span>
      </Modal.Header>
      <Modal.Content>
        {`Czy napewno chcesz ${props.mode === 'leave' ? 'opuścić "' : 'usuąć "'}${props.elementName}"?`}
      </Modal.Content>
      <Modal.Actions className='modal-actions'>
        <Button positive basic onClick={props.saveHandler}>
        <Icon name='save'/>
          {props.mode === 'leave' ? 'Opuść' : 'Usuń'}
        </Button>
        <Button negative basic onClick={props.cancelHandler}><Icon name='cancel'/>Anuluj</Button>
      </Modal.Actions>
    </Modal>
  )
}

UniversalModal.propTypes = {
  cancelHandler: PropTypes.func.isRequired,
  saveHandler: PropTypes.func.isRequired,
  mode: PropTypes.string.isRequired,
  isOpen: PropTypes.bool.isRequired,
  elementName: PropTypes.string.isRequired
}

export default UniversalModal
