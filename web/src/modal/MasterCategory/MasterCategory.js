import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Modal, Icon, Button, Input } from 'semantic-ui-react'
import { isEmpty, isPercent } from '../../common/validation'
import '../Universal/Universal.scss'

const startState = {
  name: '',
  nameError: false,
  budgetAllocation: 0,
  budgetAllocationError: false,
  color: '#919191', // by default gray color
  isOpen: false,
  isAdmin: true,
  isDataGathered: false
}

export default class MasterCategoryModal extends Component {
  constructor() {
    super()
    this.state = startState
  }

  static propTypes = {
    cancelHandler: PropTypes.func.isRequired,
    saveHandler: PropTypes.func.isRequired,
    mode: PropTypes.string.isRequired,
    isOpen: PropTypes.bool.isRequired,
    masterCategory: PropTypes.object,
    user: PropTypes.object.isRequired
  }

  static getDerivedStateFromProps (props, state) {
    let result = null

    if (!props.isOpen && state.isOpen) {
      
      return startState
    } else if (props.isOpen && !state.isOpen) {
      result = { isOpen: true }
    }

    if (props.isOpen && state.isOpen && props.mode === 'edit' && props.masterCategory && !state.isDataGathered) {
      if (result) {
        result.isDataGathered = true
        result.name = props.masterCategory.name
        result.budgetAllocation = props.masterCategory.budgetAllocation
        result.color = props.masterCategory.color
        result.isAdmin = props.masterCategory.budgetData.admin === props.user.email
      } else {
        result = {
          isDataGathered: true,
          name: props.masterCategory.name,
          budgetAllocation: props.masterCategory.budgetAllocation,
          color: props.masterCategory.color,
          isAdmin: props.masterCategory.budgetData.admin === props.user.email
        }
      }
    }

    return result
  }

  changeHandler = (stateName = '', value) => {
    this.setState({
      [stateName]: value
    }, () => {
      stateName !== 'color' && this.validateHandler(stateName, value)
    })
  }

  validateHandler = (stateName = '', value) =>
    this.setState({
      [`${stateName}Error`]: stateName === 'name'
        ? isEmpty(value)
        : !isPercent(value)
    })

  saveHandler = () => {
    this.validateHandler('name', this.state.name)
    this.validateHandler('budgetAllocation', this.state.budgetAllocation)

    if (!isEmpty(this.state.name) && isPercent(this.state.budgetAllocation)) {
      this.props.saveHandler(
        this.props.mode,
        {
          name: this.state.name,
          color: this.state.color,
          budgetAllocation: Number(this.state.budgetAllocation),
        },
        this.props.mode === 'add' ? -1 : this.props.masterCategory.id
      )
    }
  }

  closeHandler = () => {
    this.setState(startState, this.props.cancelHandler)
  }


  render() {
    return (
      <Modal open={this.props.isOpen} size='small'>
        <Modal.Header className='modal-header'>
          <Icon size='large' name='folder'/>
          <span className='modal-header-text'>Kategoria główna</span>
        </Modal.Header>
        <Modal.Content>
          <section className='section'>
            <span className={`section-header ${this.state.nameError ? 'error-color' : ''}`}>Nazwa kategorii</span>
            <Input
              fluid
              disabled={!this.state.isAdmin}
              error={this.state.nameError}
              onBlur={() => this.validateHandler('name', this.state.name)}
              onChange={event => this.changeHandler('name', event.target.value)}
              placeholder='Wpisz nazwę kategorii'
              value={this.state.name}
            />
          </section>
          <section className='section'>
            <span className='section-header'>Kolor kategorii</span>
            <Input
              transparent
              disabled={!this.state.isAdmin}
              className='input-color'
              fluid
              type='color'
              onChange={event => this.changeHandler('color', event.target.value)}
              placeholder='Wybierz kolor kategorii'
              value={this.state.color}
            />
          </section>
          <section className='section'>
            <span className={`section-header ${this.state.budgetAllocationError ? 'error-color' : ''}`}>Docelowy % w budżecie</span>
            <Input
              fluid
              disabled={!this.state.isAdmin}
              icon='percent'
              iconPosition='left'
              error={this.state.budgetAllocationError}
              onBlur={() => this.validateHandler('budgetAllocation', this.state.budgetAllocation)}
              onChange={event => this.changeHandler('budgetAllocation', event.target.value)}
              placeholder='Podaj docelowy % obciąenia w budecie'
              value={this.state.budgetAllocation}
            />
          </section>
        </Modal.Content>
        <Modal.Actions className='modal-actions'>
          <Button positive basic onClick={this.saveHandler} disabled={!this.state.isAdmin}><Icon name='save'/>Zapisz</Button>
          <Button negative basic onClick={this.closeHandler}><Icon name='cancel'/>Anuluj</Button>
        </Modal.Actions>
      </Modal>
    )
  }
}
