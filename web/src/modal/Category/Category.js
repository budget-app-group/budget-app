import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Modal, Icon, Button, Input } from 'semantic-ui-react'
import { isEmpty } from '../../common/validation'
import '../Universal/Universal.scss'

const startState = {
  name: '',
  nameError: false,
  color: '#919191', // by default gray color
  isOpen: false,
  isAdmin: true,
  isDataGathered: false
}

export default class CategoryModal extends Component {
  constructor() {
    super()
    this.state = startState
  }

  static propTypes = {
    cancelHandler: PropTypes.func.isRequired,
    saveHandler: PropTypes.func.isRequired,
    mode: PropTypes.string.isRequired,
    isOpen: PropTypes.bool.isRequired,
    category: PropTypes.object,
    user: PropTypes.object.isRequired
  }

  static getDerivedStateFromProps (props, state) {
    let result = null

    if (!props.isOpen && state.isOpen) {
      
      return startState
    } else if (props.isOpen && !state.isOpen) {
      result = { isOpen: true }
    }

    if (props.isOpen && state.isOpen && props.mode === 'edit' && props.category && !state.isDataGathered) {
      if (result) {
        result.isDataGathered = true
        result.name = props.category.name
        result.color = props.category.color
        result.isAdmin = props.category.budgetData.admin === props.user.email
      } else {
        result = {
          isDataGathered: true,
          name: props.category.name,
          color: props.category.color,
          isAdmin: props.category.budgetData.admin === props.user.email
        }
      }
    }

    return result
  }

  changeHandler = (stateName = '', value) => {
    this.setState({
      [stateName]: value
    }, () => {
      stateName !== 'color' && this.validateHandler(stateName, value)
    })
  }

  validateHandler = (stateName = '', value) =>
    this.setState({
      [`${stateName}Error`]: isEmpty(value)
    })

  saveHandler = () => {
    this.validateHandler('name', this.state.name)

    if (!isEmpty(this.state.name)) {
      this.props.saveHandler(
        this.props.mode,
        {
          name: this.state.name,
          color: this.state.color
        },
        this.props.mode === 'add' ? -1 : this.props.category.id
      )
    }
  }

  closeHandler = () => {
    this.setState(startState, this.props.cancelHandler)
  }

  render() {
    return (
      <Modal open={this.props.isOpen} size='small'>
        <Modal.Header className='modal-header'>
          <Icon size='large' name='folder open'/>
          <span className='modal-header-text'>Kategoria</span>
        </Modal.Header>
        <Modal.Content>
          <section className='section'>
            <span className={`section-header ${this.state.nameError ? 'error-color' : ''}`}>Nazwa kategorii</span>
            <Input
              fluid
              disabled={!this.state.isAdmin}
              error={this.state.nameError}
              onBlur={() => this.validateHandler('name', this.state.name)}
              onChange={event => this.changeHandler('name', event.target.value)}
              placeholder='Wpisz nazwę kategorii'
              value={this.state.name}
            />
          </section>
          <section className='section'>
            <span className='section-header'>Kolor kategorii</span>
            <Input
              transparent
              fluid
              disabled={!this.state.isAdmin}
              type='color'
              onChange={event => this.changeHandler('color', event.target.value)}
              placeholder='Wybierz kolor kategorii'
              value={this.state.color}
            />
          </section>
        </Modal.Content>
        <Modal.Actions className='modal-actions'>
          <Button positive basic onClick={this.saveHandler} disabled={!this.state.isAdmin}><Icon name='save'/>Zapisz</Button>
          <Button negative basic onClick={this.closeHandler}><Icon name='cancel'/>Anuluj</Button>
        </Modal.Actions>
      </Modal>
    )
  }
}
