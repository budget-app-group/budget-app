import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Modal, Icon, Button, Input, Table, Dropdown, Loader, Dimmer } from 'semantic-ui-react'
import { getExpenseListFromCSV } from '../../store/thunk/expense'
import { isEmpty } from '../../common/validation'
import { formDate } from '../../common/dates'
import '../Universal/Universal.scss'
import './CSV.scss'

const startState = {
  file: '',
  expenseList: [],
  isLoaderActive: false,
  isOpen: false
}

export default class CSVmodal extends Component {
  constructor() {
    super()
    this.state = startState
  }

  static propTypes = {
    cancelHandler: PropTypes.func.isRequired,
    saveHandler: PropTypes.func.isRequired,
    isOpen: PropTypes.bool.isRequired,
    token: PropTypes.string.isRequired,
    budgetId: PropTypes.number.isRequired,
    bankAccountId: PropTypes.number.isRequired,
    masterCategoryId: PropTypes.number.isRequired,
    categoryId: PropTypes.number.isRequired,
    budgetList: PropTypes.array.isRequired,
    bankAccountList: PropTypes.array.isRequired,
    masterCategoryList: PropTypes.array.isRequired,
    categoryList: PropTypes.array.isRequired
  }

  static getDerivedStateFromProps (props, state) {
    let result = null

    if (!props.isOpen && state.isOpen) {
      
      return startState
    } else if (props.isOpen && !state.isOpen) {
      result = { isOpen: true }
    }

    return result
  }

  changeHandler = (stateName = '', value) => {
    this.setState({
      [stateName]: value
    })
  }

  tableChangeHandler = (index = -1, property = '', value) => {
    let copy = this.state.expenseList
    copy[index][property] = value
    this.setState({ expenseList: copy })
  }

  saveHandler = () => {
    if (!isEmpty(this.state.file)) {
      let result = this.state.expenseList.map(expense => {
        expense.amount = expense.income ? expense.amount : -expense.amount
        expense.expenseDate = (new Date(expense.expenseDate)).getTime()
        return expense
      })
      this.props.saveHandler(result)
    }
  }

  render() {
    return (
      <Modal open={this.state.isOpen} size='large'>
        <Modal.Header className='modal-header'>
          <Icon size='large' name='table' className='modal-header-icon'/>
          <span className='modal-header-text'>Zaimportuj plik CSV</span>
        </Modal.Header>
        <Modal.Content>
          <section className='section'>
            <span className={`section-header`}>Nazwa kategorii</span>
            <Input
              fluid
              type='file'
              accept='.csv'
              onChange={event => {
                let file = event.target.files[0]
                this.changeHandler('file', event.target.value)
                this.setState({ isLoaderActive: true }, async () => {
                  let x = await getExpenseListFromCSV(this.props.budgetId, this.props.bankAccountId, file, this.props.token)
                  for (let i = 0; i < x.length; i++) {
                    x[i].expenseDate = formDate(x[i].expenseDate)
                    x[i].amount = x[i].income ? x[i].amount : -x[i].amount
                  }
                  this.setState({ expenseList: x, isLoaderActive: false })
                })
              }}
              placeholder='Wpisz nazwę kategorii'
              value={this.state.file}
            />
          </section>
          <section className='table-section'>
            <Dimmer active={this.state.isLoaderActive}><Loader/></Dimmer>
            <Table>
              <Table.Header>
                <Table.Row>
                  <Table.HeaderCell key='0'>Id</Table.HeaderCell>
                  <Table.HeaderCell key='1'>Nazwa</Table.HeaderCell>
                  <Table.HeaderCell key='2'>Kwota</Table.HeaderCell>
                  <Table.HeaderCell key='3'>Data</Table.HeaderCell>
                  <Table.HeaderCell key='4'>Sklep</Table.HeaderCell>
                  <Table.HeaderCell key='6'>Budżet</Table.HeaderCell>
                  <Table.HeaderCell key='7'>Konto bankowe</Table.HeaderCell>
                  <Table.HeaderCell key='8'>Kategoria główna</Table.HeaderCell>
                  <Table.HeaderCell key='9'>Kategoria</Table.HeaderCell>
                </Table.Row>
              </Table.Header>
              <Table.Body>
                {this.state.expenseList.map((expense, index) => {
                  return <Table.Row key={index}>
                    <Table.Cell key='0'>{index + 1}</Table.Cell>
                    <Table.Cell key='1'>
                      <Input
                        placeholder='Nazwa wydatku'
                        onChange={event => this.tableChangeHandler(index, 'name', event.target.value)}
                        value={expense.name}
                      />
                    </Table.Cell>
                    <Table.Cell key='2'>
                      <Input
                        placeholder='Kwota'
                        onChange={event => this.tableChangeHandler(index, 'amount', event.target.value)}
                        value={expense.amount}
                      />
                    </Table.Cell>
                    <Table.Cell key='3'>
                      <Input
                        type='date'
                        onChange={(event, { value }) => this.tableChangeHandler(index, 'expenseDate', value)}
                        placeholder='Wybierz datę'
                        value={expense.expenseDate}
                      />
                    </Table.Cell>
                    <Table.Cell key='4'>
                      <Input
                        value={expense.shopName || ''}
                        onChange={event => this.tableChangeHandler(index, 'shopName', event.target.value)}
                        placeholder='Wpisz nazwę sklepu'
                      />
                    </Table.Cell>
                    <Table.Cell key='5'>
                      <Dropdown
                        search
                        selection
                        placeholder='Wybierz budżet'
                        disabled
                        options={this.props.budgetList}
                        value={this.props.budgetId}
                      />
                    </Table.Cell>
                    <Table.Cell key='6'>
                      <Dropdown
                        search
                        selection
                        placeholder='Wybierz konto bankowe'
                        onChange={(event, { value }) => this.tableChangeHandler(index, 'bankAccountId', value)}
                        options={this.props.bankAccountList}
                        value={expense.bankAccountId}
                      />
                    </Table.Cell>
                    <Table.Cell key='7'>
                      <Dropdown
                        search
                        selection
                        placeholder='Wybierz kategorię główną'
                        onChange={(event, { value }) => this.tableChangeHandler(index, 'masterCatId', value)}
                        options={this.props.masterCategoryList}
                        value={expense.masterCatId}
                      />
                    </Table.Cell>
                    <Table.Cell key='8'>
                      <Dropdown
                        search
                        selection
                        placeholder='Wybierz kategorię'
                        onChange={(event, { value }) => this.tableChangeHandler(index, 'categoryId', value)}
                        options={this.props.categoryList}
                        value={expense.categoryId}
                      />
                    </Table.Cell>
                  </Table.Row>
                })}
              </Table.Body>
            </Table>
          </section>
        </Modal.Content>
        <Modal.Actions className='modal-actions'>
          <Button positive basic onClick={this.saveHandler} disabled={!this.state.expenseList.length}>
           <Icon name='save'/>Zapisz
          </Button>
          <Button negative basic onClick={this.props.cancelHandler}><Icon name='cancel'/>Anuluj</Button>
        </Modal.Actions>
      </Modal>
    )
  }
}
