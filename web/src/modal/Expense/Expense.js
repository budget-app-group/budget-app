import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Modal, Icon, Button, Input, Dropdown } from 'semantic-ui-react'
import { isEmpty, floatRegex } from '../../common/validation'
import { formDate } from '../../common/dates'
import '../Universal/Universal.scss'

const startState = {
  name: '',
  amount: 0,
  amountError: false,
  expenseDate: formDate(),
  shopName: '',
  bankAccountId: '',
  bankAccountIdError: false,
  isOpen: false,
  isAdmin: true,
  isDataGathered: false
}

export default class ExpenseModal extends Component {
  constructor() {
    super()
    this.state = startState
  }

  static propTypes = {
    cancelHandler: PropTypes.func.isRequired,
    saveHandler: PropTypes.func.isRequired,
    mode: PropTypes.string.isRequired,
    isOpen: PropTypes.bool.isRequired,
    expense: PropTypes.object,
    categoryId: PropTypes.number,
    masterCategoryId: PropTypes.number,
    bankAccountList: PropTypes.array.isRequired,
    user: PropTypes.object.isRequired
  }

  static getDerivedStateFromProps (props, state) {
    let result = null

    if (!props.isOpen && state.isOpen) {
      
      return startState
    } else if (props.isOpen && !state.isOpen) {
      result = { isOpen: true }
    }

    if (props.isOpen && state.isOpen && props.mode === 'edit' && props.expense && !state.isDataGathered) {
      if (result) {
        result.isDataGathered = true
        result.name = props.expense.name || ''
        result.amount = props.expense.amount
        result.expenseDate = formDate(props.expense.expenseDate)
        result.shopName = props.expense.shopName || ''
        result.bankAccountId = props.expense.bankAccount.id
        result.isAdmin = props.expense.expenseOwner.email === props.user.email
      } else {
        result = {
          name: props.expense.name || '',
          amount: props.expense.amount,
          expenseDate: formDate(props.expense.expenseDate),
          shopName: props.expense.shopName || '',
          bankAccountId: props.expense.bankAccount.id,
          isDataGathered: true,
          isAdmin: props.expense.expenseOwner.email === props.user.email
        }
      }
    }

    return result
  }

  changeHandler = (stateName = '', value) => {
    this.setState({
      [stateName]: value
    }, () => {
      (stateName === 'amount' || stateName === 'bankAccountId') &&
        this.validateHandler(stateName, value)
    })
  }

  validateHandler = (stateName = '', value) =>
    this.setState({
      [`${stateName}Error`]: stateName === 'amount'
        ? !floatRegex.test(value)
        : isEmpty(value)
    })

  saveHandler = () => {
    this.validateHandler('amount', this.state.name)
    this.validateHandler('bankAccountId', this.state.bankAccountId)

    if (floatRegex.test(this.state.amount) && !isEmpty(this.state.bankAccountId)) {
      this.props.saveHandler(
        this.props.mode, this.props.mode === 'edit' ? {
          name: this.state.name || null,
          amount: Number(this.state.amount),
          expenseDate: (new Date(this.state.expenseDate)).getTime(),
          shopName: this.state.shopName || null,
          categoryId: this.props.categoryId,
          mastCatId: this.props.masterCategoryId,
          bankAccountId: this.state.bankAccountId,
          income: false
        } : {
          name: this.state.name || null,
          amount: Number(this.state.amount),
          expenseDate: (new Date(this.state.expenseDate)).getTime(),
          shopName: this.state.shopName || null,
          bankAccountId: this.state.bankAccountId,
          income: false
        }, this.props.mode === 'add' ? -1 : this.props.expense.id
      )
    }
  }

  render() {
    return (
      <Modal open={this.props.isOpen} size='small'>
        <Modal.Header className='modal-header'>
          <Icon size='large' name='money'/>
          <span className='modal-header-text'>Wydatek</span>
        </Modal.Header>
        <Modal.Content>

          <section className='section'>
            <span className='section-header'>Nazwa wydatku</span>
            <Input
              fluid
              disabled={!this.state.isAdmin}
              onChange={event => this.changeHandler('name', event.target.value)}
              placeholder='Wpisz nazwę wydatku'
              value={this.state.name}
            />
          </section>

          <section className='section'>
            <span className={`section-header ${this.state.amountError ? 'error-color' : ''}`}>Kwota</span>
            <Input
              fluid
              disabled={!this.state.isAdmin}
              error={this.state.amountError}
              onBlur={() => this.validateHandler('amount', this.state.amount)}
              onChange={event => this.changeHandler('amount', event.target.value)}
              placeholder='Podaj kwotę'
              value={this.state.amount}
            />
          </section>

          <section className='section'>
            <span className='section-header'>Data</span>
            <Input
              fluid
              disabled={!this.state.isAdmin}
              type='date'
              onChange={event => this.changeHandler('expenseDate', event.target.value)}
              placeholder='Wybierz datę'
              value={this.state.expenseDate}
            />
          </section>

          <section className='section'>
            <span className='section-header'>Nazwa sklepu</span>
            <Input
              fluid
              disabled={!this.state.isAdmin}
              onChange={event => this.changeHandler('shopName', event.target.value)}
              placeholder='Wpisz nazwę sklepu'
              value={this.state.shopName}
            />
          </section>

          <section className='section'>
            <span className={`section-header ${this.state.bankAccountIdError ? 'error-color' : ''}`}>Konto bankowe</span>
            <Dropdown
              fluid
              search
              selection
              disabled={!this.state.isAdmin}
              placeholder='Wybierz konto bankowe'
              error={this.state.bankAccountIdError}
              onChange={(event, { value }) => this.changeHandler('bankAccountId', value)}
              onBlur={() => this.validateHandler('bankAccountId', this.state.bankAccountId)}
              options={this.props.bankAccountList}
              value={this.state.bankAccountId}
            />
          </section>

        </Modal.Content>
        <Modal.Actions className='modal-actions'>
          <Button positive basic onClick={this.saveHandler} disabled={!this.state.isAdmin}><Icon name='save'/>Zapisz</Button>
          <Button negative basic onClick={this.props.cancelHandler}><Icon name='cancel'/>Anuluj</Button>
        </Modal.Actions>
      </Modal>
    )
  }
}
