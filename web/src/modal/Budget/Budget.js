import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Modal, Icon, Button, Input, Dropdown, Table } from 'semantic-ui-react'
import { isEmpty } from '../../common/validation'
import { deleteUserFromBudget, inviteToBudget } from '../../store/thunk/budget'
import '../Universal/Universal.scss'

export default class BudgetModal extends Component {
  constructor() {
    super()
    this.state = {
      name: '',
      nameError: false,
      admin: '',
      userList: [],
      email: '',
      emailError: false,
      isOpen: false,
      isAdmin: false,
      isDataGathered: false
    }
  }

  static propTypes = {
    cancelHandler: PropTypes.func.isRequired,
    saveHandler: PropTypes.func.isRequired,
    mode: PropTypes.string.isRequired,
    isOpen: PropTypes.bool.isRequired,
    budget: PropTypes.object,
    user: PropTypes.object.isRequired,
    token: PropTypes.string
  }

  static getDerivedStateFromProps (props, state) {
    let result = null

    if (!props.isOpen && state.isOpen) {
      
      return {
        name: '',
        nameError: false,
        admin: '',
        email: '',
        userList: [],
        isOpen: false,
        isAdmin: false,
        isDataGathered: false
      }
    } else if (props.isOpen && !state.isOpen) {
      result = { isOpen: true }
    }

    if (props.isOpen && state.isOpen && props.mode === 'edit' && props.budget && !state.isDataGathered) {
      if (result) {
        result.isDataGathered = true
        result.name = props.budget.name
        result.admin = props.budget.admin.email
        result.userList = props.budget.users.map.map(user => {
          return {
            index: user.email,
            id: user.id,
            text: `${user.name} ${user.surname}`,
            value: user.email
          }
        })
      } else {
        result = {
          isDataGathered: true,
          name: props.budget.name,
          admin: props.budget.admin.email,
          isAdmin: props.budget.admin.email === props.user.email,
          userList: props.budget.users.map(user => {
            return {
              index: user.email,
              id: user.id,
              text: `${user.name} ${user.surname}`,
              value: user.email
            }
          })
        }
      }
    }

    return result
  }

  changeHandler = (stateName = '', value) => {
    this.setState({
      [stateName]: value
    }, () => {
      stateName === 'name' && this.validateHandler(stateName, value)
    })
  }

  validateHandler = (stateName = '', value) => this.setState({ [`${stateName}Error`]: isEmpty(value) })

  saveHandler = () => {
    this.validateHandler('name', this.state.name)

    if (!this.state.nameError) {
      this.props.saveHandler(
        this.props.mode,
        this.props.mode === 'add'
          ? { name: this.state.name, admin: this.props.user.email }
          : { name: this.state.name, admin: this.state.admin },
        this.props.mode === 'add' ? -1 : this.props.budget.id
      )
    }
  }

  closeHandler = () => {
    this.setState({
      name: '',
      nameError: false,
      admin: '',
      email: '',
      userList: [],
      isAdmin: false,
      isDataGathered: false
    }, this.props.cancelHandler)
  }

  sendInvite = async () => {
    // #TODO toasts + validation
    let response = await inviteToBudget(this.props.budget.id, this.state.email, this.props.token)
    response && this.setState({
      email: ''
    })
  }

  deleteUser = async (userId = -1, userEmail = '') => {
    // #TODO toasts + validation
    let response = await deleteUserFromBudget(this.props.budget.id, userId, this.props.token)
    response && this.setState({
      userList: this.state.userList.filter(user => user.value !== userEmail)
    })
  }

  render() {
    return (
      <Modal open={this.props.isOpen} size='small'>
        <Modal.Header className='modal-header'>
          <Icon size='large' name='calculator'/>
          <span className='modal-header-text'>Budżet</span>
        </Modal.Header>
        <Modal.Content>
          <section className='section'>
            <span className={`section-header ${this.state.nameError ? 'error-color' : ''}`}>Nazwa</span>
            <Input
              fluid
              disabled={this.props.mode === 'edit' && !this.state.isAdmin}
              error={this.state.nameError}
              onBlur={() => this.validateHandler('name', this.state.name)}
              onChange={event => this.changeHandler('name', event.target.value)}
              placeholder='Wpisz nazwę'
              value={this.state.name}
            />
          </section>
          {this.props.mode === 'edit' &&
            <section className='edit-section'>
              <section className='section'>
                <span className='section-header'>Administrator</span>
                <Dropdown
                  fluid
                  search
                  selection
                  disabled={!this.state.isAdmin}
                  onChange={(event, { value }) => this.changeHandler('admin', value)}
                  options={this.state.userList}
                  value={this.state.admin}
                />
              </section>
              <section className='section'>
                <span className='section-header'>Wyślij zaproszenie do budżetu</span><br />
                <Input
                  error={this.state.emailError}
                  onBlur={() => this.validateHandler('email', this.state.name)}
                  onChange={event => this.changeHandler('email', event.target.value)}
                  placeholder='Wpisz email'
                  disabled={!this.state.isAdmin}
                  value={this.state.email}
                />
                <Button positive basic onClick={this.sendInvite} disabled={!this.state.isAdmin}>
                <Icon name='send'/>
                  Wyślij
                </Button>
              </section>
              <Table celled className='section'>
                <Table.Header>
                  <Table.Row>
                    <Table.HeaderCell>Imię i nazwisko</Table.HeaderCell>
                    <Table.HeaderCell>Usuń</Table.HeaderCell>
                  </Table.Row>
                </Table.Header>
                <Table.Body>
                  {this.state.userList.map((user, index) => {
                    return (
                      <Table.Row key={`${index}`}>
                        <Table.Cell>{user.text}</Table.Cell>
                        <Table.Cell>
                          <Button
                            basic
                            onClick={this.deleteUser.bind(this, user.id, user.value)}
                            disabled={!this.state.isAdmin || (user.id === this.props.budget.admin.id)}>
                            <Icon name='trash alternate outline'/>
                            Usuń
                          </Button>
                        </Table.Cell>
                      </Table.Row>
                    )
                  })}
                </Table.Body>
              </Table>
            </section>
          }
        </Modal.Content>
        <Modal.Actions className='modal-actions'>
          <Button
            positive
            basic
            onClick={this.saveHandler}
            disabled={this.props.mode === 'edit' && !this.state.isAdmin}>
            <Icon name='save'/>
            Zapisz
          </Button>
          <Button negative basic onClick={this.closeHandler}><Icon name='cancel'/>Anuluj</Button>
        </Modal.Actions>
      </Modal>
    )
  }
}
