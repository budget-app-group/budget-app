import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Modal, Icon, Button, Input, Dropdown } from 'semantic-ui-react'
import { isEmpty, floatRegex } from '../../common/validation'
import { bankList } from '../../common/dictionary/enum'
import '../Universal/Universal.scss'

const startState = {
  name: '',
  nameError: false,
  bankName: '',
  bankNameError: false,
  type: '',
  typeError: false,
  startingAmount: 0,
  startingAmountError: false,
  isOpen: false,
  isAdmin: true,
  isDataGathered: false
}

export default class BankAccountModal extends Component {
  constructor() {
    super()
    this.state = startState
  }

  static propTypes = {
    cancelHandler: PropTypes.func.isRequired,
    saveHandler: PropTypes.func.isRequired,
    mode: PropTypes.string.isRequired,
    isOpen: PropTypes.bool.isRequired,
    bankAccount: PropTypes.object,
    user: PropTypes.object.isRequired
  }

  static getDerivedStateFromProps (props, state) {
    let result = null

    if (!props.isOpen && state.isOpen) {
      
      return startState
    } else if (props.isOpen && !state.isOpen) {
      result = { isOpen: true }
    }

    if (props.isOpen && state.isOpen && props.mode === 'edit' && props.bankAccount && !state.isDataGathered) {
      if (result) {
        result.isDataGathered = true
        result.name = props.bankAccount.name
        result.bankName = props.bankAccount.bankName
        result.type = props.bankAccount.type
        result.startingAmount = props.bankAccount.startingAmount
        result.isAdmin = props.bankAccount.budgetData.admin === props.user.email
      } else {
        result = {
          isDataGathered: true,
          name: props.bankAccount.name,
          bankName: props.bankAccount.bankName,
          type: props.bankAccount.type,
          startingAmount: props.bankAccount.startingAmount,
          isAdmin: props.bankAccount.budgetData.admin === props.user.email
        }
      }
    }

    return result
  }

  changeHandler = (stateName = '', value) => {
    this.setState({
      [stateName]: value
    }, () => {
      this.validateHandler(stateName, value)
    })
  }

  validateHandler = (stateName = '', value) =>
    this.setState({
      [`${stateName}Error`]: stateName === 'name' || stateName === 'bankName' || stateName === 'type'
        ? isEmpty(value)
        : !floatRegex.test(value)
    })

  saveHandler = () => {
    this.validateHandler('name', this.state.name)
    this.validateHandler('bankName', this.state.bankName)
    this.validateHandler('type', this.state.type)
    this.validateHandler('startingAmount', this.state.startingAmount)

    if (!isEmpty(this.state.name) && !isEmpty(this.state.bankName) &&
      !isEmpty(this.state.type) && floatRegex.test(this.state.startingAmount)) {
      this.props.saveHandler(
        this.props.mode,
        {
          name: this.state.name,
          bankName: this.state.bankName,
          type: this.state.type,
          startingAmount: Number(this.state.startingAmount),
        },
        this.props.mode === 'add' ? -1 : this.props.bankAccount.id
      )
    }
  }

  closeHandler = () => {
    this.setState(startState, this.props.cancelHandler)
  }

  render() {
    return (
      <Modal open={this.props.isOpen} size='small'>
        <Modal.Header className='modal-header'>
          <Icon size='large' name='credit card outline'/>
          <span className='modal-header-text'>Konto bankowe</span>
        </Modal.Header>
        <Modal.Content>
          <section className='section'>
            <span className={`section-header ${this.state.nameError ? 'error-color' : ''}`}>Nazwa konta</span>
            <Input
              fluid
              disabled={!this.state.isAdmin}
              error={this.state.nameError}
              onBlur={() => this.validateHandler('name', this.state.name)}
              onChange={event => this.changeHandler('name', event.target.value)}
              placeholder='Wpisz nazwę konta'
              value={this.state.name}
            />
          </section>
          <section className='section'>
            <span className={`section-header ${this.state.bankNameError ? 'error-color' : ''}`}>Nazwa banku</span>
            <Dropdown
              fluid
              search
              selection
              disabled={!this.state.isAdmin}
              placeholder='Wybierz nazwę banku'
              error={this.state.bankNameError}
              onChange={(event, { value }) => this.changeHandler('bankName', value)}
              onBlur={() => this.validateHandler('bankName', this.state.bankName)}
              options={bankList}
              value={this.state.bankName}
            />
          </section>
          <section className='section'>
            <span className={`section-header ${this.state.typeError ? 'error-color' : ''}`}>Typ konta</span>
            <Input
              fluid
              disabled={!this.state.isAdmin}
              error={this.state.typeError}
              onBlur={() => this.validateHandler('type', this.state.type)}
              onChange={event => this.changeHandler('type', event.target.value)}
              placeholder='Wpisz typ konta'
              value={this.state.type}
            />
          </section>
          <section className='section'>
            <span className={`section-header ${this.state.startingAmountError ? 'error-color' : ''}`}>Kwota początkowa</span>
            <Input
              fluid
              disabled={!this.state.isAdmin}
              error={this.state.startingAmountError}
              onBlur={() => this.validateHandler('startingAmount', this.state.startingAmount)}
              onChange={event => this.changeHandler('startingAmount', event.target.value)}
              placeholder='Podaj kwotę początkową'
              value={this.state.startingAmount}
            />
          </section>
        </Modal.Content>
        <Modal.Actions className='modal-actions'>
          <Button positive basic onClick={this.saveHandler} disabled={!this.state.isAdmin}><Icon name='save'/>Zapisz</Button>
          <Button negative basic onClick={this.closeHandler}><Icon name='cancel'/>Anuluj</Button>
        </Modal.Actions>
      </Modal>
    )
  }
}
