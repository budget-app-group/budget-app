import { combineReducers } from 'redux'

import authReducer from './thunk/auth'
import bankAccountReducer from './thunk/bankAccount'
import budgetReducer from './thunk/budget'
import categoryReducer from './thunk/category'
import expenseReducer from './thunk/expense'
import masterCategoryReducer from './thunk/masterCategory'

const makeRootReducer = asyncReducers => {
  const appReducer = combineReducers({
    auth: authReducer,
    bankAccount: bankAccountReducer,
    budget: budgetReducer,
    category: categoryReducer,
    masterCategory: masterCategoryReducer,
    expense: expenseReducer,
    ...asyncReducers
  })
  return (state, action) => {
    return appReducer(state, action)
  }
}

export const injectReducer = (store, { key, reducer }) => {
  if (Object.hasOwnProperty.call(store.asyncReducers, key)) return

  store.asyncReducers[key] = reducer
  store.replaceReducer(makeRootReducer(store.asyncReducers))
}

export default makeRootReducer