// all store
export const RESET_STORE = 'RESET_STORE'

// auth
export const SET_TOKEN = 'SET_TOKEN'
export const SET_USER = 'SET_USER'

// budget
export const SET_BUDGET_LIST = 'SET_BUDGET_LIST'
export const SET_BUDGET = 'SET_BUDGET'
export const SET_BUDGET_ID = 'SET_BUDGET_ID'
export const ADD_BUDGET = 'ADD_BUDGET'
export const UPDATE_BUDGET = 'UPDATE_BUDGET'
export const DELETE_BUDGET = 'DELETE_BUDGET'
export const LEAVE_BUDGET = 'LEAVE_BUDGET'

// bankAccount
export const SET_BANK_ACCOUNT_LIST = 'SET_BANK_ACCOUNT_LIST'
export const SET_BANK_ACCOUNT = 'SET_BANK_ACCOUNT'
export const SET_BANK_ACCOUNT_ID = 'SET_BANK_ACCOUNT_ID'

// masterCategory
export const SET_MASTER_CATEGORY_LIST = 'SET_MASTER_CATEGORY_LIST'
export const SET_MASTER_CATEGORY = 'SET_MASTER_CATEGORY'
export const SET_MASTER_CATEGORY_ID = 'SET_MASTER_CATEGORY_ID'

// category
export const SET_CATEGORY_LIST = 'SET_CATEGORY_LIST'
export const SET_CATEGORY = 'SET_CATEGORY'
export const SET_CATEGORY_ID = 'SET_CATEGORY_ID'

// expense
export const SET_EXPENSE_LIST = 'SET_EXPENSE_LIST'
export const SET_BUDGET_EXPENSE_LIST = 'SET_BUDGET_EXPENSE_LIST'
export const SET_EXPENSE = 'SET_EXPENSE'
export const SET_EXPENSE_ID = 'SET_EXPENSE_ID'
