import makeRootReducer from './reducerList'
import { applyMiddleware, compose, createStore as createReduxStore } from 'redux'
import thunk from 'redux-thunk'


const createStore = () => {
  const middleware = [thunk]
  const enhancers = []
  let composeEnhancers = compose

  if (process.env.NODE_ENV === 'development') {
    if (typeof window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ === 'function') {
      composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    }
  }
  
  const store = createReduxStore(
    makeRootReducer(),
    composeEnhancers(
      applyMiddleware(...middleware),
      ...enhancers
    )
  )
  store.asyncReducers = {}
  

  if (module.hot) {
    module.hot.accept('./reducerList', () => {
      const reducers = require('./reducerList').default
      store.replaceReducer(reducers(store.asyncReducers))
    })
  }

  return store
}

export default createStore