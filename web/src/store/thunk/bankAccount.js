import {
  SET_BANK_ACCOUNT_LIST,
  SET_BANK_ACCOUNT,
  SET_BANK_ACCOUNT_ID
} from '../actionList'
import { errorHandler, protocol, origin, getHeaderList } from '../../common/auth'
import axios from 'axios'
import { notify } from '../../screen'

export const setBankAccountListAction = (bankAccountList = []) => {
  return {
    type: SET_BANK_ACCOUNT_LIST,
    data: bankAccountList
  }
}

export const setBankAccountAction = (bankAccount = null) => {
  return {
    type: SET_BANK_ACCOUNT,
    data: bankAccount
  }
}

export const setBankAccountIdAction = (bankAccountId = -1) => {
  return {
    type: SET_BANK_ACCOUNT_ID,
    data: bankAccountId
  }
}

export const setBankAccountList = (budgetId = -1, token = '') => {
  return dispatch => {
    return new Promise((resolve, reject) => {
      axios.get(`${protocol}://${origin}/api/budget/${budgetId}/bankaccount`, getHeaderList(token))
      .then(response => {
        dispatch(setBankAccountListAction(response.data))
        resolve()
      })
      .catch(error => {
        errorHandler(error)
        reject(error)
      })
    })
  }
}

export const setBankAccount = (budgetId = -1, bankAccountId = -1, token = '') => {
  return dispatch => {
    return new Promise((resolve, reject) => {
      axios.get(`${protocol}://${origin}/api/budget/${budgetId}/bankaccount/${bankAccountId}`, getHeaderList(token))
      .then(response => {
        dispatch(setBankAccountAction(response.data))
        resolve()
      })
      .catch(error => {
        errorHandler(error)
        reject(error)
      })
    })
  }
}

export const setBankAccountId = (bankAccountId = -1) => {
  return dispatch => {
    dispatch(setBankAccountIdAction(bankAccountId))
  }
}

export const addBankAccount = async (budgetId = -1, data = null, token = '') => {
  try {
    let response = await axios.post(`${protocol}://${origin}/api/budget/${budgetId}/bankaccount`, data, getHeaderList(token))
    response.data.id && setBankAccountId(response.data.id)
    notify('Dodano konto bankowe', true)
    return true
  } catch (error) {
    notify('Błąd dodawania', false)
    errorHandler(error)
    return false
  }
}

export const editBankAccount = async (budgetId = -1, bankAccountId = -1, data = null, token = '') => {
  try {
    await axios.put(`${protocol}://${origin}/api/budget/${budgetId}/bankaccount/${bankAccountId}`, data, getHeaderList(token))
    notify('Zmiany konta bankowego zapisano', true)
    return true
  } catch (error) {
    notify('Błąd zapisu', false)
    errorHandler(error)
    return false
  }
}

export const deleteBankAccount = async (budgetId = -1, bankAccountId = -1, token = '') => {
  try {
    await axios.delete(`${protocol}://${origin}/api/budget/${budgetId}/bankaccount/${bankAccountId}`, getHeaderList(token))
    notify('Konto bankowe usunięto', true)
    return true
  } catch (error) {
    notify('Błąd usuwania', false)
    errorHandler(error)
    return false
  }
}

export const actions = {
  setBankAccountList,
  setBankAccount,
  setBankAccountId
}

const ACTION_HANDLERS = {
  [SET_BANK_ACCOUNT_LIST]: (state, action) => {
    return {
      ...state,
      bankAccountList: action.data
    }
  },
  [SET_BANK_ACCOUNT]: (state, action) => {
    return {
      ...state,
      bankAccount: action.data
    }
  },
  [SET_BANK_ACCOUNT_ID]: (state, action) => {
    return {
      ...state,
      bankAccountId: action.data
    }
  }
}

const initialState = {
  bankAccountList: [],
  bankAccount: null,
  bankAccountId: -1
}

export default function bankAccountReducer (state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}
