import {
  SET_CATEGORY_LIST,
  SET_CATEGORY,
  SET_CATEGORY_ID
} from '../actionList'
import { errorHandler, protocol, origin, getHeaderList } from '../../common/auth'
import axios from 'axios'
import { notify } from '../../screen'

export const setCategoryListAction = (categoryList = []) => {
  return {
    type: SET_CATEGORY_LIST,
    data: categoryList
  }
}

export const setCategoryAction = (category = null) => {
  return {
    type: SET_CATEGORY,
    data: category
  }
}

export const setCategoryIdAction = (categoryId = -1) => {
  return {
    type: SET_CATEGORY_ID,
    data: categoryId
  }
}

export const setCategoryList = (budgetId = -1, masterCategoryId = -1, token = '') => {
  return dispatch => {
    return new Promise((resolve, reject) => {
      axios.get(`${protocol}://${origin}/api/budget/${budgetId}/master/${masterCategoryId}/category`, getHeaderList(token))
      .then(response => {
        dispatch(setCategoryListAction(response.data))
        resolve()
      })
      .catch(error => {
        errorHandler(error)
        reject(error)
      })
    })
  }
}

export const setCategory = (budgetId = -1, masterCategoryId = -1, categoryId = -1, token = '') => {
  return dispatch => {
    return new Promise((resolve, reject) => {
      axios.get(`${protocol}://${origin}/api/budget/${budgetId}/master/${masterCategoryId}/category/${categoryId}`, getHeaderList(token))
      .then(response => {
        dispatch(setCategoryAction(response.data))
        resolve()
      })
      .catch(error => {
        errorHandler(error)
        reject(error)
      })
    })
  }
}

export const setCategoryId = (categoryId = -1) => {
  return dispatch => {
    dispatch(setCategoryIdAction(categoryId))
  }
}

export const addCategory = async (budgetId = -1, masterCategoryId = -1, data = null, token = '') => {
  try {
    let response = await axios.post(`${protocol}://${origin}/api/budget/${budgetId}/master/${masterCategoryId}/category`, data, getHeaderList(token))
    response.data.id && setCategoryId(response.data.id)
    notify('Kategoria dodana', true)
    return true
  } catch (error) {
    notify('Błąd dodawania kategorii', false)
    errorHandler(error)
    return false
  }
}

export const editCategory = async (budgetId = -1, masterCategoryId = -1, categoryId = -1, data = null, token = '') => {
  try {
    await axios.put(`${protocol}://${origin}/api/budget/${budgetId}/master/${masterCategoryId}/category/${categoryId}`, data, getHeaderList(token))
    notify('Zmiany kategorii zapisano', true)
    return true
  } catch (error) {
    notify('Błąd zapisu', false)
    errorHandler(error)
    return false
  }
}

export const deleteCategory = async (budgetId = -1, masterCategoryId = -1, categoryId = -1, token = '') => {
  try {
    await axios.delete(`${protocol}://${origin}/api/budget/${budgetId}/master/${masterCategoryId}/category/${categoryId}`, getHeaderList(token))
    notify('Kategorię usunięto', true)
    return true
  } catch (error) {
    notify('Błąd usuwania', false)
    errorHandler(error)
    return false
  }
}

export const actions = {
  setCategoryList,
  setCategory,
  setCategoryId
}

const ACTION_HANDLERS = {
  [SET_CATEGORY_LIST]: (state, action) => {
    return {
      ...state,
      categoryList: action.data
    }
  },
  [SET_CATEGORY]: (state, action) => {
    return {
      ...state,
      category: action.data
    }
  },
  [SET_CATEGORY_ID]: (state, action) => {
    return {
      ...state,
      categoryId: action.data
    }
  }
}

const initialState = {
  categoryList: [],
  category: null,
  categoryId: -1
}

export default function categoryReducer (state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}
