import {
  SET_MASTER_CATEGORY_LIST,
  SET_MASTER_CATEGORY,
  SET_MASTER_CATEGORY_ID
} from '../actionList'
import { errorHandler, protocol, origin, getHeaderList } from '../../common/auth'
import axios from 'axios'
import { notify } from '../../screen'

export const setMasterCategoryListAction = (masterCategoryList = []) => {
  return {
    type: SET_MASTER_CATEGORY_LIST,
    data: masterCategoryList
  }
}

export const setMasterCategoryAction = (masterCategory = null) => {
  return {
    type: SET_MASTER_CATEGORY,
    data: masterCategory
  }
}

export const setMasterCategoryIdAction = (masterCategoryId = -1) => {
  return {
    type: SET_MASTER_CATEGORY_ID,
    data: masterCategoryId
  }
}

export const setMasterCategoryList = (budgetId = -1, token = '') => {
  return dispatch => {
    return new Promise((resolve, reject) => {
      axios.get(`${protocol}://${origin}/api/budget/${budgetId}/master`, getHeaderList(token))
      .then(response => {
        dispatch(setMasterCategoryListAction(response.data))
        resolve()
      })
      .catch(error => {
        errorHandler(error)
        reject(error)
      })
    })
  }
}

export const setMasterCategory = (budgetId = -1, masterCategoryId = -1, token = '') => {
  return dispatch => {
    return new Promise((resolve, reject) => {
      axios.get(`${protocol}://${origin}/api/budget/${budgetId}/master/${masterCategoryId}`, getHeaderList(token))
      .then(response => {
        dispatch(setMasterCategoryAction(response.data))
        resolve()
      })
      .catch(error => {
        errorHandler(error)
        reject(error)
      })
    })
  }
}

export const setMasterCategoryId = (masterCategoryId = -1) => {
  return dispatch => {
    dispatch(setMasterCategoryIdAction(masterCategoryId))
  }
}

export const addMasterCategory = async (budgetId = -1, data = null, token = '') => {
  try {
    let response = await axios.post(`${protocol}://${origin}/api/budget/${budgetId}/master`, data, getHeaderList(token))
    response.data.id && setMasterCategoryId(response.data.id)
    notify('Kategoria główna dodana', true)
    return true
  } catch (error) {
    notify('Błąd dodawania kategorii głównej', false)
    errorHandler(error)
    return false
  }
}

export const editMasterCategory = async (budgetId = -1, masterCategoryId = -1, data = null, token = '') => {
  try {
    await axios.put(`${protocol}://${origin}/api/budget/${budgetId}/master/${masterCategoryId}`, data, getHeaderList(token))
    notify('Zmiany kategorii głównej zapisano', true)
    return true
  } catch (error) {
    notify('Błąd zapisu', false)
    errorHandler(error)
    return false
  }
}

export const deleteMasterCategory = async (budgetId = -1, masterCategoryId = -1, token = '') => {
  try {
    await axios.delete(`${protocol}://${origin}/api/budget/${budgetId}/master/${masterCategoryId}`, getHeaderList(token))
    notify('Kategoria główna usunięta', true)
    return true
  } catch (error) {
    notify('Błąd usuwania', false)
    errorHandler(error)
    return false
  }
}

export const actions = {
  setMasterCategoryList,
  setMasterCategory,
  setMasterCategoryId
}

const ACTION_HANDLERS = {
  [SET_MASTER_CATEGORY_LIST]: (state, action) => {
    return {
      ...state,
      masterCategoryList: action.data
    }
  },
  [SET_MASTER_CATEGORY]: (state, action) => {
    return {
      ...state,
      masterCategory: action.data
    }
  },
  [SET_MASTER_CATEGORY_ID]: (state, action) => {
    return {
      ...state,
      masterCategoryId: action.data
    }
  }
}

const initialState = {
  masterCategoryList: [],
  masterCategory: null,
  masterCategoryId: -1
}

export default function masterCategoryReducer (state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}
