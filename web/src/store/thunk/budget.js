import axios from 'axios'
import {
  SET_BUDGET_LIST,
  SET_BUDGET,
  SET_BUDGET_ID,
  ADD_BUDGET,
  UPDATE_BUDGET,
  DELETE_BUDGET,
  LEAVE_BUDGET
} from '../actionList'
import { errorHandler, protocol, origin, getHeaderList } from '../../common/auth'
import { notify } from '../../screen'

export const setBudgetListAction = (budgetList = []) => {
  return {
    type: SET_BUDGET_LIST,
    budgetList
  }
}

export const setBudgetAction = (budget = null) => {
  return {
    type: SET_BUDGET,
    budget
  }
}

export const setBudgetIdAction = (budgetId = -1) => {
  return {
    type: SET_BUDGET_ID,
    budgetId
  }
}

export const addBudgetAction = () => {
  return {
    type: ADD_BUDGET
  }
}

export const updateBudgetAction = () => {
  return {
    type: UPDATE_BUDGET
  }
}

export const deleteBudgetAction = () => {
  return {
    type: DELETE_BUDGET
  }
}

export const leaveBudgetAction = () => {
  return {
    type: LEAVE_BUDGET
  }
}

export const getBudgetList = (token = '') => {
  return dispatch => {
    return new Promise((resolve, reject) => {
      axios.get(`${protocol}://${origin}/api/budget`, getHeaderList(token))
      .then(response => {
        dispatch(setBudgetListAction(response.data))
        resolve(response.data)
      })
      .catch(error => {
        errorHandler(error)
        reject([])
      })
    })
  }
}

export const getBudget = (id = -1, token = '') => {
  return dispatch => {
    return new Promise((resolve, reject) => {
      axios.get(`${protocol}://${origin}/api/budget/${id}`, getHeaderList(token))
      .then(response => {
        dispatch(setBudgetAction(response.data))
        resolve(response.data)
      })
      .catch(error => {
        errorHandler(error)
        reject(null)
      })
    })
  }
}

export const setBudgetId = (id = -1) => {
  return dispatch => dispatch(setBudgetIdAction(id))
}

export const addBudget = (budget = null, token = '') => {
  return dispatch => {
    return new Promise((resolve, reject) => {
      axios.post(`${protocol}://${origin}/api/budget`, budget, getHeaderList(token))
      .then(response => {
        dispatch(addBudgetAction())
        notify('Budżet dodano', true)
        dispatch(setBudgetIdAction(response.data.id))
        resolve(true)
      })
      .catch(error => {
        notify('Błąd dodawania budżetu', false)
        errorHandler(error)
        reject(false)
      })
    })
  }
}

export const updateBudget = (id = -1, budget = null, token = '') => {
  return dispatch => {
    return new Promise((resolve, reject) => {
      axios.put(`${protocol}://${origin}/api/budget/${id}`, budget, getHeaderList(token))
      .then(() => {
        dispatch(updateBudgetAction())
        notify('Zmiany budżetu zapisano', true)
        resolve(true)
      })
      .catch(error => {
        notify('Błąd zapisu', false)
        errorHandler(error)
        reject(false)
      })
    })
  }
}

export const deleteBudget = (id = -1, token = '') => {
  return dispatch => {
    return new Promise((resolve, reject) => {
      axios.delete(`${protocol}://${origin}/api/budget/${id}`, getHeaderList(token))
      .then(() => {
        dispatch(deleteBudgetAction())
        notify('Budżet usunięto', true)
        resolve(true)
      })
      .catch(error => {
        notify('Błąd usuwania', false)
        errorHandler(error)
        reject(false)
      })
    })
  }
}

export const leaveBudget = (id = -1, token = '') => {
  return dispatch => {
    return new Promise((resolve, reject) => {
      axios.put(`${protocol}://${origin}/api/budget/${id}/leave`, null, getHeaderList(token))
      .then(() => {
        dispatch(leaveBudgetAction())
        notify('Opuszczono budżet', true)
        resolve(true)
      })
      .catch(error => {
        notify('Błąd opuszczania budżetu', false)
        errorHandler(error)
        reject(false)
      })
    })
  }
}

export const inviteToBudget = async (budgetId = -1, email = '', token = '') => {
  try {
    await axios.put(`${protocol}://${origin}/api/budget/${budgetId}/email/${email}`, null, getHeaderList(token))
    notify('Zaproszenie do budżetu wysłano', true)
    return true
  } catch (error) {
    notify('Błąd zapraszania', false)
    errorHandler(error)
    return false
  }
}

export const deleteUserFromBudget = async (budgetId = -1, userId = -1, token = '') => {
  try {
    await axios.put(`${protocol}://${origin}/api/budget/${budgetId}/remove/${userId}`, null, getHeaderList(token))
    notify('Użytkownik usunięty z budżetu', true)
    return true
  } catch (error) {
    notify('Błąd usuwania użytkownika z budżetu', false)
    errorHandler(error)
    return false
  }
}

export const confirmBudgetJoin = async (token = '') => {
  try {
    await axios.post(`${protocol}://${origin}/confirm-budget-join?token=${token}`)
    notify('Dołączono do budżetu', true)
    return true
  } catch (error) {
    notify('Błąd dołączania z budżetu', false)
    errorHandler(error)
    return false
  }
}

export const actions = {
  getBudgetList,
  getBudget,
  setBudgetId,
  addBudget,
  updateBudget,
  deleteBudget,
  leaveBudget
}


const ACTION_HANDLERS = {
  [SET_BUDGET_LIST]: (state, action) => {
    return {
      ...state,
      budgetList: action.budgetList
    }
  },
  [SET_BUDGET]: (state, action) => {
    return {
      ...state,
      budget: action.budget
    }
  },
  [SET_BUDGET_ID]: (state, action) => {
    return {
      ...state,
      budgetId: action.budgetId
    }
  }
}

const initialState = {
  budgetList: [],
  budget: null,
  budgetId: -1
}

export default function budgetReducer (state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}
