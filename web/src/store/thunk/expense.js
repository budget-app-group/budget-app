import {
  SET_EXPENSE_LIST,
  SET_EXPENSE,
  SET_EXPENSE_ID,
  SET_BUDGET_EXPENSE_LIST
} from '../actionList'
import { errorHandler, protocol, origin, getHeaderList } from '../../common/auth'
import axios from 'axios'
import { notify } from '../../screen'

export const setBudgetExpenseListAction = (expenseList = []) => {
  return {
    type: SET_BUDGET_EXPENSE_LIST,
    data: expenseList
  }
}

export const setExpenseListAction = (expenseList = []) => {
  return {
    type: SET_EXPENSE_LIST,
    data: expenseList
  }
}

export const setExpenseAction = (expense = null) => {
  return {
    type: SET_EXPENSE,
    data: expense
  }
}

export const setExpenseIdAction = (expenseId = -1) => {
  return {
    type: SET_EXPENSE_ID,
    data: expenseId
  }
}

export const setBudgetExpenseList = (budgetId = -1, token = '') => {
  return dispatch => {
    return new Promise((resolve, reject) => {
      axios.get(`${protocol}://${origin}/api/budget/${budgetId}/expense`, getHeaderList(token))
      .then(response => {
        dispatch(setBudgetExpenseListAction(response.data))
        resolve()
      })
      .catch(error => {
        errorHandler(error)
        reject(error)
      })
    })
  }
}

export const setExpenseList = (budgetId = -1, masterCategoryId = -1, categoryId = -1, token = '') => {
  return dispatch => {
    return new Promise((resolve, reject) => {
      axios.get(`${protocol}://${origin}/api/budget/${budgetId}/master/${masterCategoryId}/category/${categoryId}/expense`, getHeaderList(token))
      .then(response => {
        dispatch(setExpenseListAction(response.data))
        resolve()
      })
      .catch(error => {
        errorHandler(error)
        reject(error)
      })
    })
  }
}

export const setExpense = (budgetId = -1, masterCategoryId = -1, categoryId = -1, expenseId = -1, token = '') => {
  return dispatch => {
    return new Promise((resolve, reject) => {
      axios.get(`${protocol}://${origin}/api/budget/${budgetId}/master/${masterCategoryId}/category/${categoryId}/expense/${expenseId}`, getHeaderList(token))
      .then(response => {
        dispatch(setExpenseAction(response.data))
        resolve()
      })
      .catch(error => {
        errorHandler(error)
        reject(error)
      })
    })
  }
}

export const setExpenseId = (expenseId = -1) => {
  return dispatch => {
    dispatch(setExpenseIdAction(expenseId))
  }
}

export const getExpenseListFromCSV = async (budgetId = -1, bankAccountId = -1, file, token = '') => {
  try {
    let data = new FormData()
    data.append('csvfile', file)
    data.append('budgetId', budgetId)
    data.append('bankAccountId', bankAccountId)
    let response = await axios.post(`${protocol}://${origin}/api/budget/upload-csv`, data, getHeaderList(token))
    return response.data
  } catch (error) {
    errorHandler(error)
    return []
  }
}

export const addExpenseList = async (budgetId = -1, expenseList = [], token = '') => {
  try {
    await axios.post(`${protocol}://${origin}/api/budget/${budgetId}/expensecsvlist`, expenseList, getHeaderList(token))
    notify('Dodano wydatki', true)
    return true
  } catch (error) {
    notify('Błąd dodawania wydatków', false)
    errorHandler(error)
    return false
  }
}

export const addExpense = async (budgetId = -1, masterCategoryId = -1, categoryId = -1, data = null, token = '') => {
  try {
    let response = await axios.post(`${protocol}://${origin}/api/budget/${budgetId}/master/${masterCategoryId}/category/${categoryId}/expense`, data, getHeaderList(token))
    response.data.id && setExpenseId(response.data.id)
    notify('Wydatek dodano', true)
    return true
  } catch (error) {
    notify('Błąd dodawania wydatku', false)
    errorHandler(error)
    return false
  }
}

export const editExpense = async (budgetId = -1, masterCategoryId = -1, categoryId = -1, expenseId = -1, data = null, token = '') => {
  try {
    await axios.put(`${protocol}://${origin}/api/budget/${budgetId}/master/${masterCategoryId}/category/${categoryId}/expense/${expenseId}`, data, getHeaderList(token))
    notify('Zmiany w wydatku zapisano', true)
    return true
  } catch (error) {
    notify('Błąd zapisu', false)
    errorHandler(error)
    return false
  }
}

export const deleteExpense = async (budgetId = -1, masterCategoryId = -1, categoryId = -1, expenseId = -1, token = '') => {
  try {
    await axios.delete(`${protocol}://${origin}/api/budget/${budgetId}/master/${masterCategoryId}/category/${categoryId}/expense/${expenseId}`, getHeaderList(token))
    notify('Wydatek usunięto', true)
    return true
  } catch (error) {
    notify('Błąd usuwania', false)
    errorHandler(error)
    return false
  }
}

export const actions = {
  setExpenseList,
  setExpense,
  setExpenseId
}

const ACTION_HANDLERS = {
  [SET_EXPENSE_LIST]: (state, action) => {
    return {
      ...state,
      expenseList: action.data
    }
  },
  [SET_EXPENSE]: (state, action) => {
    return {
      ...state,
      expense: action.data
    }
  },
  [SET_EXPENSE_ID]: (state, action) => {
    return {
      ...state,
      expenseId: action.data
    }
  },
  [SET_BUDGET_EXPENSE_LIST]: (state, action) => {
    return {
      ...state,
      budgetExpenseList: action.data
    }
  }
}

const initialState = {
  budgetExpenseList: [],
  expenseList: [],
  expense: null,
  expenseId: -1
}

export default function expenseReducer (state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}
