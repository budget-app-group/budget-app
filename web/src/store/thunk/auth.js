import { SET_TOKEN, SET_USER } from '../actionList'
import { errorHandler, protocol, origin } from '../../common/auth'
import axios from 'axios'
import { notify } from '../../screen'

export const setTokenAction = (token = false) => {
  return {
    type: SET_TOKEN,
    data: token
  }
}

export const setUserAction = (user = {}) => {
  return {
    type: SET_USER,
    data: user
  }
}

export const login = (email = '', password = '') => {
  return dispatch => {
    return new Promise((resolve, reject) => {
      axios.post(`${protocol}://${origin}/auth/login`, { email, password })
      .then(response => {
        localStorage.setItem('token', `Bearer ${response.data.token.accessToken}`)
        localStorage.setItem('user', JSON.stringify(response.data.user))
        dispatch(setTokenAction(`Bearer ${response.data.token.accessToken}`))
        dispatch(setUserAction(response.data.user))
        notify('Zalogowano', true)
        resolve()
      })
      .catch(error => {
        errorHandler(error)
        notify('Błąd logowania', false)
        reject(error)
      })
    })
  }
}

export const logout = () => {
  localStorage.clear()
  window.location.reload()
}

export const register = async (name = '', surname = '', email = '', password = '') => {
  try {
    await axios.post(`${protocol}://${origin}/auth/register`, { name, surname, email, password })
    notify('Zarejestrowano', true)
    return true
  } catch (error) {
    notify('Błąd rejestracji', false)
    errorHandler(error)
    return false
  }
}

export const resetPassword = async (email = '') => {
  try {
    await axios.post(`${protocol}://${origin}/reset-password`, { email })
    notify('Mail wysłany', true)
    return true
  } catch (error) {
    notify('Wystąpił błąd przy wysyłaniu maila', false)
    errorHandler(error)
    return false
  }
}

export const confirmEmail = async (token = '') => {
  try {
    await axios.post(`${protocol}://${origin}/confirm-email?token=${token}`)
    notify('Adres email potwierdzony', true)
    return true
  } catch (error) {
    notify('Bład potwierdzania adresu email', false)
    errorHandler(error)
    return false
  }
}

export const confirmResetPassword = async (token = '', newPassword = '') => {
  try {
    await axios.post(`${protocol}://${origin}/confirm-reset-password?token=${token}`, { password: newPassword })
    notify('Hasło zmieniono', true)
    return true
  } catch (error) {
    notify('Błąd zmiany hasła', false)
    errorHandler(error)
    return false
  }
}

export const actions = {
  login,
  logout,
}


const ACTION_HANDLERS = {
  [SET_USER]: (state, action) => {
    return {
      ...state,
      user: action.data
    }
  },
  [SET_TOKEN]: (state, action) => {
    return {
      ...state,
      token: action.data
    }
  }
}

const initialState = {
  token: localStorage.getItem('token') || '',
  user: JSON.parse(localStorage.getItem('user'))
}

export default function authReducer (state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type]
  return handler ? handler(state, action) : state
}
