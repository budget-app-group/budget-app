import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Button, Card, Icon, Input } from 'semantic-ui-react'
import { isEmpty } from '../../common/validation'
import * as dict from '../../common/dictionary/polish.json'
import { resetPassword } from '../../store/thunk/auth'
import '../Login/Login.scss'

export default class RememberPassword extends Component {
  constructor () {
    super()
    this.state = {
      email: '',
      emailError: false,
    }
  }

  static propTypes = {
    history: PropTypes.object.isRequired
  }

  changeHandler = (stateName = '', value, isValidatable = true) => {
    this.setState({
      [stateName]: value
    }, () => {
      isValidatable && this.validateHandler(stateName, value)
    })
  }

  resetPassword = async () => {
    this.validateHandler('email', this.state.email)
    
    if (!this.state.emailError) {
      let response = await resetPassword(this.state.email)
      if (response) {
        this.props.history.push('/login')
      }
    }
  }

  validateHandler = (stateName = '', value) => this.setState({ [`${stateName}Error`]: isEmpty(value) })

  render () {
    return (
      <main className='layout-container--auth'>
        <Card className='login-raised-card' raised>
          <Icon
            name='lock'
            size='huge'
            className='auth-icon'
          />
          <section className='section'>
            <span className={`section-header ${this.state.emailError ? 'error-color' : ''}`}>Email</span>
            <Input
              className='login-input'
              icon='at'
              iconPosition='left'
              fluid
              error={this.state.emailError}
              onBlur={() => this.validateHandler('email', this.state.email)}
              onChange={event => this.changeHandler('email', event.target.value)}
              placeholder={dict.placeholder.email}
              value={this.state.email}
            />
          </section>
          <section className='section section--button'>
            <Button
              positive
              onClick={this.resetPassword}
              className='button'
              content={dict.button.reset_password}
            />
            <Button
              onClick={() => this.props.history.push('/login')}
              className='button'
              content={dict.button.login}
            />
          </section>
        </Card>
      </main>
    )
  }
}