import Budget from './Budget'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import {
  getBudgetList,
  addBudget,
  updateBudget,
  deleteBudget,
  setBudgetId,
  getBudget,
  leaveBudget
} from '../../store/thunk/budget'

const mapDispatchToProps = {
  getBudgetList,
  getBudget,
  addBudget,
  updateBudget,
  deleteBudget,
  setBudgetId,
  leaveBudget
}

const mapStateToProps = (state) => ({
  budgetList: state.budget.budgetList,
  budgetId: state.budget.budgetId,
  user: state.auth.user,
  token: state.auth.token,
  budget: state.budget.budget
})

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Budget))