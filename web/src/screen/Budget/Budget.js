import React, { Component } from 'react'
import PropTypes from 'prop-types'
import List from '../../component/List/List'
import UniversalModal from '../../modal/Universal/Universal'
import BudgetModal from '../../modal/Budget/Budget'
import _ from 'lodash'
import { Container } from 'semantic-ui-react'

export default class Budget extends Component {
  constructor () {
    super()
    this.state = {
      isBudgetModalOpen: false,
      isUniversalModalOpen: false,
      universalModalMode: '',
      budgetModalMode: '',
      data: [],
      selectedBudgetName: '',
      searchedPhrase: ''
    }
  }

  static propTypes = {
    getBudgetList: PropTypes.func.isRequired,
    getBudget: PropTypes.func.isRequired,
    addBudget: PropTypes.func.isRequired,
    updateBudget: PropTypes.func.isRequired,
    deleteBudget: PropTypes.func.isRequired,
    setBudgetId: PropTypes.func.isRequired,
    budgetList: PropTypes.array.isRequired,
    budgetId: PropTypes.number.isRequired,
    user: PropTypes.object.isRequired,
    token: PropTypes.string.isRequired,
    leaveBudget: PropTypes.func.isRequired,
    budget: PropTypes.object
  }

  static getDerivedStateFromProps (props, state) {
    if (!_.isEqual(props.budgetList, state.data) && state.searchedPhrase === '') {
      
      return {
        data: props.budgetList
      }
    }

    return null
  }

  componentDidMount = () => {
    this.props.getBudgetList(this.props.token)
  }

  setIdHandler = (id = -1) => {
    if (id !== this.props.budgetId) {
      this.props.setBudgetId(id)
    }
  }

  searchHandler = (phrase = '') => {
    let lowerPhrase = phrase.toLowerCase()
    this.setState({
      searchedPhrase: phrase,
      data: phrase === ''
        ? this.props.budgetList
        : this.props.budgetList.filter(budget => budget.name.toLowerCase().indexOf(lowerPhrase) !== -1)
    })
  }

  saveHandler = async (type = '', budget = {}, id = -1) => {
    let response

    if (type === 'add') {
      response = await this.props.addBudget(budget, this.props.token)
      if (response) {
        this.setState({ isBudgetModalOpen: false, searchedPhrase: '' })
      }
    } else if (type === 'edit') {
      response = await this.props.updateBudget(id, budget, this.props.token)
      if (response) {
        this.setState({ isBudgetModalOpen: false, searchedPhrase: '' })
      }
    } else if (type === 'delete') {
      response = await this.props.deleteBudget(id, this.props.token)
      if (response) {
        this.setState({ isUniversalModalOpen: false, searchedPhrase: '' })
      }
    } else if (type === 'leave') {
      response = await this.props.leaveBudget(id, this.props.token)
      if (response) {
        this.setState({ isUniversalModalOpen: false, searchedPhrase: '' })
      }
    }

    if (response) {
      this.props.getBudgetList(this.props.token)
    }
  }

  render () {
    return (
      <Container fluid style={{ flex: 1 }}>
        <List
          headerName='Budżety'
          searchHandler={this.searchHandler}
          addHandler={() => this.setState({ isBudgetModalOpen: true, budgetModalMode: 'add' })}
          data={this.state.data}
          setIdHandler={this.setIdHandler}
          editHandler={id => {
            this.props.getBudget(id, this.props.token)
            this.setState({
              isBudgetModalOpen: true,
              budgetModalMode: 'edit'
            })
          }}
          deleteHandler={(budgetName = '') => {
            this.setState({
              isUniversalModalOpen: true,
              universalModalMode: 'delete',
              selectedBudgetName: budgetName
            })
          }}
          leaveHandler={(budgetName = '') => {
            this.setState({
              isUniversalModalOpen: true,
              universalModalMode: 'leave',
              selectedBudgetName: budgetName
            })
          }}
          isAdmin={budgetAdminId => budgetAdminId === this.props.user.id}
          adminElementProperty='admin.id'
          isLeavable
          isAddEnabled
          user={this.props.user}
          dropdownList={[]}
          propertyList={[
            { field: 'name', label: 'Nazwa' },
            { field: 'admin.name', label: 'Administrator' }
          ]}
          selectedId={this.props.budgetId}
        />
        <UniversalModal
          cancelHandler={() => this.setState({ isUniversalModalOpen: false })}
          saveHandler={this.saveHandler.bind(this, this.state.universalModalMode, null, this.props.budgetId)}
          mode={this.state.universalModalMode}
          isOpen={this.state.isUniversalModalOpen}
          elementName={this.state.selectedBudgetName}
        />
        <BudgetModal
          cancelHandler={() => this.setState({ isBudgetModalOpen: false })}
          saveHandler={this.saveHandler}
          mode={this.state.budgetModalMode}
          isOpen={this.state.isBudgetModalOpen}
          budget={this.props.budget}
          user={this.props.user}
          token={this.props.token}
        />
      </Container>
    )
  }
}
