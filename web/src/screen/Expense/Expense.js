import React, { Component } from 'react'
import PropTypes from 'prop-types'
import List from '../../component/List/List'
import UniversalModal from '../../modal/Universal/Universal'
import ExpenseModal from '../../modal/Expense/Expense'
import CSVmodal from '../../modal/CSV/CSV'
import _ from 'lodash'
import {
  addExpense,
  editExpense,
  deleteExpense,
  addExpenseList
} from '../../store/thunk/expense'

export default class Expense extends Component {
  constructor () {
    super()
    this.state = {
      isExpenseModalOpen: false,
      isUniversalModalOpen: false,
      isCSVmodalOpen: false,
      universalModalMode: '',
      expenseModalMode: '',
      data: [],
      selectedExpenseName: '',
      searchedPhrase: '',
      budgetList: [],
      bankAccountList: [],
      masterCategoryList: [],
      categoryList: []
    }
  }

  static propTypes= {
    // functions
    setExpenseList: PropTypes.func.isRequired,
    setExpense: PropTypes.func.isRequired,
    setExpenseId: PropTypes.func.isRequired,
    setExpenseListAction: PropTypes.func.isRequired,

    setBankAccountList: PropTypes.func.isRequired,
    setBankAccountId: PropTypes.func.isRequired,

    setCategoryList: PropTypes.func.isRequired,
    setCategoryId: PropTypes.func.isRequired,

    setMasterCategoryList: PropTypes.func.isRequired,
    setMasterCategoryId: PropTypes.func.isRequired,

    getBudgetList: PropTypes.func.isRequired,
    setBudgetId: PropTypes.func.isRequired,

    // state properties
    token: PropTypes.string.isRequired,
    user: PropTypes.object.isRequired,

    budgetId: PropTypes.number.isRequired,
    budgetList: PropTypes.array.isRequired,

    bankAccountId: PropTypes.number.isRequired,
    bankAccountList: PropTypes.array.isRequired,

    categoryList: PropTypes.array.isRequired,
    setCategoryListAction: PropTypes.func.isRequired,
    categoryId: PropTypes.number.isRequired,

    masterCategoryList: PropTypes.array.isRequired,
    masterCategoryId: PropTypes.number.isRequired,

    expenseList: PropTypes.array.isRequired,
    expense: PropTypes.object,
    expenseId: PropTypes.number.isRequired
  }

  static getDerivedStateFromProps (props, state) {
    if (!_.isEqual(props.expenseList, state.data) && state.searchedPhrase === '') {
      return {
        data: props.expenseList.sort((expense_a, expense_b) => expense_b.expenseDate - expense_a.expenseDate)
      }
    }

    return null
  }

  componentDidMount = () => {
    !this.props.budgetList.length && this.props.getBudgetList(this.props.token)

    this.props.budgetId !== -1 && !this.props.bankAccountList.length &&
      this.props.setBankAccountList(this.props.budgetId, this.props.token)

    this.props.budgetId !== -1 && !this.props.masterCategoryList.length &&
      this.props.setMasterCategoryList(this.props.budgetId, this.props.token)

    this.props.budgetId !== -1 && this.props.masterCategoryId !== -1 &&
      this.props.setCategoryList(this.props.budgetId, this.props.masterCategoryId, this.props.token)

    this.props.budgetId !== -1 && this.props.masterCategoryId !== -1 && this.props.categoryId !== -1 &&
      this.props.setExpenseList(this.props.budgetId, this.props.masterCategoryId, this.props.categoryId, this.props.token)
  }

  setIdHandler = (id = -1) => {
    if (id !== this.props.expenseId) {
      this.props.setExpenseId(id)
    }
  }

  searchHandler = (phrase = '') => {
    let lowerPhrase = phrase.toLowerCase()
    this.setState({
      searchedPhrase: phrase,
      data: phrase === ''
        ? this.props.expenseList
        : this.props.expenseList.filter(expense => expense.name.toLowerCase().indexOf(lowerPhrase) !== -1)
    })
  }

  saveHandler = async (type = '', expense = {}, id = -1) => {
    let response

    if (type === 'add') {
      response = await addExpense(this.props.budgetId, this.props.masterCategoryId, this.props.categoryId, expense, this.props.token)
      if (response) {
        this.setState({ isExpenseModalOpen: false, searchedPhrase: '' })
      }
    } else if (type === 'edit') {
      response = await editExpense(this.props.budgetId, this.props.masterCategoryId, this.props.categoryId, id, expense, this.props.token)
      if (response) {
        this.setState({ isExpenseModalOpen: false, searchedPhrase: '' })
      }
    } else if (type === 'delete') {
      response = await deleteExpense(this.props.budgetId, this.props.masterCategoryId, this.props.categoryId, id, this.props.token)
      if (response) {
        this.setState({ isUniversalModalOpen: false, searchedPhrase: '' })
      }
    }

    if (response) {
      this.props.setExpenseList(this.props.budgetId, this.props.masterCategoryId, this.props.categoryId, this.props.token)
    }
  }

  render () {
    return (
      <section style={{ flex: 1 }}>
        <List
          headerName='Wydatki'
          searchHandler={this.searchHandler}
          addHandler={() => this.setState({ isExpenseModalOpen: true, expenseModalMode: 'add' })}
          data={this.state.data}
          setIdHandler={this.setIdHandler}
          editHandler={id => {
            this.props.setExpense(this.props.budgetId, this.props.masterCategoryId, this.props.categoryId, id, this.props.token)
            this.setState({
              isExpenseModalOpen: true,
              expenseModalMode: 'edit'
            })
          }}
          deleteHandler={(expenseName = '') => {
            this.setState({
              isUniversalModalOpen: true,
              universalModalMode: 'delete',
              selectedExpenseName: expenseName
            })
          }}
          leaveHandler={(expenseName = '') => {
            this.setState({
              isUniversalModalOpen: true,
              universalModalMode: 'leave',
              selectedExpenseName: expenseName
            })
          }}
          user={this.props.user}
          isAdmin={ownerEmail => ownerEmail === this.props.user.email}
          isCSVenabled={this.props.budgetId !== -1 && this.props.bankAccountId !== -1 &&
            this.props.masterCategoryId !== -1 && this.props.categoryId !== -1}
          isCSVshown
          addCSVhandler={() => this.setState({
            isCSVmodalOpen: true,
            budgetList: this.props.budgetList.map(budget => {
              return {
                key: budget.id,
                text: budget.name,
                value: budget.id
              }
            }),
            bankAccountList: this.props.bankAccountList.map(bankAccount => {
              return {
                key: bankAccount.id,
                text: bankAccount.name,
                value: bankAccount.id
              }
            }),
            masterCategoryList: this.props.masterCategoryList.map(masterCategory => {
              return {
                key: masterCategory.id,
                text: masterCategory.name,
                value: masterCategory.id
              }
            }),
            categoryList: this.props.categoryList.map(category => {
              return {
                key: category.id,
                text: category.name,
                value: category.id
              }
            })
          })}
          isAddEnabled={this.props.budgetId !== -1 && this.props.masterCategoryId !== -1 && this.props.categoryId !== -1}
          adminElementProperty='expenseOwner.email'
          propertyList={[
            { field: 'name', label: 'Nazwa' },
            { field: 'amount', label: 'Kwota' },
            { field: 'expenseDate', label: 'Data', isDate: true },
            { field: 'shopName', label: 'Sklep' },
            { field: 'bankAccount.name', label: 'Konto bankowe' }
          ]}
          dropdownList={[
            {
              label: 'Budżet',
              placeholder: 'Wybierz budżet',
              value: this.props.budgetId === -1 ? '' : this.props.budgetId,
              optionList: this.props.budgetList.map((budget, index) => {
                return {
                  index: index,
                  key: index,
                  value: budget.id,
                  text: budget.name
                }
              }),
              onChange: id => {
                if (id !== this.props.budgetId) {
                  this.props.setBudgetId(id)
                  this.props.setBankAccountList(id, this.props.token)
                  this.props.expenseList.length && this.props.setExpenseListAction([])
                  this.props.masterCategoryId !== -1 && this.props.setMasterCategoryId(-1)
                  this.props.bankAccountId !== -1 && this.props.setBankAccountId(-1)
                  this.props.setMasterCategoryList(id, this.props.token)
                  this.props.categoryId !== -1 && this.props.setCategoryId(-1)
                  this.props.categoryList.length && this.props.setCategoryListAction([])
                }
              }
            },
            {
              label: 'Konto bankowe',
              placeholder: 'Wybierz konto bankowe',
              value: this.props.bankAccountId === -1 ? '' : this.props.bankAccountId,
              optionList: this.props.bankAccountList.map((bankAccount, index) => {
                return {
                  index: index,
                  key: index,
                  value: bankAccount.id,
                  text: bankAccount.name
                }
              }),
              onChange: id => {
                if (id !== this.props.bankAccountId) {
                  this.props.setBankAccountId(id)
                }
              }
            },
            {
              label: 'Kategoria główna',
              placeholder: 'Wybierz kategorię główną',
              value: this.props.masterCategoryId === -1 ? '' : this.props.masterCategoryId,
              optionList: this.props.masterCategoryList.map((masterCategory, index) => {
                return {
                  index: index,
                  key: index,
                  value: masterCategory.id,
                  text: masterCategory.name
                }
              }),
              onChange: id => {
                if (id !== this.props.masterCategory) {
                  this.props.setMasterCategoryId(id)
                  this.props.categoryId !== -1 && this.props.setCategoryId(-1)
                  this.props.setCategoryList(this.props.budgetId, id, this.props.token)
                  this.props.expenseList.length && this.props.setExpenseListAction([])
                }
              }
            },
            {
              label: 'Kategoria',
              placeholder: 'Wybierz kategorię',
              value: this.props.categoryId === -1 ? '' : this.props.categoryId,
              optionList: this.props.categoryList.map((category, index) => {
                return {
                  index: index,
                  key: index,
                  value: category.id,
                  text: category.name
                }
              }),
              onChange: id => {
                if (id !== this.props.categoryId) {
                  this.props.setCategoryId(id)
                  this.props.setExpenseList(this.props.budgetId, this.props.masterCategoryId, id, this.props.token)
                }
              }
            }
          ]}
          selectedId={this.props.expenseId}
        />
        <UniversalModal
          cancelHandler={() => this.setState({ isUniversalModalOpen: false })}
          saveHandler={this.saveHandler.bind(this, this.state.universalModalMode, null, this.props.expenseId)}
          mode={this.state.universalModalMode}
          isOpen={this.state.isUniversalModalOpen}
          elementName={this.state.selectedExpenseName}
        />
        <CSVmodal
          cancelHandler={() => this.setState({ isCSVmodalOpen: false })}
          saveHandler={async expenseList => {
            let response = await addExpenseList(this.props.budgetId, expenseList, this.props.token)
            response && this.setState({ isCSVmodalOpen: false }, () =>
            this.props.setExpenseList(this.props.budgetId, this.props.masterCategoryId, this.props.categoryId, this.props.token))
          }}
          isOpen={this.state.isCSVmodalOpen}
          token={this.props.token}
          budgetId={this.props.budgetId}
          bankAccountId={this.props.bankAccountId}
          masterCategoryId={this.props.masterCategoryId}
          categoryId={this.props.categoryId}
          budgetList={this.state.budgetList}
          bankAccountList={this.state.bankAccountList}
          masterCategoryList={this.state.masterCategoryList}
          categoryList={this.state.categoryList}
        />
        <ExpenseModal
          cancelHandler={() => this.setState({ isExpenseModalOpen: false })}
          saveHandler={this.saveHandler}
          mode={this.state.expenseModalMode}
          isOpen={this.state.isExpenseModalOpen}
          expense={this.props.expense}
          categoryId={this.props.categoryId}
          user={this.props.user}
          masterCategoryId={this.props.masterCategoryId}
          bankAccountList={this.props.bankAccountList.map(bankAccount => {
            return {
              index: bankAccount.id,
              key: bankAccount.id,
              text: bankAccount.name,
              value: bankAccount.id
            }
          })}
        />
      </section>
    )
  }
}