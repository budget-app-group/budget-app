import Expense from './Expense'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import {
  setExpenseList,
  setExpense,
  setExpenseId,
  setExpenseListAction
} from '../../store/thunk/expense'
import {
  setCategoryList,
  setCategoryListAction,
  setCategoryId
} from '../../store/thunk/category'
import {
  setMasterCategoryList,
  setMasterCategoryId
} from '../../store/thunk/masterCategory'
import {
  setBankAccountList,
  setBankAccountId
} from '../../store/thunk/bankAccount'
import {
  getBudgetList,
  setBudgetId
} from '../../store/thunk/budget'

const mapDispatchToProps = {
  setExpenseList,
  setExpense,
  setExpenseId,
  setExpenseListAction,

  setBankAccountList,
  setBankAccountId,

  setCategoryList,
  setCategoryListAction,
  setCategoryId,

  setMasterCategoryList,
  setMasterCategoryId,

  getBudgetList,
  setBudgetId
}

const mapStateToProps = state => ({
  token: state.auth.token,
  user: state.auth.user,

  budgetId: state.budget.budgetId,
  budgetList: state.budget.budgetList,

  bankAccountList: state.bankAccount.bankAccountList,
  bankAccountId: state.bankAccount.bankAccountId,

  masterCategoryList: state.masterCategory.masterCategoryList,
  masterCategoryId: state.masterCategory.masterCategoryId,

  categoryList: state.category.categoryList,
  categoryId: state.category.categoryId,

  expenseList: state.expense.expenseList,
  expense: state.expense.expense,
  expenseId: state.expense.expenseId
})

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Expense))