import React from 'react'
import PropTypes from 'prop-types'
import { Provider } from 'react-redux'
import Router from '../common/router'
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'

export const notify = (title = '', isSuccess = true) => toast(title, { type: toast.TYPE[isSuccess ? 'SUCCESS' : 'ERROR'] })

const App = (props) => {
  return (
    <Provider store={props.store}>
      {Router}
      <ToastContainer autoClose={3000} position={toast.POSITION.TOP_RIGHT}  />
    </Provider>
  )
}

App.propTypes = {
  store: PropTypes.object.isRequired
}

export default App
