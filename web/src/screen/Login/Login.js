import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Button, Card, Icon, Input, Container } from 'semantic-ui-react'
import { isEmpty } from '../../common/validation'
import * as dict from '../../common/dictionary/polish.json'
import { Link } from 'react-router-dom'
import './Login.scss'

export default class Login extends Component {
  constructor () {
    super()
    this.state = {
      email: '',
      emailError: false,
      password: '',
      passwordError: false,
      isRemembered: false
    }
  }

  static propTypes = {
    login: PropTypes.func.isRequired,
    token: PropTypes.string.isRequired,
    history: PropTypes.object.isRequired
  }

  static getDerivedStateFromProps(nextProps) {
    if (nextProps.token && nextProps.token !== '') {
      nextProps.history.push('/')
    }
    return null
  }

  changeHandler = (stateName = '', value, isValidatable = true) => {
    this.setState({
      [stateName]: value
    }, () => {
      isValidatable && this.validateHandler(stateName, value)
    })
  }

  login = async () => {
    await this.validateHandler('email', this.state.email)
    await this.validateHandler('password', this.state.password)
    !this.state.emailError && !this.state.passwordError &&
      this.props.login(this.state.email, this.state.password)
  }

  validateHandler = (stateName = '', value) => this.setState({ [`${stateName}Error`]: isEmpty(value) })

  render () {
    return (
      <Container className='layout-container--auth'>
        <Card className='login-raised-card' raised>
          <Icon
            name='user outline'
            size='huge'
            className='auth-icon'
          />
          <section className='section'>
            <span className={`section-header ${this.state.emailError ? 'error-color' : ''}`}>Email</span>
            <Input
              className='login-input'
              icon='at'
              iconPosition='left'
              fluid
              error={this.state.emailError}
              onBlur={() => this.validateHandler('email', this.state.email)}
              onChange={event => this.changeHandler('email', event.target.value)}
              placeholder={dict.placeholder.email}
              value={this.state.email}
            />
          </section>
          <section className='section'>
            <span className={`section-header ${this.state.passwordError ? 'error-color' : ''}`}>Hasło</span>
            <Input
              className='login-input'
              icon='shield'
              iconPosition='left'
              type='password'
              fluid
              error={this.state.passwordError}
              onBlur={() => this.validateHandler('password', this.state.email)}
              onChange={event => this.changeHandler('password', event.target.value)}
              placeholder={dict.placeholder.password}
              value={this.state.password}
            />
          </section>
          <section className='section section--button'>
            <Button
              positive
              onClick={this.login}
              className='button'
              content={dict.button.login}
            />
            <Button
              color='blue'
              onClick={() => this.props.history.push('/register')}
              className='button'
              content={dict.button.register}
            />
          </section>
          <section className='section section--link'>
            <Link to='/reset-password'>{dict.placeholder.reset_password}</Link>
          </section>
        </Card>
      </Container>
    )
  }
}