import Login from './Login'
import { connect } from 'react-redux'
import { login } from '../../store/thunk/auth'
import { withRouter } from 'react-router-dom'

const mapDispatchToProps = {
  login
}

const mapStateToProps = (state) => ({
  token: state.auth.token
})

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Login))