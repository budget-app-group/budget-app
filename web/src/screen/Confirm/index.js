import Confirm from './Confirm'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'

const mapDispatchToProps = {}

const mapStateToProps = (state) => ({
  token: state.auth.token
})

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Confirm))