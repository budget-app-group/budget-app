import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Button, Card, Icon, Loader, Dimmer } from 'semantic-ui-react'
import * as dict from '../../common/dictionary/polish.json'
import { confirmEmail } from '../../store/thunk/auth'
import { confirmBudgetJoin } from '../../store/thunk/budget'

export default class Confirm extends Component {
  constructor() {
    super()
    this.state = {
      isRequestSend: false,
      isResponsePositive: false
    }
  }

  static propTypes = {
    history: PropTypes.object.isRequired,
    match: PropTypes.object.isRequired,
    token: PropTypes.string.isRequired
  }

  componentDidMount = async () => {
    if (this.props.location.search) {
      // .slice(7) -> i am cutting '?token=' from search
      let isResponsePositive = this.props.match.path === '/confirm-email'
        ? await confirmEmail(this.props.location.search.slice(7))
        : await confirmBudgetJoin(this.props.location.search.slice(7))
      this.setState({ isResponsePositive, isRequestSend: true })
    } else { this.setState({ isRequestSend: true }) }
  }

  getTextFromPath = (path = '') => {
    switch(path) {
      case '/confirm-email': {
        return 'confirm_email'
      }
      case '/confirm-budget-join': {
        return 'confirm_budget_join'
      }
      default: {
        return ''
      }
    }
  }

  render() {
    return (
      <main className='layout-container--auth raised'>
        <Card className='raised-card' raised>
          <Dimmer active={!this.state.isRequestSend}>
            <Loader />
          </Dimmer>
          <Icon
            name={`${this.state.isResponsePositive ? 'handshake' : 'cancel'}`}
            size='massive'
            className='auth-icon'
          />
          <section className='section text-center'>
            {this.state.isResponsePositive
              ? dict.text[this.getTextFromPath(this.props.match.path)]
              : dict.error.confirm}<br />
            {this.state.isResponsePositive && dict.text.welcome}
          </section>
          <section className='section section--button'>
            <Button
              onClick={() => this.props.history.push(this.props.token ? '/budget' : '/login')}
              className='button'
              content={dict.button[this.props.token ? 'budget' : 'login']}
            />
          </section>
        </Card>
      </main>
    )
  }
}
