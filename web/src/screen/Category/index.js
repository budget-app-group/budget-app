import Category from './Category'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import {
  setCategoryList,
  setCategory,
  setCategoryId,
  setCategoryListAction
} from '../../store/thunk/category'
import {
  setMasterCategoryList,
  setMasterCategoryId
} from '../../store/thunk/masterCategory'
import {
  getBudgetList,
  setBudgetId
} from '../../store/thunk/budget'

const mapDispatchToProps = {
  setCategoryList,
  setCategory,
  setCategoryId,
  setCategoryListAction,

  setMasterCategoryList,
  setMasterCategoryId,

  getBudgetList,
  setBudgetId
}

const mapStateToProps = state => ({
  token: state.auth.token,
  user: state.auth.user,

  budgetId: state.budget.budgetId,
  budgetList: state.budget.budgetList,

  masterCategoryList: state.masterCategory.masterCategoryList,
  masterCategoryId: state.masterCategory.masterCategoryId,

  categoryList: state.category.categoryList,
  category: state.category.category,
  categoryId: state.category.categoryId
})

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Category))