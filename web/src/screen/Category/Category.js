import React, { Component } from 'react'
import PropTypes from 'prop-types'
import List from '../../component/List/List'
import UniversalModal from '../../modal/Universal/Universal'
import CategoryModal from '../../modal/Category/Category'
import _ from 'lodash'
import {
  addCategory,
  editCategory,
  deleteCategory
} from '../../store/thunk/category'

export default class Category extends Component {
  constructor () {
    super()
    this.state = {
      isCategoryModalOpen: false,
      isUniversalModalOpen: false,
      universalModalMode: '',
      categoryModalMode: '',
      data: [],
      selectedCategoryName: '',
      searchedPhrase: ''
    }
  }

  static propTypes= {
    // functions
    setCategoryList: PropTypes.func.isRequired,
    setCategory: PropTypes.func.isRequired,
    setCategoryId: PropTypes.func.isRequired,
    setCategoryListAction: PropTypes.func.isRequired,

    setMasterCategoryList: PropTypes.func.isRequired,
    setMasterCategoryId: PropTypes.func.isRequired,

    getBudgetList: PropTypes.func.isRequired,
    setBudgetId: PropTypes.func.isRequired,

    // state properties
    token: PropTypes.string.isRequired,
    user: PropTypes.object.isRequired,

    budgetId: PropTypes.number.isRequired,
    budgetList: PropTypes.array.isRequired,

    masterCategoryList: PropTypes.array.isRequired,
    masterCategoryId: PropTypes.number.isRequired,

    categoryList: PropTypes.array.isRequired,
    category: PropTypes.object,
    categoryId: PropTypes.number.isRequired
  }

  static getDerivedStateFromProps (props, state) {
    if (!_.isEqual(props.categoryList, state.data) && state.searchedPhrase === '') {
      
      return {
        data: props.categoryList
      }
    }

    return null
  }

  componentDidMount = () => {
    !this.props.budgetList.length && this.props.getBudgetList(this.props.token)

    this.props.budgetId !== -1 && !this.props.masterCategoryList.length &&
      this.props.setMasterCategoryList(this.props.budgetId, this.props.token)

    this.props.budgetId !== -1 && this.props.masterCategoryId !== -1 &&
      this.props.setCategoryList(this.props.budgetId, this.props.masterCategoryId, this.props.token)
  }

  setIdHandler = (id = -1) => {
    if (id !== this.props.categoryId) {
      this.props.setCategoryId(id)
    }
  }

  searchHandler = (phrase = '') => {
    let lowerPhrase = phrase.toLowerCase()
    this.setState({
      searchedPhrase: phrase,
      data: phrase === ''
        ? this.props.categoryList
        : this.props.categoryList.filter(category => category.name.toLowerCase().indexOf(lowerPhrase) !== -1)
    })
  }

  saveHandler = async (type = '', category = {}, id = -1) => {
    let response

    if (type === 'add') {
      response = await addCategory(this.props.budgetId, this.props.masterCategoryId, category, this.props.token)
      if (response) {
        this.setState({ isCategoryModalOpen: false, searchedPhrase: '' })
      }
    } else if (type === 'edit') {
      response = await editCategory(this.props.budgetId, this.props.masterCategoryId, id, category, this.props.token)
      if (response) {
        this.setState({ isCategoryModalOpen: false, searchedPhrase: '' })
      }
    } else if (type === 'delete') {
      response = await deleteCategory(this.props.budgetId, this.props.masterCategoryId, id, this.props.token)
      if (response) {
        this.setState({ isUniversalModalOpen: false, searchedPhrase: '' })
      }
    }

    if (response) {
      this.props.setCategoryList(this.props.budgetId, this.props.masterCategoryId, this.props.token)
    }
  }

  render () {
    return (
      <section style={{ flex: 1 }}>
        <List
          headerName='Kategorie'
          searchHandler={this.searchHandler}
          addHandler={() => this.setState({ isCategoryModalOpen: true, categoryModalMode: 'add' })}
          data={this.state.data}
          setIdHandler={this.setIdHandler}
          editHandler={id => {
            this.props.setCategory(this.props.budgetId, this.props.masterCategoryId, id, this.props.token)
            this.setState({
              isCategoryModalOpen: true,
              categoryModalMode: 'edit'
            })
          }}
          deleteHandler={(categoryName = '') => {
            this.setState({
              isUniversalModalOpen: true,
              universalModalMode: 'delete',
              selectedCategoryName: categoryName
            })
          }}
          leaveHandler={(categoryName = '') => {
            this.setState({
              isUniversalModalOpen: true,
              universalModalMode: 'leave',
              selectedCategoryName: categoryName
            })
          }}
          user={this.props.user}
          isAdmin={adminEmail => adminEmail === this.props.user.email}
          isAddEnabled={this.props.budgetId !== -1 && this.props.masterCategoryId !== -1}
          adminElementProperty='budgetData.admin'
          propertyList={[
            { field: 'name', label: 'Nazwa' },
            { field: 'color', label: 'Kolor', isColor: true }
          ]}
          dropdownList={[
            {
              label: 'Budżet',
              placeholder: 'Wybierz budżet',
              value: this.props.budgetId === -1 ? '' : this.props.budgetId,
              optionList: this.props.budgetList.map((budget, index) => {
                return {
                  index: index,
                  key: index,
                  value: budget.id,
                  text: budget.name
                }
              }),
              onChange: id => {
                if (id !== this.props.budgetId) {
                  this.props.setBudgetId(id)
                  this.props.categoryList.length && this.props.setCategoryListAction([])
                  this.props.setMasterCategoryId(-1)
                  this.props.setMasterCategoryList(id, this.props.token)
                }
              }
            },
            {
              label: 'Kategoria główna',
              placeholder: 'Wybierz kategorię główną',
              value: this.props.masterCategoryId === -1 ? '' : this.props.masterCategoryId,
              optionList: this.props.masterCategoryList.map((masterCategory, index) => {
                return {
                  index: index,
                  key: index,
                  value: masterCategory.id,
                  text: masterCategory.name
                }
              }),
              onChange: id => {
                if (id !== this.props.masterCategory) {
                  this.props.setMasterCategoryId(id)
                  this.props.setCategoryList(this.props.budgetId, id, this.props.token)
                }
              }
            }
          ]}
          selectedId={this.props.categoryId}
        />
        <UniversalModal
          cancelHandler={() => this.setState({ isUniversalModalOpen: false })}
          saveHandler={this.saveHandler.bind(this, this.state.universalModalMode, null, this.props.categoryId)}
          mode={this.state.universalModalMode}
          isOpen={this.state.isUniversalModalOpen}
          elementName={this.state.selectedCategoryName}
        />
        <CategoryModal
          cancelHandler={() => this.setState({ isCategoryModalOpen: false })}
          saveHandler={this.saveHandler}
          mode={this.state.categoryModalMode}
          isOpen={this.state.isCategoryModalOpen}
          category={this.props.category}
          user={this.props.user}
        />
      </section>
    )
  }
}