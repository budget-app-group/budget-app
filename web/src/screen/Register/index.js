import Register from './Register'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'

const mapDispatchToProps = {}

const mapStateToProps = () => ({})

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Register))