import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Button, Card, Icon, Input, Container } from 'semantic-ui-react'
import { isEmpty } from '../../common/validation'
import * as dict from '../../common/dictionary/polish.json'
import { register } from '../../store/thunk/auth'
import { passwordRegex } from '../../common/validation'
import '../Login/Login.scss'

export default class Register extends Component {
  constructor () {
    super()
    this.state = {
      name: '',
      nameError: false,
      surname: '',
      surnameError: false,
      email: '',
      emailError: false,
      password: '',
      passwordError: false,
      isRemembered: false
    }
  }

  static propTypes = {
    history: PropTypes.object.isRequired
  }

  changeHandler = (stateName = '', value, isValidatable = true) => {
    this.setState({
      [stateName]: value
    }, () => {
      isValidatable && this.validateHandler(stateName, value)
    })
  }

  register = async () => {
    this.validateHandler('name', this.state.name)
    this.validateHandler('surname', this.state.surname)
    this.validateHandler('email', this.state.email)
    this.validateHandler('password', this.state.password)
    
    if (
      !isEmpty(this.state.name) &&
      !isEmpty(this.state.surname) &&
      !isEmpty(this.state.email) &&
      passwordRegex.test(this.state.password)
    ) {
      let response = await register(
        this.state.name,
        this.state.surname,
        this.state.email,
        this.state.password)
      
      response && this.props.history.push('/login')
    }
  }

  validateHandler = (stateName = '', value) => this.setState({
    [`${stateName}Error`]: stateName === 'password' ? !passwordRegex.test(value) : isEmpty(value)
  })

  render () {
    return (
      <Container className='layout-container--auth'>
        <Card className='login-raised-card register-card' raised>
          <Icon
            name='user outline'
            size='huge'
            className='auth-icon'
          />
          <section className='section'>
            <span className={`section-header ${this.state.nameError ? 'error-color' : ''}`}>Imię</span>
            <Input
              className='login-input'
              icon='address card'
              iconPosition='left'
              fluid
              error={this.state.nameError}
              onBlur={() => this.validateHandler('name', this.state.name)}
              onChange={event => this.changeHandler('name', event.target.value)}
              placeholder={dict.placeholder.name}
              value={this.state.name}
            />
          </section>
          <section className='section'>
            <span className={`section-header ${this.state.surnameError ? 'error-color' : ''}`}>Nazwisko</span>
            <Input
              className='login-input'
              icon='address card'
              iconPosition='left'
              fluid
              error={this.state.surnameError}
              onBlur={() => this.validateHandler('surname', this.state.surname)}
              onChange={event => this.changeHandler('surname', event.target.value)}
              placeholder={dict.placeholder.surname}
              value={this.state.surname}
            />
          </section>
          <section className='section'>
            <span className={`section-header ${this.state.emailError ? 'error-color' : ''}`}>Email</span>
            <Input
              className='login-input'
              icon='at'
              iconPosition='left'
              fluid
              error={this.state.emailError}
              onBlur={() => this.validateHandler('email', this.state.email)}
              onChange={event => this.changeHandler('email', event.target.value)}
              placeholder={dict.placeholder.email}
              value={this.state.email}
            />
          </section>
          <section className='section'>
            <span className={`section-header ${this.state.passwordError ? 'error-color' : ''}`}>Hasło</span>
            <Input
              className='login-input'
              icon='shield'
              iconPosition='left'
              type='password'
              fluid
              error={this.state.passwordError}
              onBlur={() => this.validateHandler('password', this.state.email)}
              onChange={event => this.changeHandler('password', event.target.value)}
              placeholder={dict.placeholder.password}
              value={this.state.password}
            />
          </section>
          <section className='section section--button'>
            <Button
              color='blue'
              onClick={this.register}
              className='button'
              content={dict.button.register}
            />
            <Button
              positive
              onClick={() => this.props.history.push('/login')}
              className='button'
              content={dict.button.login}
            />
          </section>
        </Card>
      </Container>
    )
  }
}