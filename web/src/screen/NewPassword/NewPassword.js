import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Button, Card, Icon, Input } from 'semantic-ui-react'
import * as dict from '../../common/dictionary/polish.json'
import { confirmResetPassword } from '../../store/thunk/auth'
import { passwordRegex } from '../../common/validation'

export default class NewPassword extends Component {
  constructor() {
    super()
    this.state = {
      newPassword: '',
      newPasswordError: false,
      newPasswordRepeat: '',
      newPasswordRepeatError: false
    }
  }

  static propTypes = {
    history: PropTypes.object.isRequired,
    match: PropTypes.object.isRequired
  }

  validateHandler = (stateName = '', value) => this.setState({
    [`${stateName}Error`]: stateName === 'newPassword'
      ? !passwordRegex.test(value)
      : this.state.newPassword !== value
  })

  changeHandler = (stateName = '', value) => {
    this.setState({
      [stateName]: value
    }, () => this.validateHandler(stateName, value))
  }

  setNewPassword = async () => {
    this.validateHandler('newPassword', this.state.newPassword)
    this.validateHandler('newPasswordRepeat', this.state.newPasswordRepeat)

    if (
      !this.state.newPasswordError &&
      !this.state.newPasswordRepeatError &&
      this.props.location.search
    ) {
      // .slice(7) -> i am cutting '?token=' from search
      let result = await confirmResetPassword(this.props.location.search.slice(7), this.state.newPassword)
      if (result) {
        this.props.history.push('/login')
      }
    }
  }

  render() {
    return (
      <main className='layout-container--auth raised'>
        <Card className='raised-card' raised>
          <Icon
            name='lock'
            size='massive'
            className='auth-icon'
          />
          <section className='section'>
            <span className={`section-header ${this.state.newPasswordError ? 'error-color' : ''}`}>Nowe hasło</span>
            <Input
              icon='key'
              iconPosition='left'
              type = 'password'
              fluid
              error={this.state.newPasswordError}
              onBlur={() => this.validateHandler('newPassword', this.state.newPassword)}
              onChange={event => this.changeHandler('newPassword', event.target.value)}
              placeholder={dict.placeholder.new_password}
              value={this.state.newPassword}
            />
          </section>
          <section className='section'>
            <span className={`section-header ${this.state.newPasswordRepeatError ? 'error-color' : ''}`}>Powtórz hasło</span>
            <Input
              icon='key'
              iconPosition='left'
              type = 'password'
              fluid
              error={this.state.newPasswordRepeatError}
              onBlur={() => this.validateHandler('newPasswordRepeat', this.state.newPasswordRepeat)}
              onChange={event => this.changeHandler('newPasswordRepeat', event.target.value)}
              placeholder={dict.placeholder.repeat_new_password}
              value={this.state.newPasswordRepeat}
            />
          </section>
          <section className='section section--button'>
            <Button
              onClick={this.setNewPassword}
              className='button'
              content={dict.button.reset_password}
            />
          </section>
        </Card>
      </main>
    )
  }
}
