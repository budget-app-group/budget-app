import NewPassword from './NewPassword'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'

const mapDispatchToProps = {}

const mapStateToProps = () => ({})

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(NewPassword))