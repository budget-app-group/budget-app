import BankAccount from './BankAccount'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import {
  setBankAccountList,
  setBankAccount,
  setBankAccountId
} from '../../store/thunk/bankAccount'
import {
  getBudgetList,
  setBudgetId
} from '../../store/thunk/budget'

const mapDispatchToProps = {
  setBankAccountList,
  setBankAccount,
  setBankAccountId,

  getBudgetList,
  setBudgetId
}

const mapStateToProps = state => ({
  token: state.auth.token,
  user: state.auth.user,

  budgetId: state.budget.budgetId,
  budgetList: state.budget.budgetList,

  bankAccountList: state.bankAccount.bankAccountList,
  bankAccount: state.bankAccount.bankAccount,
  bankAccountId: state.bankAccount.bankAccountId
})

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(BankAccount))