import React, { Component } from 'react'
import PropTypes from 'prop-types'
import List from '../../component/List/List'
import UniversalModal from '../../modal/Universal/Universal'
import BankAccountModal from '../../modal/BankAccount/BankAccount'
import _ from 'lodash'
import { bankList } from '../../common/dictionary/enum'
import {
  addBankAccount,
  editBankAccount,
  deleteBankAccount
} from '../../store/thunk/bankAccount'

export default class BankAccount extends Component {
  constructor () {
    super()
    this.state = {
      isBankAccountModalOpen: false,
      isUniversalModalOpen: false,
      universalModalMode: '',
      bankAccountModalMode: '',
      data: [],
      selectedBankAccountName: '',
      searchedPhrase: ''
    }
  }

  static propTypes= {
    // functions
    setBankAccountList: PropTypes.func.isRequired,
    setBankAccount: PropTypes.func.isRequired,
    setBankAccountId: PropTypes.func.isRequired,
    getBudgetList: PropTypes.func.isRequired,
    setBudgetId: PropTypes.func.isRequired,

    // state properties
    token: PropTypes.string.isRequired,
    user: PropTypes.object.isRequired,

    budgetId: PropTypes.number.isRequired,
    budgetList: PropTypes.array.isRequired,

    bankAccountList: PropTypes.array.isRequired,
    bankAccount: PropTypes.object,
    bankAccountId: PropTypes.number.isRequired
  }

  static getDerivedStateFromProps (props, state) {
    if (props.bankAccountList.length &&
      !_.isEqual(props.bankAccountList, state.data) &&
      state.searchedPhrase === '') {
      
      return {
        data: props.bankAccountList
      }
    }

    return null
  }

  componentDidMount = () => {
    !this.props.budgetList.length && this.props.getBudgetList(this.props.token)
    this.props.budgetId !== -1 && this.props.setBankAccountList(this.props.budgetId, this.props.token)
  }

  setIdHandler = (id = -1) => {
    if (id !== this.props.bankAccountId) {
      this.props.setBankAccountId(id)
    }
  }

  searchHandler = (phrase = '') => {
    let lowerPhrase = phrase.toLowerCase()
    this.setState({
      searchedPhrase: phrase,
      data: phrase === ''
        ? this.props.bankAccountList
        : this.props.bankAccountList.filter(bankAccount => bankAccount.name.toLowerCase().indexOf(lowerPhrase) !== -1)
    })
  }

  saveHandler = async (type = '', bankAccount = {}, id = -1) => {
    let response

    if (type === 'add') {
      response = await addBankAccount(this.props.budgetId, bankAccount, this.props.token)
      if (response) {
        this.setState({ isBankAccountModalOpen: false, searchedPhrase: '' })
      }
    } else if (type === 'edit') {
      response = await editBankAccount(this.props.budgetId, id, bankAccount, this.props.token)
      if (response) {
        this.setState({ isBankAccountModalOpen: false, searchedPhrase: '' })
      }
    } else if (type === 'delete') {
      response = await deleteBankAccount(this.props.budgetId, id, this.props.token)
      if (response) {
        this.setState({ isUniversalModalOpen: false, searchedPhrase: '' })
      }
    }

    if (response) {
      this.props.setBankAccountList(this.props.budgetId, this.props.token)
    }
  }

  render () {
    return (
      <section style={{ flex: 1 }}>
        <List
          headerName='Konta bankowe'
          searchHandler={this.searchHandler}
          addHandler={() => this.setState({ isBankAccountModalOpen: true, bankAccountModalMode: 'add' })}
          data={this.state.data}
          setIdHandler={this.setIdHandler}
          editHandler={id => {
            this.props.setBankAccount(this.props.budgetId, id, this.props.token)
            this.setState({
              isBankAccountModalOpen: true,
              bankAccountModalMode: 'edit'
            })
          }}
          deleteHandler={(bankAccountName = '') => {
            this.setState({
              isUniversalModalOpen: true,
              universalModalMode: 'delete',
              selectedBankAccountName: bankAccountName
            })
          }}
          leaveHandler={(bankAccountName = '') => {
            this.setState({
              isUniversalModalOpen: true,
              universalModalMode: 'leave',
              selectedBankAccountName: bankAccountName
            })
          }}
          user={this.props.user}
          isAdmin={adminEmail => adminEmail === this.props.user.email}
          isAddEnabled={this.props.budgetId !== -1}
          adminElementProperty='budgetData.admin'
          propertyList={[
            { field: 'name', label: 'Nazwa' },
            { field: 'bankName', label: 'Bank', influenceList: bankList },
            { field: 'type', label: 'Typ' },
            { field: 'means', label: 'Środki' }
          ]}
          dropdownList={[
            {
              label: 'Budżet',
              placeholder: 'Wybierz budżet',
              value: this.props.budgetId === -1 ? '' : this.props.budgetId,
              optionList: this.props.budgetList.map((budget, index) => {
                return {
                  index: index,
                  key: index,
                  value: budget.id,
                  text: budget.name
                }
              }),
              onChange: id => {
                if (id !== this.props.budgetId) {
                  this.props.setBudgetId(id)
                  this.props.setBankAccountList(id, this.props.token)
                }
              }
            }
          ]}
          selectedId={this.props.bankAccountId}
        />
        <UniversalModal
          cancelHandler={() => this.setState({ isUniversalModalOpen: false })}
          saveHandler={this.saveHandler.bind(this, this.state.universalModalMode, null, this.props.bankAccountId)}
          mode={this.state.universalModalMode}
          isOpen={this.state.isUniversalModalOpen}
          elementName={this.state.selectedBankAccountName}
        />
        <BankAccountModal
          cancelHandler={() => this.setState({ isBankAccountModalOpen: false })}
          saveHandler={this.saveHandler}
          mode={this.state.bankAccountModalMode}
          isOpen={this.state.isBankAccountModalOpen}
          bankAccount={this.props.bankAccount}
          user={this.props.user}
        />
      </section>
    )
  }
}