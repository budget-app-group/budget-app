import Home from './Home'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import { getBudgetList } from '../../store/thunk/budget'
import { setBankAccountList } from '../../store/thunk/bankAccount'
import { setBudgetExpenseList } from '../../store/thunk/expense'

const mapDispatchToProps = {
  getBudgetList,
  setBudgetExpenseList,
  setBankAccountList
}

const mapStateToProps = (state) => ({
  token: state.auth.token,
  budgetList: state.budget.budgetList,
  selectedBudgetId: state.budget.budgetId,
  budgetExpenseList: state.expense.budgetExpenseList,
  bankAccountList: state.bankAccount.bankAccountList,
  selectedBankAccountId: state.bankAccount.bankAccountId
})

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Home))