import React, { Component } from 'react'
import { Container, Segment } from 'semantic-ui-react'
import PropTypes from 'prop-types'
import {
  LineChart, Line, CartesianGrid, XAxis, YAxis, Tooltip, ResponsiveContainer,
  PieChart, Pie, Cell
} from 'recharts'
import './Home.scss'

export default class Home extends Component {
  constructor() {
    super()
    this.state = {
      dataLeft: [],
      dataMiddle: [],
      dataRight: [],
      dataBottom: [
        { name: 'Styczeń', stan: 400, pv: 2400, amt: 2400 },
        { name: 'Luty', stan: 500, pv: 2400, amt: 2400 },
        { name: 'Marzec', stan: 300, pv: 2400, amt: 2400 },
        { name: 'Kwiecień', stan: 700, pv: 2400, amt: 2400 },
        { name: 'Maj', stan: 800, pv: 2400, amt: 2400 },
        { name: 'Czerwiec', stan: 400, pv: 2400, amt: 2400 },
        { name: 'Lipiec', stan: 500, pv: 2400, amt: 2400 },
        { name: 'Sierpień', stan: 300, pv: 2400, amt: 2400 },
        { name: 'Wrzesień', stan: 700, pv: 2400, amt: 2400 },
        { name: 'Październik', stan: 800, pv: 2400, amt: 2400 },
        { name: 'Listopad', stan: 400, pv: 2400, amt: 2400 },
        { name: 'Grudzień', stan: 500, pv: 2400, amt: 2400 }
      ]
    }
  }

  static propTypes = {
    getBudgetList: PropTypes.func.isRequired,
    setBudgetExpenseList: PropTypes.func.isRequired,
    token: PropTypes.string.isRequired,
    budgetList: PropTypes.array.isRequired,
    selectedBudgetId: PropTypes.number.isRequired,
    budgetExpenseList: PropTypes.array.isRequired
  }

  /*
    Opis gównianego pomysłu na szybko:
    1. na didMount sprawdzam czy jest zdefiniowana lista budżetów (jak nie to rzucam na nią)
    2. w getDerrived nasłuchuję zmiany listy budżetów i jeśli sie zmieniła, to sprawdzam czy mam selectedBudgetId
      (jak nie to ustawiam je na id 1-wszego elementu tablicy budżetów)
    3. Rzucam na listę wszystkich wydatków z danego budżetu
    4. W getDerrived również nasłuchuję zmiany samej listy wydatków - jeśli się zmieniła to odfiltrowywuję sobie
      wszystkie potrzebne dane
  */

  componentDidMount = () => {
    if (!this.props.budgetList.length) {
      this.props.getBudgetList(this.props.token)
    } else {
      this.props.setBudgetExpenseList(this.props.selectedBudgetId !== -1
        ? this.props.selectedBudgetId
        : this.props.budgetList[0].id, this.props.token)
    }
  }

  componentDidUpdate = (previousProps) => {
    if (previousProps.budgetList.length !== this.props.budgetList.length &&
      this.props.budgetList.length !== 0) {
      this.props.setBudgetExpenseList(this.props.selectedBudgetId !== -1
        ? this.props.selectedBudgetId
        : this.props.budgetList[0].id, this.props.token)
    } else if (
    (previousProps.budgetExpenseList.length !== this.props.budgetExpenseList.length &&
    this.props.budgetExpenseList.length !== 0) ||
    (this.props.budgetExpenseList.length !== 0 && this.state.dataLeft.length === 0)) {

      let dataLeft = [ ...this.state.dataLeft ]
      let dataMiddle = [ ...this.state.dataMiddle ]
      let dataRight = [ ...this.state.dataRight ]
      let i, indexLeft, indexMiddle, indexRight

      for (i = 0; i < this.props.budgetExpenseList.length; i++) {
        indexLeft = this.getIndexOfNamedArrayElement(dataLeft, this.props.budgetExpenseList[i].bankAccount.name)
        indexMiddle = this.getIndexOfNamedArrayElement(dataMiddle, this.props.budgetExpenseList[i].category.budgetCategory.name)
        indexRight = this.getIndexOfNamedArrayElement(dataRight, this.props.budgetExpenseList[i].category.name)

        if (indexLeft === -1) {
          dataLeft.push({ name: this.props.budgetExpenseList[i].bankAccount.name, value: this.props.budgetExpenseList[i].amount })
        } else {
          dataLeft[indexLeft].value += this.props.budgetExpenseList[i].amount
        }

        if (indexMiddle === -1) {
          dataMiddle.push({
            name: this.props.budgetExpenseList[i].category.budgetCategory.name,
            value: this.props.budgetExpenseList[i].amount,
            color: this.props.budgetExpenseList[i].category.budgetCategory.color
          })
        } else {
          dataMiddle[indexMiddle].value += this.props.budgetExpenseList[i].amount
        }

        if (indexRight === -1) {
          dataRight.push({
            name: this.props.budgetExpenseList[i].category.name,
            value: this.props.budgetExpenseList[i].amount,
            color: this.props.budgetExpenseList[i].category.color
          })
        } else {
          dataRight[indexRight].value += this.props.budgetExpenseList[i].amount
        }
      }

      this.setState({ dataLeft, dataMiddle, dataRight })
    }
  }

  getIndexOfNamedArrayElement = (array = [], name = '') => {
    let i = 0
    while (i < array.length) {
      if (array[i].name === name) {
        break
      }
      i++
    }
    return i === array.length ? -1 : i 
  }
  
  render () {
    return (
      <Container fluid style={{ flex: 1 }}>
        <Segment>Analiza wydatków</Segment>
        <Segment.Group horizontal>
          <Segment>
            <Segment>Konta bankowe</Segment>
            <PieChart width={400} height={400}>
              <Pie dataKey='value' data={this.state.dataLeft} fill='#0071bc' label />
              <Tooltip />
            </PieChart>
          </Segment>
          <Segment>
          <Segment>Kategorie główne</Segment>
            <PieChart width={400} height={400}>
              <Pie dataKey='value' data={this.state.dataMiddle} label>
                {
                  this.state.dataMiddle.map((masterCategory, index) => <Cell key={`cell-${index}`} fill={masterCategory.color} />)
                }
              </Pie>
              <Tooltip />
            </PieChart>
          </Segment>
          <Segment>
          <Segment>Kategorie</Segment>
            <PieChart width={400} height={400}>
              <Pie dataKey='value' data={this.state.dataRight} label>
                {
                  this.state.dataRight.map((category, index) => <Cell key={`cell-${index}`} fill={category.color} />)
                }
              </Pie>
              <Tooltip />
            </PieChart>
          </Segment>
        </Segment.Group>
        <Segment>
          <ResponsiveContainer width='95%' height={400}>
            <LineChart data={this.state.dataBottom} className='line-chart'>
              <Line type='monotone' dataKey='stan' stroke='#8884d8' />
              <CartesianGrid stroke='#ccc' strokeDasharray='3 3' />
              <XAxis dataKey='name' />
              <YAxis />
              <Tooltip />
            </LineChart>
          </ResponsiveContainer>
        </Segment>
      </Container>
    )
  }
}
