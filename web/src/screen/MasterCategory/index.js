import MasterCategory from './MasterCategory'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import {
  setMasterCategoryList,
  setMasterCategory,
  setMasterCategoryId
} from '../../store/thunk/masterCategory'
import {
  getBudgetList,
  setBudgetId
} from '../../store/thunk/budget'

const mapDispatchToProps = {
  setMasterCategoryList,
  setMasterCategory,
  setMasterCategoryId,

  getBudgetList,
  setBudgetId
}

const mapStateToProps = state => ({
  token: state.auth.token,
  user: state.auth.user,

  budgetId: state.budget.budgetId,
  budgetList: state.budget.budgetList,

  masterCategoryList: state.masterCategory.masterCategoryList,
  masterCategory: state.masterCategory.masterCategory,
  masterCategoryId: state.masterCategory.masterCategoryId
})

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(MasterCategory))