import React, { Component } from 'react'
import PropTypes from 'prop-types'
import List from '../../component/List/List'
import UniversalModal from '../../modal/Universal/Universal'
import MasterCategoryModal from '../../modal/MasterCategory/MasterCategory'
import _ from 'lodash'
import {
  addMasterCategory,
  editMasterCategory,
  deleteMasterCategory
} from '../../store/thunk/masterCategory'

export default class MasterCategory extends Component {
  constructor () {
    super()
    this.state = {
      isMasterCategoryModalOpen: false,
      isUniversalModalOpen: false,
      universalModalMode: '',
      masterCategoryModalMode: '',
      data: [],
      selectedMasterCategoryName: '',
      searchedPhrase: ''
    }
  }

  static propTypes= {
    // functions
    setMasterCategoryList: PropTypes.func.isRequired,
    setMasterCategory: PropTypes.func.isRequired,
    setMasterCategoryId: PropTypes.func.isRequired,
    getBudgetList: PropTypes.func.isRequired,
    setBudgetId: PropTypes.func.isRequired,

    // state properties
    token: PropTypes.string.isRequired,
    user: PropTypes.object.isRequired,

    budgetId: PropTypes.number.isRequired,
    budgetList: PropTypes.array.isRequired,

    masterCategoryList: PropTypes.array.isRequired,
    masterCategory: PropTypes.object,
    masterCategoryId: PropTypes.number.isRequired
  }

  static getDerivedStateFromProps (props, state) {
    if (!_.isEqual(props.masterCategoryList, state.data) && state.searchedPhrase === '') {
      
      return {
        data: props.masterCategoryList
      }
    }

    return null
  }

  componentDidMount = () => {
    !this.props.budgetList.length && this.props.getBudgetList(this.props.token)
    this.props.budgetId !== -1 && this.props.setMasterCategoryList(this.props.budgetId, this.props.token)
  }

  setIdHandler = (id = -1) => {
    if (id !== this.props.masterCategoryId) {
      this.props.setMasterCategoryId(id)
    }
  }

  searchHandler = (phrase = '') => {
    let lowerPhrase = phrase.toLowerCase()
    this.setState({
      searchedPhrase: phrase,
      data: phrase === ''
        ? this.props.masterCategoryList
        : this.props.masterCategoryList.filter(masterCategory => masterCategory.name.toLowerCase().indexOf(lowerPhrase) !== -1)
    })
  }

  saveHandler = async (type = '', masterCategory = {}, id = -1) => {
    let response

    if (type === 'add') {
      response = await addMasterCategory(this.props.budgetId, masterCategory, this.props.token)
      if (response) {
        this.setState({ isMasterCategoryModalOpen: false, searchedPhrase: '' })
      }
    } else if (type === 'edit') {
      response = await editMasterCategory(this.props.budgetId, id, masterCategory, this.props.token)
      if (response) {
        this.setState({ isMasterCategoryModalOpen: false, searchedPhrase: '' })
      }
    } else if (type === 'delete') {
      response = await deleteMasterCategory(this.props.budgetId, id, this.props.token)
      if (response) {
        this.setState({ isUniversalModalOpen: false, searchedPhrase: '' })
      }
    }

    if (response) {
      this.props.setMasterCategoryList(this.props.budgetId, this.props.token)
    }
  }

  render () {
    return (
      <section style={{ flex: 1 }}>
        <List
          headerName='Kategorie główne'
          searchHandler={this.searchHandler}
          addHandler={() => this.setState({ isMasterCategoryModalOpen: true, masterCategoryModalMode: 'add' })}
          data={this.state.data}
          setIdHandler={this.setIdHandler}
          editHandler={id => {
            this.props.setMasterCategory(this.props.budgetId, id, this.props.token)
            this.setState({
              isMasterCategoryModalOpen: true,
              masterCategoryModalMode: 'edit'
            })
          }}
          deleteHandler={(masterCategoryName = '') => {
            this.setState({
              isUniversalModalOpen: true,
              universalModalMode: 'delete',
              selectedMasterCategoryName: masterCategoryName
            })
          }}
          leaveHandler={(masterCategoryName = '') => {
            this.setState({
              isUniversalModalOpen: true,
              universalModalMode: 'leave',
              selectedMasterCategoryName: masterCategoryName
            })
          }}
          user={this.props.user}
          isAdmin={adminEmail => adminEmail === this.props.user.email}
          isAddEnabled={this.props.budgetId !== -1}
          adminElementProperty='budgetData.admin'
          propertyList={[
            { field: 'name', label: 'Nazwa' },
            { field: 'color', label: 'Kolor', isColor: true },
            { field: 'budgetAllocation', label: 'Docelowy %' }
          ]}
          dropdownList={[
            {
              label: 'Budżet',
              placeholder: 'Wybierz budżet',
              value: this.props.budgetId === -1 ? '' : this.props.budgetId,
              optionList: this.props.budgetList.map((budget, index) => {
                return {
                  index: index,
                  key: index,
                  value: budget.id,
                  text: budget.name
                }
              }),
              onChange: id => {
                if (id !== this.props.budgetId) {
                  this.props.setBudgetId(id)
                  this.props.setMasterCategoryList(id, this.props.token)
                }
              }
            }
          ]}
          selectedId={this.props.masterCategoryId}
        />
        <UniversalModal
          cancelHandler={() => this.setState({ isUniversalModalOpen: false })}
          saveHandler={this.saveHandler.bind(this, this.state.universalModalMode, null, this.props.masterCategoryId)}
          mode={this.state.universalModalMode}
          isOpen={this.state.isUniversalModalOpen}
          elementName={this.state.selectedMasterCategoryName}
        />
        <MasterCategoryModal
          cancelHandler={() => this.setState({ isMasterCategoryModalOpen: false })}
          saveHandler={this.saveHandler}
          mode={this.state.masterCategoryModalMode}
          isOpen={this.state.isMasterCategoryModalOpen}
          masterCategory={this.props.masterCategory}
          user={this.props.user}
        />
      </section>
    )
  }
}